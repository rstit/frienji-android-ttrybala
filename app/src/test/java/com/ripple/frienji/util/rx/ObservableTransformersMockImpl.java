package com.ripple.frienji.util.rx;

import android.support.annotation.NonNull;
import rx.Observable;

/**
 * @author Kamil Ratajczak
 * @since 27.09.16
 */
public class ObservableTransformersMockImpl implements ObservableTransformers {
    private static final int DEFAULT_RETRY_COUNT = 3;

    public ObservableTransformersMockImpl() {
    }

    @Override
    public @NonNull <T> Observable.Transformer<T, T> networkOperation() {
        return networkOperation(DEFAULT_RETRY_COUNT);
    }

    @Override
    public @NonNull <T> Observable.Transformer<T, T> networkOperation(int retryCount) {
        return observable -> observable;
    }

    @Override
    public @NonNull <T> Observable.Transformer<T, T> backgroundOperation() {
        return observable -> observable;
    }

    @Override
    public @NonNull <T> Observable.Transformer<T, T> databaseOperation() {
        return observable -> observable;
    }
}