package com.ripple.frienji.ui.model.activity;

import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.auth.SignUpBody;
import org.junit.Before;
import org.junit.Test;
import java.util.List;
import javax.inject.Inject;
import rx.observers.TestSubscriber;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * @author kamil ratajczak
 * @since 08.08.2016
 */
public class SignUpViewModelTest {

    private final String TEST_WHO_YOU_ARE = "WHO YOU ARE TEST";
    private final String TEST_USER_NAME = "USER_NAME_TEST";
    private final String TEST_AVATAR_NAME = "EMOTICON_NAME_TEST";
    private final int TEST_AVATAR_TYPE = 0;

    private final String TEST_COUNTRY_CODE_NUMBER = "+48";
    private final String TEST_PHONE_NUMBER = "791580177";

    private final String TEST_WRONG_COUNTRY_CODE_NUMBER = "+11";
    private final String TEST_WRONG_PHONE_NUMBER = "112333";

    @Inject
    protected SignUpViewModel mSignUpViewModel;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    @Test
    public void buildSignUpBody() throws Exception {
        Avatar avatar = Avatar.builder().name(TEST_AVATAR_NAME).type(TEST_AVATAR_TYPE).build();

        SignUpBody signUpBody = SignUpBody.builder().avatar(avatar)
                .whoYouAre(TEST_WHO_YOU_ARE)
                .userName(TEST_USER_NAME)
                .phoneNumber(TEST_PHONE_NUMBER)
                .countryCode(TEST_COUNTRY_CODE_NUMBER).build();

        TestSubscriber<SignUpBody> subscriber = new TestSubscriber<>();

        mSignUpViewModel.buildRequestConfirmCodeBody(signUpBody).subscribe(subscriber);

        subscriber.assertNoErrors();
        List<SignUpBody> data = subscriber.getOnNextEvents();
        assertEquals(1, data.size());

        SignUpBody body = data.get(0);

        assertEquals(TEST_COUNTRY_CODE_NUMBER, body.countryCode());
        assertEquals(TEST_PHONE_NUMBER, body.phoneNumber());
        assertEquals(TEST_AVATAR_NAME, body.avatar().name());
        assertEquals(TEST_AVATAR_TYPE, body.avatar().type());
        assertEquals(TEST_USER_NAME, body.userName());
        assertEquals(TEST_WHO_YOU_ARE, body.whoYouAre());
    }

    @Test(expected = NullPointerException.class)
    public void buildSignUpBody_Null() throws Exception {
        //noinspection ConstantConditions
        mSignUpViewModel.buildRequestConfirmCodeBody(null);
    }

    @Test(expected = NullPointerException.class)
    public void signUp_NullBody() throws Exception {
        //noinspection ConstantConditions
        mSignUpViewModel.sendSignUpRequestBody(null);
    }

    @Test
    public void validatePhoneNumber_Valid() throws Exception {
        boolean valid = mSignUpViewModel.validateInput(TEST_COUNTRY_CODE_NUMBER, TEST_PHONE_NUMBER);
        assertTrue(valid);
        assertFalse(mSignUpViewModel.mErrorVisible.get());
    }

    @Test
    public void validatePhoneNumber_Invalid_1() throws Exception {
        boolean valid = mSignUpViewModel.validateInput(TEST_COUNTRY_CODE_NUMBER, "");
        assertFalse(valid);
        assertTrue(mSignUpViewModel.mErrorVisible.get());
    }

    @Test
    public void validatePhoneNumber_Invalid_2() throws Exception {
        boolean valid = mSignUpViewModel.validateInput("", TEST_PHONE_NUMBER);
        assertFalse(valid);
        assertTrue(mSignUpViewModel.mErrorVisible.get());
    }

    @Test
    public void validatePhoneNumber_Invalid_3() throws Exception {
        boolean valid = mSignUpViewModel.validateInput(TEST_WRONG_COUNTRY_CODE_NUMBER, TEST_PHONE_NUMBER);
        assertFalse(valid);
        assertTrue(mSignUpViewModel.mErrorVisible.get());
    }
}
