package com.ripple.frienji.ui.model.activity;

import android.content.Context;
import android.support.annotation.NonNull;
import com.google.common.base.Optional;
import com.ripple.frienji.R;
import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.ui.view.activity.AvatarTypeActivity;
import com.ripple.frienji.util.rx.ObservableTransformers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import java.util.List;
import javax.inject.Inject;
import okhttp3.MultipartBody;
import rx.Observable;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by kamil ratajczak on 15.09.16.
 */
public class ChangeFrienjiDataViewModelTest {
    private final String TEST_WHO_YOU_ARE = "WHO YOU ARE TEST";
    private final String TEST_USER_NAME = "USER_NAME_TEST";
    private final String TEST_AVATAR_NAME = "AVATAR_NAME_TEST";
    private final int TEST_AVATAR_TYPE = Avatar.EMOTICON_AVATAR;
    private final long TEST_USER_PROFILE_ID = 1;

    @Inject
    protected ChangeFrienjiDataViewModel mChangeFrienjiDataViewModel;

    @Inject
    protected Context mContext;

    @Inject
    protected ObservableTransformers mObservableTransformers;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    @Test
    public void changeAvatarTest() {
        mChangeFrienjiDataViewModel.changeAvatar();
        verify(mChangeFrienjiDataViewModel.mNavigationHandler).startActivityForResult(AvatarTypeActivity.class, ChangeFrienjiDataViewModel.CHANGE_AVATAR_ACTION);
    }

    @Test
    public void loadUserProfileData() {
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserName()).thenReturn(Optional.fromNullable("TEST_USER_NAME"));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUUserWhoYouAre()).thenReturn(Optional.fromNullable("TEST_WHO_YOU_ARE"));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserAvatarName()).thenReturn(Optional.fromNullable("TEST_AVATAR_NAME"));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserAvatarType()).thenReturn(Optional.fromNullable(new Integer(TEST_AVATAR_TYPE)));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserProfileId()).thenReturn(Optional.fromNullable(new Long(TEST_USER_PROFILE_ID)));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserCoverImage()).thenReturn(Optional.fromNullable("TEST_COVER_IMAGE"));

        mChangeFrienjiDataViewModel.loadUserProfileData();

        assertEquals("TEST_USER_NAME", mChangeFrienjiDataViewModel.mUserName.get());
        assertEquals("TEST_WHO_YOU_ARE", mChangeFrienjiDataViewModel.mWhoYouAre.get());
        assertEquals("TEST_AVATAR_NAME", mChangeFrienjiDataViewModel.mAvatarName.get());
        assertEquals(TEST_AVATAR_TYPE, mChangeFrienjiDataViewModel.mAvatarType.get());
        assertEquals(TEST_USER_PROFILE_ID, mChangeFrienjiDataViewModel.mUserProfileId.get());
    }

    @Test
    public void setAvatar() {
        Avatar avatar = Avatar.builder().name(TEST_AVATAR_NAME).type(TEST_AVATAR_TYPE).build();
        mChangeFrienjiDataViewModel.setAvatar(avatar);

        assertEquals(TEST_AVATAR_NAME, mChangeFrienjiDataViewModel.mAvatarName.get());
        assertEquals(TEST_AVATAR_TYPE, mChangeFrienjiDataViewModel.mAvatarType.get());
    }

    @Test
    public void sendUpdateProfileRequest() {
        mChangeFrienjiDataViewModel.mObservableTransformers = mObservableTransformers;
        ChangeFrienjiDataViewModel spyChangeFrienjiDataViewModel = Mockito.spy(mChangeFrienjiDataViewModel);

        UserProfile userProfile = prepareTestUserProfileModel();

        List<MultipartBody.Part> parts = userProfile.buildMultipartBodyParts();
        when(spyChangeFrienjiDataViewModel.mApi.updateUserProfile(parts)).thenReturn(Observable.just(userProfile));

        spyChangeFrienjiDataViewModel.sendUpdateProfileRequest(parts);

        verify(spyChangeFrienjiDataViewModel).saveUserProfileData(userProfile);
    }

    private @NonNull UserProfile prepareTestUserProfileModel() {
        return UserProfile.builder().avatar(prepareTestAvatarModel()).userName(TEST_USER_NAME).id(1).whoYouAre(TEST_WHO_YOU_ARE).build();
    }

    private @NonNull Avatar prepareTestAvatarModel() {
        return Avatar.builder().type(Avatar.EMOTICON_AVATAR).name("TEST_ANATAR_NAME").build();
    }

    @Test
    public void buildUserProfileForAvatarChange() {
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserName()).thenReturn(Optional.fromNullable(TEST_USER_NAME));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUUserWhoYouAre()).thenReturn(Optional.fromNullable(TEST_WHO_YOU_ARE));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserAvatarName()).thenReturn(Optional.fromNullable(TEST_AVATAR_NAME));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserAvatarType()).thenReturn(Optional.fromNullable(new Integer(TEST_AVATAR_TYPE)));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserProfileId()).thenReturn(Optional.fromNullable(new Long(TEST_USER_PROFILE_ID)));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserCoverImage()).thenReturn(Optional.fromNullable("TEST_COVER_IMAGE"));
        when(mChangeFrienjiDataViewModel.mContext.getString(R.string.edit_data_avatar_title)).thenReturn("TEST");

        mChangeFrienjiDataViewModel.editFrienjiAvatar();
        UserProfile userProfile = mChangeFrienjiDataViewModel.buildUserProfile();

        assertEquals(TEST_AVATAR_NAME, userProfile.avatar().name());
        assertEquals(TEST_AVATAR_TYPE, userProfile.avatar().type());
        assertEquals(TEST_USER_PROFILE_ID, userProfile.id());
        assertNull(userProfile.userName());
        assertNull(userProfile.whoYouAre());
    }

    @Test
    public void buildUserProfileForCoverImageChange() {
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserName()).thenReturn(Optional.fromNullable(TEST_USER_NAME));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUUserWhoYouAre()).thenReturn(Optional.fromNullable(TEST_WHO_YOU_ARE));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserAvatarName()).thenReturn(Optional.fromNullable(TEST_AVATAR_NAME));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserAvatarType()).thenReturn(Optional.fromNullable(new Integer(TEST_AVATAR_TYPE)));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserProfileId()).thenReturn(Optional.fromNullable(new Long(TEST_USER_PROFILE_ID)));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserCoverImage()).thenReturn(Optional.fromNullable("TEST_COVER_IMAGE"));
        when(mChangeFrienjiDataViewModel.mContext.getString(R.string.edit_data_cover_image_title)).thenReturn("TEST");

        mChangeFrienjiDataViewModel.editFrienjiCoverImage();
        UserProfile userProfile = mChangeFrienjiDataViewModel.buildUserProfile();

        assertEquals(TEST_USER_PROFILE_ID, userProfile.id());
        assertNull(userProfile.avatar());
        assertNull(userProfile.userName());
        assertNull(userProfile.whoYouAre());
    }

    @Test
    public void buildUserProfileForBioChange() {
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserName()).thenReturn(Optional.fromNullable(TEST_USER_NAME));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUUserWhoYouAre()).thenReturn(Optional.fromNullable(TEST_WHO_YOU_ARE));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserAvatarName()).thenReturn(Optional.fromNullable(TEST_AVATAR_NAME));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserAvatarType()).thenReturn(Optional.fromNullable(new Integer(TEST_AVATAR_TYPE)));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserProfileId()).thenReturn(Optional.fromNullable(new Long(TEST_USER_PROFILE_ID)));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserCoverImage()).thenReturn(Optional.fromNullable("TEST_COVER_IMAGE"));
        when(mChangeFrienjiDataViewModel.mContext.getString(R.string.edit_data_bio_title)).thenReturn("TEST");

        mChangeFrienjiDataViewModel.editFrienjiBio();
        UserProfile userProfile = mChangeFrienjiDataViewModel.buildUserProfile();

        assertEquals(TEST_USER_PROFILE_ID, userProfile.id());
        assertEquals(TEST_WHO_YOU_ARE, userProfile.whoYouAre());
        assertNull(userProfile.avatar());
        assertNull(userProfile.userName());
    }

    @Test
    public void editBioChange() {
        String expectedTitle = "EditBio";

        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserName()).thenReturn(Optional.fromNullable(TEST_USER_NAME));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUUserWhoYouAre()).thenReturn(Optional.fromNullable(TEST_WHO_YOU_ARE));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserAvatarName()).thenReturn(Optional.fromNullable(TEST_AVATAR_NAME));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserAvatarType()).thenReturn(Optional.fromNullable(new Integer(TEST_AVATAR_TYPE)));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserProfileId()).thenReturn(Optional.fromNullable(new Long(TEST_USER_PROFILE_ID)));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserCoverImage()).thenReturn(Optional.fromNullable("TEST_COVER_IMAGE"));
        when(mChangeFrienjiDataViewModel.mContext.getString(R.string.edit_data_bio_title)).thenReturn(expectedTitle);

        mChangeFrienjiDataViewModel.editFrienjiBio();
        assertEquals(expectedTitle, mChangeFrienjiDataViewModel.mTitle.get());
    }

    @Test
    public void editAvatar() {
        String expectedTitle = "EditAvatar";

        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserName()).thenReturn(Optional.fromNullable(TEST_USER_NAME));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUUserWhoYouAre()).thenReturn(Optional.fromNullable(TEST_WHO_YOU_ARE));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserAvatarName()).thenReturn(Optional.fromNullable(TEST_AVATAR_NAME));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserAvatarType()).thenReturn(Optional.fromNullable(new Integer(TEST_AVATAR_TYPE)));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserProfileId()).thenReturn(Optional.fromNullable(new Long(TEST_USER_PROFILE_ID)));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserCoverImage()).thenReturn(Optional.fromNullable("TEST_COVER_IMAGE"));
        when(mChangeFrienjiDataViewModel.mContext.getString(R.string.edit_data_avatar_title)).thenReturn(expectedTitle);

        mChangeFrienjiDataViewModel.editFrienjiAvatar();
        assertEquals(expectedTitle, mChangeFrienjiDataViewModel.mTitle.get());
    }

    @Test
    public void editCoverImage() {
        String expectedTitle = "EditCoverImage";

        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserName()).thenReturn(Optional.fromNullable(TEST_USER_NAME));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUUserWhoYouAre()).thenReturn(Optional.fromNullable(TEST_WHO_YOU_ARE));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserAvatarName()).thenReturn(Optional.fromNullable(TEST_AVATAR_NAME));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserAvatarType()).thenReturn(Optional.fromNullable(new Integer(TEST_AVATAR_TYPE)));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserProfileId()).thenReturn(Optional.fromNullable(new Long(TEST_USER_PROFILE_ID)));
        when(mChangeFrienjiDataViewModel.mAppSettings.loadUserCoverImage()).thenReturn(Optional.fromNullable("TEST_COVER_IMAGE"));
        when(mChangeFrienjiDataViewModel.mContext.getString(R.string.edit_data_cover_image_title)).thenReturn(expectedTitle);

        mChangeFrienjiDataViewModel.editFrienjiCoverImage();
        assertEquals(expectedTitle, mChangeFrienjiDataViewModel.mTitle.get());
    }
}