package com.ripple.frienji.ui.model.row;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;
import com.google.common.eventbus.EventBus;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.message.FrienjiPost;
import com.ripple.frienji.model.response.StatusResponse;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.net.FrienjiApi;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.util.DateTimeUtil;
import com.ripple.frienji.util.rx.ObservableTransformers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import java.util.Date;
import javax.inject.Inject;
import rx.Observable;
import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * @author Kamil Ratajczak
 * @since 11.10.16
 */
public class UserProfileMessageRowViewModelTest {
    private static final int NUMBER_OF_COMMENTS = 10;
    private static final int NUMBER_OF_LIKES = 5;
    private final String TEST_ANATAR_NAME = "grinning_face_with_smiling_eyes";
    private final String TEST_USER_NAME = "USER_NAME_TEST";
    private final String TEST_WHO_YOU_ARE = "WHO YOU ARE TEST";
    private final String TEST_IMAGE_NAME = "TEST_IMAGE_NAME";


    @Inject
    protected NavigationHandler mNavigationHandler;

    @Inject
    protected Context mContext;

    @Inject
    protected AvatarReader mAvatarReader;

    @Inject
    protected FrienjiApi mFrienjiApi;

    @Inject
    protected EventBus mEventBus;

    @Inject
    protected ObservableTransformers mObservableTransformers;

    @Inject
    protected IntentProvider mIntentProvider;

    protected UserProfileMessageRowViewModel mUserProfileMessageRowViewModel;

    private DateTimeUtil mDateTimeUtil = Mockito.mock(DateTimeUtil.class);

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    @Test
    public void like() {
        MessageRowViewModel messageRowViewModel = prepareUserProfileMessageRowViewModel(false);
        assertEquals(String.valueOf(NUMBER_OF_LIKES), messageRowViewModel.mNumberOfLikes.get());

        when(messageRowViewModel.mApi.likeUnlikePost(1)).thenReturn(Observable.just(StatusResponse.builder().status(StatusResponse.SUCCESS_STATUS).build()));
        messageRowViewModel.likeMessage();
        assertEquals(String.valueOf(NUMBER_OF_LIKES+1), messageRowViewModel.mNumberOfLikes.get());
    }

    @Test
    public void unlike() {
        MessageRowViewModel messageRowViewModel = prepareUserProfileMessageRowViewModel(true);
        assertEquals(String.valueOf(NUMBER_OF_LIKES), messageRowViewModel.mNumberOfLikes.get());

        when(messageRowViewModel.mApi.likeUnlikePost(1)).thenReturn(Observable.just(StatusResponse.builder().status(StatusResponse.SUCCESS_STATUS).build()));

        messageRowViewModel.likeMessage();

        assertEquals(String.valueOf(NUMBER_OF_LIKES-1), messageRowViewModel.mNumberOfLikes.get());
    }

    private @NonNull MessageRowViewModel prepareUserProfileMessageRowViewModel(boolean liked) {
        return UserProfileMessageRowViewModel.from(mContext,mAvatarReader,mFrienjiApi,mNavigationHandler,mEventBus,mObservableTransformers,mDateTimeUtil, prepareFrienjiPost(liked),1);
    }

    private @NonNull FrienjiPost prepareFrienjiPost(boolean liked) {
        return FrienjiPost.builder().numberOfComments(NUMBER_OF_COMMENTS).numberOfLikes(NUMBER_OF_LIKES).liked(liked).id(1).author(prepareTestUserProfileModel())
                .message("TEST_MESSAGE")
                .createdAt(new Date(System.currentTimeMillis()))
                .myOwnMessage(false)
                .attachmentImageUrl(TEST_IMAGE_NAME)
                .build();
    }

    private @NonNull UserProfile prepareTestUserProfileModel() {
        return UserProfile.builder().avatar(prepareTestAvatarModel()).userName(TEST_USER_NAME).id(1).unreadMessagesCount(10).whoYouAre(TEST_WHO_YOU_ARE).build();
    }

    private @NonNull Avatar prepareTestAvatarModel() {
        return Avatar.builder().type(Avatar.EMOTICON_AVATAR).name(TEST_ANATAR_NAME).build();
    }

}
