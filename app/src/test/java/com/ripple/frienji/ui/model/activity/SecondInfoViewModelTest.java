package com.ripple.frienji.ui.model.activity;

import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.ui.view.activity.LoginActivity;
import org.junit.Before;
import org.junit.Test;
import javax.inject.Inject;
import static org.mockito.Mockito.verify;

/**
 * @author kamil ratajczak
 * @since 08.08.2016
 */
public class SecondInfoViewModelTest {

    @Inject
    protected SecondInfoViewModel mSecondInfoViewModel;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    @Test
    public void launchSecondInfoScreen() throws Exception {
        mSecondInfoViewModel.launchLoginScreen();
        verify(mSecondInfoViewModel.mNavigator).startActivity(LoginActivity.class);
    }
}