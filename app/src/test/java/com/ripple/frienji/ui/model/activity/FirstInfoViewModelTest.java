package com.ripple.frienji.ui.model.activity;

import android.content.Context;
import android.content.Intent;
import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.ui.view.activity.SecondInfoActivity;
import org.junit.Before;
import org.junit.Test;
import javax.inject.Inject;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author kamil ratajczak
 * @since 08.08.2016
 */
public class FirstInfoViewModelTest {

    @Inject
    protected FirstInfoViewModel mFirstInfoViewModel;

    @Inject
    protected Context mContext;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    @Test
    public void launchSecondInfoScreen() throws Exception {
        Intent testIntent = new Intent(mContext, SecondInfoActivity.class);
        when(mFirstInfoViewModel.mIntentProvider.createSecondIntroduction()).thenReturn(testIntent);
        mFirstInfoViewModel.launchSecondInfoScreen();
        verify(mFirstInfoViewModel.mNavigator).startActivity(testIntent);
    }
}