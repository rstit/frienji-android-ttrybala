package com.ripple.frienji.ui.model.row;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import com.ripple.frienji.R;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.model.action.Activity;
import com.ripple.frienji.model.action.ActivityEntity;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.util.DateTimeUtil;
import com.ripple.frienji.ui.view.activity.CommentsActivity;
import com.ripple.frienji.ui.view.activity.UserProfileActivity;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import java.util.Date;
import javax.inject.Inject;
import static com.google.common.base.Preconditions.checkNotNull;
import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Kamil Ratajczak
 * @since 17.10.16
 */
public class UserActionRowViewModelTest {
    private static final String TEST_USER_NAME = "TEST_USER_NAME";
    private static final String TEST_MESSAGE = "TEST_MESSAGE";

    @Inject
    protected NavigationHandler mNavigationHandler;

    @Inject
    protected Context mContext;

    @Inject
    protected AvatarReader mAvatarReader;

    @Inject
    protected IntentProvider mIntentProvider;

    private DateTimeUtil mDateTimeUtil = Mockito.mock(DateTimeUtil.class);

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    @Test
    public void postContentCommentOnYourWallCreated() {
        String expectedMessage = "postContentCommentOnYourWallCreatedExpectedContent";
        when(mContext.getString(R.string.user_action_comment_post_on_wall_template, TEST_USER_NAME, TEST_MESSAGE)).thenReturn(expectedMessage);

        UserActionRowViewModel userActionRowViewModel = UserActionRowViewModel.from(mContext, mAvatarReader,
                mDateTimeUtil, mNavigationHandler, mIntentProvider, prepareTestActivity(Activity.POST_CONTENT_COMMENT_ON_YOUR_WALL_CREATED));

        assertEquals("TEST_AVATAR_NAME", userActionRowViewModel.mAvatarName.get());
        assertEquals(Avatar.EMOTICON_AVATAR, userActionRowViewModel.mAvatarType.get());
        assertEquals(expectedMessage, userActionRowViewModel.mContent.get());
    }

    @Test
    public void openActionPostContentCommentOnYourWallCreated() {
        UserActionRowViewModel userActionRowViewModel = UserActionRowViewModel.from(mContext, mAvatarReader,
                mDateTimeUtil, mNavigationHandler, mIntentProvider, prepareTestActivity(Activity.POST_CONTENT_COMMENT_ON_YOUR_WALL_CREATED));

        Intent mockIntent = new Intent(mContext, CommentsActivity.class);
        when(mIntentProvider.createComents(anyLong(), anyLong())).thenReturn(mockIntent);
        userActionRowViewModel.openAction();
        verify(mNavigationHandler).startActivity(mockIntent);
    }

    @Test
    public void postContentCommentCreated() {
        String expectedMessage = "postContentCommentCreatedExpectedContent";
        when(mContext.getString(R.string.user_action_comment_on_post_template, TEST_USER_NAME, TEST_MESSAGE)).thenReturn(expectedMessage);

        UserActionRowViewModel userActionRowViewModel = UserActionRowViewModel.from(mContext, mAvatarReader,
                mDateTimeUtil, mNavigationHandler, mIntentProvider, prepareTestActivity(Activity.POST_CONTENT_COMMENT_CREATED));

        assertEquals("TEST_AVATAR_NAME", userActionRowViewModel.mAvatarName.get());
        assertEquals(Avatar.EMOTICON_AVATAR, userActionRowViewModel.mAvatarType.get());
        assertEquals(expectedMessage, userActionRowViewModel.mContent.get());
    }

    @Test
    public void openActionPostContentCommentCreated() {
        UserActionRowViewModel userActionRowViewModel = UserActionRowViewModel.from(mContext, mAvatarReader,
                mDateTimeUtil, mNavigationHandler, mIntentProvider, prepareTestActivity(Activity.POST_CONTENT_COMMENT_CREATED));

        Intent mockIntent = new Intent(mContext, CommentsActivity.class);
        when(mIntentProvider.createComents(anyLong(), anyLong())).thenReturn(mockIntent);
        userActionRowViewModel.openAction();
        verify(mNavigationHandler).startActivity(mockIntent);
    }

    @Test
    public void likeForCommentCreated() {
        String expectedMessage = "likeForCommentCreatedExpectedContent";
        when(mContext.getString(R.string.user_action_like_on_comment, TEST_USER_NAME)).thenReturn(expectedMessage);

        UserActionRowViewModel userActionRowViewModel = UserActionRowViewModel.from(mContext, mAvatarReader,
                mDateTimeUtil, mNavigationHandler, mIntentProvider, prepareTestActivity(Activity.LIKE_FOR_COMMENT_CREATED));

        assertEquals("TEST_AVATAR_NAME", userActionRowViewModel.mAvatarName.get());
        assertEquals(Avatar.EMOTICON_AVATAR, userActionRowViewModel.mAvatarType.get());
        assertEquals(expectedMessage, userActionRowViewModel.mContent.get());
    }

    @Test
    public void openActionLikeForCommentCreated() {
        UserActionRowViewModel userActionRowViewModel = UserActionRowViewModel.from(mContext, mAvatarReader,
                mDateTimeUtil, mNavigationHandler, mIntentProvider, prepareTestActivity(Activity.LIKE_FOR_COMMENT_CREATED));

        Intent mockIntent = new Intent(mContext, CommentsActivity.class);
        when(mIntentProvider.createComents(anyLong(), anyLong())).thenReturn(mockIntent);
        userActionRowViewModel.openAction();
        verify(mNavigationHandler).startActivity(mockIntent);
    }

    @Test
    public void likeForPostCreated() {
        String expectedMessage = "likeForPostCreatedExpectedContent";
        when(mContext.getString(R.string.user_action_like_on_post, TEST_USER_NAME)).thenReturn(expectedMessage);

        UserActionRowViewModel userActionRowViewModel = UserActionRowViewModel.from(mContext, mAvatarReader,
                mDateTimeUtil, mNavigationHandler, mIntentProvider, prepareTestActivity(Activity.LIKE_FOR_POST_CREATED));

        assertEquals("TEST_AVATAR_NAME", userActionRowViewModel.mAvatarName.get());
        assertEquals(Avatar.EMOTICON_AVATAR, userActionRowViewModel.mAvatarType.get());
        assertEquals(expectedMessage, userActionRowViewModel.mContent.get());
    }

    @Test
    public void openActionLikeForPostCreated() {
        UserActionRowViewModel userActionRowViewModel = UserActionRowViewModel.from(mContext, mAvatarReader,
                mDateTimeUtil, mNavigationHandler, mIntentProvider, prepareTestActivity(Activity.LIKE_FOR_POST_CREATED));

        Intent mockIntent = new Intent(mContext, UserProfileActivity.class);
        when(mIntentProvider.createUserProfile(anyObject())).thenReturn(mockIntent);
        userActionRowViewModel.openAction();
        verify(mNavigationHandler).startActivity(mockIntent);
    }

    @Test
    public void relationshipCreatedSaved() {
        String expectedMessage = "relationshipCreatedSavedExpectedContent";
        when(mContext.getString(R.string.user_action_saved, TEST_USER_NAME)).thenReturn(expectedMessage);

        UserActionRowViewModel userActionRowViewModel = UserActionRowViewModel.from(mContext, mAvatarReader,
                mDateTimeUtil, mNavigationHandler, mIntentProvider, prepareTestActivity(Activity.RELATIONSHIP_CREATED_SAVED));

        assertEquals("TEST_AVATAR_NAME", userActionRowViewModel.mAvatarName.get());
        assertEquals(Avatar.EMOTICON_AVATAR, userActionRowViewModel.mAvatarType.get());
        assertEquals(expectedMessage, userActionRowViewModel.mContent.get());
    }

    @Test
    public void openActionRelationshipCreatedSaved() {
        UserActionRowViewModel userActionRowViewModel = UserActionRowViewModel.from(mContext, mAvatarReader,
                mDateTimeUtil, mNavigationHandler, mIntentProvider, prepareTestActivity(Activity.RELATIONSHIP_CREATED_SAVED));

        Intent mockIntent = new Intent(mContext, UserProfileActivity.class);
        when(mIntentProvider.createUserProfile(anyObject())).thenReturn(mockIntent);
        userActionRowViewModel.openAction();
        verify(mNavigationHandler).startActivity(mockIntent);
    }

    @Test
    public void relationshipRemovedSaved() {
        String expectedMessage = "relationshipCreatedSavedExpectedContent";
        when(mContext.getString(R.string.user_action_rejected, TEST_USER_NAME)).thenReturn(expectedMessage);

        UserActionRowViewModel userActionRowViewModel = UserActionRowViewModel.from(mContext, mAvatarReader,
                mDateTimeUtil, mNavigationHandler, mIntentProvider, prepareTestActivity(Activity.RELATIONSHIP_REMOVED_SAVED));

        assertEquals("TEST_AVATAR_NAME", userActionRowViewModel.mAvatarName.get());
        assertEquals(Avatar.EMOTICON_AVATAR, userActionRowViewModel.mAvatarType.get());
        assertEquals(expectedMessage, userActionRowViewModel.mContent.get());
    }

    @Test
    public void openActionRelationshipRemovedSaved() {
        UserActionRowViewModel userActionRowViewModel = UserActionRowViewModel.from(mContext, mAvatarReader,
                mDateTimeUtil, mNavigationHandler, mIntentProvider, prepareTestActivity(Activity.RELATIONSHIP_REMOVED_SAVED));

        Intent mockIntent = new Intent(mContext, UserProfileActivity.class);
        when(mIntentProvider.createUserProfile(anyObject())).thenReturn(mockIntent);
        userActionRowViewModel.openAction();
        verify(mNavigationHandler).startActivity(mockIntent);
    }

    @Test
    public void postContentCreated() {
        String expectedMessage = "relationshipCreatedSavedExpectedContent";
        when(mContext.getString(R.string.user_action_post_on_wall_template, TEST_USER_NAME, TEST_MESSAGE)).thenReturn(expectedMessage);

        UserActionRowViewModel userActionRowViewModel = UserActionRowViewModel.from(mContext, mAvatarReader,
                mDateTimeUtil, mNavigationHandler, mIntentProvider, prepareTestActivity(Activity.POST_CONTENT_CREATED));

        assertEquals("TEST_AVATAR_NAME", userActionRowViewModel.mAvatarName.get());
        assertEquals(Avatar.EMOTICON_AVATAR, userActionRowViewModel.mAvatarType.get());
        assertEquals(expectedMessage, userActionRowViewModel.mContent.get());
    }

    @Test
    public void openActionPostContentCreated() {
        UserActionRowViewModel userActionRowViewModel = UserActionRowViewModel.from(mContext, mAvatarReader,
                mDateTimeUtil, mNavigationHandler, mIntentProvider, prepareTestActivity(Activity.POST_CONTENT_CREATED));

        Intent mockIntent = new Intent(mContext, UserProfileActivity.class);
        when(mIntentProvider.createMyOwnUserProfile()).thenReturn(mockIntent);
        userActionRowViewModel.openAction();
        verify(mNavigationHandler).startActivity(mockIntent);
    }

    @Test
    public void relationshipCreatedRejected() {
        String expectedMessage = "relationshipCreatedSavedExpectedContent";
        when(mContext.getString(R.string.user_action_rejected, TEST_USER_NAME)).thenReturn(expectedMessage);

        UserActionRowViewModel userActionRowViewModel = UserActionRowViewModel.from(mContext, mAvatarReader,
                mDateTimeUtil, mNavigationHandler, mIntentProvider, prepareTestActivity(Activity.RELATIONSHIP_CREATED_REJECTED));

        assertEquals("TEST_AVATAR_NAME", userActionRowViewModel.mAvatarName.get());
        assertEquals(Avatar.EMOTICON_AVATAR, userActionRowViewModel.mAvatarType.get());
        assertEquals(expectedMessage, userActionRowViewModel.mContent.get());
    }

    @Test
    public void openActionRelationshipCreatedRejected() {
        UserActionRowViewModel userActionRowViewModel = UserActionRowViewModel.from(mContext, mAvatarReader,
                mDateTimeUtil, mNavigationHandler, mIntentProvider, prepareTestActivity(Activity.RELATIONSHIP_CREATED_REJECTED));

        Intent mockIntent = new Intent(mContext, UserProfileActivity.class);
        when(mIntentProvider.createUserProfile(anyObject())).thenReturn(mockIntent);
        userActionRowViewModel.openAction();
        verify(mNavigationHandler).startActivity(mockIntent);
    }

    private @NonNull Activity prepareTestActivity(@NonNull String type) {
        return Activity.builder()
                .activityEntity(prepareTestActivityEntity())
                .owner(prepareTesUserProfile())
                .receiver(prepareTesUserProfile())
                .key(checkNotNull(type))
                .entityType("TEST_ENTITY")
                .build();

    }

    private @NonNull ActivityEntity prepareTestActivityEntity() {
        return ActivityEntity.builder().frienjiId(1)
                .id(2)
                .createdAt(new Date(System.currentTimeMillis()))
                .message(TEST_MESSAGE)
                .postId(3)
                .liked(false)
                .parentId(4)
                .relatedFrienjiId(5)
                .relationType("TEST_RELATION")
                .wallOwnerId(1)
                .build();
    }

    private @NonNull UserProfile prepareTesUserProfile() {
        return UserProfile.builder()
                .id(1)
                .userName(TEST_USER_NAME)
                .whoYouAre("TEST_WHO_YOU_ARE")
                .avatar(Avatar.builder().name("TEST_AVATAR_NAME").type(Avatar.EMOTICON_AVATAR).build()).build();
    }
}