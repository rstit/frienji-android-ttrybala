package com.ripple.frienji.ui.model.row;

import android.content.Context;
import android.content.Intent;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.conversation.Conversation;
import com.ripple.frienji.model.message.ChatMessage;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.util.DateTimeUtil;
import com.ripple.frienji.ui.view.activity.ConversationActivity;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import java.util.Date;
import javax.inject.Inject;
import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by kamil ratajczak on 21.08.16.
 */
public class InBoxRowViewModelTest {
    private final int TEST_ID = 1;
    private final String TEST_MESSAGE_CONTENT = "Test message";

    @Inject
    protected NavigationHandler mNavigationHandler;

    @Inject
    protected Context mContext;

    @Inject
    protected AvatarReader mAvatarReader;

    @Inject
    protected IntentProvider mIntentProvider;

    protected InBoxRowViewModel mInBoxRowViewModel;

    private DateTimeUtil mDateTimeUtil = Mockito.mock(DateTimeUtil.class);

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    private Conversation generateConversation() {

        UserProfile userProfile = UserProfile.builder()
                .id(1)
                .userName("TEST_USER_NAME")
                .whoYouAre("TEST_WHO_YOU_ARE")
                .avatar(Avatar.builder().name("TEST_AVATAR_NAME").type(Avatar.EMOTICON_AVATAR).build()).build();

        ChatMessage chatMessage = ChatMessage.builder().content(TEST_MESSAGE_CONTENT).read(false)
                .id(TEST_ID).myOwnMessage(false).createdAt(new Date(System.currentTimeMillis())).build();

        return Conversation.builder().createdAt(new Date(System.currentTimeMillis())).id(TEST_ID).receiver(userProfile).
                senderId(TEST_ID).recipientId(TEST_ID).lastMessage(chatMessage).messagesCount(1).unreadCount(1).build();
    }

    @Test
    public void openConversation() {
        when(mDateTimeUtil.getTimestampString(anyObject())).thenReturn("TEST_TIME");
        when(mAvatarReader.getEmoticonAvatar(anyString())).thenReturn("TEST_EMOTICON");
        mInBoxRowViewModel = InBoxRowViewModel.from(mContext, mNavigationHandler, mAvatarReader, mDateTimeUtil, mIntentProvider, generateConversation());

        Intent intent = new Intent(mContext, ConversationActivity.class);
        when(mInBoxRowViewModel.mIntentProvider.createConversation(anyObject())).thenReturn(intent);

        mInBoxRowViewModel.openConversation();
        verify(mInBoxRowViewModel.mNavigationHandler).startActivity(intent);
    }

    @Test
    public void testContent() {
        when(mDateTimeUtil.getTimestampString(anyObject())).thenReturn("TEST_TIME");
        when(mAvatarReader.getEmoticonAvatar(anyString())).thenReturn("TEST_EMOTICON");
        mInBoxRowViewModel = InBoxRowViewModel.from(mContext, mNavigationHandler, mAvatarReader, mDateTimeUtil, mIntentProvider, generateConversation());

        assertEquals(mInBoxRowViewModel.mUserName.get(), "TEST_USER_NAME");
        assertEquals(mInBoxRowViewModel.mAvatarType.get(), Avatar.EMOTICON_AVATAR);
        assertEquals(mInBoxRowViewModel.mAvatarName.get(), "TEST_AVATAR_NAME");
    }
}