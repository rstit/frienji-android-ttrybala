package com.ripple.frienji.ui.model.row;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.message.ChatMessage;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.net.FrienjiApi;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.util.rx.ObservableTransformers;
import org.junit.Before;
import org.junit.Test;
import java.util.Date;
import javax.inject.Inject;
import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Kamil Ratajczak
 * @since 08.11.16
 */

public class ChatMessageRowViewModelTest {
    private static final String TEST_MESSAGE = "TEST_MESSAGE";
    private static final String TEST_MESSAGE_IMAGE = "TEST_MESSAGE_IMAGE";
    private static final String TEST_USER_NAME = "TEST_USER_NAME";
    private static final String TEST_COVER_IMAGE = "TEST_COVER_IMAGE";
    private static final String TEST_WHO_YOU_ARE = "TEST_WHO_YOU_ARE";
    private static final String TEST_AVATAR_NAME = "TEST_AVATAR_NAME";

    @Inject
    protected NavigationHandler mNavigationHandler;

    @Inject
    protected Context mContext;

    @Inject
    protected FrienjiApi mFrienjiApi;

    @Inject
    protected AvatarReader mAvatarReader;

    @Inject
    protected IntentProvider mIntentProvider;

    @Inject
    protected ObservableTransformers mObservableTransformers;

    protected ChatMessageRowViewModel mChatMessageRowViewModel;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    @Test
    public void fillMessage() {
        ChatMessage chatMessage = createChatMessage();
        UserProfile userProfile = createUserProfile();

        mChatMessageRowViewModel = ChatLeftMessageRowViewModel.from(mContext, mAvatarReader, mFrienjiApi, mNavigationHandler,
                mObservableTransformers, mIntentProvider, chatMessage, userProfile);

        assertEquals(TEST_MESSAGE, mChatMessageRowViewModel.mMessage.get());
        assertEquals(TEST_MESSAGE_IMAGE, mChatMessageRowViewModel.mImageName.get());
        assertEquals(TEST_AVATAR_NAME, mChatMessageRowViewModel.mAvatarName.get());
        assertEquals(Avatar.EMOTICON_AVATAR, mChatMessageRowViewModel.mAvatarType.get());
        assertEquals(userProfile, mChatMessageRowViewModel.mUserProfile);
    }

    @Test
    public void openUserProfile() {
        ChatMessage chatMessage = createChatMessage();
        UserProfile userProfile = createUserProfile();

        Intent testIntent = new Intent();

        when(mIntentProvider.createUserProfile(userProfile)).thenReturn(testIntent);

        mChatMessageRowViewModel = ChatLeftMessageRowViewModel.from(mContext, mAvatarReader, mFrienjiApi, mNavigationHandler,
                mObservableTransformers, mIntentProvider, chatMessage, userProfile);

        mChatMessageRowViewModel.openUserProfile();
        verify(mNavigationHandler).startActivity(testIntent);
    }

    @Test
    public void openMyOwnUserProfile() {
        ChatMessage chatMessage = createChatMessage();
        UserProfile userProfile = createUserProfile();
        userProfile.setMyOwnProfile(true);

        Intent testIntent = new Intent();

        when(mIntentProvider.createMyOwnUserProfile()).thenReturn(testIntent);

        mChatMessageRowViewModel = ChatRightMessageRowViewModel.from(mContext, mAvatarReader, mFrienjiApi, mNavigationHandler,
                mObservableTransformers, mIntentProvider, chatMessage, userProfile);

        mChatMessageRowViewModel.openUserProfile();
        verify(mNavigationHandler).startActivity(testIntent);
    }

    private @NonNull ChatMessage createChatMessage() {
        return ChatMessage.builder().content(TEST_MESSAGE)
                .attachmentImageUrl(TEST_MESSAGE_IMAGE)
                .id(new Long(1))
                .read(false)
                .createdAt(new Date(System.currentTimeMillis()))
                .myOwnMessage(false)
                .build();
    }

    private @NonNull UserProfile createUserProfile() {
        return UserProfile.builder()
                .userName(TEST_USER_NAME)
                .coverImage(TEST_COVER_IMAGE)
                .id(new Long(1))
                .unreadMessagesCount(0)
                .avatar(prepareTestAvatarModel())
                .whoYouAre(TEST_WHO_YOU_ARE).build();
    }

    private @NonNull Avatar prepareTestAvatarModel() {
        return Avatar.builder().type(Avatar.EMOTICON_AVATAR).name(TEST_AVATAR_NAME).build();
    }
}