package com.ripple.frienji.ui.model.activity;

import com.ripple.frienji.R;
import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.auth.SignUpBody;
import org.junit.Before;
import org.junit.Test;
import javax.inject.Inject;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * @author kamil ratajczak
 * @since 08.08.2016
 */
public class WhoAreYouViewModelTest {
    private final String TEST_WHO_YOU_ARE = "WHO YOU ARE TEST";
    private final String TEST_USER_NAME = "USER_NAME_TEST";
    private final String TEST_AVATAR_NAME = "AVATAR_NAME_TEST";
    private final int TEST_AVATAR_TYPE = 1;

    @Inject
    protected WhoAreYouViewModel mWhoAreYouViewModel;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    @Test
    public void createSignInBodyTest() {
        mWhoAreYouViewModel.mWhoYouAre.set(TEST_WHO_YOU_ARE);
        mWhoAreYouViewModel.mUserName.set(TEST_USER_NAME);

        mWhoAreYouViewModel.mAvatar = Avatar.builder().name(TEST_AVATAR_NAME).type(TEST_AVATAR_TYPE).build();

        SignUpBody signUpBody = mWhoAreYouViewModel.createSignInBody();

        assertEquals(signUpBody.userName(), TEST_USER_NAME);
        assertEquals(signUpBody.whoYouAre(), TEST_WHO_YOU_ARE);
        assertEquals(signUpBody.avatar().name(), TEST_AVATAR_NAME);
        assertEquals(signUpBody.avatar().type(), TEST_AVATAR_TYPE);
        assertNull(signUpBody.countryCode());
        assertNull(signUpBody.phoneNumber());
    }

    @Test
    public void populateAvatarTest() {
        Avatar avatar = Avatar.builder().name(TEST_AVATAR_NAME).type(TEST_AVATAR_TYPE).build();

        mWhoAreYouViewModel.populateAvatar(avatar);

        assertEquals(mWhoAreYouViewModel.mAvatar.name(), TEST_AVATAR_NAME);
        assertEquals(mWhoAreYouViewModel.mAvatar.type(), TEST_AVATAR_TYPE);
    }

    @Test
    public void testValidateInput() {
        when(mWhoAreYouViewModel.mContext.getString(R.string.input_error_username_not_valid)).thenReturn("ERROR");
        assertTrue(mWhoAreYouViewModel.validateInput(TEST_USER_NAME, TEST_WHO_YOU_ARE));
        assertNull(mWhoAreYouViewModel.mUserNameError.get());
    }

    @Test
    public void testValidateInput_NotValidCase1() {
        when(mWhoAreYouViewModel.mContext.getString(R.string.input_error_username_not_valid)).thenReturn("ERROR");
        assertFalse(mWhoAreYouViewModel.validateInput("", TEST_WHO_YOU_ARE));
        assertNotNull(mWhoAreYouViewModel.mUserNameError.get());
    }

    @Test
    public void testValidateInput_NotValidCase2() {
        when(mWhoAreYouViewModel.mContext.getString(R.string.input_error_whoyouare_not_valid)).thenReturn("ERROR");
        assertFalse(mWhoAreYouViewModel.validateInput(TEST_USER_NAME, ""));
        assertNotNull(mWhoAreYouViewModel.mWhoYouAreError.get());
    }

    @Test(expected = NullPointerException.class)
    public void testValidate_NullExpected1() {
        assertFalse(mWhoAreYouViewModel.validateInput(null, TEST_WHO_YOU_ARE));
    }

    @Test(expected = NullPointerException.class)
    public void testValidate_NullExpected2() {
        assertFalse(mWhoAreYouViewModel.validateInput(TEST_USER_NAME, null));
    }
}