package com.ripple.frienji.ui.model.row;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.view.activity.ConversationActivity;
import com.ripple.frienji.ui.view.activity.UserProfileActivity;
import org.junit.Before;
import org.junit.Test;
import javax.inject.Inject;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Kamil Ratajczak
 * @since 09.10.16
 */
public class ZooRowViewModelTest {

    @Inject
    protected NavigationHandler mNavigationHandler;

    @Inject
    protected Context mContext;

    @Inject
    protected AvatarReader mAvatarReader;

    @Inject
    protected IntentProvider mIntentProvider;

    protected ZooRowViewModel mZooRowViewModel;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    @Test
    public void openUserChat() {
        mZooRowViewModel = ZooRowViewModel.from(mContext, mAvatarReader, mNavigationHandler, mIntentProvider, prepareUserProfile());
        Intent intent = new Intent(mContext, ConversationActivity.class);

        when(mZooRowViewModel.mIntentProvider.createConversation(prepareUserProfile())).thenReturn(intent);
        mZooRowViewModel.openUserChat();
        verify(mZooRowViewModel.mNavigationHandler).startActivity(intent);
    }

    @Test
    public void openUserProfile() {
        mZooRowViewModel = ZooRowViewModel.from(mContext, mAvatarReader, mNavigationHandler, mIntentProvider, prepareUserProfile());
        Intent intent = new Intent(mContext, UserProfileActivity.class);

        when(mZooRowViewModel.mIntentProvider.createUserProfile(prepareUserProfile())).thenReturn(intent);
        mZooRowViewModel.openUserProfile();
        verify(mZooRowViewModel.mNavigationHandler).startActivity(intent);
    }

    private @NonNull UserProfile prepareUserProfile() {
        return UserProfile.builder()
                .id(1)
                .userName("TEST_USER_NAME")
                .whoYouAre("TEST_WHO_YOU_ARE")
                .avatar(Avatar.builder().name("TEST_AVATAR_NAME").type(Avatar.EMOTICON_AVATAR).build()).build();
    }
}