package com.ripple.frienji.ui.model.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import com.google.common.base.Optional;
import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.message.FrienjiPost;
import com.ripple.frienji.model.page.PagePaginator;
import com.ripple.frienji.model.response.FrienjiPostCommentsResponse;
import com.ripple.frienji.model.response.FrienjiPostsResponse;
import com.ripple.frienji.model.response.StatusResponse;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.ui.util.DateTimeUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import rx.Observable;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Kamil Ratajczak
 * @since 04.10.16
 */
public class CommentsViewModelTest {
    private final String TEST_WHO_YOU_ARE = "WHO YOU ARE TEST";
    private final String TEST_USER_NAME = "USER_NAME_TEST";

    @Inject
    protected CommentsViewModel mCommentsViewModel;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
        mCommentsViewModel.mDateTimeUtil = Mockito.mock(DateTimeUtil.class);
    }

    @Test
    public void commentsCoachMarkIsVisible() throws Exception {
        mCommentsViewModel.showCoachmarkIfNecessary();
        assertTrue(mCommentsViewModel.mShowCoachMark.get());
    }

    @Test
    public void commentsCoachMarkIsHidden() throws Exception {
        mCommentsViewModel.showCoachmarkIfNecessary();
        assertTrue(mCommentsViewModel.mShowCoachMark.get());

        mCommentsViewModel.hideCoachMark();

        assertFalse(mCommentsViewModel.mShowCoachMark.get());
    }

    @Test
    public void deleteOperation() throws Exception {
        long testUserId = 1;
        long testPostId = 1;
        int testPosition = 1;

        when(mCommentsViewModel.mApi.loadPostComments(testUserId,testPostId)).thenReturn(Observable.just(repareFrienjiPostCommentsResponse()));

        mCommentsViewModel.fillMessageCommentsList(testUserId,testPostId);
        assertEquals(20, mCommentsViewModel.mCommentsAdapter.getItemCount());

        when(mCommentsViewModel.mApi.deletePost(testPostId)).thenReturn(Observable.just(StatusResponse.builder().status("ok").build()));

        mCommentsViewModel.deleteComment(testPostId, testPosition);

        assertEquals(19, mCommentsViewModel.mCommentsAdapter.getItemCount());
    }

    @Test
    public void showUser() {
        mCommentsViewModel.mCommentAuthor = prepareTestUserProfileModel(1);
        Intent testIntent = new Intent();

        when(mCommentsViewModel.mAppSettings.loadUserProfileId()).thenReturn(Optional.fromNullable(2L));
        when(mCommentsViewModel.mIntentProvider.createUserProfile(mCommentsViewModel.mCommentAuthor)).thenReturn(testIntent);

        mCommentsViewModel.showUser();
        verify(mCommentsViewModel.mNavigator).startActivity(testIntent);
    }

    @Test
    public void showMyOwnUser() {
        mCommentsViewModel.mCommentAuthor = prepareTestUserProfileModel(2);
        Intent testIntent = new Intent();

        when(mCommentsViewModel.mAppSettings.loadUserProfileId()).thenReturn(Optional.fromNullable(2L));
        when(mCommentsViewModel.mIntentProvider.createMyOwnUserProfile()).thenReturn(testIntent);

        mCommentsViewModel.showUser();
        verify(mCommentsViewModel.mNavigator).startActivity(testIntent);
    }

    private @NonNull FrienjiPostCommentsResponse repareFrienjiPostCommentsResponse() {
        PagePaginator pagePaginator = PagePaginator.builder().currentPage(1).nextPage(2).prevPage(0).hasNext(true).totalPages(2).build();
        return FrienjiPostCommentsResponse.builder().pagePaginator(pagePaginator).post(prepareFrienjiPost()).build();
    }

    private @NonNull FrienjiPost prepareFrienjiPost() {
        return FrienjiPost.builder().id(1).createdAt(new Date(System.currentTimeMillis()))
                .myOwnMessage(false)
                .numberOfLikes(0)
                .numberOfComments(0)
                .liked(false)
                .author(prepareTestUserProfileModel(1))
                .numberOfComments(20)
                .comments(prepareFrienjiComments(20))
                .build();
    }

    private @NonNull List<FrienjiPost> prepareFrienjiComments(int numberOfPosts) {
        List<FrienjiPost> frienjiPosts = new ArrayList<>();

        for (int i = 1; i <= numberOfPosts; i++) {
            frienjiPosts.add(prepareFrienjiPost(i));
        }

        return frienjiPosts;
    }

    private @NonNull FrienjiPost prepareFrienjiPost(int tag) {
        return FrienjiPost.builder().id(tag).createdAt(new Date(System.currentTimeMillis()))
                .myOwnMessage(false)
                .numberOfLikes(0)
                .numberOfComments(0)
                .liked(false)
                .author(prepareTestUserProfileModel(tag)).build();
    }

    private @NonNull UserProfile prepareTestUserProfileModel(int number) {
        return UserProfile.builder().avatar(prepareTestAvatarModel()).userName(TEST_USER_NAME + "_" + number).id(number).whoYouAre(TEST_WHO_YOU_ARE + "_" + number).build();
    }

    private @NonNull Avatar prepareTestAvatarModel() {
        return Avatar.builder().type(Avatar.EMOTICON_AVATAR).name("TEST_ANATAR_NAME").build();
    }

    private @NonNull FrienjiPostsResponse prepraerFrienjiPostsResponseTest() {
        return FrienjiPostsResponse.builder().frienjiPosts(new ArrayList<>())
                .pagePaginator(PagePaginator.builder()
                        .totalPages(1)
                        .hasNext(false)
                        .currentPage(1)
                        .nextPage(1)
                        .prevPage(1)
                        .build()).build();
    }
}