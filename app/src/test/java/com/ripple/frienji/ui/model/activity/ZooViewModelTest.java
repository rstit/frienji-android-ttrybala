package com.ripple.frienji.ui.model.activity;

import android.content.Context;
import android.support.annotation.NonNull;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.page.PagePaginator;
import com.ripple.frienji.model.response.FrienjiZooResponse;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.ui.model.row.ZooRowViewModel;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.view.activity.InBoxActivity;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import rx.Observable;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by kamil ratajczak on 21.08.16.
 */
public class ZooViewModelTest {
    private final String TEST_ANATAR_NAME = "grinning_face_with_smiling_eyes";
    private final String TEST_USER_NAME = "USER_NAME_TEST";
    private final String TEST_WHO_YOU_ARE = "WHO YOU ARE TEST";

    private final int NUMBER_OF_TEST_ELEMENTS = 5;
    private final int ID_ELEMENT_TO_DELETE_1 = 2;
    private final int ID_ELEMENT_TO_DELETE_2 = 4;
    private final int NUMBER_OF_DELETE_ELEMENTS = 2;

    @Inject
    protected ZooViewModel mZooViewModel;

    @Inject
    protected Context mContext;

    @Inject
    protected AvatarReader mAvatarReader;

    @Inject
    protected IntentProvider mIntentProvider;

    @Inject
    protected NavigationHandler mNavigationHandler;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    @Test
    public void openMessageBox() throws Exception {
        mZooViewModel.openMessageBox();
        verify(mZooViewModel.mNavigator).startActivity(InBoxActivity.class);
    }

    @Test
    public void switchToEditMode() throws Exception {
        mZooViewModel.mZooAdapter = mock(ZooViewModel.ZooListAdapter.class);

        assertFalse(mZooViewModel.mEditMode.get());
        assertFalse(mZooViewModel.mHasUsersToDelete.get());
        assertTrue(mZooViewModel.mZooUsersToDelete.isEmpty());

        mZooViewModel.switchToEditMode();

        assertTrue(mZooViewModel.mEditMode.get());
        assertFalse(mZooViewModel.mHasUsersToDelete.get());
        assertTrue(mZooViewModel.mZooUsersToDelete.isEmpty());
    }

    @Test
    public void deleteAction() throws Exception {
        mZooViewModel.mZooAdapter = mock(ZooViewModel.ZooListAdapter.class);

        when(mZooViewModel.mZooAdapter.removeElement(any())).thenReturn(true);

        List<ZooRowViewModel> zooRowViewModels = prepareTestZooRowViewModels();

        mZooViewModel.mZooAdapter.fillData(zooRowViewModels);

        assertFalse(mZooViewModel.mEditMode.get());
        assertFalse(mZooViewModel.mHasUsersToDelete.get());
        assertTrue(mZooViewModel.mZooUsersToDelete.isEmpty());

        mZooViewModel.switchToEditMode();

        assertTrue(mZooViewModel.mEditMode.get());
        assertFalse(mZooViewModel.mHasUsersToDelete.get());
        assertTrue(mZooViewModel.mZooUsersToDelete.isEmpty());

        ZooRowViewModel zooRowViewModelToDelete1 = zooRowViewModels.get(ID_ELEMENT_TO_DELETE_1);
        ZooRowViewModel zooRowViewModelToDelete2 = zooRowViewModels.get(ID_ELEMENT_TO_DELETE_2);

        zooRowViewModelToDelete1.remove();
        zooRowViewModelToDelete2.remove();

        assertEquals(NUMBER_OF_DELETE_ELEMENTS, mZooViewModel.mZooUsersToDelete.size());
    }

    @Test
    public void testPagination() throws Exception {
        FrienjiZooResponse testFrienjiZooResponse = prepareFrienjiZooResponse(20);

        when(mZooViewModel.mApi.loadUserZoo()).thenReturn(Observable.just(testFrienjiZooResponse));
        when(mZooViewModel.mApi.loadUserProfile()).thenReturn(Observable.just(prepareTestUserProfileModel(1)));

        mZooViewModel.loadUserZoo();
        assertEquals(mZooViewModel.mZooAdapter.getItemCount(), 20);

        when(mZooViewModel.mApi.loadUserZoo(2)).thenReturn(Observable.just(testFrienjiZooResponse));
        mZooViewModel.fillUserZoo(2);

        assertEquals(mZooViewModel.mZooAdapter.getItemCount(), 40);
    }

    @Test
    public void testFrienjiWallCoachMarkIsVisible() throws Exception {
        mZooViewModel.showCoachmarkIfNecessary();
        assertTrue(mZooViewModel.mShowCoachMark.get());
    }

    @Test
    public void testFrienjiWallCoachMarkIsHidden() throws Exception {
        mZooViewModel.showCoachmarkIfNecessary();
        assertTrue(mZooViewModel.mShowCoachMark.get());

        mZooViewModel.hideCoachMark();

        assertFalse(mZooViewModel.mShowCoachMark.get());
    }

    @Test
    public void loadUserInfo() {
        when(mZooViewModel.mApi.loadUserProfile()).thenReturn(Observable.just(prepareTestUserProfileModel(1)));
        assertNull(mZooViewModel.mUnreadMessagesNumber.get());
        mZooViewModel.loadUserInfo();
        assertEquals("10", mZooViewModel.mUnreadMessagesNumber.get());
    }

    private @NonNull List<ZooRowViewModel> prepareTestZooRowViewModels() {
        List<ZooRowViewModel> zooRowViewModels = new ArrayList<>();

        for (int i = 1; i <= NUMBER_OF_TEST_ELEMENTS; i++) {
            UserProfile userProfile = prepareTestUserProfileModel(i);

            ZooRowViewModel zooRowViewModel = ZooRowViewModel.from(mContext, mAvatarReader, mNavigationHandler, mIntentProvider, userProfile);
            zooRowViewModel.setRemoveFrienjiListener(mZooViewModel);
            zooRowViewModels.add(zooRowViewModel);
        }

        return zooRowViewModels;
    }

    private @NonNull UserProfile prepareTestUserProfileModel(int number) {
        return UserProfile.builder().avatar(prepareTestAvatarModel()).userName(TEST_USER_NAME + "_" + number).id(number).unreadMessagesCount(10).whoYouAre(TEST_WHO_YOU_ARE + "_" + number).build();
    }

    private @NonNull Avatar prepareTestAvatarModel() {
        return Avatar.builder().type(Avatar.EMOTICON_AVATAR).name(TEST_ANATAR_NAME).build();
    }

    private @NonNull FrienjiZooResponse prepareFrienjiZooResponse(int numberOfTestElements) {
        PagePaginator pagePaginator = PagePaginator.builder().currentPage(1).nextPage(2).prevPage(0).hasNext(true).totalPages(2).build();
        return FrienjiZooResponse.builder().frienjiZoos(prepareTestUserProfiles(numberOfTestElements)).pagePaginator(pagePaginator).build();
    }

    private @NonNull List<UserProfile> prepareTestUserProfiles(int number) {
        List<UserProfile> userProfiles = new ArrayList<>();

        for (int i = 1; i <= number; i++) {
            userProfiles.add(prepareTestUserProfileModel(i));
        }

        return userProfiles;
    }
}