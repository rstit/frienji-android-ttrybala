package com.ripple.frienji.ui.model.activity;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import com.google.android.gms.maps.model.LatLng;
import com.google.common.base.Optional;
import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.conversation.Conversation;
import com.ripple.frienji.model.message.FrienjiPost;
import com.ripple.frienji.model.page.PagePaginator;
import com.ripple.frienji.model.response.FrienjiPostsResponse;
import com.ripple.frienji.model.response.StatusResponse;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.ui.util.DateTimeUtil;
import com.ripple.frienji.ui.view.activity.ConversationActivity;
import com.ripple.frienji.ui.view.activity.MySettingsActivity;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import rx.Observable;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by kamil ratajczak on 21.08.16.
 */
public class UserProfileViewModelTest {
    private final static String TEST_COVER_IMAGE = "coverImage";
    private final int TEST_AVATAR_TYPE = 0;
    private final int TEST_USER_ID = 1;
    private final String TEST_WHO_YOU_ARE = "WHO YOU ARE TEST";
    private final String TEST_USER_NAME = "USER_NAME_TEST";
    private final String TEST_AVATAR_NAME = "EMOTICON_NAME_TEST";

    @Inject
    protected UserProfileViewModel mUserProfileViewModel;

    @Inject
    protected Context mContext;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
        mUserProfileViewModel.mDateTimeUtil = Mockito.mock(DateTimeUtil.class);
    }

    @Test
    public void loadUserProfileData() {
        mUserProfileViewModel.mUserId = 1;
        when(mUserProfileViewModel.mAppSettings.loadUserName()).thenReturn(Optional.fromNullable("TEST_USER_NAME"));
        when(mUserProfileViewModel.mAppSettings.loadUUserWhoYouAre()).thenReturn(Optional.fromNullable("TEST_WHO_YOU_ARE"));
        when(mUserProfileViewModel.mAppSettings.loadUserAvatarName()).thenReturn(Optional.fromNullable("TEST_AVATAR_NAME"));
        when(mUserProfileViewModel.mAppSettings.loadUserAvatarType()).thenReturn(Optional.fromNullable(new Integer(TEST_AVATAR_TYPE)));
        when(mUserProfileViewModel.mAppSettings.loadUserProfileId()).thenReturn(Optional.fromNullable(new Long(TEST_USER_ID)));
        when(mUserProfileViewModel.mAppSettings.loadUserCoverImage()).thenReturn(Optional.fromNullable(TEST_COVER_IMAGE));
        when(mUserProfileViewModel.getEmoticon(anyString())).thenReturn("TEST_EMOTIOCON_NAME");

        when(mUserProfileViewModel.mApi.loadUserPosts(mUserProfileViewModel.mUserId)).thenReturn(Observable.just(prepraerFrienjiPostsResponseTest()));

        mUserProfileViewModel.loadUserOwnProfileData();

        assertEquals("TEST_USER_NAME", mUserProfileViewModel.mUserName.get());
        assertEquals("TEST_WHO_YOU_ARE", mUserProfileViewModel.mWhoYouAre.get());
        assertEquals("TEST_AVATAR_NAME", mUserProfileViewModel.mAvatarName.get());
        assertEquals(TEST_AVATAR_TYPE, mUserProfileViewModel.mAvatarType.get());
        assertEquals(TEST_USER_ID, mUserProfileViewModel.mUserId);
        assertEquals(TEST_COVER_IMAGE, mUserProfileViewModel.mCoverImage.get());
    }

    @Test
    public void loadFrienjiProfileData() {
        mUserProfileViewModel.mUserId = 1;
        Avatar avatar = Avatar.builder().name(TEST_AVATAR_NAME).type(TEST_AVATAR_TYPE).build();

        UserProfile frienjiProfile = UserProfile.builder().id(TEST_USER_ID).avatar(avatar).userName(TEST_USER_NAME)
                .coverImage(TEST_COVER_IMAGE).whoYouAre(TEST_WHO_YOU_ARE).build();

        when(mUserProfileViewModel.mApi.loadUserPosts(mUserProfileViewModel.mUserId)).thenReturn(Observable.just(prepraerFrienjiPostsResponseTest()));

        mUserProfileViewModel.loadFrienjiProfileData(frienjiProfile);

        assertEquals(TEST_USER_NAME, mUserProfileViewModel.mUserName.get());
        assertEquals(TEST_WHO_YOU_ARE, mUserProfileViewModel.mWhoYouAre.get());
        assertEquals(TEST_AVATAR_NAME, mUserProfileViewModel.mAvatarName.get());
        assertEquals(TEST_AVATAR_TYPE, mUserProfileViewModel.mAvatarType.get());
        assertEquals(TEST_USER_ID, mUserProfileViewModel.mUserId);
        assertEquals(TEST_COVER_IMAGE, mUserProfileViewModel.mCoverImage.get());
    }

    @Test
    public void launchEditUserProfile() throws Exception {
        mUserProfileViewModel.editUserProfile();
        verify(mUserProfileViewModel.mNavigator).startActivity(MySettingsActivity.class);
    }

    @Test
    public void launchOpenChat() throws Exception {
        Avatar avatar = Avatar.builder().name(TEST_AVATAR_NAME).type(TEST_AVATAR_TYPE).build();

        UserProfile frienjiProfile = UserProfile.builder().id(TEST_USER_ID).avatar(avatar).userName(TEST_USER_NAME)
                .coverImage(TEST_COVER_IMAGE).whoYouAre(TEST_WHO_YOU_ARE).build();

        mUserProfileViewModel.mFrienjiProfile = frienjiProfile;

        Intent intent = new Intent(mContext, ConversationActivity.class);
        when(mUserProfileViewModel.mIntentProvider.createConversation(frienjiProfile)).thenReturn(intent);

        mUserProfileViewModel.openChat();
        verify(mUserProfileViewModel.mNavigator).startActivity(intent);
    }

    @Test
    public void userWallCoachMarkIsVisible() throws Exception {
        mUserProfileViewModel.initUserCoachMark();
        assertTrue(mUserProfileViewModel.mShowCoachMark.get());
    }

    @Test
    public void userWallCoachMarkIsHidden() throws Exception {
        mUserProfileViewModel.initUserCoachMark();
        assertTrue(mUserProfileViewModel.mShowCoachMark.get());

        mUserProfileViewModel.hideCoachMark();

        assertFalse(mUserProfileViewModel.mShowCoachMark.get());
    }

    @Test
    public void frienjiWallCoachMarkIsVisible() throws Exception {
        mUserProfileViewModel.initFrienjiCoachMark();
        assertTrue(mUserProfileViewModel.mShowCoachMark.get());
    }

    @Test
    public void frienjiWallCoachMarkIsHidden() throws Exception {
        mUserProfileViewModel.initFrienjiCoachMark();
        assertTrue(mUserProfileViewModel.mShowCoachMark.get());

        mUserProfileViewModel.hideCoachMark();

        assertFalse(mUserProfileViewModel.mShowCoachMark.get());
    }

    @Test
    public void testPagination() throws Exception {
        long testUserId = 1;
        mUserProfileViewModel.mUserId = testUserId;

        when(mUserProfileViewModel.mApi.loadUserPosts(testUserId)).thenReturn(Observable.just(prepareFrienjiPostsResponse(20)));

        mUserProfileViewModel.fillMessagesList();
        assertEquals(mUserProfileViewModel.mMessagesAdapter.getItemCount(), 20);

        when(mUserProfileViewModel.mApi.loadUserPosts(testUserId, 2)).thenReturn(Observable.just(prepareFrienjiPostsResponse(20)));
        mUserProfileViewModel.fillMessagesList(2);

        assertEquals(mUserProfileViewModel.mMessagesAdapter.getItemCount(), 40);
    }

    @Test
    public void deleteOperation() throws Exception {
        long testUserId = 1;
        long testPostId = 1;
        int testPosition = 1;
        mUserProfileViewModel.mUserId = testUserId;

        when(mUserProfileViewModel.mApi.loadUserPosts(testUserId)).thenReturn(Observable.just(prepareFrienjiPostsResponse(20)));

        mUserProfileViewModel.fillMessagesList();
        assertEquals(20, mUserProfileViewModel.mMessagesAdapter.getItemCount());

        when(mUserProfileViewModel.mApi.deletePost(testPostId)).thenReturn(Observable.just(StatusResponse.builder().status("OK").build()));

        mUserProfileViewModel.deleteMessage(testPostId, testPosition);

        assertEquals(19, mUserProfileViewModel.mMessagesAdapter.getItemCount());
    }

    @Test
    public void refreshUserProfileData() {
        UserProfileViewModel spyUserProfileViewModel = Mockito.spy(mUserProfileViewModel);

        when(spyUserProfileViewModel.mAppSettings.loadUserName()).thenReturn(Optional.fromNullable("TEST_USER_NAME"));
        when(spyUserProfileViewModel.mAppSettings.loadUUserWhoYouAre()).thenReturn(Optional.fromNullable("TEST_WHO_YOU_ARE"));
        when(spyUserProfileViewModel.mAppSettings.loadUserAvatarName()).thenReturn(Optional.fromNullable("TEST_AVATAR_NAME"));
        when(spyUserProfileViewModel.mAppSettings.loadUserAvatarType()).thenReturn(Optional.fromNullable(new Integer(TEST_AVATAR_TYPE)));
        when(spyUserProfileViewModel.mAppSettings.loadUserProfileId()).thenReturn(Optional.fromNullable(new Long(TEST_USER_ID)));
        when(spyUserProfileViewModel.mAppSettings.loadUserCoverImage()).thenReturn(Optional.fromNullable("TEST_COVER_IMAGE"));

        when(mUserProfileViewModel.mApi.loadUserPosts(mUserProfileViewModel.mUserId)).thenReturn(Observable.just(prepraerFrienjiPostsResponseTest()));

        spyUserProfileViewModel.mIsMyOwnProfile.set(true);

        spyUserProfileViewModel.refreshUserProfileData();

        verify(spyUserProfileViewModel).setUserProfileData();
    }

    @Test
    public void setUserProfileData() {
        when(mUserProfileViewModel.mAppSettings.loadUserName()).thenReturn(Optional.fromNullable("TEST_USER_NAME"));
        when(mUserProfileViewModel.mAppSettings.loadUUserWhoYouAre()).thenReturn(Optional.fromNullable("TEST_WHO_YOU_ARE"));
        when(mUserProfileViewModel.mAppSettings.loadUserAvatarName()).thenReturn(Optional.fromNullable("TEST_AVATAR_NAME"));
        when(mUserProfileViewModel.mAppSettings.loadUserAvatarType()).thenReturn(Optional.fromNullable(new Integer(TEST_AVATAR_TYPE)));
        when(mUserProfileViewModel.mAppSettings.loadUserProfileId()).thenReturn(Optional.fromNullable(new Long(TEST_USER_ID)));
        when(mUserProfileViewModel.mAppSettings.loadUserCoverImage()).thenReturn(Optional.fromNullable("TEST_COVER_IMAGE"));

        mUserProfileViewModel.setUserProfileData();

        assertEquals("TEST_USER_NAME", mUserProfileViewModel.mUserName.get());
        assertEquals("TEST_WHO_YOU_ARE", mUserProfileViewModel.mWhoYouAre.get());
        assertEquals("TEST_AVATAR_NAME", mUserProfileViewModel.mAvatarName.get());
        assertEquals(TEST_AVATAR_TYPE, mUserProfileViewModel.mAvatarType.get());
        assertEquals("TEST_COVER_IMAGE", mUserProfileViewModel.mCoverImage.get());
    }

    @Test
    public void clearNewMessageData() {
        mUserProfileViewModel.mLatLongPosition.set(new LatLng(1, 1));
        mUserProfileViewModel.mImageUri.set("TEST_URI");
        mUserProfileViewModel.mMessage.set("TEST_MESSAGE");
        mUserProfileViewModel.mMessageImageBytes = new byte[1];

        assertNotNull(mUserProfileViewModel.mLatLongPosition.get());
        assertNotNull(mUserProfileViewModel.mImageUri.get());
        assertNotNull(mUserProfileViewModel.mMessage.get());
        assertNotNull(mUserProfileViewModel.mMessageImageBytes);

        mUserProfileViewModel.clearNewMessageData();

        assertNull(mUserProfileViewModel.mLatLongPosition.get());
        assertNull(mUserProfileViewModel.mImageUri.get());
        assertNull(mUserProfileViewModel.mMessage.get());
        assertNull(mUserProfileViewModel.mMessageImageBytes);
    }

    private @NonNull FrienjiPostsResponse prepareFrienjiPostsResponse(int numberOfPosts) {
        PagePaginator pagePaginator = PagePaginator.builder().currentPage(1).nextPage(2).prevPage(0).hasNext(true).totalPages(2).build();
        return FrienjiPostsResponse.builder().pagePaginator(pagePaginator).frienjiPosts(prepareFrienjiPosts(numberOfPosts)).build();
    }

    private @NonNull List<FrienjiPost> prepareFrienjiPosts(int numberOfPosts) {
        List<FrienjiPost> frienjiPosts = new ArrayList<>();

        for (int i = 1; i <= numberOfPosts; i++) {
            frienjiPosts.add(prepareFrienjiPost(i));
        }

        return frienjiPosts;
    }

    private @NonNull FrienjiPost prepareFrienjiPost(int tag) {
        return FrienjiPost.builder().id(tag).createdAt(new Date(System.currentTimeMillis()))
                .myOwnMessage(false)
                .numberOfLikes(0)
                .numberOfComments(0)
                .liked(false)
                .author(prepareTestUserProfileModel(tag)).build();
    }

    private @NonNull UserProfile prepareTestUserProfileModel(int number) {
        return UserProfile.builder().avatar(prepareTestAvatarModel()).userName(TEST_USER_NAME + "_" + number).id(number).whoYouAre(TEST_WHO_YOU_ARE + "_" + number).build();
    }

    private @NonNull Avatar prepareTestAvatarModel() {
        return Avatar.builder().type(Avatar.EMOTICON_AVATAR).name("TEST_ANATAR_NAME").build();
    }

    private @NonNull FrienjiPostsResponse prepraerFrienjiPostsResponseTest() {
        return FrienjiPostsResponse.builder().frienjiPosts(new ArrayList<>())
                .pagePaginator(PagePaginator.builder()
                        .totalPages(1)
                        .hasNext(false)
                        .currentPage(1)
                        .nextPage(1)
                        .prevPage(1)
                        .build()).build();
    }

}