package com.ripple.frienji.ui.model.activity;

import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import org.junit.Before;
import javax.inject.Inject;

/**
 * @author kamil ratajczak
 * @since 08.08.2016
 */
public class ChooseFrienjiModelTest {

    @Inject
    protected ChooseFrienjiViewModel mChooseFrienjiViewModel;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }
}
