package com.ripple.frienji.ui.model.activity;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.frienji.Frienji;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.ui.view.activity.UserActionsActivity;
import com.ripple.frienji.ui.view.activity.UserProfileActivity;
import com.ripple.frienji.ui.view.activity.ZooActivity;
import org.junit.Before;
import org.junit.Test;
import javax.inject.Inject;
import static com.google.common.base.Preconditions.checkNotNull;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author kamil ratajczak
 * @since 08.08.2016
 */
public class ArViewModelTest {

    @Inject
    protected ArViewModel mArViewModel;

    @Inject
    protected Context mContext;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    @Test
    public void launchUserProfileScreen() throws Exception {
        mArViewModel.launchProfileScreen();
        verify(mArViewModel.mNavigator).startActivity(UserProfileActivity.class);
    }

    @Test
    public void launchUserActionsScreen() throws Exception {
        mArViewModel.launchUserActionsScreen();
        verify(mArViewModel.mNavigator).startActivity(UserActionsActivity.class);
    }

    @Test
    public void launchUserZooScreen() throws Exception {
        mArViewModel.launchUserZooScreen();
        verify(mArViewModel.mNavigator).startActivity(ZooActivity.class);
    }

    @Test
    public void openFrienjiProfile() throws Exception {
        Intent mockIntent = new Intent(mContext, checkNotNull(UserProfileActivity.class));
        Frienji frienji = prepareTestFrienjiModel();
        when(mArViewModel.mIntentProvider.createUserProfile(mArViewModel.buildUserProfile(frienji)))
                .thenReturn(mockIntent);
        mArViewModel.openFrienjiProfile(frienji);
        verify(mArViewModel.mNavigator).startActivity(mockIntent);
    }

    @Test
    public void testCoachMarkIsVisible() throws Exception {
        mArViewModel.showCoachMark();
        assertTrue(mArViewModel.mShowCoachMark.get());
    }

    @Test
    public void testCoachMarkIsHidden() throws Exception {
        mArViewModel.showCoachMark();
        assertTrue(mArViewModel.mShowCoachMark.get());

        mArViewModel.hideCoachMark();

        assertFalse(mArViewModel.mShowCoachMark.get());
    }

    private @NonNull Frienji prepareTestFrienjiModel() {
        return Frienji.builder().avatar(prepareTestAvatarModel()).name("TEST_FRIENJI_NAME").id(1).whoYouAre("TEST_WHO_YOU_ARE").build();
    }

    private @NonNull Avatar prepareTestAvatarModel() {
        return Avatar.builder().type(Avatar.EMOTICON_AVATAR).name("TEST_ANATAR_NAME").build();
    }
}