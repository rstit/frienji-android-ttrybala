package com.ripple.frienji.ui.model.activity;

import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.ui.view.activity.SelectFrienjiActivity;
import org.junit.Before;
import org.junit.Test;
import javax.inject.Inject;
import static org.mockito.Mockito.verify;

/**
 * @author Kamil Ratajczak
 * @since 09.10.16
 */
public class InBoxViewModelTest {

    @Inject
    protected InBoxViewModel mInBoxViewModel;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    @Test
    public void openConversation() {
        mInBoxViewModel.openConversation();
        verify(mInBoxViewModel.mNavigator).startActivity(SelectFrienjiActivity.class);
    }
}