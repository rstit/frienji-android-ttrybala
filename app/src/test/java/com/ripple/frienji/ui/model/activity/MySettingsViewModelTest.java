package com.ripple.frienji.ui.model.activity;

import android.content.Context;
import android.content.Intent;
import com.google.common.base.Optional;
import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.ui.view.activity.InfoActivity;
import org.junit.Before;
import org.junit.Test;
import javax.inject.Inject;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by kamil ratajczak on 15.09.16.
 */
public class MySettingsViewModelTest {
    private final int TEST_AVATAR_TYPE = 0;
    private final int TEST_USER_PROFILE_ID = 1;

    @Inject
    protected MySettingsViewModel mMySettingsViewModel;

    @Inject
    protected Context mContext;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    @Test
    public void loadUserProfileData() {
        when(mMySettingsViewModel.mAppSettings.loadUserName()).thenReturn(Optional.fromNullable("TEST_USER_NAME"));
        when(mMySettingsViewModel.mAppSettings.loadUUserWhoYouAre()).thenReturn(Optional.fromNullable("TEST_WHO_YOU_ARE"));
        when(mMySettingsViewModel.mAppSettings.loadUserAvatarName()).thenReturn(Optional.fromNullable("TEST_AVATAR_NAME"));
        when(mMySettingsViewModel.mAppSettings.loadUserAvatarType()).thenReturn(Optional.fromNullable(new Integer(TEST_AVATAR_TYPE)));
        when(mMySettingsViewModel.mAppSettings.loadUserProfileId()).thenReturn(Optional.fromNullable(new Long(TEST_USER_PROFILE_ID)));
        when(mMySettingsViewModel.mAppSettings.loadUserCoverImage()).thenReturn(Optional.fromNullable("TEST_COVER_IMAGE"));

        mMySettingsViewModel.loadUserProfileData();

        assertEquals("TEST_USER_NAME", mMySettingsViewModel.mUserName.get());
        assertEquals("TEST_WHO_YOU_ARE", mMySettingsViewModel.mWhoYouAre.get());
        assertEquals("TEST_AVATAR_NAME", mMySettingsViewModel.mAvatarName.get());
        assertEquals(TEST_AVATAR_TYPE, mMySettingsViewModel.mAvatarType.get());
        assertEquals(TEST_USER_PROFILE_ID, mMySettingsViewModel.mUserProfileId.get());
    }

    @Test
    public void openInfo() throws Exception {
        mMySettingsViewModel.openInfo();
        verify(mMySettingsViewModel.mNavigator).startActivity(InfoActivity.class);
    }

    @Test
    public void testCoachMarkIsVisible() throws Exception {
        when(mMySettingsViewModel.mAppSettings.loadUserName()).thenReturn(Optional.fromNullable("TEST_USER_NAME"));
        when(mMySettingsViewModel.mAppSettings.loadUUserWhoYouAre()).thenReturn(Optional.fromNullable("TEST_WHO_YOU_ARE"));
        when(mMySettingsViewModel.mAppSettings.loadUserAvatarName()).thenReturn(Optional.fromNullable("TEST_AVATAR_NAME"));
        when(mMySettingsViewModel.mAppSettings.loadUserAvatarType()).thenReturn(Optional.fromNullable(new Integer(TEST_AVATAR_TYPE)));
        when(mMySettingsViewModel.mAppSettings.loadUserProfileId()).thenReturn(Optional.fromNullable(new Long(TEST_USER_PROFILE_ID)));
        when(mMySettingsViewModel.mAppSettings.loadUserCoverImage()).thenReturn(Optional.fromNullable("TEST_COVER_IMAGE"));

        mMySettingsViewModel.loadUserProfileData();
        assertTrue(mMySettingsViewModel.mShowCoachMark.get());
    }

    @Test
    public void testCoachMarkIsHidden() throws Exception {
        when(mMySettingsViewModel.mAppSettings.loadUserName()).thenReturn(Optional.fromNullable("TEST_USER_NAME"));
        when(mMySettingsViewModel.mAppSettings.loadUUserWhoYouAre()).thenReturn(Optional.fromNullable("TEST_WHO_YOU_ARE"));
        when(mMySettingsViewModel.mAppSettings.loadUserAvatarName()).thenReturn(Optional.fromNullable("TEST_AVATAR_NAME"));
        when(mMySettingsViewModel.mAppSettings.loadUserAvatarType()).thenReturn(Optional.fromNullable(new Integer(TEST_AVATAR_TYPE)));
        when(mMySettingsViewModel.mAppSettings.loadUserProfileId()).thenReturn(Optional.fromNullable(new Long(TEST_USER_PROFILE_ID)));
        when(mMySettingsViewModel.mAppSettings.loadUserCoverImage()).thenReturn(Optional.fromNullable("TEST_COVER_IMAGE"));

        mMySettingsViewModel.loadUserProfileData();
        assertTrue(mMySettingsViewModel.mShowCoachMark.get());

        mMySettingsViewModel.hideCoachMark();

        assertFalse(mMySettingsViewModel.mShowCoachMark.get());
    }

    @Test
    public void changeAvatar() {
        Intent testIntent = new Intent();
        when((mMySettingsViewModel.mIntentProvider.createChangeAvatar())).thenReturn(testIntent);

        mMySettingsViewModel.changeAvatar();
        verify(mMySettingsViewModel.mNavigator).startActivity(testIntent);
    }

    @Test
    public void changeCoverImage() {
        Intent testIntent = new Intent();
        when((mMySettingsViewModel.mIntentProvider.createChangeCoverImage())).thenReturn(testIntent);

        mMySettingsViewModel.changeCoverImage();
        verify(mMySettingsViewModel.mNavigator).startActivity(testIntent);
    }

    @Test
    public void changeBio() {
        Intent testIntent = new Intent();
        when((mMySettingsViewModel.mIntentProvider.createChangeBio())).thenReturn(testIntent);

        mMySettingsViewModel.changeBio();
        verify(mMySettingsViewModel.mNavigator).startActivity(testIntent);
    }
}