package com.ripple.frienji.ui.model.activity;

import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.model.auth.RequestConfirmCodeBody;
import org.junit.Before;
import org.junit.Test;
import java.util.List;
import javax.inject.Inject;
import rx.observers.TestSubscriber;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * @author kamil ratajczak
 * @since 08.08.2016
 */
public class SignInViewModelTest {
    private final String TEST_COUNTRY_CODE_NUMBER = "+48";
    private final String TEST_PHONE_NUMBER = "791580177";

    private final String TEST_WRONG_COUNTRY_CODE_NUMBER = "+11";
    private final String TEST_WRONG_PHONE_NUMBER = "112333";

    @Inject
    protected SignInViewModel mSignInViewModel;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    @Test
    public void buildSignInBody() throws Exception {
        TestSubscriber<RequestConfirmCodeBody> subscriber = new TestSubscriber<>();

        mSignInViewModel.buildRequestConfirmCodeBody(TEST_COUNTRY_CODE_NUMBER, TEST_PHONE_NUMBER).subscribe(subscriber);

        subscriber.assertNoErrors();
        List<RequestConfirmCodeBody> data = subscriber.getOnNextEvents();
        assertEquals(1, data.size());

        RequestConfirmCodeBody body = data.get(0);

        assertEquals(TEST_COUNTRY_CODE_NUMBER, body.countryCode());
        assertEquals(TEST_PHONE_NUMBER, body.phoneNumber());
    }

    @Test(expected = NullPointerException.class)
    public void buildSignInBody_NullCountryCode() throws Exception {
        //noinspection ConstantConditions
        mSignInViewModel.buildRequestConfirmCodeBody(null, TEST_PHONE_NUMBER);
    }

    @Test(expected = NullPointerException.class)
    public void buildSignInBody_NullPhoneNumber() throws Exception {
        //noinspection ConstantConditions
        mSignInViewModel.buildRequestConfirmCodeBody(TEST_COUNTRY_CODE_NUMBER, null);
    }

    @Test(expected = NullPointerException.class)
    public void signIn_NullBody() throws Exception {
        //noinspection ConstantConditions
        mSignInViewModel.sendRequestConfirmCodeBody(null);
    }

    @Test
    public void validatePhoneNumber_Valid() throws Exception {
        boolean valid = mSignInViewModel.validateInput(TEST_COUNTRY_CODE_NUMBER, TEST_PHONE_NUMBER);
        assertTrue(valid);
        assertFalse(mSignInViewModel.mErrorVisible.get());
    }

    @Test
    public void validatePhoneNumber_Invalid_1() throws Exception {
        boolean valid = mSignInViewModel.validateInput(TEST_COUNTRY_CODE_NUMBER, "");
        assertFalse(valid);
        assertTrue(mSignInViewModel.mErrorVisible.get());
    }

    @Test
    public void validatePhoneNumber_Invalid_2() throws Exception {
        boolean valid = mSignInViewModel.validateInput("", TEST_PHONE_NUMBER);
        assertFalse(valid);
        assertTrue(mSignInViewModel.mErrorVisible.get());
    }

    @Test
    public void validatePhoneNumber_Invalid_3() throws Exception {
        boolean valid = mSignInViewModel.validateInput(TEST_WRONG_COUNTRY_CODE_NUMBER, TEST_PHONE_NUMBER);
        assertFalse(valid);
        assertTrue(mSignInViewModel.mErrorVisible.get());
    }
}