package com.ripple.frienji.ui.model.activity;

import android.content.Context;
import android.support.annotation.NonNull;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.model.action.Activity;
import com.ripple.frienji.model.action.ActivityEntity;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.page.PagePaginator;
import com.ripple.frienji.model.response.FriejiActiviesResponse;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.ui.model.row.UserActionRowViewModel;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.util.DateTimeUtil;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import rx.Observable;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

/**
 * @author Kamil Ratajczak
 * @since 26.09.16
 */
public class UserActionsViewModelTest {
    private final int EXPECTED_ADAPTER_ROWS_SIZE = 2;

    @Inject
    protected UserActionsViewModel mUserActionsViewModel;

    @Inject
    protected Context mContext;

    @Inject
    protected AvatarReader mAvatarReader;

    @Inject
    protected DateTimeUtil mDateTimeUtil;

    @Inject
    protected NavigationHandler mNavigationHandler;

    @Inject
    protected IntentProvider mIntentProvider;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
        mUserActionsViewModel.mDateTimeUtil = mDateTimeUtil;
    }

    @Test
    public void loadUserActions() throws Exception {
        mUserActionsViewModel.handleLoadUserActionsSuccess(userActions());
        assertEquals(EXPECTED_ADAPTER_ROWS_SIZE, mUserActionsViewModel.mUserActionsAdapter.getItemCount());
    }

    @Test
    public void testFrienjiWallCoachMarkIsVisible() throws Exception {
        mUserActionsViewModel.showCoachmarkIfNecessary();
        assertTrue(mUserActionsViewModel.mShowCoachMark.get());
    }

    @Test
    public void testFrienjiWallCoachMarkIsHidden() throws Exception {
        mUserActionsViewModel.showCoachmarkIfNecessary();
        assertTrue(mUserActionsViewModel.mShowCoachMark.get());

        mUserActionsViewModel.hideCoachMark();

        assertFalse(mUserActionsViewModel.mShowCoachMark.get());
    }

    private List<UserActionRowViewModel> userActions() {
        List<UserActionRowViewModel> activities = new ArrayList<>();

        Avatar avatar = Avatar.builder().name("TEST_AVATAR").type(Avatar.EMOTICON_AVATAR).build();

        UserProfile frienjiProfile = UserProfile.builder().id(1).avatar(avatar).userName("TEST_USER_NAME")
                .coverImage("TEST_COVER_IMAGE").whoYouAre("TEST_WHO_YOU_ARE").build();

        ActivityEntity activityEntity1 = ActivityEntity.builder().createdAt(new Date(System.currentTimeMillis())).id(1).frienjiId(1)
                .postId(1).relatedFrienjiId(1).liked(false).parentId(1).wallOwnerId(1).build();
        Activity activity1 = Activity.builder().activityEntity(activityEntity1).key(Activity.LIKE_FOR_COMMENT_CREATED).owner(frienjiProfile).build();
        activities.add(UserActionRowViewModel.from(mContext, mAvatarReader, mDateTimeUtil, mNavigationHandler, mIntentProvider, activity1));

        ActivityEntity activityEntity2 = ActivityEntity.builder().createdAt(new Date(System.currentTimeMillis())).id(1).frienjiId(1)
                .postId(1).relatedFrienjiId(1).liked(false).parentId(1).wallOwnerId(1).build();
        Activity activity2 = Activity.builder().activityEntity(activityEntity2).key(Activity.POST_AVATAR_TRAIL_COMMENT_CREATED).owner(frienjiProfile).build();
        activities.add(UserActionRowViewModel.from(mContext, mAvatarReader, mDateTimeUtil, mNavigationHandler, mIntentProvider, activity2));

        return activities;
    }

    @Test
    public void testPagination() throws Exception {
        when(mUserActionsViewModel.mDateTimeUtil.getTimestampString(anyObject())).thenReturn("TEST_DATE");
        when(mUserActionsViewModel.mApi.loadUserActions()).thenReturn(Observable.just(prepareFriejiActiviesResponse(20)));

        mUserActionsViewModel.fillUserActions();
        assertEquals(mUserActionsViewModel.mUserActionsAdapter.getItemCount(), 20);

        when(mUserActionsViewModel.mApi.loadUserActions(2)).thenReturn(Observable.just(prepareFriejiActiviesResponse(20)));
        mUserActionsViewModel.fillUserActions(2);

        assertEquals(mUserActionsViewModel.mUserActionsAdapter.getItemCount(), 40);
    }

    private @NonNull FriejiActiviesResponse prepareFriejiActiviesResponse(int numberOfActivities) {
        PagePaginator pagePaginator = PagePaginator.builder().currentPage(1).nextPage(2).prevPage(0).hasNext(true).totalPages(2).build();
        return FriejiActiviesResponse.builder().pagePaginator(pagePaginator).frienjiActivities(prepareActivities(numberOfActivities)).build();
    }

    private @NonNull List<Activity> prepareActivities(int numberOfPosts) {

        List<Activity> activities = new ArrayList<>();

        for (int i = 1; i <= numberOfPosts; i++) {
            activities.add(Activity.builder().key("KEY").owner(prepareTestUserProfileModel(i)).activityEntity(prepareActivityEntity()).build());
        }
        return activities;
    }

    private @NonNull ActivityEntity prepareActivityEntity() {
        return ActivityEntity.builder().liked(false).relatedFrienjiId(1)
                .postId(1).createdAt(new Date(System.currentTimeMillis())).id(1).frienjiId(1).parentId(1).message("TEST").wallOwnerId(1).build();
    }

    private @NonNull UserProfile prepareTestUserProfileModel(int number) {
        return UserProfile.builder().avatar(prepareTestAvatarModel()).userName("TEST_USER_NAME_" + number).id(number).whoYouAre("TEST_WHO_YOU_ARE_" + number).build();
    }

    private @NonNull Avatar prepareTestAvatarModel() {
        return Avatar.builder().type(Avatar.EMOTICON_AVATAR).name("TEST_ANATAR_NAME").build();
    }
}