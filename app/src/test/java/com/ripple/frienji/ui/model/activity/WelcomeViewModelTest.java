package com.ripple.frienji.ui.model.activity;

import android.content.Context;
import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.ui.view.activity.LoginActivity;
import org.junit.Before;
import org.junit.Test;
import javax.inject.Inject;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author kamil ratajczak
 * @since 08.08.2016
 */
public class WelcomeViewModelTest {

    @Inject
    protected WelcomeViewModel mWelcomeViewModel;

    @Inject
    protected Context mContext;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    @Test
    public void launchLoginScreen() throws Exception {
        when(mWelcomeViewModel.mAppSettings.skipStartInfoActivity()).thenReturn(true);
        mWelcomeViewModel.launchNextScreen();
        verify(mWelcomeViewModel.mNavigator).startActivity(LoginActivity.class);
    }
}