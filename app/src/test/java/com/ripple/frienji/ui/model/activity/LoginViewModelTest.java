package com.ripple.frienji.ui.model.activity;

import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.ui.view.activity.AvatarTypeActivity;
import com.ripple.frienji.ui.view.activity.SignInActivity;
import org.junit.Before;
import org.junit.Test;
import javax.inject.Inject;
import static org.mockito.Mockito.verify;

/**
 * @author kamil ratajczak
 * @since 08.08.2016
 */
public class LoginViewModelTest {

    @Inject
    protected LoginViewModel mLoginViewModel;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    @Test
    public void launchLoginScreen() throws Exception {
        mLoginViewModel.login();
        verify(mLoginViewModel.mNavigator).startActivity(SignInActivity.class);
    }

    @Test
    public void launchRegisterScreen() throws Exception {
        mLoginViewModel.register();
        verify(mLoginViewModel.mNavigator).startActivity(AvatarTypeActivity.class);
    }
}