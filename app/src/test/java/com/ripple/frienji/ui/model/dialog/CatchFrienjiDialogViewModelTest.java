package com.ripple.frienji.ui.model.dialog;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.frienji.Frienji;
import com.ripple.frienji.model.response.StatusResponse;
import com.ripple.frienji.ui.view.activity.UserProfileActivity;
import org.junit.Before;
import org.junit.Test;
import javax.inject.Inject;
import static com.google.common.base.Preconditions.checkNotNull;
import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Kamil Ratajczak
 * @since 09.10.16
 */
public class CatchFrienjiDialogViewModelTest {
    private final String TEST_WHO_YOU_ARE = "WHO YOU ARE TEST";
    private final String TEST_USER_NAME = "USER_NAME_TEST";
    private final String TEST_AVATAR_NAME = "EMOTICON_NAME_TEST";
    private final String TEST_COVER_PHOTO = "TEST_COVER_PHOTO";

    @Inject
    public CatchFrienjiDialogViewModel mCatchFrienjiDialogViewModel;

    @Inject
    public Context mContext;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    @Test
    public void loadFrienji() {
        Frienji frienji = prepareTestFrienjiModel();

        mCatchFrienjiDialogViewModel.loadFrienji(frienji);

        assertEquals(TEST_USER_NAME, mCatchFrienjiDialogViewModel.mUserName.get());
        assertEquals(TEST_WHO_YOU_ARE, mCatchFrienjiDialogViewModel.mWhoYouAre.get());
        assertEquals(TEST_AVATAR_NAME, mCatchFrienjiDialogViewModel.mAvatarName.get());
        assertEquals(Avatar.EMOTICON_AVATAR, mCatchFrienjiDialogViewModel.mAvatarType.get());
        assertEquals(TEST_COVER_PHOTO, mCatchFrienjiDialogViewModel.mCoverImage.get());
    }

    @Test
    public void handleCatchFrienjiSuccess() {
        Intent mockIntent = new Intent(mContext, checkNotNull(UserProfileActivity.class));
        mCatchFrienjiDialogViewModel.mFrienji = prepareTestFrienjiModel();
        when(mCatchFrienjiDialogViewModel.mIntentProvider.createUserProfile(mCatchFrienjiDialogViewModel.buildUserProfile()))
                .thenReturn(mockIntent);

        StatusResponse statusResponse = StatusResponse.builder().status(StatusResponse.SUCCESS_STATUS).build();
        mCatchFrienjiDialogViewModel.handleCatchFrienjiSuccess(statusResponse);
        verify(mCatchFrienjiDialogViewModel.mNavigationHandler).startActivity(mockIntent);
    }

    private @NonNull Frienji prepareTestFrienjiModel() {
        return Frienji.builder().avatar(prepareTestAvatarModel()).name(TEST_USER_NAME).id(1).whoYouAre(TEST_WHO_YOU_ARE).coverPhoto(TEST_COVER_PHOTO).build();
    }

    private @NonNull Avatar prepareTestAvatarModel() {
        return Avatar.builder().type(Avatar.EMOTICON_AVATAR).name(TEST_AVATAR_NAME).build();
    }
}