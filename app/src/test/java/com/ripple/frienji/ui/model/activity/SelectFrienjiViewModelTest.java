package com.ripple.frienji.ui.model.activity;

import android.support.annotation.NonNull;
import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.page.PagePaginator;
import com.ripple.frienji.model.response.FrienjiZooResponse;
import com.ripple.frienji.model.user.UserProfile;
import org.junit.Before;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import rx.Observable;
import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Kamil Ratajczak
 * @since 09.10.16
 */
public class SelectFrienjiViewModelTest {
    private final String TEST_ANATAR_NAME = "grinning_face_with_smiling_eyes";
    private final String TEST_USER_NAME = "USER_NAME_TEST";
    private final String TEST_WHO_YOU_ARE = "WHO YOU ARE TEST";

    @Inject
    public SelectFrienjiViewModel mSelectFrienjiViewModel;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    @Test
    public void testPagination() throws Exception {
        FrienjiZooResponse testFrienjiZooResponse = prepareFrienjiZooResponse(20);

        when(mSelectFrienjiViewModel.mApi.loadUserZoo()).thenReturn(Observable.just(testFrienjiZooResponse));

        mSelectFrienjiViewModel.loadUserZoo();
        assertEquals(mSelectFrienjiViewModel.mZooAdapter.getItemCount(), 20);

        when(mSelectFrienjiViewModel.mApi.loadUserZoo(2)).thenReturn(Observable.just(testFrienjiZooResponse));
        mSelectFrienjiViewModel.fillUserZoo(2);

        assertEquals(mSelectFrienjiViewModel.mZooAdapter.getItemCount(), 40);
    }

    private @NonNull FrienjiZooResponse prepareFrienjiZooResponse(int numberOfTestElements) {
        PagePaginator pagePaginator = PagePaginator.builder().currentPage(1).nextPage(2).prevPage(0).hasNext(true).totalPages(2).build();
        return FrienjiZooResponse.builder().frienjiZoos(prepareTestUserProfiles(numberOfTestElements)).pagePaginator(pagePaginator).build();
    }

    private @NonNull List<UserProfile> prepareTestUserProfiles(int number) {
        List<UserProfile> userProfiles = new ArrayList<>();

        for (int i = 1; i <= number; i++) {
            userProfiles.add(prepareTestUserProfileModel(i));
        }

        return userProfiles;
    }

    private @NonNull UserProfile prepareTestUserProfileModel(int number) {
        return UserProfile.builder().avatar(prepareTestAvatarModel()).userName(TEST_USER_NAME + "_" + number).id(number).whoYouAre(TEST_WHO_YOU_ARE + "_" + number).build();
    }

    private @NonNull Avatar prepareTestAvatarModel() {
        return Avatar.builder().type(Avatar.EMOTICON_AVATAR).name(TEST_ANATAR_NAME).build();
    }
}