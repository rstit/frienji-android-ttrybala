package com.ripple.frienji.ui.model.activity;

import com.google.common.base.Optional;
import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.model.auth.SignInBody;
import org.junit.Before;
import org.junit.Test;
import java.util.List;
import javax.inject.Inject;
import rx.observers.TestSubscriber;
import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Created by kamil ratajczak on 21.08.16.
 */
public class ConfirmCodeViewModelTest {
    private final String TEST_COUNTRY_CODE_NUMBER = "+48";
    private final String TEST_PHONE_NUMBER = "791580177";
    private final String TEST_CONFIRMATION_CODE = "1234";

    @Inject
    protected ConfirmCodeViewModel mConfirmCodeViewModel;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    @Test
    public void buildRequestConfirmCodeBody() throws Exception {
        SignInBody signInBody = SignInBody.builder()
                .password(TEST_CONFIRMATION_CODE)
                .countryCode(TEST_COUNTRY_CODE_NUMBER)
                .phoneNumber(TEST_PHONE_NUMBER)
                .deviceType(SignInBody.ANDROID_DEVICE_TYPE)
                .build();

        TestSubscriber<SignInBody> subscriber = new TestSubscriber<>();

        mConfirmCodeViewModel.buildRequestConfirmCodeBody(signInBody).subscribe(subscriber);

        subscriber.assertNoErrors();
        List<SignInBody> data = subscriber.getOnNextEvents();
        assertEquals(1, data.size());

        SignInBody body = data.get(0);

        assertEquals(TEST_COUNTRY_CODE_NUMBER, body.countryCode());
        assertEquals(TEST_PHONE_NUMBER, body.phoneNumber());
        assertEquals(TEST_CONFIRMATION_CODE, body.password());
    }

    @Test(expected = NullPointerException.class)
    public void buildRequestConfirmCodeBody_NullCountryCode() throws Exception {

        SignInBody signInBody = SignInBody.builder()
                .password(TEST_CONFIRMATION_CODE)
                .phoneNumber(TEST_PHONE_NUMBER)
                .deviceType(SignInBody.ANDROID_DEVICE_TYPE)
                .build();

        //noinspection ConstantConditions
        mConfirmCodeViewModel.buildRequestConfirmCodeBody(signInBody);
    }

    @Test(expected = NullPointerException.class)
    public void buildRequestConfirmCodeBody_NullPhoneNumber() throws Exception {
        SignInBody signInBody = SignInBody.builder()
                .password(TEST_CONFIRMATION_CODE)
                .countryCode(TEST_COUNTRY_CODE_NUMBER)
                .deviceType(SignInBody.ANDROID_DEVICE_TYPE)
                .build();

        //noinspection ConstantConditions
        mConfirmCodeViewModel.buildRequestConfirmCodeBody(signInBody);
    }

    @Test(expected = NullPointerException.class)
    public void buildRequestConfirmCodeBody_NullConfirmationCode() throws Exception {
        SignInBody signInBody = SignInBody.builder()
                .countryCode(TEST_COUNTRY_CODE_NUMBER)
                .phoneNumber(TEST_PHONE_NUMBER)
                .deviceType(SignInBody.ANDROID_DEVICE_TYPE)
                .build();

        //noinspection ConstantConditions
        mConfirmCodeViewModel.buildRequestConfirmCodeBody(signInBody);
    }

    @Test
    public void updateSignInBodyWithConfirmCode() {
        when(mConfirmCodeViewModel.mAppSettings.loadDeviceId()).thenReturn(Optional.fromNullable("DEVICE_ID"));
        when(mConfirmCodeViewModel.mAppSettings.loadFcmToken()).thenReturn(Optional.fromNullable("FCM_TOKEN"));

        mConfirmCodeViewModel.mSignInBody = SignInBody.builder()
                .phoneNumber(TEST_PHONE_NUMBER)
                .countryCode(TEST_COUNTRY_CODE_NUMBER)
                .deviceType(SignInBody.ANDROID_DEVICE_TYPE)
                .build();

        mConfirmCodeViewModel.mSecretCode.set(TEST_CONFIRMATION_CODE);

        SignInBody signInBody = mConfirmCodeViewModel.updateSignInBodyWithConfirmCode();

        assertEquals(TEST_COUNTRY_CODE_NUMBER, signInBody.countryCode());
        assertEquals(TEST_PHONE_NUMBER, signInBody.phoneNumber());
        assertEquals(TEST_CONFIRMATION_CODE, signInBody.password());
    }
}
