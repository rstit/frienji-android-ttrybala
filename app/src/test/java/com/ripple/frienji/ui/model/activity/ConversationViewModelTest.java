package com.ripple.frienji.ui.model.activity;

import com.google.common.base.Optional;
import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.user.UserProfile;
import org.junit.Before;
import org.junit.Test;
import javax.inject.Inject;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Created by kamil ratajczak on 21.08.16.
 */
public class ConversationViewModelTest {

    @Inject
    protected ConversationViewModel mConversationViewModel;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    @Test
    public void fillUserData() {
        UserProfile userProfile = UserProfile.builder()
                .id(1)
                .userName("TEST_USER_NAME")
                .whoYouAre("TEST_WHO_YOU_ARE")
                .avatar(Avatar.builder().name("TEST_AVATAR_NAME").type(Avatar.EMOTICON_AVATAR).build()).build();

        when(mConversationViewModel.mAvatarReader.getEmoticonAvatar("TEST_AVATAR_NAME")).thenReturn("TEST_AVATAR_NAME");

        mConversationViewModel.fillUserData(userProfile);
        assertEquals("TEST_USER_NAME", mConversationViewModel.mUserName.get());
    }

    @Test
    public void fillMyOwnProfileUserData() {
        when(mConversationViewModel.mAppSettings.loadUserName()).thenReturn(Optional.fromNullable("MYOWN_TEST_USER_NAME"));
        when(mConversationViewModel.mAppSettings.loadUUserWhoYouAre()).thenReturn(Optional.fromNullable("MYOWN_TEST_WHO_YOU_ARE"));
        when(mConversationViewModel.mAppSettings.loadUserAvatarName()).thenReturn(Optional.fromNullable("MYOWN_TEST_AVATAR_NAME"));
        when(mConversationViewModel.mAppSettings.loadUserAvatarType()).thenReturn(Optional.fromNullable(new Integer(Avatar.EMOTICON_AVATAR)));
        when(mConversationViewModel.mAppSettings.loadUserProfileId()).thenReturn(Optional.fromNullable(new Long(1)));
        when(mConversationViewModel.mAppSettings.loadUserCoverImage()).thenReturn(Optional.fromNullable("MYOWN_TEST_COVER_IMAGE"));

        UserProfile userProfile = UserProfile.builder()
                .id(1)
                .userName("TEST_USER_NAME")
                .whoYouAre("TEST_WHO_YOU_ARE")
                .avatar(Avatar.builder().name("TEST_AVATAR_NAME").type(Avatar.EMOTICON_AVATAR).build()).build();

        when(mConversationViewModel.mAvatarReader.getEmoticonAvatar("TEST_AVATAR_NAME")).thenReturn("TEST_AVATAR_NAME");

        mConversationViewModel.loadUserProfile(userProfile);
        assertEquals("TEST_USER_NAME", mConversationViewModel.mUserName.get());

        UserProfile mMyOwnUserProfile = mConversationViewModel.mMyOwnUserProfile;
        assertEquals("MYOWN_TEST_USER_NAME", mMyOwnUserProfile.userName());
        assertEquals("MYOWN_TEST_WHO_YOU_ARE", mMyOwnUserProfile.whoYouAre());
        assertEquals("MYOWN_TEST_COVER_IMAGE", mMyOwnUserProfile.coverImage());
        assertEquals("MYOWN_TEST_AVATAR_NAME", mMyOwnUserProfile.avatar().name());
        assertEquals(Avatar.EMOTICON_AVATAR, mMyOwnUserProfile.avatar().type());
        assertEquals(1, mMyOwnUserProfile.id());
        assertTrue(mMyOwnUserProfile.isMyOwnProfile());
    }

    @Test
    public void commentsCoachMarkIsVisible() throws Exception {
        mConversationViewModel.showCoachmarkIfNecessary();
        assertTrue(mConversationViewModel.mShowCoachMark.get());
    }

    @Test
    public void commentsCoachMarkIsHidden() throws Exception {
        mConversationViewModel.showCoachmarkIfNecessary();
        assertTrue(mConversationViewModel.mShowCoachMark.get());

        mConversationViewModel.hideCoachMark();

        assertFalse(mConversationViewModel.mShowCoachMark.get());
    }
}