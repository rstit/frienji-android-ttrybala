package com.ripple.frienji.ui.model.activity;

import android.content.Context;
import android.content.Intent;
import com.ripple.frienji.R;
import com.ripple.frienji.di.component.DaggerViewModelTestComponent;
import com.ripple.frienji.ui.view.activity.LoginActivity;
import org.junit.Before;
import org.junit.Test;
import javax.inject.Inject;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author Kamil Ratajczak
 * @since 03.11.16
 */
public class InfoViewModelTest {

    @Inject
    protected Context mContext;

    @Inject
    protected  InfoViewModel mInfoViewModel;

    @Before
    public void setUp() throws Exception {
        DaggerViewModelTestComponent.builder().build().inject(this);
    }

    @Test
    public void logout() {
        Intent intent = new Intent(mContext, LoginActivity.class);
        when(mInfoViewModel.mIntentProvider.createLogIn()).thenReturn(intent);

        mInfoViewModel.logout();

        verify(mInfoViewModel.mAppSettings).clearSettings();
        verify(mInfoViewModel.mNavigationHandler).startActivity(intent);
    }

    @Test
    public void openContactUs() {
        when(mInfoViewModel.mContext.getString(R.string.contact_us_email)).thenReturn("contact@frienji.io");
        when(mInfoViewModel.mContext.getString(R.string.contact_us_subject)).thenReturn("Contact us submission");

        mInfoViewModel.openContactUs();
        verify(mInfoViewModel.mNavigationHandler).openEmailActivity("contact@frienji.io","Contact us submission");
    }

    @Test
    public void openTermsAndConditions() {
        when(mInfoViewModel.mContext.getString(R.string.terms_and_conditions_url)).thenReturn("http://www.frienji.io/terms");

        mInfoViewModel.openTermsAndConditions();
        verify(mInfoViewModel.mNavigationHandler).openUrlLink("http://www.frienji.io/terms");
    }
}
