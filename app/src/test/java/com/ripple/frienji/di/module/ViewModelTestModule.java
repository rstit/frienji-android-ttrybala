package com.ripple.frienji.di.module;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import com.google.common.eventbus.EventBus;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.net.FrienjiApi;
import com.ripple.frienji.settings.AppSettings;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.util.DateTimeUtil;
import com.ripple.frienji.ui.util.validation.PhoneNumberValidator;
import com.ripple.frienji.ui.util.validation.SecretCodeValidator;
import com.ripple.frienji.ui.util.validation.UserNameValidator;
import com.ripple.frienji.util.rx.ObservableTransformers;
import com.ripple.frienji.util.rx.ObservableTransformersMockImpl;
import com.ryanharter.auto.value.gson.AutoValueGsonTypeAdapterFactory;
import javax.inject.Named;
import dagger.Module;
import dagger.Provides;
import static org.mockito.Mockito.mock;

/**
 * @author Marcin Przepiórkowski
 * @since 28.05.2016
 */
@Module
public class ViewModelTestModule {
    public static final String API_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";

    @Provides
    @NonNull Context provideContext() {
        return mock(Context.class);
    }

    @Provides @Named("activityContext")
    @NonNull Context provideActivityContext() {
        return mock(Context.class);
    }

    @Provides
    @NonNull NavigationHandler provideNavigationHandler() {
        return mock(NavigationHandler.class);
    }

    @Provides
    @NonNull Resources provideResources() {
        return mock(Resources.class);
    }

    @Provides
    @NonNull ObservableTransformers provideObservableTransformers() {
        return new ObservableTransformersMockImpl();
    }

    @Provides
    @NonNull AvatarReader provideAvatarReader() {
        return mock(AvatarReader.class);
    }

    @Provides
    @NonNull FrienjiApi provideFrienjiApi() {
        return mock(FrienjiApi.class);
    }

    @Provides
    @NonNull PhoneNumberValidator providePhoneNumberValidator() {
        return new PhoneNumberValidator();
    }

    @Provides
    @NonNull UserNameValidator provideUserNameValidator() {
        return new UserNameValidator();
    }

    @Provides
    @NonNull EventBus provideEventBus() {
        return mock(EventBus.class);
    }

    @Provides
    @NonNull AppSettings provideAppSettings() {
        return mock(AppSettings.class);
    }

    @Provides
    @NonNull IntentProvider provideIntentProvider() {
        return mock(IntentProvider.class);
    }

    @Provides
    @NonNull SecretCodeValidator provideSecretCodeValidator() {
        return new SecretCodeValidator();
    }

    @Provides
    @NonNull Activity provideActivity() {
        return mock(Activity.class);
    }

    @Provides
    @NonNull DateTimeUtil provideDateTimeUtil() {
        return mock(DateTimeUtil.class);
    }

    @Provides
    @NonNull Gson provideGson() {
        return new GsonBuilder()
                .setDateFormat(API_DATE_FORMAT)
                .registerTypeAdapterFactory(new AutoValueGsonTypeAdapterFactory())
                .create();
    }
}