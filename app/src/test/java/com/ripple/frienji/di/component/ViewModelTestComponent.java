package com.ripple.frienji.di.component;

import com.ripple.frienji.di.module.ViewModelTestModule;
import com.ripple.frienji.di.scope.ActivityScope;
import com.ripple.frienji.ui.model.activity.ArViewModelTest;
import com.ripple.frienji.ui.model.activity.ChooseFrienjiModelTest;
import com.ripple.frienji.ui.model.activity.CommentsViewModelTest;
import com.ripple.frienji.ui.model.activity.ConfirmCodeViewModelTest;
import com.ripple.frienji.ui.model.activity.ConversationViewModelTest;
import com.ripple.frienji.ui.model.activity.ChangeFrienjiDataViewModelTest;
import com.ripple.frienji.ui.model.activity.FirstInfoViewModelTest;
import com.ripple.frienji.ui.model.activity.InBoxViewModelTest;
import com.ripple.frienji.ui.model.activity.InfoViewModelTest;
import com.ripple.frienji.ui.model.activity.LoginViewModelTest;
import com.ripple.frienji.ui.model.activity.MySettingsViewModelTest;
import com.ripple.frienji.ui.model.activity.SecondInfoViewModelTest;
import com.ripple.frienji.ui.model.activity.SelectFrienjiViewModelTest;
import com.ripple.frienji.ui.model.activity.SignInViewModelTest;
import com.ripple.frienji.ui.model.activity.SignUpViewModelTest;
import com.ripple.frienji.ui.model.activity.UserActionsViewModelTest;
import com.ripple.frienji.ui.model.activity.UserProfileViewModelTest;
import com.ripple.frienji.ui.model.activity.WelcomeViewModelTest;
import com.ripple.frienji.ui.model.activity.WhoAreYouViewModelTest;
import com.ripple.frienji.ui.model.activity.ZooViewModelTest;
import com.ripple.frienji.ui.model.dialog.CatchFrienjiDialogViewModelTest;
import com.ripple.frienji.ui.model.row.ChatMessageRowViewModelTest;
import com.ripple.frienji.ui.model.row.InBoxRowViewModelTest;
import com.ripple.frienji.ui.model.row.UserActionRowViewModelTest;
import com.ripple.frienji.ui.model.row.UserProfileMessageRowViewModelTest;
import com.ripple.frienji.ui.model.row.ZooRowViewModelTest;
import dagger.Component;

/**
 * @author Marcin Przepiórkowski
 * @since 28.05.2016
 */
@ActivityScope
@Component(modules = {ViewModelTestModule.class})
public interface ViewModelTestComponent {

    void inject(WelcomeViewModelTest welcomeViewModelTest);

    void inject(LoginViewModelTest loginViewModelTest);

    void inject(ChooseFrienjiModelTest chooseFrienjiModelTest);

    void inject(SignInViewModelTest signInViewModelTest);

    void inject(FirstInfoViewModelTest startInfoViewModelTest);

    void inject(WhoAreYouViewModelTest whoAreYouViewModelTest);

    void inject(SignUpViewModelTest signUpViewModelTest);

    void inject(ArViewModelTest arViewModelTest);

    void inject(ConfirmCodeViewModelTest confirmCodeViewModelTest);

    void inject(ConversationViewModelTest conversationViewModelTest);

    void inject(UserProfileViewModelTest userProfileViewModelTest);

    void inject(ZooViewModelTest zooViewModelTest);

    void inject(InBoxRowViewModelTest inBoxRowViewModelTest);

    void inject(SecondInfoViewModelTest secondInfoViewModelTest);

    void inject(MySettingsViewModelTest mySettingsViewModelTest);

    void inject(ChangeFrienjiDataViewModelTest changeFrienjiDataViewModelTest);

    void inject(UserActionsViewModelTest userActionsViewModelTest);

    void inject(CommentsViewModelTest commentsViewModelTest);

    void inject(InBoxViewModelTest inBoxViewModelTest);

    void inject(SelectFrienjiViewModelTest selectFrienjiViewModelTest);

    void inject(CatchFrienjiDialogViewModelTest catchFrienjiDialogViewModelTest);

    void inject(ZooRowViewModelTest zooRowViewModelTest);

    void inject(UserProfileMessageRowViewModelTest userProfileMessageRowViewModelTest);

    void inject(UserActionRowViewModelTest userActionRowViewModelTest);

    void inject(InfoViewModelTest infoViewModelTest);

    void inject(ChatMessageRowViewModelTest chatMessageRowViewModelTest);
}