package com.ripple.frienji.settings;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import com.ripple.frienji.di.DaggerTestAppComponent;
import com.ripple.frienji.di.TestAppModule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import javax.inject.Inject;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * @author Kamil Ratajczak
 * @since 28.09.16
 */
@RunWith(AndroidJUnit4.class)
public class AppSettingsTest {

    @Inject
    protected AppSettings mAppSettings;

    @Before
    public void setUp() {
        DaggerTestAppComponent.builder()
                .testAppModule(new TestAppModule(InstrumentationRegistry.getTargetContext()))
                .build().inject(this);
        mAppSettings.clearSettings();
        mAppSettings.clearDeviceSettings();
    }

    @Test
    public void setSkipStartInfoActivity() throws Exception {
        assertFalse(mAppSettings.skipStartInfoActivity());
        mAppSettings.setSkipStartInfoActivity();
        assertTrue(mAppSettings.skipStartInfoActivity());
    }

    @Test
    public void setUserToken() throws Exception {
        assertFalse(mAppSettings.loadUserToken().isPresent());
        mAppSettings.setUserToken("TEST_TOKEN");
        assertEquals(mAppSettings.loadUserToken().get(), "TEST_TOKEN");
    }

    @Test
    public void setClientId() throws Exception {
        assertFalse(mAppSettings.loadClientId().isPresent());
        mAppSettings.setClientId("TEST_CLIENT");
        assertEquals(mAppSettings.loadClientId().get(), "TEST_CLIENT");
    }

    @Test
    public void setUId() throws Exception {
        assertFalse(mAppSettings.loadUId().isPresent());
        mAppSettings.setUId("TEST_UID");
        assertEquals(mAppSettings.loadUId().get(), "TEST_UID");
    }

    @Test
    public void setUserName() throws Exception {
        assertFalse(mAppSettings.loadUserName().isPresent());
        mAppSettings.setUserName("TEST_USER_NAME");
        assertEquals(mAppSettings.loadUserName().get(), "TEST_USER_NAME");
    }

    @Test
    public void setUserAvatarName() throws Exception {
        assertFalse(mAppSettings.loadUserAvatarName().isPresent());
        mAppSettings.setUserAvatarName("TEST_AVATAR_NAME");
        assertEquals(mAppSettings.loadUserAvatarName().get(), "TEST_AVATAR_NAME");
    }

    @Test
    public void setUserAvatarType() throws Exception {
        assertFalse(mAppSettings.loadUserAvatarType().isPresent());
        mAppSettings.setUserAvatarType(1);
        assertEquals(mAppSettings.loadUserAvatarType().get(), Integer.valueOf(1));
    }

    @Test
    public void setUserCoverImage() throws Exception {
        assertFalse(mAppSettings.loadUserCoverImage().isPresent());
        mAppSettings.setUserCoverImage("TEST_COVER_IMAGE");
        assertEquals(mAppSettings.loadUserCoverImage().get(), "TEST_COVER_IMAGE");
    }

    @Test
    public void setUserProfileId() throws Exception {
        assertFalse(mAppSettings.loadUserProfileId().isPresent());
        mAppSettings.setUserProfileId(1L);
        assertEquals(mAppSettings.loadUserProfileId().get(), Long.valueOf(1));
    }

    @Test
    public void setUserWhoYouAre() throws Exception {
        assertFalse(mAppSettings.loadUUserWhoYouAre().isPresent());
        mAppSettings.setUserWhoYouAre("TEST_WHO_YOU_ARE");
        assertEquals(mAppSettings.loadUUserWhoYouAre().get(), "TEST_WHO_YOU_ARE");
    }

    @Test
    public void shouldSkipArCoachMark() throws Exception {
        assertFalse(mAppSettings.shouldSkipArCoachMark());
        mAppSettings.hideArCoachMark();
        assertTrue(mAppSettings.shouldSkipArCoachMark());
        mAppSettings.showArCoachMark();
        assertFalse(mAppSettings.shouldSkipArCoachMark());
    }

    @Test
    public void shouldSkipMySettingsCoachMark() throws Exception {
        assertFalse(mAppSettings.shouldSkipMySettingsCoachMark());
        mAppSettings.hideMySettingsCoachMark();
        assertTrue(mAppSettings.shouldSkipMySettingsCoachMark());
        mAppSettings.showMySettingsCoachMark();
        assertFalse(mAppSettings.shouldSkipMySettingsCoachMark());
    }

    @Test
    public void shouldSkipUserActionsCoachMark() throws Exception {
        assertFalse(mAppSettings.shouldSkipUserActionsCoachMark());
        mAppSettings.hideUserActionsCoachMark();
        assertTrue(mAppSettings.shouldSkipUserActionsCoachMark());
        mAppSettings.showUserActionsCoachMark();
        assertFalse(mAppSettings.shouldSkipUserActionsCoachMark());
    }

    @Test
    public void shouldSkipUserWallCoachMark() throws Exception {
        assertFalse(mAppSettings.shouldSkipUserWallCoachMark());
        mAppSettings.hideUserWallCoachMark();
        assertTrue(mAppSettings.shouldSkipUserWallCoachMark());
        mAppSettings.showUserWallCoachMark();
        assertFalse(mAppSettings.shouldSkipUserWallCoachMark());
    }

    @Test
    public void shouldSkipFrienjiWallCoachMark() throws Exception {
        assertFalse(mAppSettings.shouldSkipFrienjiWallCoachMark());
        mAppSettings.hideFrienjiWallCoachMark();
        assertTrue(mAppSettings.shouldSkipFrienjiWallCoachMark());
        mAppSettings.showFrienjiWallCoachMark();
        assertFalse(mAppSettings.shouldSkipFrienjiWallCoachMark());
    }

    @Test
    public void shouldSkipZooCoachMark() throws Exception {
        assertFalse(mAppSettings.shouldSkipZooCoachMark());
        mAppSettings.hideZooCoachMark();
        assertTrue(mAppSettings.shouldSkipZooCoachMark());
        mAppSettings.showZooCoachMark();
        assertFalse(mAppSettings.shouldSkipZooCoachMark());
    }

    @Test
    public void setFcmToken() throws Exception {
        assertFalse(mAppSettings.loadFcmToken().isPresent());
        mAppSettings.setFcmToken("TEST_TOKEN");
        assertEquals("TEST_TOKEN", mAppSettings.loadFcmToken().get());
    }

    @Test
    public void logOutNotClearFcmToken() throws Exception {
        assertFalse(mAppSettings.loadFcmToken().isPresent());
        mAppSettings.setFcmToken("TEST_TOKEN");
        assertEquals("TEST_TOKEN", mAppSettings.loadFcmToken().get());
        mAppSettings.clearSettings();
        assertEquals("TEST_TOKEN", mAppSettings.loadFcmToken().get());
    }

    @Test
    public void setDeviceId() throws Exception {
        assertFalse(mAppSettings.loadDeviceId().isPresent());
        mAppSettings.setDeviceId("TEST_DEVICE_ID");
        assertEquals("TEST_DEVICE_ID", mAppSettings.loadDeviceId().get());
    }

    @Test
    public void logOutNotClearDeviceId() throws Exception {
        assertFalse(mAppSettings.loadDeviceId().isPresent());
        mAppSettings.setDeviceId("TEST_DEVICE_ID");
        assertEquals("TEST_DEVICE_ID", mAppSettings.loadDeviceId().get());
        mAppSettings.clearSettings();
        assertEquals("TEST_DEVICE_ID", mAppSettings.loadDeviceId().get());
    }
}
