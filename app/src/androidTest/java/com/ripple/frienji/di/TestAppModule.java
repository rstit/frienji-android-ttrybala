package com.ripple.frienji.di;

import android.content.Context;
import android.support.annotation.NonNull;
import dagger.Module;
import dagger.Provides;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Marcin Przepiórkowski
 * @since 28.05.2016
 */
@Module
public class TestAppModule {
    private final Context mAppContext;

    public TestAppModule(@NonNull Context appContext) {
        mAppContext = checkNotNull(appContext);
    }

    @Provides
    public Context provideAppContext() {
        return mAppContext;
    }
}