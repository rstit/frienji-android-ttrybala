package com.ripple.frienji.di;

import com.ripple.frienji.settings.AppSettingsTest;
import dagger.Component;

/**
 * @author Marcin Przepiórkowski
 * @since 28.05.2016
 */
@Component(modules = TestAppModule.class)
public interface TestAppComponent {
    void inject(AppSettingsTest test);
}