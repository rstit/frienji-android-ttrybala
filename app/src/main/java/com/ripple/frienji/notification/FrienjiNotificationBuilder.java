package com.ripple.frienji.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import com.ripple.frienji.R;
import com.ripple.frienji.model.action.Activity;
import com.ripple.frienji.model.action.ActivityEntity;
import com.ripple.frienji.model.message.ChatMessage;
import com.ripple.frienji.ui.view.activity.ConversationActivity;
import com.ripple.frienji.ui.view.activity.UserActionsActivity;
import javax.inject.Inject;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Kamil Ratajczak
 * @since 05.10.16
 */
public class FrienjiNotificationBuilder {
    private static final int CHAT_MESSAGE_NOTIFICATION_ID = 1;
    private static final int ACTIVITY_NOTIFICATION_ID = 2;

    @Inject
    public Context mContext;

    private NotificationManager mNotificationManager;

    @Inject
    public FrienjiNotificationBuilder(@NonNull Context context) {
        mContext = checkNotNull(context);
        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public void showNewChatMessageNotyfication(@NonNull ChatMessage chatMessage) {
        NotificationCompat.Builder nBuilder = prepareNotificationCompatBuilder(R.string.new_chat_message_notyfication_title,
                R.string.new_chat_message_notyfication_content);
        Intent intent = ConversationActivity.createIntent(chatMessage.author(), mContext);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pIntent = PendingIntent.getActivity(mContext, (int) System.currentTimeMillis(), intent, 0);
        nBuilder.setDefaults(Notification.DEFAULT_ALL);
        nBuilder.setContentIntent(pIntent);
        nBuilder.setAutoCancel(true);

        mNotificationManager.notify(CHAT_MESSAGE_NOTIFICATION_ID, nBuilder.build());
    }

    public void showActivityNotyfication(@NonNull ActivityEntity activity, @Nullable String notificationType, @Nullable String userName) {
        NotificationCompat.Builder nBuilder = prepareActivityNotificationCompatBuilder(checkNotNull(activity), notificationType, userName);

        Intent intent = new Intent(mContext, UserActionsActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(mContext, (int) System.currentTimeMillis(), intent, 0);
        nBuilder.setDefaults(Notification.DEFAULT_ALL);
        nBuilder.setContentIntent(pIntent);
        nBuilder.setAutoCancel(true);

        mNotificationManager.notify(ACTIVITY_NOTIFICATION_ID, nBuilder.build());
    }

    private @NonNull NotificationCompat.Builder prepareActivityNotificationCompatBuilder(@Nullable ActivityEntity activity,
                                                                                         @Nullable String notificationType,
                                                                                         @Nullable String userName) {
        String content = null;

        if (notificationType != null) {
            switch (notificationType) {
                case Activity.POST_CONTENT_COMMENT_ON_YOUR_WALL_CREATED:
                    if (activity != null) {
                        content = mContext.getString(R.string.user_action_comment_on_post_template, userName, activity.message());
                    }
                    break;

                case Activity.POST_CONTENT_COMMENT_CREATED:
                    if (activity != null) {
                        content = mContext.getString(R.string.user_action_comment_on_post_template, userName, activity.message());
                    }
                    break;

                case Activity.LIKE_FOR_COMMENT_CREATED:
                    content = mContext.getString(R.string.user_action_like_on_comment, userName);
                    break;

                case Activity.LIKE_FOR_POST_CREATED:
                    content = mContext.getString(R.string.user_action_like_on_post, userName);
                    break;

                case Activity.RELATIONSHIP_CREATED_SAVED:
                    content = mContext.getString(R.string.user_action_saved, userName);
                    break;

                case Activity.RELATIONSHIP_REMOVED_SAVED:
                    content = mContext.getString(R.string.user_action_rejected, userName);
                    break;

                case Activity.POST_CONTENT_CREATED:
                    if (activity != null) {
                        content = mContext.getString(R.string.user_action_post_on_wall_template, userName, activity.message());
                    }
                    break;
                case Activity.RELATIONSHIP_CREATED_REJECTED:
                    content = mContext.getString(R.string.user_action_rejected, userName);
                    break;
            }
        }

        return new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(mContext.getString(R.string.new_activity_notyfication_title))
                .setContentText(content);
    }

    private NotificationCompat.Builder prepareNotificationCompatBuilder(int titleRes, int contentRes) {
        return new NotificationCompat.Builder(mContext)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(mContext.getString(titleRes))
                .setContentText(mContext.getString(contentRes));
    }
}