package com.ripple.frienji.util.rx;

import android.support.annotation.NonNull;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @author Marcin Przepiórkowski
 * @since 23.03.2016
 */
public class ObservableTransformersImpl implements ObservableTransformers {
    private static final int DEFAULT_RETRY_COUNT = 3;

    public ObservableTransformersImpl() {
    }

    @Override
    public @NonNull <T> Observable.Transformer<T, T> networkOperation() {
        return networkOperation(DEFAULT_RETRY_COUNT);
    }

    @Override
    public @NonNull <T> Observable.Transformer<T, T> networkOperation(int retryCount) {
        return observable -> observable.subscribeOn(Schedulers.io()).unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).retry(retryCount);
    }

    @Override
    public @NonNull <T> Observable.Transformer<T, T> backgroundOperation() {
        return observable -> observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public @NonNull <T> Observable.Transformer<T, T> databaseOperation() {
        return observable -> observable.subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread());
    }
}