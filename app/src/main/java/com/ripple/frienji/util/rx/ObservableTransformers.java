package com.ripple.frienji.util.rx;

import android.support.annotation.NonNull;
import rx.Observable;

/**
 * @author jakub
 * @since 25.05.16.
 */
public interface ObservableTransformers {
    @NonNull
    <T> Observable.Transformer<T, T> networkOperation();

    @NonNull
    <T> Observable.Transformer<T, T> networkOperation(int retryCount);

    @NonNull
    <T> Observable.Transformer<T, T> backgroundOperation();

    @NonNull
    <T> Observable.Transformer<T, T> databaseOperation();
}