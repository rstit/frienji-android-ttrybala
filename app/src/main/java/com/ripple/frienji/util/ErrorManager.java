package com.ripple.frienji.util;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import com.google.gson.Gson;
import com.ripple.frienji.R;
import com.ripple.frienji.model.error.ErrorCodes;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import retrofit2.adapter.rxjava.HttpException;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Kamil Ratajczak
 * @since 24.10.16
 */
public class ErrorManager {
    private static final int UNKNOWN_ERROR = -1;
    private static final int SESSION_BAD_CREDENTIALS = 10001;
    private static final int USERNAME_CANT_BE_BLANK = 40001;
    private static final int USERNAME_IS_TAKEN = 40004;
    private static final int AVATAR_CANT_BE_BLANK = 50001;
    private static final int AVATAR_TYPE_CANT_BE_BLANK = 51001;
    private static final int AVATAR_NAME_CANT_BE_BLANK = 52001;
    private static final int PHONE_NUMBER_CANT_BE_BLANK = 60001;
    private static final int PHONE_NUMBER_IN_USE = 60004;
    private static final int PHONE_NUMBER_INCORRECT = 61001;
    private static final int PHONE_NUMBER_NOT_MOBILE_NUMBER = 61002;
    private static final int COUNTRY_CANT_BE_BLANK = 70001;
    private static final int BIO_CANT_BE_BLANK = 80001;
    private static final int BIO_CANT_TOO_SHORT = 80002;
    private static final int BIO_CANT_TOO_LONG = 80003;

    private final Map<Integer, Integer> mErrors = new HashMap<>();

    @Inject
    public Gson mGson;

    @Inject
    public ErrorManager() {
        initErrors();
    }

    public @StringRes int getSignUpErrorMessageId(@Nullable Throwable error) {
        if (error instanceof HttpException) {

            int errorResInt = getErrorCodeRes((HttpException) error);

            if (errorResInt == UNKNOWN_ERROR) {
                return R.string.error_cannot_sign_up;
            }

            return errorResInt;
        } else {
            return R.string.error_cannot_sign_up;
        }
    }

    public @StringRes int getSignInErrorMessageId(@Nullable Throwable error) {
        if (error instanceof HttpException) {
            HttpException httpException = (HttpException) error;

            if (httpException.code() == HttpURLConnection.HTTP_NOT_FOUND) {
                return R.string.error_user_not_found;
            }

            int errorResInt = getErrorCodeRes(httpException);

            if (errorResInt == UNKNOWN_ERROR) {
                return R.string.error_cannot_sign_in;
            }

            return errorResInt;
        } else {
            return R.string.error_cannot_sign_in;
        }
    }

    protected @Nullable ErrorCodes getErrorCodes(@NonNull String json) {
        try {
            return ErrorCodes.typeAdapter(mGson).fromJson(checkNotNull(json));
        } catch (IOException e) {
            return null;
        }
    }

    private @StringRes int getErrorCodeRes(@NonNull HttpException httpException) {
        try {
            ErrorCodes errorCodes = getErrorCodes(httpException.response().errorBody().string());

            if (errorCodes == null) {
                return UNKNOWN_ERROR;
            }

            List<Integer> codeNumbers = errorCodes.errorCodes();
            for (Integer codeNumber : codeNumbers) {
                if (mErrors.containsKey(codeNumber)) {
                    return mErrors.get(codeNumber);
                }
            }

            return UNKNOWN_ERROR;
        } catch (IOException ioe) {
            return UNKNOWN_ERROR;
        }
    }

    private void initErrors() {
        mErrors.put(PHONE_NUMBER_IN_USE,R.string.error_phone_number_already_in_use);
        mErrors.put(SESSION_BAD_CREDENTIALS,R.string.error_session_bad_credentials);
        mErrors.put(USERNAME_CANT_BE_BLANK,R.string.error_username_cant_be_blank);
        mErrors.put(USERNAME_IS_TAKEN,R.string.error_username_is_taken);
        mErrors.put(AVATAR_CANT_BE_BLANK,R.string.error_avatar_cant_be_blank);
        mErrors.put(AVATAR_TYPE_CANT_BE_BLANK,R.string.error_avatar_type_cant_be_blank);
        mErrors.put(AVATAR_NAME_CANT_BE_BLANK,R.string.error_avatar_name_cant_be_blank);
        mErrors.put(PHONE_NUMBER_CANT_BE_BLANK,R.string.error_phone_number_cant_be_blank);
        mErrors.put(PHONE_NUMBER_INCORRECT,R.string.error_phone_number_incorrect);
        mErrors.put(PHONE_NUMBER_NOT_MOBILE_NUMBER,R.string.error_phone_number_not_mobile_number);
        mErrors.put(COUNTRY_CANT_BE_BLANK,R.string.error_country_code_cant_be_blank);
        mErrors.put(BIO_CANT_BE_BLANK,R.string.error_bio_cant_be_blank);
        mErrors.put(BIO_CANT_TOO_SHORT,R.string.error_bio_too_short);
        mErrors.put(BIO_CANT_TOO_LONG,R.string.error_bio_too_long);
    }
}