package com.ripple.frienji.util;

import android.opengl.GLES20;

/**
 * @author Tomasz Trybala
 * @since 2016-07-21
 */
public class BubbleProgramUtils {
    private static final int NON_INITIAL = -44;
    private static int sBubbleProgram = NON_INITIAL;
    private static int sExplosionProgram = NON_INITIAL;
    private static int sNoBubbleProgram = NON_INITIAL;

    public static int getBubbleProgram() {
        if (sBubbleProgram == NON_INITIAL) {
            sBubbleProgram = GLES20.glCreateProgram();
        }

        return sBubbleProgram;
    }

    public static int getExplosionProgram() {
        if (sExplosionProgram == NON_INITIAL) {
            sExplosionProgram = GLES20.glCreateProgram();
        }

        return sExplosionProgram;
    }

    public static int getNoBubbleProgram() {
        if (sNoBubbleProgram == NON_INITIAL) {
            sNoBubbleProgram = GLES20.glCreateProgram();
        }

        return sNoBubbleProgram;
    }

    public static void clearBubbleProgram() {
        sBubbleProgram = NON_INITIAL;
    }

    public static void clearExplosionProgram() {
        sExplosionProgram = NON_INITIAL;
    }

    public static void clearNoBubbleProgram() {
        sNoBubbleProgram = NON_INITIAL;
    }
}