package com.ripple.frienji.util;

import android.location.Location;
import android.support.annotation.NonNull;
import com.ripple.frienji.model.location.FrienjiLocation;

/**
 * @author Tomasz Trybala
 * @since 2016-08-01
 */
public class LocationUtils {
    @NonNull
    public static float[] getDistanceAndBearing(@NonNull FrienjiLocation start, @NonNull FrienjiLocation end) {
        float[] results = new float[2];
        Location.distanceBetween(start.latitude(), start.longitude(),
                end.latitude(), end.longitude(), results);

        return results;
    }

    public static float getDistance(@NonNull Location start, @NonNull FrienjiLocation end) {
        float[] results = new float[2];
        Location.distanceBetween(start.getLatitude(), start.getLongitude(),
                end.latitude(), end.longitude(), results);

        return results[0] / 1000f;
    }
}
