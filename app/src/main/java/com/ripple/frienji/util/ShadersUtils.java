package com.ripple.frienji.util;

import android.opengl.GLES20;
import android.support.annotation.NonNull;
import android.support.annotation.RawRes;
import com.ripple.frienji.FrienjiApplication;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @author Tomasz Trybala
 * @since 2016-07-28
 */
public class ShadersUtils {
    private static ShadersUtils sInstance;

    private ShadersUtils() {
    }

    public static @NonNull ShadersUtils getInstance() {
        if (sInstance == null) {
            sInstance = new ShadersUtils();
        }

        return sInstance;
    }

    public int loadShader(@RawRes int shaderResource, int shaderType) {
        String shaderCode;
        InputStream inputStream = FrienjiApplication.getAppContext().getResources()
                .openRawResource(shaderResource);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        String nextLine;
        final StringBuilder body = new StringBuilder();

        try {
            while ((nextLine = bufferedReader.readLine()) != null) {
                body.append(nextLine);
                body.append('\n');
            }

            shaderCode = body.toString();
        } catch (IOException e) {
            shaderCode = null;
        }

        int shader = GLES20.glCreateShader(shaderType);

        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);

        return shader;
    }
}