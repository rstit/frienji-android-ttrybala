package com.ripple.frienji.util;

import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;

/**
 * @author Tomasz Trybala
 * @since 2016-07-28
 */
public class BitmapUtils {
    private static int mBitmapSize;

    public static int getBitmapSize() {
        if (mBitmapSize == 0) {
            mBitmapSize = FrienjiApplication.getAppContext().getResources().getDimensionPixelSize(R.dimen.bubble_logo_size);
        }

        return mBitmapSize;
    }
}
