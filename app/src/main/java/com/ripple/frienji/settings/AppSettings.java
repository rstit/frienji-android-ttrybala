package com.ripple.frienji.settings;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import com.google.common.base.Optional;
import javax.inject.Inject;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Marcin Przepiórkowski
 * @since 14.05.2016
 */
public class AppSettings {
    protected static final String SETTINGS_NAME = "appSettings";
    protected static final String DEVICE_SETTINGS_NAME = "appDeviceSettings";
    protected static final String SETTING_SKIP_STAR_INFO_ACTIVITY = "skipStartInfoActivity";
    protected static final String SETTING_USER_TOKEN = "userToken";
    protected static final String SETTING_CLIENT_ID = "clientId";
    protected static final String SETTING_UID = "uId";
    protected static final String SETTING_USER_NAME = "userName";
    protected static final String SETTING_USER_AVATAR_NAME = "userAvatarName";
    protected static final String SETTING_USER_AVATAR_TYPE = "userAvatarType";
    protected static final String SETTING_USER_WHOYOUARE = "userWhoYouAre";
    protected static final String SETTING_USER_PROFILE_ID = "userProfileId";
    protected static final String SETTING_USER_COVER_IMAGE = "userCoverImage";
    protected static final String AR_COACH_MARK_KEY = "arCoachMark";
    protected static final String SETTINGS_COACH_MARK_KEY = "settingsCoachMark";
    protected static final String USER_ACTIONS_COACH_MARK_KEY = "userActionsCoachMark";
    protected static final String MY_WALL_COACH_MARK_KEY = "myWallCoachMark";
    protected static final String FRIENJI_WALL_COACH_MARK_KEY = "frienjiWallCoachMark";
    protected static final String ZOO_COACH_MARK_KEY = "zooCoachMark";
    protected static final String FCM_TOKEN_KEY = "fcmToken";
    protected static final String DEVICE_ID_KEY = "deviceIdKey";
    protected static final String COMMENTS_COACH_MARK_KEY = "commentsCoachMark";
    protected static final String CONVERSATION_COACH_MARK_KEY = "conversationCoachMark";
    protected static final String CONVERSATION_USER_ID = "conversationUserId";

    private static final int INVALID_INTEGER = -1;
    private static final long INVALID_LONG = -1;

    protected Context mContext;

    @Inject
    public AppSettings(@NonNull Context context) {
        mContext = context.getApplicationContext();
    }

    protected void saveToSettings(@NonNull String key, @NonNull String value) {
        checkNotNull(key, "key cannot be null");
        checkNotNull(value, "value cannot be null");

        SharedPreferences settings = mContext.getSharedPreferences(SETTINGS_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = settings.edit();
        preferencesEditor.putString(key, value);
        preferencesEditor.apply();
    }

    protected void saveToDeviceSettings(@NonNull String key, @NonNull String value) {
        checkNotNull(key, "key cannot be null");
        checkNotNull(value, "value cannot be null");

        SharedPreferences settings = mContext.getSharedPreferences(DEVICE_SETTINGS_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = settings.edit();
        preferencesEditor.putString(key, value);
        preferencesEditor.apply();
    }

    protected void saveToSettings(@NonNull String key, int value) {
        checkNotNull(key, "key cannot be null");
        checkNotNull(value, "value cannot be null");

        SharedPreferences settings = mContext.getSharedPreferences(SETTINGS_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = settings.edit();
        preferencesEditor.putInt(key, value);
        preferencesEditor.apply();
    }

    protected void saveToSettings(@NonNull String key, long value) {
        checkNotNull(key, "key cannot be null");
        checkNotNull(value, "value cannot be null");

        SharedPreferences settings = mContext.getSharedPreferences(SETTINGS_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = settings.edit();
        preferencesEditor.putLong(key, value);
        preferencesEditor.apply();
    }

    protected void saveToSettings(@NonNull String key, boolean value) {
        checkNotNull(key, "key cannot be null");

        SharedPreferences settings = mContext.getSharedPreferences(SETTINGS_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = settings.edit();
        preferencesEditor.putBoolean(key, value);

        preferencesEditor.apply();
    }

    protected @NonNull Optional<String> loadFromSettings(@NonNull String key) {
        checkNotNull(key, "key cannot be null");

        SharedPreferences settings = mContext.getSharedPreferences(SETTINGS_NAME, Activity.MODE_PRIVATE);
        String value = settings.getString(key, null);

        return Optional.fromNullable(value);
    }

    protected @NonNull Optional<String> loadFromDeviceSettings(@NonNull String key) {
        checkNotNull(key, "key cannot be null");

        SharedPreferences settings = mContext.getSharedPreferences(DEVICE_SETTINGS_NAME, Activity.MODE_PRIVATE);
        String value = settings.getString(key, null);

        return Optional.fromNullable(value);
    }

    protected boolean loadBoolFromSettings(@NonNull String key) {
        checkNotNull(key, "key cannot be null");

        SharedPreferences settings = mContext.getSharedPreferences(SETTINGS_NAME, Activity.MODE_PRIVATE);
        return settings.getBoolean(key, false);
    }

    protected int loadIntFromSettings(@NonNull String key) {
        checkNotNull(key, "key cannot be null");

        SharedPreferences settings = mContext.getSharedPreferences(SETTINGS_NAME, Activity.MODE_PRIVATE);
        return settings.getInt(key, INVALID_INTEGER);
    }

    protected long loadLongFromSettings(@NonNull String key) {
        checkNotNull(key, "key cannot be null");

        SharedPreferences settings = mContext.getSharedPreferences(SETTINGS_NAME, Activity.MODE_PRIVATE);
        return settings.getLong(key, INVALID_INTEGER);
    }

    protected void clearSetting(@NonNull String key) {
        checkNotNull(key, "key cannot be null");

        SharedPreferences settings = mContext.getSharedPreferences(SETTINGS_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(key);
        editor.apply();
    }

    public void setSkipStartInfoActivity() {
        saveToSettings(SETTING_SKIP_STAR_INFO_ACTIVITY, true);
    }

    public boolean skipStartInfoActivity() {
        return loadBoolFromSettings(SETTING_SKIP_STAR_INFO_ACTIVITY);
    }

    public void setUserToken(@NonNull String userToken) {
        saveToSettings(SETTING_USER_TOKEN, checkNotNull(userToken));
    }

    public @NonNull Optional<String> loadUserToken() {
        return loadFromSettings(SETTING_USER_TOKEN);
    }

    public void setClientId(@NonNull String clientId) {
        saveToSettings(SETTING_CLIENT_ID, checkNotNull(clientId));
    }

    public @NonNull Optional<String> loadClientId() {
        return loadFromSettings(SETTING_CLIENT_ID);
    }

    public void setUId(@NonNull String uId) {
        saveToSettings(SETTING_UID, checkNotNull(uId));
    }

    public @NonNull Optional<String> loadUId() {
        return loadFromSettings(SETTING_UID);
    }

    public void setUserName(@NonNull String userName) {
        saveToSettings(SETTING_USER_NAME, checkNotNull(userName));
    }

    public @NonNull Optional<String> loadUserName() {
        return loadFromSettings(SETTING_USER_NAME);
    }

    public void setUserAvatarName(@NonNull String avatarName) {
        saveToSettings(SETTING_USER_AVATAR_NAME, checkNotNull(avatarName));
    }

    public @NonNull Optional<String> loadUserAvatarName() {
        return loadFromSettings(SETTING_USER_AVATAR_NAME);
    }

    public void setUserAvatarType(int avatarType) {
        saveToSettings(SETTING_USER_AVATAR_TYPE, avatarType);
    }

    public void setUserCoverImage(@NonNull String coverImage) {
        saveToSettings(SETTING_USER_COVER_IMAGE, checkNotNull(coverImage));
    }

    public @NonNull Optional<String> loadUserCoverImage() {
        return loadFromSettings(SETTING_USER_COVER_IMAGE);
    }

    public void setFcmToken(@NonNull String token) {
        saveToDeviceSettings(FCM_TOKEN_KEY, checkNotNull(token));
    }

    public @NonNull Optional<String> loadFcmToken() {
        return loadFromDeviceSettings(FCM_TOKEN_KEY);
    }

    public void setDeviceId(@NonNull String deviceId) {
        saveToDeviceSettings(DEVICE_ID_KEY, checkNotNull(deviceId));
    }

    public @NonNull Optional<String> loadDeviceId() {
        return loadFromDeviceSettings(DEVICE_ID_KEY);
    }

    public @NonNull Optional<Integer> loadUserAvatarType() {
        int value = loadIntFromSettings(SETTING_USER_AVATAR_TYPE);
        return value > INVALID_INTEGER ? Optional.fromNullable(value) : Optional.absent();
    }

    public void setUserProfileId(long userProfileId) {
        saveToSettings(SETTING_USER_PROFILE_ID, userProfileId);
    }

    public @NonNull Optional<Long> loadUserProfileId() {
        long value = loadLongFromSettings(SETTING_USER_PROFILE_ID);
        return value > INVALID_INTEGER ? Optional.fromNullable(value) : Optional.absent();
    }

    public void setConversationUserProfileId(long userProfileId) {
        saveToSettings(CONVERSATION_USER_ID, userProfileId);
    }

    public @NonNull Optional<Long> loadConversationUserProfileId() {
        long value = loadLongFromSettings(CONVERSATION_USER_ID);
        return value > INVALID_LONG ? Optional.fromNullable(value) : Optional.absent();
    }

    public void setUserWhoYouAre(@NonNull String whoYouAre) {
        saveToSettings(SETTING_USER_WHOYOUARE, checkNotNull(whoYouAre));
    }

    public @NonNull Optional<String> loadUUserWhoYouAre() {
        return loadFromSettings(SETTING_USER_WHOYOUARE);
    }

    public boolean shouldSkipArCoachMark() {
        return loadBoolFromSettings(checkNotNull(AR_COACH_MARK_KEY));
    }

    public void hideArCoachMark() {
        saveToSettings(checkNotNull(AR_COACH_MARK_KEY), true);
    }

    public void showArCoachMark() {
        saveToSettings(checkNotNull(AR_COACH_MARK_KEY), false);
    }

    public boolean shouldSkipMySettingsCoachMark() {
        return loadBoolFromSettings(checkNotNull(SETTINGS_COACH_MARK_KEY));
    }

    public void hideMySettingsCoachMark() {
        saveToSettings(SETTINGS_COACH_MARK_KEY, true);
    }

    public void showMySettingsCoachMark() {
        saveToSettings(SETTINGS_COACH_MARK_KEY, false);
    }

    public boolean shouldSkipUserActionsCoachMark() {
        return loadBoolFromSettings(USER_ACTIONS_COACH_MARK_KEY);
    }

    public void hideUserActionsCoachMark() {
        saveToSettings(USER_ACTIONS_COACH_MARK_KEY, true);
    }

    public void showUserActionsCoachMark() {
        saveToSettings(USER_ACTIONS_COACH_MARK_KEY, false);
    }

    public boolean shouldSkipUserWallCoachMark() {
        return loadBoolFromSettings(MY_WALL_COACH_MARK_KEY);
    }

    public void hideUserWallCoachMark() {
        saveToSettings(MY_WALL_COACH_MARK_KEY, true);
    }

    public void showUserWallCoachMark() {
        saveToSettings(MY_WALL_COACH_MARK_KEY, false);
    }

    public void hideCommentsCoachMark() {
        saveToSettings(COMMENTS_COACH_MARK_KEY, true);
    }

    public void showCommentsCoachMark() {
        saveToSettings(COMMENTS_COACH_MARK_KEY, false);
    }

    public void showConversationCoachMark() {
        saveToSettings(CONVERSATION_COACH_MARK_KEY, false);
    }

    public void hideConversationCoachMark() {
        saveToSettings(CONVERSATION_COACH_MARK_KEY, true);
    }

    public boolean shouldSkipFrienjiWallCoachMark() {
        return loadBoolFromSettings(FRIENJI_WALL_COACH_MARK_KEY);
    }

    public boolean shouldSkipCommentsCoachMark() {
        return loadBoolFromSettings(COMMENTS_COACH_MARK_KEY);
    }

    public boolean shouldSkipConversationCoachMark() {
        return loadBoolFromSettings(CONVERSATION_COACH_MARK_KEY);
    }

    public void hideFrienjiWallCoachMark() {
        saveToSettings(FRIENJI_WALL_COACH_MARK_KEY, true);
    }

    public void showFrienjiWallCoachMark() {
        saveToSettings(FRIENJI_WALL_COACH_MARK_KEY, false);
    }

    public boolean shouldSkipZooCoachMark() {
        return loadBoolFromSettings(ZOO_COACH_MARK_KEY);
    }

    public void hideZooCoachMark() {
        saveToSettings(checkNotNull(ZOO_COACH_MARK_KEY), true);
    }

    public void showZooCoachMark() {
        saveToSettings(checkNotNull(ZOO_COACH_MARK_KEY), false);
    }

    public void clearSettings() {
        SharedPreferences settings = mContext.getSharedPreferences(SETTINGS_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.apply();
    }

    public void clearDeviceSettings() {
        SharedPreferences settings = mContext.getSharedPreferences(DEVICE_SETTINGS_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.apply();
    }
}