package com.ripple.frienji.model.response;

import android.support.annotation.NonNull;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by kamil ratajczak on 19.09.16.
 */
@AutoValue
public abstract class StatusResponse {
    public static final String SUCCESS_STATUS = "ok";

    @SerializedName("status")
    public abstract @NonNull String status();

    public static @NonNull TypeAdapter<StatusResponse> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_StatusResponse.GsonTypeAdapter(checkNotNull(gson));
    }

    public static Builder builder() {
        return new AutoValue_StatusResponse.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder status(@NonNull String status);

        public abstract StatusResponse build();
    }
}