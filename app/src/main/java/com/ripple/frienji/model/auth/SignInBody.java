package com.ripple.frienji.model.auth;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 02.08.2016
 */
@AutoValue
public abstract class SignInBody implements Parcelable {
    public static final int ANDROID_DEVICE_TYPE = 1;

    @SerializedName("country_code")
    public abstract @Nullable String countryCode();

    @SerializedName("phone_number")
    public abstract @Nullable String phoneNumber();

    @SerializedName("password")
    public abstract @Nullable String password();

    @SerializedName("device_type")
    public abstract int deviceType();

    @SerializedName("registration_token")
    public abstract @Nullable String registrationToken();

    @SerializedName("hardware_token")
    public abstract @Nullable String hardwareToken();


    public static @NonNull TypeAdapter<SignInBody> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_SignInBody.GsonTypeAdapter(checkNotNull(gson));
    }

    public static Builder builder() {
        return new AutoValue_SignInBody.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder phoneNumber(@Nullable String phoneNumber);

        public abstract Builder password(@Nullable String password);

        public abstract Builder countryCode(@Nullable String countryCode);

        public abstract Builder deviceType(int deviceType);

        public abstract Builder registrationToken(@Nullable String registrationToken);

        public abstract Builder hardwareToken(@Nullable String hardwareToken);

        public abstract SignInBody build();
    }
}