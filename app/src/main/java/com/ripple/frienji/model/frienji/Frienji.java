package com.ripple.frienji.model.frienji;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.location.FrienjiLocation;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by kamil ratajczak on 15.09.16.
 */
@AutoValue
public abstract class Frienji {
    public final static String RELATION_TYPE_SAVED = "saved";
    public final static String RELATION_TYPE_REJECTED = "rejected";

    @SerializedName("id")
    public abstract int id();

    @SerializedName("username")
    public abstract @NonNull String name();

    @SerializedName("avatar")
    public abstract @Nullable Avatar avatar();

    @SerializedName("who_you_are")
    public abstract @Nullable String whoYouAre();

    @SerializedName("cover_image")
    public abstract @Nullable String coverImage();

    @SerializedName("cover_photo_url")
    public abstract @Nullable String coverPhoto();

    @SerializedName("relation_type")
    public abstract @Nullable String relationType();

    @SerializedName("location")
    public abstract @Nullable FrienjiLocation location();

    public static @NonNull TypeAdapter<Frienji> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_Frienji.GsonTypeAdapter(checkNotNull(gson));
    }

    public static @NonNull Builder builder() {
        return new AutoValue_Frienji.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder id(int id);

        public abstract Builder name(@NonNull String name);

        public abstract Builder avatar(@Nullable Avatar avatar);

        public abstract Builder location(@Nullable FrienjiLocation location);

        public abstract Builder whoYouAre(@NonNull String whoYouAre);

        public abstract Builder coverImage(@Nullable String coverImage);

        public abstract Builder relationType(@Nullable String relationType);

        public abstract Builder coverPhoto(@Nullable String coverPhoto);

        public abstract Frienji build();
    }
}