package com.ripple.frienji.model.auth;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 02.08.2016
 */
@AutoValue
public abstract class RequestConfirmCodeResponse implements Parcelable {

    @SerializedName("confirmation_code")
    public abstract @Nullable String confirmCode();

    public static @NonNull TypeAdapter<RequestConfirmCodeResponse> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_RequestConfirmCodeResponse.GsonTypeAdapter(checkNotNull(gson));
    }

    public static Builder builder() {
        return new AutoValue_RequestConfirmCodeResponse.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder confirmCode(@Nullable String confirmCode);

        public abstract RequestConfirmCodeResponse build();
    }
}