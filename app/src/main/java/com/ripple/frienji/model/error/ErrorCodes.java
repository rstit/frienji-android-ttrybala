package com.ripple.frienji.model.error;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import java.util.List;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Kamil Ratajczak
 * @since 20.10.16
 */
@AutoValue
public abstract class ErrorCodes {

    @SerializedName("error_codes")
    public abstract @Nullable List<Integer> errorCodes();

    public static @NonNull TypeAdapter<ErrorCodes> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_ErrorCodes.GsonTypeAdapter(checkNotNull(gson));
    }

    public static @NonNull Builder builder() {
        return new AutoValue_ErrorCodes.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder errorCodes(@Nullable List<Integer> errorCodes);

        public abstract ErrorCodes build();
    }
}