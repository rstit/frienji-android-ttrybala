package com.ripple.frienji.model.auth;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 03.08.2016
 */
@AutoValue
public abstract class SignUpResponse {

    @SerializedName("status")
    public abstract @Nullable String status();

    public static @NonNull TypeAdapter<SignUpResponse> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_SignUpResponse.GsonTypeAdapter(checkNotNull(gson));
    }

    public static Builder builder() {
        return new AutoValue_SignUpResponse.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder status(@Nullable String status);

        public abstract SignUpResponse build();
    }
}