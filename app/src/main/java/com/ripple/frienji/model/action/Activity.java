package com.ripple.frienji.model.action;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.ripple.frienji.model.user.UserProfile;
import java.util.Date;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Kamil Ratajczak
 * @since 26.09.16
 */
@AutoValue
public abstract class Activity {
    public static final String POST_CONTENT_COMMENT_ON_YOUR_WALL_CREATED = "comment_on_your_wall_created";
    public static final String POST_AVATAR_TRAIL_COMMENT_CREATED = "post_avatar_trail_comment_created";
    public static final String POST_CONTENT_COMMENT_CREATED = "comment_created";
    public static final String LIKE_FOR_COMMENT_CREATED = "like_for_comment_created";
    public static final String LIKE_FOR_POST_CREATED = "like_for_post_created";
    public static final String POST_CONTENT_CREATED = "post_created";
    public static final String RELATIONSHIP_CREATED_SAVED = "saved_created";
    public static final String RELATIONSHIP_REMOVED_SAVED = "saved_removed";
    public static final String RELATIONSHIP_CREATED_REJECTED = "rejected_created";
    public static final String RECEIVE_MESSAGE = "receive_message";

    @SerializedName("key")
    public abstract @Nullable String key();

    @SerializedName("entity_type")
    public abstract @Nullable String entityType();

    @SerializedName("owner")
    public abstract @Nullable UserProfile owner();

    @SerializedName("receiver")
    public abstract @Nullable UserProfile receiver();

    @SerializedName("entity")
    public abstract @Nullable ActivityEntity activityEntity();

    @SerializedName("created_at")
    public abstract @Nullable Date createdAt();

    public static @NonNull TypeAdapter<Activity> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_Activity.GsonTypeAdapter(checkNotNull(gson));
    }

    public static @NonNull Builder builder() {
        return new AutoValue_Activity.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder key(@Nullable String key);

        public abstract Builder entityType(@Nullable String entityType);

        public abstract Builder owner(@Nullable UserProfile owner);

        public abstract Builder receiver(@Nullable UserProfile receiver);

        public abstract Builder activityEntity(@Nullable ActivityEntity activityEntity);

        public abstract Builder createdAt(@Nullable Date createdAt);

        public abstract Activity build();
    }
}