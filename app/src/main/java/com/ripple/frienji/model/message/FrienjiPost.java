package com.ripple.frienji.model.message;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.ripple.frienji.model.avatar.AvatarTrail;
import com.ripple.frienji.model.location.FrienjiLocation;
import com.ripple.frienji.model.user.UserProfile;
import java.util.Date;
import java.util.List;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 04.08.2016
 */
@AutoValue
public abstract class FrienjiPost {

    @SerializedName("id")
    public abstract long id();

    @SerializedName("location")
    public abstract @Nullable FrienjiLocation location();

    @SerializedName("message")
    public abstract @Nullable String message();

    @SerializedName("attachment_url")
    public abstract @Nullable String attachmentImageUrl();

    @SerializedName("created_at")
    public abstract @NonNull Date createdAt();

    @SerializedName("is_author")
    public abstract boolean myOwnMessage();

    @SerializedName("number_of_comments")
    public abstract int numberOfComments();

    @SerializedName("number_of_likes")
    public abstract int numberOfLikes();

    @SerializedName("liked")
    public abstract boolean liked();

    @SerializedName("avatar_trail")
    public abstract @Nullable AvatarTrail avatarTrail();

    @SerializedName("author")
    public abstract @Nullable UserProfile author();

    public abstract @Nullable List<FrienjiPost> comments();

    public static @NonNull TypeAdapter<FrienjiPost> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_FrienjiPost.GsonTypeAdapter(checkNotNull(gson));
    }

    public static Builder builder() {
        return new AutoValue_FrienjiPost.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder id(long id);

        public abstract Builder location(@Nullable FrienjiLocation location);

        public abstract Builder message(@Nullable String message);

        public abstract Builder attachmentImageUrl(@Nullable String attachmentImageUrl);

        public abstract Builder createdAt(@NonNull Date createdAt);

        public abstract Builder myOwnMessage(boolean myOwnMessage);

        public abstract Builder numberOfComments(int numberOfComments);

        public abstract Builder numberOfLikes(int numberOfLikes);

        public abstract Builder liked(boolean like);

        public abstract Builder avatarTrail(@Nullable AvatarTrail like);

        public abstract Builder author(@Nullable UserProfile author);

        public abstract Builder comments(@Nullable List<FrienjiPost> comments);

        public abstract FrienjiPost build();
    }
}