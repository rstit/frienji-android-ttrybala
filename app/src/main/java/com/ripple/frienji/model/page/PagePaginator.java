package com.ripple.frienji.model.page;

import android.support.annotation.NonNull;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by kamil ratajczak on 20.09.16.
 */
@AutoValue
public abstract class PagePaginator {

    @SerializedName("current_page")
    public abstract int currentPage();

    @SerializedName("next_page")
    public abstract int nextPage();

    @SerializedName("prev_page")
    public abstract int prevPage();

    @SerializedName("total_pages")
    public abstract int totalPages();

    @SerializedName("has_next")
    public abstract boolean hasNext();

    public static @NonNull TypeAdapter<PagePaginator> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_PagePaginator.GsonTypeAdapter(checkNotNull(gson));
    }

    public static Builder builder() {
        return new AutoValue_PagePaginator.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder currentPage(int messageId);

        public abstract Builder nextPage(int nextPage);

        public abstract Builder prevPage(int prevPage);

        public abstract Builder totalPages(int totalPages);

        public abstract Builder hasNext(boolean hasNext);

        public abstract PagePaginator build();
    }
}