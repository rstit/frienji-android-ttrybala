package com.ripple.frienji.model.auth;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.ripple.frienji.model.user.UserProfile;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 02.08.2016
 */
@AutoValue
public abstract class SignInResponse implements Parcelable {

    @SerializedName("token")
    public abstract @NonNull String token();

    @SerializedName("client")
    public abstract @NonNull String client();

    @SerializedName("uid")
    public abstract @NonNull String uid();

    @SerializedName("data")
    public abstract @NonNull UserProfile userProfile();

    public static @NonNull TypeAdapter<SignInResponse> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_SignInResponse.GsonTypeAdapter(checkNotNull(gson));
    }

    public static Builder builder() {
        return new AutoValue_SignInResponse.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder token(@NonNull String token);

        public abstract Builder client(@NonNull String client);

        public abstract Builder uid(@NonNull String uid);

        public abstract Builder userProfile(@NonNull UserProfile userProfile);

        public abstract SignInResponse build();
    }
}