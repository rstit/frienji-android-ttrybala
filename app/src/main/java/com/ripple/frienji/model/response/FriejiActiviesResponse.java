package com.ripple.frienji.model.response;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.ripple.frienji.model.action.Activity;
import com.ripple.frienji.model.page.PagePaginator;
import java.util.List;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Kamil Ratajczak
 * @since 26.09.16
 */
@AutoValue
public abstract class FriejiActiviesResponse {

    @SerializedName("meta")
    public abstract @Nullable PagePaginator pagePaginator();

    @SerializedName("data")
    public abstract @Nullable List<Activity> frienjiActivities();

    public static @NonNull TypeAdapter<FriejiActiviesResponse> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_FriejiActiviesResponse.GsonTypeAdapter(checkNotNull(gson));
    }

    public static Builder builder() {
        return new AutoValue_FriejiActiviesResponse.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder pagePaginator(@NonNull PagePaginator pagePaginator);

        public abstract Builder frienjiActivities(@NonNull List<Activity> frienjiActivities);

        public abstract FriejiActiviesResponse build();
    }
}