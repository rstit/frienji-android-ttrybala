package com.ripple.frienji.model.auth;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.ripple.frienji.model.avatar.Avatar;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 02.08.2016
 */
@AutoValue
public abstract class SignUpBody implements Parcelable {

    public static Builder builder() {
        return new AutoValue_SignUpBody.Builder();
    }

    @SerializedName("avatar_attributes")
    public abstract @Nullable Avatar avatar();

    @SerializedName("country_code")
    public abstract @Nullable String countryCode();

    @SerializedName("phone_number")
    public abstract @Nullable String phoneNumber();

    @SerializedName("username")
    public abstract @Nullable String userName();

    @SerializedName("who_you_are")
    public abstract @Nullable String whoYouAre();

    public static @NonNull TypeAdapter<SignUpBody> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_SignUpBody.GsonTypeAdapter(checkNotNull(gson));
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder phoneNumber(@Nullable String phoneNumber);

        public abstract Builder countryCode(@Nullable String countryCode);

        public abstract Builder userName(@Nullable String userName);

        public abstract Builder whoYouAre(@Nullable String whoYouAre);

        public abstract Builder avatar(@Nullable Avatar avatar);

        public abstract SignUpBody build();
    }
}