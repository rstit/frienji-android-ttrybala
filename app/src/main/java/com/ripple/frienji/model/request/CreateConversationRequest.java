package com.ripple.frienji.model.request;

import android.support.annotation.NonNull;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by kamil ratajczak on 23.09.16.
 */
@AutoValue
public abstract class CreateConversationRequest {

    @SerializedName("recipient_id")
    public abstract long recipientId();

    public static @NonNull TypeAdapter<CreateConversationRequest> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_CreateConversationRequest.GsonTypeAdapter(checkNotNull(gson));
    }

    public static @NonNull Builder builder() {
        return new AutoValue_CreateConversationRequest.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder recipientId(long recipientId);

        public abstract CreateConversationRequest build();
    }
}