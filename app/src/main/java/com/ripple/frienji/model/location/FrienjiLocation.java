package com.ripple.frienji.model.location;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 17.08.2016
 */
@AutoValue
public abstract class FrienjiLocation implements Parcelable {
    public static final String LOCATION_LATITUDE_SERIALIZED_NAME = "latitude";
    public static final String LOCATION_LONGITUDE_SERIALIZED_NAME = "longitude";

    @SerializedName(LOCATION_LATITUDE_SERIALIZED_NAME)
    public abstract double latitude();

    @SerializedName(LOCATION_LONGITUDE_SERIALIZED_NAME)
    public abstract double longitude();

    public static @NonNull TypeAdapter<FrienjiLocation> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_FrienjiLocation.GsonTypeAdapter(checkNotNull(gson));
    }

    public static @NonNull Builder builder() {
        return new AutoValue_FrienjiLocation.Builder();
    }

    @AutoValue.Builder
    public abstract static class Builder {
        public abstract Builder latitude(double value);

        public abstract Builder longitude(double value);

        public abstract FrienjiLocation build();
    }
}