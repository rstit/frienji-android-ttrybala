package com.ripple.frienji.model.response;

import android.support.annotation.NonNull;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.ripple.frienji.model.message.FrienjiPost;
import com.ripple.frienji.model.page.PagePaginator;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by kamil ratajczak on 21.09.16.
 */
@AutoValue
public abstract class FrienjiPostCommentsResponse {

    @SerializedName("data")
    public abstract @NonNull FrienjiPost post();

    @SerializedName("meta")
    public abstract @NonNull PagePaginator pagePaginator();

    public static @NonNull TypeAdapter<FrienjiPostCommentsResponse> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_FrienjiPostCommentsResponse.GsonTypeAdapter(checkNotNull(gson));
    }

    public static @NonNull Builder builder() {
        return new AutoValue_FrienjiPostCommentsResponse.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder post(@NonNull FrienjiPost post);

        public abstract Builder pagePaginator(@NonNull PagePaginator pagePaginator);

        public abstract FrienjiPostCommentsResponse build();
    }
}