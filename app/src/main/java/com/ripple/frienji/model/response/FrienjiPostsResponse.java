package com.ripple.frienji.model.response;

import android.support.annotation.NonNull;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.ripple.frienji.model.message.FrienjiPost;
import com.ripple.frienji.model.page.PagePaginator;
import java.util.List;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by kamil ratajczak on 20.09.16.
 */
@AutoValue
public abstract class FrienjiPostsResponse {

    @SerializedName("meta")
    public abstract @NonNull PagePaginator pagePaginator();

    @SerializedName("data")
    public abstract @NonNull List<FrienjiPost> frienjiPosts();

    public static @NonNull TypeAdapter<FrienjiPostsResponse> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_FrienjiPostsResponse.GsonTypeAdapter(checkNotNull(gson));
    }

    public static Builder builder() {
        return new AutoValue_FrienjiPostsResponse.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder pagePaginator(@NonNull PagePaginator pagePaginator);

        public abstract Builder frienjiPosts(@NonNull List<FrienjiPost> frienjiPosts);

        public abstract FrienjiPostsResponse build();
    }
}