package com.ripple.frienji.model.message;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.auto.value.AutoValue;
import com.google.common.base.Strings;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.ripple.frienji.model.location.FrienjiLocation;
import java.util.ArrayList;
import java.util.List;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by kamil ratajczak on 21.09.16.
 */
@AutoValue
public abstract class Message {
    private static final String FILE_NAME = "coverImage";
    private static final String BODY_PART_TYPE = "image/*";
    private static final String MESSAGE_IMAGE_SERIALIZED_NAME ="attachment";
    private static final String MESSAGE_SERIALIZED_NAME ="message";
    private static final String CHAT_MESSAGE_SERIALIZED_NAME ="content";
    private static final String LOCATION_SERIALIZED_NAME ="location";

    @SerializedName(MESSAGE_SERIALIZED_NAME)
    public abstract @Nullable String message();

    @SerializedName(MESSAGE_IMAGE_SERIALIZED_NAME)
    public abstract @Nullable byte[] messageImageData();

    @SerializedName(LOCATION_SERIALIZED_NAME)
    public abstract @Nullable FrienjiLocation location();

    public abstract boolean isChatMesssage();

    public static @NonNull TypeAdapter<Message> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_Message.GsonTypeAdapter(checkNotNull(gson));
    }

    public static @NonNull Builder builder() {
        return new AutoValue_Message.Builder();
    }

    public @NonNull List<MultipartBody.Part> buildMultipartBodyParts() {
        List<MultipartBody.Part> multiPartFields = new ArrayList<>();

        if (!Strings.isNullOrEmpty(message()) && isChatMesssage()) {
            multiPartFields.add(MultipartBody.Part.createFormData(CHAT_MESSAGE_SERIALIZED_NAME, message()));
        } else if (!Strings.isNullOrEmpty(message()) && !isChatMesssage()) {
            multiPartFields.add(MultipartBody.Part.createFormData(MESSAGE_SERIALIZED_NAME, message()));
        }

        if (location() != null) {
            multiPartFields.add(MultipartBody.Part.createFormData(LOCATION_SERIALIZED_NAME + "[" +
                    FrienjiLocation.LOCATION_LATITUDE_SERIALIZED_NAME + "]", String.valueOf(location().latitude())));
            multiPartFields.add(MultipartBody.Part.createFormData(LOCATION_SERIALIZED_NAME + "[" +
                    FrienjiLocation.LOCATION_LONGITUDE_SERIALIZED_NAME + "]", String.valueOf(location().longitude())));
        }

        if (messageImageData() != null && messageImageData().length > 0) {
            multiPartFields.add(MultipartBody.Part.createFormData(MESSAGE_IMAGE_SERIALIZED_NAME, FILE_NAME,
                    RequestBody.create(
                            MediaType.parse(BODY_PART_TYPE), messageImageData())));
        }

        return multiPartFields;
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder message(@Nullable String message);

        public abstract Builder messageImageData(@Nullable byte[] messageImageUri);

        public abstract Builder location(@Nullable FrienjiLocation location);

        public abstract Builder isChatMesssage(boolean isChatMesssage);

        public abstract Message build();
    }
}