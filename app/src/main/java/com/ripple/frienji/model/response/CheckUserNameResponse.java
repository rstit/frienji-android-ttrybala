package com.ripple.frienji.model.response;

import android.support.annotation.NonNull;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Kamil Ratajczak
 * @since 20.10.16
 */
@AutoValue
public abstract class CheckUserNameResponse {

    @SerializedName("username_available")
    public abstract boolean usernameAvailable();

    public static @NonNull TypeAdapter<CheckUserNameResponse> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_CheckUserNameResponse.GsonTypeAdapter(checkNotNull(gson));
    }

    public static @NonNull Builder builder() {
        return new AutoValue_CheckUserNameResponse.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder usernameAvailable(boolean usernameAvailable);

        public abstract CheckUserNameResponse build();
    }
}