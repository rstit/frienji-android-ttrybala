package com.ripple.frienji.model.avatar;

import android.support.annotation.NonNull;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by kamil ratajczak on 20.09.16.
 */
@AutoValue
public abstract class AvatarTrail {

    @SerializedName("old_avatar")
    public abstract @NonNull Avatar oldAvatar();

    @SerializedName("new_avatar")
    public abstract @NonNull Avatar newAvatar();

    public static @NonNull TypeAdapter<AvatarTrail> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_AvatarTrail.GsonTypeAdapter(checkNotNull(gson));
    }

    public static Builder builder() {
        return new AutoValue_AvatarTrail.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder oldAvatar(@NonNull Avatar oldAvatar);

        public abstract Builder newAvatar(@NonNull Avatar newAvatar);

        public abstract AvatarTrail build();
    }
}