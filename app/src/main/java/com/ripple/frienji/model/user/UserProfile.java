package com.ripple.frienji.model.user;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.location.FrienjiLocation;
import java.util.ArrayList;
import java.util.List;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 04.08.2016
 */
@AutoValue
public abstract class UserProfile implements Parcelable {
    private static final String FILE_BODY_PART_NAME = "cover_photo";
    private static final String FILE_NAME = "coverImage";
    private static final String BODY_PART_TYPE = "image/*";
    private static final String WHO_YOU_ARE_MULTIPART_NAME = "who_you_are";
    private static final String AVATAR_TYPE_MULTIPART_NAME = "avatar_attributes[avatar_type]";
    private static final String AVATAR_NAME_MULTIPART_NAME = "avatar_attributes[avatar_name]";
    private static final String LOCATION_LAT_MULTIPART_NAME = "location[" + FrienjiLocation.LOCATION_LATITUDE_SERIALIZED_NAME + "]";
    private static final String LOCATION_LON_MULTIPART_NAME = "location[" + FrienjiLocation.LOCATION_LONGITUDE_SERIALIZED_NAME + "]";

    private boolean mMyOwnProfile;

    @SerializedName("id")
    public abstract long id();

    @SerializedName("username")
    public abstract @Nullable String userName();

    @SerializedName("avatar")
    public abstract @Nullable Avatar avatar();

    @SerializedName("who_you_are")
    public abstract @Nullable String whoYouAre();

    @SerializedName("cover_photo_url")
    public abstract @Nullable String coverImage();

    @SerializedName("location")
    public abstract @Nullable FrienjiLocation location();

    @SerializedName("unread_messages_count")
    public abstract @Nullable Integer unreadMessagesCount();

    public abstract @Nullable byte[] coverImageData();

    public static @NonNull TypeAdapter<UserProfile> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_UserProfile.GsonTypeAdapter(checkNotNull(gson));
    }

    public static Builder builder() {
        return new AutoValue_UserProfile.Builder();
    }

    public @NonNull List<MultipartBody.Part> buildMultipartBodyParts() {
        List<MultipartBody.Part> multiPartFields = new ArrayList<>();

        if (whoYouAre() != null) {
            multiPartFields.add(MultipartBody.Part.createFormData(UserProfile.WHO_YOU_ARE_MULTIPART_NAME, whoYouAre()));
        }

        if (avatar() != null) {
            multiPartFields.add(MultipartBody.Part.createFormData(UserProfile.AVATAR_TYPE_MULTIPART_NAME, String.valueOf(avatar().type())));
            multiPartFields.add(MultipartBody.Part.createFormData(UserProfile.AVATAR_NAME_MULTIPART_NAME, avatar().name()));
        }

        if (coverImageData() != null && coverImageData().length > 0) {
            multiPartFields.add(MultipartBody.Part.createFormData(FILE_BODY_PART_NAME, FILE_NAME,
                    RequestBody.create(
                            MediaType.parse(BODY_PART_TYPE), coverImageData())));
        }

        if (location() != null) {
            multiPartFields.add(MultipartBody.Part.createFormData(UserProfile.LOCATION_LAT_MULTIPART_NAME, String.valueOf(location().latitude())));
            multiPartFields.add(MultipartBody.Part.createFormData(UserProfile.LOCATION_LON_MULTIPART_NAME, String.valueOf(location().longitude())));
        }

        return multiPartFields;
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder id(long id);

        public abstract Builder userName(@Nullable String userName);

        public abstract Builder avatar(@Nullable Avatar emoticonName);

        public abstract Builder whoYouAre(@Nullable String whoYouAre);

        public abstract Builder coverImage(@Nullable String coverImage);

        public abstract Builder coverImageData(@Nullable byte[] coverImageData);

        public abstract Builder location(@Nullable FrienjiLocation location);

        public abstract Builder unreadMessagesCount(@Nullable Integer unreadMessagesCount);

        public abstract UserProfile build();
    }

    public boolean isMyOwnProfile() {
        return mMyOwnProfile;
    }

    public void setMyOwnProfile(boolean myOwnProfile) {
        mMyOwnProfile = myOwnProfile;
    }
}