package com.ripple.frienji.model.request;

import android.support.annotation.NonNull;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import java.util.List;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Kamil Ratajczak
 * @since 12.10.16
 */
@AutoValue
public abstract class RejectFrienjiesRequest {

    @SerializedName("ids")
    public abstract @NonNull List<Long> ids();

    public static @NonNull TypeAdapter<RejectFrienjiesRequest> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_RejectFrienjiesRequest.GsonTypeAdapter(checkNotNull(gson));
    }

    public static @NonNull Builder builder() {
        return new AutoValue_RejectFrienjiesRequest.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder ids(@NonNull List<Long> ids);

        public abstract RejectFrienjiesRequest build();
    }
}