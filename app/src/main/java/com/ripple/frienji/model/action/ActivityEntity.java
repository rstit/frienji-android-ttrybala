package com.ripple.frienji.model.action;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.ripple.frienji.model.location.FrienjiLocation;
import com.ripple.frienji.model.user.UserProfile;
import java.util.Date;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Kamil Ratajczak
 * @since 26.09.16
 */
@AutoValue
public abstract class ActivityEntity {

    @SerializedName("id")
    public abstract long id();

    @SerializedName("post_id")
    public abstract long postId();

    @SerializedName("wall_owner_id")
    public abstract long wallOwnerId();

    @SerializedName("parent_id")
    public abstract long parentId();

    @SerializedName("frienji_id")
    public abstract long frienjiId();

    @SerializedName("related_frienji_id")
    public abstract long relatedFrienjiId();

    @SerializedName("location")
    public abstract @Nullable FrienjiLocation location();

    @SerializedName("message")
    public abstract @Nullable String message();

    @SerializedName("relation_type")
    public abstract @Nullable String relationType();

    @SerializedName("created_at")
    public abstract @Nullable Date createdAt();

    @SerializedName("updated_at")
    public abstract @Nullable Date updatedAt();

    @SerializedName("liked")
    public abstract boolean liked();

    @SerializedName("author")
    public abstract @Nullable UserProfile author();

    public static @NonNull TypeAdapter<ActivityEntity> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_ActivityEntity.GsonTypeAdapter(checkNotNull(gson));
    }

    public static @NonNull Builder builder() {
        return new AutoValue_ActivityEntity.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder id(long id);

        public abstract Builder postId(long postId);

        public abstract Builder wallOwnerId(long wallOwnerId);

        public abstract Builder frienjiId(long frienjiId);

        public abstract Builder parentId(long parentId);

        public abstract Builder relatedFrienjiId(long relatedFrienjiId);

        public abstract Builder location(@Nullable FrienjiLocation location);

        public abstract Builder message(@Nullable String message);

        public abstract Builder relationType(@Nullable String relationType);

        public abstract Builder createdAt(@Nullable Date createdAt);

        public abstract Builder updatedAt(@Nullable Date updatedAt);

        public abstract Builder liked(boolean liked);

        public abstract Builder author(@Nullable UserProfile author);

        public abstract ActivityEntity build();
    }
}