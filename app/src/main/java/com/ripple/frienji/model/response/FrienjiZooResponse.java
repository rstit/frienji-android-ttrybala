package com.ripple.frienji.model.response;

import android.support.annotation.NonNull;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.ripple.frienji.model.page.PagePaginator;
import com.ripple.frienji.model.user.UserProfile;
import java.util.List;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by kamil ratajczak on 20.09.16.
 */
@AutoValue
public abstract class FrienjiZooResponse {

    @SerializedName("meta")
    public abstract @NonNull PagePaginator pagePaginator();

    @SerializedName("data")
    public abstract @NonNull List<UserProfile> frienjiZoos();

    public static @NonNull TypeAdapter<FrienjiZooResponse> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_FrienjiZooResponse.GsonTypeAdapter(checkNotNull(gson));
    }

    public static Builder builder() {
        return new AutoValue_FrienjiZooResponse.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder pagePaginator(@NonNull PagePaginator pagePaginator);

        public abstract Builder frienjiZoos(@NonNull List<UserProfile> frienjiZoos);

        public abstract FrienjiZooResponse build();
    }
}