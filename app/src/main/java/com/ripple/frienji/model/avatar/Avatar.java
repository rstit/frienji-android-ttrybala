package com.ripple.frienji.model.avatar;

import android.os.Parcelable;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by kamil ratajczak on 18.08.16.
 */
@AutoValue
public abstract class Avatar implements Parcelable {
    public final static String ERROR_AVATAR_NAME = "crying_face";
    public final static String DEFAULT_AVATAR_NAME = "grinning_face_with_smiling_eyes";

    @IntDef({EMOTICON_AVATAR, IMAGE_AVATAR, GIF_AVATAR})
    public @interface AvatarType {}

    public static final int EMOTICON_AVATAR = 0;
    public static final int IMAGE_AVATAR = 1;
    public static final int GIF_AVATAR = 2;

    @SerializedName("avatar_name")
    public abstract @NonNull String name();

    @SerializedName("avatar_type")
    public abstract @AvatarType int type();

    public static @NonNull TypeAdapter<Avatar> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_Avatar.GsonTypeAdapter(checkNotNull(gson));
    }

    public static Builder builder() {
        return new AutoValue_Avatar.Builder();
    }

    public static @NonNull Avatar createDefaultAvatar() {
        return Avatar.builder().name(DEFAULT_AVATAR_NAME).type(EMOTICON_AVATAR).build();
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder name(@NonNull String name);

        public abstract Builder type(@AvatarType int type);

        public abstract Avatar build();
    }
}