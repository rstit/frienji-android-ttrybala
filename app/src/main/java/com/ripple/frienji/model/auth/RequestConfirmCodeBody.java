package com.ripple.frienji.model.auth;

import android.os.Parcelable;
import android.support.annotation.NonNull;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 02.08.2016
 */
@AutoValue
public abstract class RequestConfirmCodeBody implements Parcelable {

    @SerializedName("country_code")
    public abstract @NonNull String countryCode();

    @SerializedName("phone_number")
    public abstract @NonNull String phoneNumber();

    public static @NonNull TypeAdapter<RequestConfirmCodeBody> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_RequestConfirmCodeBody.GsonTypeAdapter(checkNotNull(gson));
    }

    public static Builder builder() {
        return new AutoValue_RequestConfirmCodeBody.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder phoneNumber(@NonNull String phoneNumber);

        public abstract Builder countryCode(@NonNull String countryCode);

        public abstract RequestConfirmCodeBody build();
    }
}