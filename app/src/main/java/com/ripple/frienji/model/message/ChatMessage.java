package com.ripple.frienji.model.message;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.ripple.frienji.model.frienji.Frienji;
import com.ripple.frienji.model.location.FrienjiLocation;
import com.ripple.frienji.model.user.UserProfile;
import java.util.Date;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 12.08.2016
 */
@AutoValue
public abstract class ChatMessage {

    @SerializedName("id")
    public abstract long id();

    @SerializedName("content")
    public abstract @Nullable String content();

    @SerializedName("attachment_url")
    public abstract @Nullable String attachmentImageUrl();

    @SerializedName("location")
    public abstract @Nullable FrienjiLocation location();

    @SerializedName("is_author")
    public abstract boolean myOwnMessage();

    @SerializedName("read")
    public abstract boolean read();

    @SerializedName("created_at")
    public abstract @NonNull Date createdAt();

    @SerializedName("frienji")
    public abstract @Nullable UserProfile author();


    public static @NonNull TypeAdapter<ChatMessage> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_ChatMessage.GsonTypeAdapter(checkNotNull(gson));
    }

    public static @NonNull Builder builder() {
        return new AutoValue_ChatMessage.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder id(long id);

        public abstract Builder content(@Nullable String content);

        public abstract Builder myOwnMessage(boolean myOwnMessage);

        public abstract Builder read(boolean read);

        public abstract Builder attachmentImageUrl(@Nullable String attachmentImageUrl);

        public abstract Builder location(@Nullable FrienjiLocation location);

        public abstract Builder createdAt(@NonNull Date createdAt);

        public abstract Builder author(@Nullable UserProfile author);

        public abstract ChatMessage build();
    }
}