package com.ripple.frienji.model.response;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.ripple.frienji.model.conversation.Conversation;
import com.ripple.frienji.model.page.PagePaginator;
import java.util.List;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Kamil Ratajczak
 * @since 26.09.16
 */
@AutoValue
public abstract class FrienjiConversationsResponse {

    @SerializedName("data")
    public abstract @Nullable List<Conversation> conversations();

    @SerializedName("meta")
    public abstract @Nullable PagePaginator pagePaginator();

    public static @NonNull TypeAdapter<FrienjiConversationsResponse> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_FrienjiConversationsResponse.GsonTypeAdapter(checkNotNull(gson));
    }

    public static @NonNull Builder builder() {
        return new AutoValue_FrienjiConversationsResponse.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder conversations(@Nullable List<Conversation> conversations);

        public abstract Builder pagePaginator(@Nullable PagePaginator pagePaginator);

        public abstract FrienjiConversationsResponse build();
    }
}