package com.ripple.frienji.model.conversation;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.auto.value.AutoValue;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.ripple.frienji.model.message.ChatMessage;
import com.ripple.frienji.model.user.UserProfile;
import java.util.Date;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by kamil ratajczak on 23.09.16.
 */
@AutoValue
public abstract class Conversation {

    @SerializedName("id")
    public abstract long id();

    @SerializedName("sender_id")
    public abstract long senderId();

    @SerializedName("recipient_id")
    public abstract long recipientId();

    @SerializedName("messages_count")
    public abstract int messagesCount();

    @SerializedName("unread_count")
    public abstract int unreadCount();

    @SerializedName("created_at")
    public abstract @Nullable Date createdAt();

    @SerializedName("last_message")
    public abstract @Nullable ChatMessage lastMessage();

    @SerializedName("receiver")
    public abstract @Nullable UserProfile receiver();

    @SerializedName("updated_at")
    public abstract @Nullable Date updatedAt();

    public static @NonNull TypeAdapter<Conversation> typeAdapter(@NonNull Gson gson) {
        return new AutoValue_Conversation.GsonTypeAdapter(checkNotNull(gson));
    }

    public static @NonNull Builder builder() {
        return new AutoValue_Conversation.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder id(long id);

        public abstract Builder senderId(long senderId);

        public abstract Builder recipientId(long recipientId);

        public abstract Builder messagesCount(int messagesCount);

        public abstract Builder createdAt(@NonNull Date createdAt);

        public abstract Builder updatedAt(@Nullable Date updatedAt);

        public abstract Builder unreadCount(int unreadCount);

        public abstract Builder lastMessage(@Nullable ChatMessage lastMessage);

        public abstract Builder receiver(@Nullable UserProfile receiver);

        public abstract Conversation build();
    }
}