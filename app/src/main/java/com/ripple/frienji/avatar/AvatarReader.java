package com.ripple.frienji.avatar;

import android.support.annotation.NonNull;
import java.io.IOException;
import rx.Observable;

/**
 * @author kamil ratajczak
 * @since 25.08.2016
 */
public interface AvatarReader {

    @NonNull Observable<String> getEmoticonAvatars();

    @NonNull String getEmoticonAvatar(@NonNull String name);

    @NonNull Observable<String> getImageAvatarNames();

    @NonNull String getImageAvatarPath(@NonNull String name);

    @NonNull Observable<String> getGifAvatarNames();

    @NonNull String getGifAvatarPath(@NonNull String name);
}