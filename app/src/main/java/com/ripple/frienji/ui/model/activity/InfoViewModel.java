package com.ripple.frienji.ui.model.activity;

import com.ripple.frienji.R;
import com.ripple.frienji.ui.model.BaseViewModel;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import javax.inject.Inject;

/**
 * @author Kamil Ratajczak
 * @since 03.11.16
 */
public class InfoViewModel extends BaseViewModel {

    @Inject
    public NavigationHandler mNavigationHandler;

    @Inject
    public InfoViewModel() {
    }

    public void openContactUs() {
        mNavigationHandler.openEmailActivity(mContext.getString(R.string.contact_us_email), mContext.getString(R.string.contact_us_subject));
    }

    public void openTermsAndConditions() {
        mNavigationHandler.openUrlLink(mContext.getString(R.string.terms_and_conditions_url));
    }

    public void askAboutLogout() {
        mEventBus.post(new LogoutEvent());
    }

    public void logout() {
        mAppSettings.clearSettings();
        mNavigationHandler.startActivity(mIntentProvider.createLogIn());
    }

    public class LogoutEvent {
    }
}