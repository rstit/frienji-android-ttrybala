package com.ripple.frienji.ui.view.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import com.google.common.eventbus.EventBus;
import com.ripple.frienji.R;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Kamil Ratajczak
 * @since 29.09.16
 */
public abstract class ConfirmDialogFragment extends BaseDialogFragment {

    public ConfirmDialogFragment() {
    }

    protected ConfirmDialogFragment(@NonNull EventBus eventBus){
        mEventBus = checkNotNull(eventBus);
    }

    protected abstract @NonNull String getTitle();

    protected abstract @NonNull String getConfirmMessage();

    protected abstract @NonNull ConfirmEvent getConfirmEvent();

    @Override
    public @NonNull Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getContext())
                .setTitle(getTitle())
                .setMessage(getConfirmMessage())
                .setPositiveButton(R.string.yes, (dialog, which) -> {
                    mEventBus.post(getConfirmEvent());
                })
                .setNegativeButton(R.string.no, (dialog, which) -> {
                })
                .create();
    }

    public class ConfirmEvent {
    }
}