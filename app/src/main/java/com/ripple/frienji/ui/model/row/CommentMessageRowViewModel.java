package com.ripple.frienji.ui.model.row;

import android.content.Context;
import android.support.annotation.NonNull;
import com.google.android.gms.maps.model.LatLng;
import com.google.common.base.Strings;
import com.google.common.eventbus.EventBus;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.model.message.FrienjiPost;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.net.FrienjiApi;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.util.DateTimeUtil;
import com.ripple.frienji.util.rx.ObservableTransformers;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 12.08.2016
 */
public class CommentMessageRowViewModel extends MessageRowViewModel {

    protected IntentProvider mIntentProvider;
    protected UserProfile mAuthor;

    private CommentMessageRowViewModel(@NonNull Context context, @NonNull NavigationHandler navigationHandler, @NonNull EventBus eventBus,
                                       @NonNull AvatarReader avatarReader, @NonNull FrienjiApi frienjiApi,
                                       @NonNull ObservableTransformers observableTransformers,
                                       @NonNull IntentProvider intentProvider, boolean showLikeAndComment) {
        super(checkNotNull(context), checkNotNull(navigationHandler), checkNotNull(eventBus), checkNotNull(avatarReader), checkNotNull(frienjiApi),
                checkNotNull(observableTransformers), showLikeAndComment);
        mIntentProvider = checkNotNull(intentProvider);
    }

    public static @NonNull MessageRowViewModel from(@NonNull Context context, @NonNull AvatarReader avatarReader, @NonNull FrienjiApi frienjiApi,
                                                    @NonNull NavigationHandler navigationHandler, @NonNull EventBus eventBus,
                                                    @NonNull ObservableTransformers observableTransformers, @NonNull IntentProvider intentProvider,
                                                    @NonNull DateTimeUtil dateTimeUtil, @NonNull FrienjiPost frienjiPost) {
        CommentMessageRowViewModel commentMessageRowViewModel = new CommentMessageRowViewModel(checkNotNull(context),
                checkNotNull(navigationHandler), checkNotNull(eventBus), checkNotNull(avatarReader),
                checkNotNull(frienjiApi), checkNotNull(observableTransformers), checkNotNull(intentProvider), true);

        commentMessageRowViewModel.mId = frienjiPost.id();
        commentMessageRowViewModel.mMessage.set(frienjiPost.message());

        if (frienjiPost.author() != null) {
            commentMessageRowViewModel.mAuthor = frienjiPost.author();
            commentMessageRowViewModel.mUserName.set(frienjiPost.author().userName());
        }

        if (frienjiPost.createdAt() != null) {
            commentMessageRowViewModel.mCreationTime.set(dateTimeUtil.getTimestampString(frienjiPost.createdAt()));
        }

        if (checkImagePath(frienjiPost.attachmentImageUrl())) {
            commentMessageRowViewModel.mImageName.set(frienjiPost.attachmentImageUrl());
        }

        if (frienjiPost.author() != null && frienjiPost.author().avatar() != null) {
            commentMessageRowViewModel.mAvatarName.set(frienjiPost.author().avatar().name());
            commentMessageRowViewModel.mAvatarType.set(frienjiPost.author().avatar().type());
        }

        if (frienjiPost.location() != null) {
            LatLng latLng = new LatLng(frienjiPost.location().latitude(), frienjiPost.location().longitude());
            commentMessageRowViewModel.mLatLongPosition.set(latLng);
        }

        commentMessageRowViewModel.mHasLike.set(frienjiPost.liked());

        if (frienjiPost.numberOfLikes() > 0) {
            commentMessageRowViewModel.mNumberOfLikes.set(String.valueOf(frienjiPost.numberOfLikes()));
        }

        commentMessageRowViewModel.mMyOwnMessage = frienjiPost.myOwnMessage();

        return commentMessageRowViewModel;
    }

    @Override
    public void showComments() {
    }

    @Override
    public void createImage() {
        if (!Strings.isNullOrEmpty(mImageName.get())) {
            mNavigationHandler.startActivity(createImageIntent(mImageName.get()));
        }
    }

    @Override
    public void showUser() {
        if (mMyOwnMessage) {
            mNavigationHandler.startActivity(mIntentProvider.createMyOwnUserProfile());
        } else {
            mNavigationHandler.startActivity(mIntentProvider.createUserProfile(mAuthor));
        }
    }
}