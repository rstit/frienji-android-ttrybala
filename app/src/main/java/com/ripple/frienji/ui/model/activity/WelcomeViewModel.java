package com.ripple.frienji.ui.model.activity;

import com.ripple.frienji.settings.AppSettings;
import com.ripple.frienji.ui.model.BaseViewModel;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.view.activity.ArActivity;
import com.ripple.frienji.ui.view.activity.LoginActivity;
import javax.inject.Inject;

/**
 * @author kamil ratajczak
 * @since 27.07.2016
 */
public class WelcomeViewModel extends BaseViewModel {

    @Inject
    public AppSettings mAppSettings;

    @Inject
    public NavigationHandler mNavigator;

    @Inject
    public WelcomeViewModel() {}

    public boolean shouldSkipSignIn() {
        return mAppSettings.loadUserToken().isPresent();
    }

    public void launchArScreen() {
        mNavigator.startActivity(ArActivity.class);
    }

    public void launchNextScreen() {
        if (mAppSettings.skipStartInfoActivity()) {
            launchLoginScreen();
        } else {
            launchFirstIntroductionScreen();
        }
    }

    private void launchLoginScreen() {
        mNavigator.startActivity(LoginActivity.class);
    }

    private void launchFirstIntroductionScreen() {
        mNavigator.startActivity(mIntentProvider.createFirstIntroduction());
    }
}