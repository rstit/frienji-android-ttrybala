package com.ripple.frienji.ui.view.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.DialogModule;
import javax.inject.Inject;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Marcin Przepiórkowski
 * @since 12.05.2015
 */
public class ChoosePhotoDialogFragment extends BaseDialogFragment {
    public static class ChoosePhotoEvent {

        private final ChoosePhotoEventType mActionType;

        public ChoosePhotoEvent(@NonNull ChoosePhotoEventType actionType) {
            mActionType = checkNotNull(actionType);
        }

        public boolean chooseFromCamera() {
            return mActionType == ChoosePhotoEventType.FROM_CAMERA;
        }

        public boolean chooseFromGallery() {
            return mActionType == ChoosePhotoEventType.FROM_GALLERY;
        }

        public boolean removeImage() {
            return mActionType == ChoosePhotoEventType.REMOVE_IMAGE;
        }

        public enum ChoosePhotoEventType {
            FROM_GALLERY, FROM_CAMERA, REMOVE_IMAGE
        }
    }

    @Inject
    public ChoosePhotoDialogFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrienjiApplication app = (FrienjiApplication) getActivity().getApplication();

        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new DialogModule(getActivity())).inject(this);
    }

    @Override
    public @NonNull Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.choose_photo_title))
                .setPositiveButton(R.string.from_camera, (dialog, which) -> {
                    mEventBus.post(new ChoosePhotoEvent(ChoosePhotoEvent.ChoosePhotoEventType.FROM_CAMERA));
                })
                .setNegativeButton(R.string.from_gallery, (dialog, which) -> {
                    mEventBus.post(new ChoosePhotoEvent(ChoosePhotoEvent.ChoosePhotoEventType.FROM_GALLERY));
                })
                .setNeutralButton(R.string.remove_image,  (dialog, which) -> {
                    mEventBus.post(new ChoosePhotoEvent(ChoosePhotoEvent.ChoosePhotoEventType.REMOVE_IMAGE));
                })
                .create();
    }
}