package com.ripple.frienji.ui.model.activity;

import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.ui.model.BaseViewModel;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import javax.inject.Inject;

/**
 * @author kamil ratajczak
 * @since 25.08.2016
 */
public class AvatarTypeViewModel extends BaseViewModel {

    @Inject
    public NavigationHandler mNavigator;

    @Inject
    public AvatarTypeViewModel() {
    }

    public void choseEmoticonAvatar() {
        if (mNavigator.isStartedForResult()) {
            mNavigator.startActivityForResult(mIntentProvider.createChoseFrienji(Avatar.EMOTICON_AVATAR), mNavigator.getRequestCode());
        } else {
            mNavigator.startActivity(mIntentProvider.createChoseFrienji(Avatar.EMOTICON_AVATAR));
        }
    }

    public void choseImageAvatar() {
        if (mNavigator.isStartedForResult()) {
            mNavigator.startActivityForResult(mIntentProvider.createChoseFrienji(Avatar.IMAGE_AVATAR), mNavigator.getRequestCode());
        } else {
            mNavigator.startActivity(mIntentProvider.createChoseFrienji(Avatar.IMAGE_AVATAR));
        }
    }

    public void choseGifAvatar() {
        if (mNavigator.isStartedForResult()) {
            mNavigator.startActivityForResult(mIntentProvider.createChoseFrienji(Avatar.GIF_AVATAR), mNavigator.getRequestCode());
        } else {
            mNavigator.startActivity(mIntentProvider.createChoseFrienji(Avatar.GIF_AVATAR));
        }
    }
}