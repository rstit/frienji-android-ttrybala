package com.ripple.frienji.ui.view.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.google.common.eventbus.Subscribe;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.ActivityInfoBinding;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.ViewModelModule;
import com.ripple.frienji.di.module.ViewModule;
import com.ripple.frienji.ui.model.activity.InfoViewModel;
import com.ripple.frienji.ui.view.dialog.LogOutConfirmDialogFragment;
import javax.inject.Inject;

public class InfoActivity extends BaseActivity {

    @Inject
    protected InfoViewModel mModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrienjiApplication app = (FrienjiApplication) getApplication();
        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new ViewModule(this)).plus(new ViewModelModule()).inject(this);

        ActivityInfoBinding mBinding = DataBindingUtil.setContentView(this, R.layout.activity_info);
        mBinding.setModel(mModel);
    }

    @Subscribe
    public void onLogoutEvent(@NonNull InfoViewModel.LogoutEvent event) {
        LogOutConfirmDialogFragment.createInstance().show(getSupportFragmentManager(), null);
    }

    @Subscribe
    public void onLogoutConfirmEvent(@NonNull LogOutConfirmDialogFragment.ConfirmEvent event) {
        mModel.logout();
    }
}