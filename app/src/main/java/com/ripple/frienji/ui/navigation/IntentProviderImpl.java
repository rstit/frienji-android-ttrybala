package com.ripple.frienji.ui.navigation;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import com.ripple.frienji.model.auth.SignInBody;
import com.ripple.frienji.model.auth.SignUpBody;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.ui.view.activity.ChangeFrienjiDataActivity;
import com.ripple.frienji.ui.view.activity.ChooseFrienjiActivity;
import com.ripple.frienji.ui.view.activity.CommentsActivity;
import com.ripple.frienji.ui.view.activity.ConfirmCodeActivity;
import com.ripple.frienji.ui.view.activity.ConversationActivity;
import com.ripple.frienji.ui.view.activity.FirstInfoActivity;
import com.ripple.frienji.ui.view.activity.LoginActivity;
import com.ripple.frienji.ui.view.activity.SecondInfoActivity;
import com.ripple.frienji.ui.view.activity.SignUpActivity;
import com.ripple.frienji.ui.view.activity.UserProfileActivity;
import com.ripple.frienji.ui.view.activity.WhoAreYouActivity;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 14.08.2016
 */
public class IntentProviderImpl implements IntentProvider {

    protected Context mContext;

    public IntentProviderImpl(@NonNull Context context) {
        mContext = checkNotNull(context);
    }

    @Override
    public @NonNull Intent createWhoAreYou(@NonNull Avatar avatar) {
        return WhoAreYouActivity.createIntent(checkNotNull(avatar), mContext);
    }

    @Override
    public @NonNull Intent createConfirmCode(@NonNull SignInBody signInBody, boolean isRegisterConfirm) {
        return ConfirmCodeActivity.createIntent(checkNotNull(signInBody), isRegisterConfirm, mContext);
    }

    @Override
    public @NonNull Intent createSignUp(@NonNull SignUpBody signUpBody) {
        return SignUpActivity.createIntent(checkNotNull(signUpBody), mContext);
    }

    @Override
    public @NonNull Intent createFirstIntroduction() {
        return FirstInfoActivity.createIntent(mContext);
    }

    @Override
    public @NonNull Intent createSecondIntroduction() {
        return SecondInfoActivity.createIntent(mContext);
    }

    @Override
    public @NonNull Intent createChoseFrienji(int avatarType) {
        return ChooseFrienjiActivity.createIntent(avatarType, mContext);
    }

    @Override
    public @NonNull Intent createLogIn() {
        return LoginActivity.createIntent(mContext);
    }

    @Override
    public @NonNull Intent createUserProfile(@NonNull UserProfile userProfile) {
        return UserProfileActivity.createIntent(checkNotNull(userProfile), mContext);
    }

    @Override
    public @NonNull Intent createConversation(@NonNull UserProfile userProfile) {
        return ConversationActivity.createIntent(checkNotNull(userProfile), mContext);
    }

    @Override
    public @NonNull Intent createMyOwnUserProfile() {
        return UserProfileActivity.createIntent(mContext);
    }

    @Override
    public @NonNull Intent createComents(long frienjiId, long postId) {
        return CommentsActivity.createIntent(frienjiId, postId, mContext);
    }

    @Override
    public @NonNull Intent createChangeAvatar() {
        return ChangeFrienjiDataActivity.createIntent(ChangeFrienjiDataActivity.CHANGE_AVATAR, mContext);
    }

    @Override
    public @NonNull Intent createChangeCoverImage() {
        return ChangeFrienjiDataActivity.createIntent(ChangeFrienjiDataActivity.CHANGE_COVER_IMAGE, mContext);
    }

    @Override
    public @NonNull Intent createChangeBio() {
        return ChangeFrienjiDataActivity.createIntent(ChangeFrienjiDataActivity.CHANGE_BIO_TEXT, mContext);
    }
}