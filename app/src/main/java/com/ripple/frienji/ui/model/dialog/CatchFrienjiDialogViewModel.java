package com.ripple.frienji.ui.model.dialog;

import android.databinding.ObservableBoolean;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.common.base.Strings;
import com.ripple.frienji.R;
import com.ripple.frienji.model.frienji.Frienji;
import com.ripple.frienji.model.response.StatusResponse;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.net.FrienjiApi;
import com.ripple.frienji.ui.binding.ObservableString;
import com.ripple.frienji.ui.model.BaseViewModel;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.view.activity.BaseActivity;
import com.ripple.frienji.ui.view.dialog.CatchFrienjiDialogFragment;
import javax.inject.Inject;

/**
 * Created by kamil ratajczak on 15.09.16.
 */
public class CatchFrienjiDialogViewModel extends BaseViewModel {
    public final ObservableString mUserName = new ObservableString();
    public final ObservableString mWhoYouAre = new ObservableString();
    public final ObservableString mAvatarName = new ObservableString();
    public final ObservableInt mAvatarType = new ObservableInt();
    public final ObservableString mCoverImage = new ObservableString();

    public final ObservableBoolean mActionRejecting = new ObservableBoolean();
    public final ObservableBoolean mActionKeeping = new ObservableBoolean();

    @Inject
    protected FrienjiApi mFrienjiApi;

    @Inject
    protected NavigationHandler mNavigationHandler;

    @Inject
    protected IntentProvider mIntentProvider;

    protected Frienji mFrienji;

    @Inject
    public CatchFrienjiDialogViewModel() {
    }

    public void setRecjectAction() {
        mActionRejecting.set(true);
        mActionKeeping.set(false);
    }

    public void setKeepAction() {
        mActionRejecting.set(false);
        mActionKeeping.set(true);
    }

    public void cleanAction() {
        mActionRejecting.set(false);
        mActionKeeping.set(false);
    }

    public void loadFrienji(@NonNull Frienji frienji) {
        mFrienji = frienji;

        mUserName.set(frienji.name());
        mWhoYouAre.set(frienji.whoYouAre());
        mAvatarName.set(frienji.avatar().name());
        mAvatarType.set(frienji.avatar().type());

        if (!Strings.isNullOrEmpty(frienji.coverPhoto())) {
            mCoverImage.set(frienji.coverPhoto());
        }
    }

    public void catchFrienji() {
        subscribe(mApi.catchFrienji(mFrienji.id())
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleCatchFrienjiSuccess, this::handleCatchFrienjiError));
    }

    public void rejectFrienji() {
        subscribe(mApi.rejectFrienji(mFrienji.id())
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleRejectFrienjiSuccess, this::handleRejectFrienjiError));
    }

    protected void handleCatchFrienjiSuccess(@NonNull StatusResponse statusResponse) {
        mNavigationHandler.startActivity(mIntentProvider.createUserProfile(buildUserProfile()));
        closeDialog(CatchFrienjiDialogFragment.CatchFrienjiEvent.CatchFrienjiEventType.CATCHED);
    }

    public void handleCatchFrienjiError(@Nullable Throwable throwable) {
        handleApiError(throwable, R.string.error_cannot_catch_frienji);
        closeDialog(CatchFrienjiDialogFragment.CatchFrienjiEvent.CatchFrienjiEventType.ERROR);
    }

    private void handleRejectFrienjiSuccess(@NonNull StatusResponse statusResponse) {
        mEventBus.post(new BaseActivity.ShowSnackbarEvent(R.string.reject_frienji_success));
        closeDialog(CatchFrienjiDialogFragment.CatchFrienjiEvent.CatchFrienjiEventType.REJECTED);
    }

    public void handleRejectFrienjiError(@Nullable Throwable throwable) {
        handleApiError(throwable, R.string.error_cannot_reject_frienji);
        closeDialog(CatchFrienjiDialogFragment.CatchFrienjiEvent.CatchFrienjiEventType.ERROR);
    }

    private void closeDialog(CatchFrienjiDialogFragment.CatchFrienjiEvent.CatchFrienjiEventType type) {
        mEventBus.post(new CatchFrienjiDialogFragment.CatchFrienjiEvent(type));
    }

    protected @NonNull UserProfile buildUserProfile() {
        return UserProfile.builder().id(mFrienji.id())
                .userName(mFrienji.name())
                .avatar(mFrienji.avatar())
                .whoYouAre(mFrienji.whoYouAre())
                .coverImage(mFrienji.coverPhoto()).build();
    }
}