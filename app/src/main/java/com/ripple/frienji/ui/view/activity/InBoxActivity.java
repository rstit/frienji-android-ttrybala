package com.ripple.frienji.ui.view.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.ActivityInBoxBinding;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.ViewModelModule;
import com.ripple.frienji.di.module.ViewModule;
import com.ripple.frienji.ui.model.activity.InBoxViewModel;
import javax.inject.Inject;

/**
 * @author kamil ratajczak
 * @since 11.08.2016
 */
public class InBoxActivity extends BaseActivity {

    @Inject
    protected InBoxViewModel mModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrienjiApplication app = (FrienjiApplication) getApplication();

        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new ViewModule(this)).plus(new ViewModelModule()).inject(this);

        ActivityInBoxBinding mBinding = DataBindingUtil.setContentView(this, R.layout.activity_in_box);
        mBinding.setModel(mModel);
    }

    @Override
    public void onStart() {
        super.onStart();
        mModel.loadUserBoxMessages();
    }
}