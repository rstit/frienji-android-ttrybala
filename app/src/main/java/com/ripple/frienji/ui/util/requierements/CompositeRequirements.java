package com.ripple.frienji.ui.util.requierements;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Tomasz Trybala
 * @since 2016-08-04
 */
public class CompositeRequirements implements Requirements {
    private final Map<Integer, InstrumentationStateRequirement> mRequirements = new HashMap<>();

    @Override
    public void add(int requestCode, @NonNull InstrumentationStateRequirement requirement) {
        mRequirements.put(requestCode, checkNotNull(requirement));
    }

    @Override
    public void setIsDone(int requestCode, boolean isDone) {
        InstrumentationStateRequirement requirement = mRequirements.get(requestCode);
        if (requirement != null) {
            if (isDone) {
                requirement.setAsDone();
            } else {
                requirement.undo();
            }
        }
    }

    @Override
    public void setAsImpossible(int requestCode) {
        InstrumentationStateRequirement requirement = mRequirements.get(requestCode);
        if (requirement != null) {
            requirement.setAsImpossibleToDo();
        }
    }

    @Override
    public void clear() {
        mRequirements.clear();
    }

    @Override
    public InstrumentationStateRequirement next() {
        List<Integer> keySet = new ArrayList<>(mRequirements.keySet());
        Collections.sort(keySet);
        for (Integer position : keySet) {
            if (!mRequirements.get(position).isDone()) {
                return mRequirements.get(position);
            }
        }

        return null;
    }

    @Override
    public @Nullable InstrumentationStateRequirement get(int requestCode) {
        return mRequirements.get(requestCode);
    }
}
