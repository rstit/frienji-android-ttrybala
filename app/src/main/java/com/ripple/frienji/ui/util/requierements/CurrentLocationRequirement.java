package com.ripple.frienji.ui.util.requierements;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.ripple.frienji.R;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Tomasz Trybala
 * @since 2016-08-08
 */
public class CurrentLocationRequirement extends InstrumentationStateRequirement {
    private Context mContext;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private LocationListener mLocationListener;

    public CurrentLocationRequirement(@NonNull GoogleApiClient googleApiClient,
                                      @NonNull LocationRequest locationRequest, @NonNull LocationListener locationListener, @NonNull Context context) {
        mGoogleApiClient = checkNotNull(googleApiClient);
        mLocationRequest = checkNotNull(locationRequest);
        mLocationListener = checkNotNull(locationListener);
        mContext = checkNotNull(context);
    }

    @Override
    public void resolve() {
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, mLocationListener);
        }
    }

    @Override
    public void undo() {
        super.undo();
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, mLocationListener);
        }
    }

    @Override
    public int getErrorMessage() {return R.string.current_location_error;
    }
}
