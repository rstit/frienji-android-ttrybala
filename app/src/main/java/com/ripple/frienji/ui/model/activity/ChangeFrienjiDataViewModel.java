package com.ripple.frienji.ui.model.activity;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.ripple.frienji.R;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.ui.binding.ObservableString;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.view.activity.AvatarTypeActivity;
import com.ripple.frienji.ui.view.activity.BaseActivity;
import java.io.File;
import java.util.List;
import javax.inject.Inject;
import okhttp3.MultipartBody;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Kamil Ratajczak
 * @since 03.11.16
 */
public class ChangeFrienjiDataViewModel extends BaseSettingsViewModel {
    public static final int CHANGE_AVATAR_ACTION = 100;
    public static final String EXTRA_AVATAR = "avatar";

    public enum ChangeDataType {BIO, AVATAR, COVER_IMAGE}

    public ChangeDataType mChangeDataType;

    public final ObservableString mTitle = new ObservableString();

    @Inject
    public NavigationHandler mNavigationHandler;

    @Inject
    public IntentProvider mIntentProvider;

    private byte[] mCoverImageBytes = new byte[0];

    @Inject
    public ChangeFrienjiDataViewModel() {
    }

    public void setAvatar(@NonNull Avatar avatar) {
        mAvatarName.set(avatar.name());
        mAvatarType.set(avatar.type());
    }

    public void editFrienjiBio() {
        loadUserProfileData();
        mChangeDataType = ChangeDataType.BIO;
        mTitle.set(mContext.getString(R.string.edit_data_bio_title));
    }

    public void editFrienjiAvatar() {
        loadUserProfileData();
        mChangeDataType = ChangeDataType.AVATAR;
        mTitle.set(mContext.getString(R.string.edit_data_avatar_title));
    }

    public void editFrienjiCoverImage() {
        loadUserProfileData();
        mChangeDataType = ChangeDataType.COVER_IMAGE;
        mTitle.set(mContext.getString(R.string.edit_data_cover_image_title));
    }

    public void uploadCoverImageFile(@Nullable File coverImageFile) {
        if (coverImageFile != null) {
            mCoverImage.set(Uri.fromFile(coverImageFile).toString());
            makeImageSmaller(Uri.fromFile(coverImageFile).toString(), new SimpleTarget<byte[]>() {
                @Override public void onResourceReady(byte[] resource, GlideAnimation<? super byte[]> glideAnimation) {
                    mCoverImageBytes = resource;
                }
            });
        } else {
            mCoverImage.set(null);
            mCoverImageBytes = null;
        }
    }

    public void changeAvatar() {
        mNavigationHandler.startActivityForResult(AvatarTypeActivity.class, CHANGE_AVATAR_ACTION);
    }

    public void save() {
        sendUpdateProfileRequest(buildUserProfile().buildMultipartBodyParts());
    }

    public void cancel() {
        mNavigationHandler.finish();
    }

    protected @NonNull UserProfile buildUserProfile() {
        UserProfile.Builder builder = UserProfile.builder();
        builder.id(mUserProfileId.get());

        switch (mChangeDataType) {
            case BIO:
                builder.whoYouAre(mWhoYouAre.get());
                break;
            case COVER_IMAGE:
                builder.coverImageData(mCoverImageBytes);
                break;
            case AVATAR:
                Avatar avatar = Avatar.builder().type(mAvatarType.get()).name(mAvatarName.get()).build();
                builder.avatar(avatar);
                break;
        }

        return builder.build();
    }

    protected void sendUpdateProfileRequest(@NonNull List<MultipartBody.Part> multiPartFields) {
        subscribe(mApi.updateUserProfile(checkNotNull(multiPartFields))
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleUpdateProfileSuccess, this::handleUpdateProfileError));
    }

    private void handleUpdateProfileSuccess(@NonNull UserProfile userProfile) {
        saveUserProfileData(checkNotNull(userProfile));
        mNavigationHandler.finish();
    }

    private void handleUpdateProfileError(@Nullable Throwable e) {
        mEventBus.post(new BaseActivity.ShowSnackbarEvent(R.string.my_settings_error_update_profile));
    }

    protected void saveUserProfileData(@NonNull UserProfile userProfile) {
        switch (mChangeDataType) {
            case BIO:
                mAppSettings.setUserWhoYouAre(userProfile.whoYouAre());
                break;
            case COVER_IMAGE:
                mAppSettings.setUserCoverImage(userProfile.coverImage());
                break;
            case AVATAR:
                mAppSettings.setUserAvatarName(userProfile.avatar().name());
                mAppSettings.setUserAvatarType(userProfile.avatar().type());
                break;
        }
    }
}