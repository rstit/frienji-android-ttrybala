package com.ripple.frienji.ui.view.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.ActivityAvatarTypeBinding;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.ViewModelModule;
import com.ripple.frienji.di.module.ViewModule;
import com.ripple.frienji.ui.model.activity.AvatarTypeViewModel;
import com.ripple.frienji.ui.model.activity.ChangeFrienjiDataViewModel;
import javax.inject.Inject;

/**
 * @author kamil ratajczak
 * @since 25.08.2016
 */
public class AvatarTypeActivity extends BaseActivity {

    @Inject
    protected AvatarTypeViewModel mModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrienjiApplication app = (FrienjiApplication) getApplication();

        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new ViewModule(this)).plus(new ViewModelModule()).inject(this);

        ActivityAvatarTypeBinding mBinding = DataBindingUtil.setContentView(this, R.layout.activity_avatar_type);
        mBinding.setModel(mModel);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ChangeFrienjiDataViewModel.CHANGE_AVATAR_ACTION && resultCode == RESULT_OK) {
            mModel.mNavigator.finishWithResult(resultCode, data);
        }
    }
}