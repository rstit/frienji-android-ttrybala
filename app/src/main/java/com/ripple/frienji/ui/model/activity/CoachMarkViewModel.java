package com.ripple.frienji.ui.model.activity;

import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;
import com.ripple.frienji.ui.model.BaseViewModel;

/**
 * @author Kamil Ratajczak
 * @since 26.09.16
 */
public abstract class CoachMarkViewModel extends BaseViewModel {

    public final ObservableBoolean mShowCoachMark = new ObservableBoolean();

    public abstract @NonNull String getCoachMarkTitle();

    public abstract @NonNull String getCoachMarkContent();

    public abstract void hideCoachMark();
}