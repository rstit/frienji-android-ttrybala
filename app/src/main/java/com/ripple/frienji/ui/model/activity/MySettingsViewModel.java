package com.ripple.frienji.ui.model.activity;

import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.view.activity.InfoActivity;
import javax.inject.Inject;

/**
 * @author kamil ratajczak
 * @since 10.08.2016
 */
public class MySettingsViewModel extends BaseSettingsViewModel {

    @Inject
    public NavigationHandler mNavigator;

    @Inject
    public IntentProvider mIntentProvider;

    @Inject
    public MySettingsViewModel() {
    }

    public void openInfo() {
        mNavigator.startActivity(InfoActivity.class);
    }

    public void changeAvatar() {
        mNavigator.startActivity(mIntentProvider.createChangeAvatar());
    }

    public void changeCoverImage() {
        mNavigator.startActivity(mIntentProvider.createChangeCoverImage());
    }

    public void changeBio() {
        mNavigator.startActivity(mIntentProvider.createChangeBio());
    }
}