package com.ripple.frienji.ui.navigation;

import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import com.ripple.frienji.ui.view.activity.BaseActivity;
import java.util.Map;
import rx.functions.Action1;

/**
 * @author Marcin Przepiórkowski
 * @since 12.05.2016
 */
public interface NavigationHandler {
    void finish();

    void finishAffinity();

    void finishWithResult(int resultCode, @NonNull Intent intent);

    void startActivity(@NonNull Intent intent);

    void startActivities(@NonNull Intent[] intents);

    void startActivity(@NonNull Class<? extends BaseActivity> activity);

    void startActivityForResult(@NonNull Intent intent, int requestCode);

    void startActivityForResult(@NonNull Class<? extends BaseActivity> cls, int requestCode);

    void startActivityForResult(@NonNull Intent intent, int requestCode, final Action1<Intent> onNext, final Action1<Throwable> onError);

    void finishWithResult(int resultCode);

    void finishWithResult(int resultCode, @NonNull Map<String, Parcelable> extra);

    void showDialog(@NonNull DialogFragment dialogFragment);

    void handleOnActivityResult(int requestCode, int resultCode, Intent intent);

    boolean isStartedForResult();

    int getRequestCode();

    void openUrlLink(@NonNull String urlLink);

    void openEmailActivity(@NonNull String emailAddress, @NonNull String emailSubject);
}