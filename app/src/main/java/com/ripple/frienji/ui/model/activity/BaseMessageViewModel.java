package com.ripple.frienji.ui.model.activity;

import android.app.Activity;
import android.content.Intent;
import android.databinding.ObservableField;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.common.base.Strings;
import com.ripple.frienji.R;
import com.ripple.frienji.model.location.FrienjiLocation;
import com.ripple.frienji.model.message.Message;
import com.ripple.frienji.ui.binding.ObservableString;
import com.ripple.frienji.ui.navigation.NavigationHandler;

/**
 * @author Kamil Ratajczak
 * @since 04.10.16
 */
public abstract class BaseMessageViewModel extends CoachMarkViewModel {
    public static final int TAKE_LOACATION_ACTION = 1;
    public static final int TAKE_IMAGE_ACTION = 2;
    private static final String INTENT_TYPE_IMAGE = "image/*";

    public final ObservableField<LatLng> mLatLongPosition = new ObservableField();
    public final ObservableString mImageUri = new ObservableString();
    public final ObservableString mMessage = new ObservableString();
    protected byte[] mMessageImageBytes = new byte[0];

    public abstract @NonNull NavigationHandler getNavigationHandler();

    public abstract void onSendMessage(@NonNull Message message);

    public void sendMessage() {
        closeKeyboard();
        onSendMessage(buildMessage());
    }

    public void clearNewMessageData() {
        mLatLongPosition.set(null);
        mImageUri.set(null);
        mMessage.set(null);
        mMessageImageBytes = null;
    }

    public boolean hasNewMessageContent() {
        return mLatLongPosition.get() != null || mImageUri.get() != null || !Strings.isNullOrEmpty(mMessage.get());
    }

    public void takePhoto() {
        new AlertDialog.Builder(mContext)
                .setTitle(R.string.choose_photo_title)
                .setMessage(R.string.choose_photo_body)
                .setPositiveButton(R.string.photo_from_camera, (dialog, which) -> {
                    takePhotoFromCamera();
                })
                .setNegativeButton(R.string.photo_from_gallery, (dialog, which) -> {
                    takePhotoFromGallery();
                })
                .create().show();
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        getNavigationHandler().startActivityForResult(intent, TAKE_IMAGE_ACTION, this::uploadImage, this::handleImageError);
    }

    private void takePhotoFromGallery() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType(INTENT_TYPE_IMAGE);
        getNavigationHandler().startActivityForResult(intent, TAKE_IMAGE_ACTION, this::uploadImage, this::handleImageError);
    }

    public void takeLocation() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            getNavigationHandler().startActivityForResult(builder.build((Activity) mContext), TAKE_LOACATION_ACTION, this::uploadMap, this::handleImageError);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            handleApiError(e, R.string.error_cannot_load_location);
        }
    }

    private void uploadImage(@Nullable Intent intent) {
        if (intent != null && intent.getData() != null) {
            mImageUri.set(intent.getData().toString());

            makeImageSmaller(intent.getData().toString(), new SimpleTarget<byte[]>() {
                @Override public void onResourceReady(byte[] resource, GlideAnimation<? super byte[]> glideAnimation) {
                    mMessageImageBytes = resource;
                }
            });
        }
    }

    private void handleImageError(@NonNull Throwable e) {
        handleApiError(e, R.string.error_cannot_load_image);
    }

    private void uploadMap(@Nullable Intent intent) {
        if (intent != null) {
            Place place = PlacePicker.getPlace(mContext, intent);
            mLatLongPosition.set(place.getLatLng());
        }
    }

    private @NonNull Message buildMessage() {

        Message.Builder builder = Message.builder();

        builder.isChatMesssage(false);
        builder.message(mMessage.get());
        builder.messageImageData(mMessageImageBytes);

        if (mLatLongPosition.get() != null) {
            FrienjiLocation frienjiLocation = FrienjiLocation.builder().latitude(mLatLongPosition.get().latitude)
                    .longitude(mLatLongPosition.get().longitude).build();
            builder.location(frienjiLocation);
        }

        return builder.build();
    }
}