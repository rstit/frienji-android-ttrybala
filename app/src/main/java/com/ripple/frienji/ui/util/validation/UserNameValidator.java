package com.ripple.frienji.ui.util.validation;

import android.support.annotation.Nullable;
import com.google.common.base.Strings;

/**
 * @author kamil ratajczak
 * @since 03.08.2016
 */
public class UserNameValidator implements TextValidator {
    @Override
    public boolean isValid(@Nullable String text) {
        return !Strings.isNullOrEmpty(text) && !text.trim().contains(" ");
    }
}
