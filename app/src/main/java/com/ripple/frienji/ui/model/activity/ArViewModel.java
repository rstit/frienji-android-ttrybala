package com.ripple.frienji.ui.model.activity;

import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;
import com.ripple.frienji.R;
import com.ripple.frienji.di.scope.ActivityScope;
import com.ripple.frienji.model.frienji.Frienji;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.view.activity.UserActionsActivity;
import com.ripple.frienji.ui.view.activity.UserProfileActivity;
import com.ripple.frienji.ui.view.activity.ZooActivity;
import javax.inject.Inject;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 04.08.2016
 */
@ActivityScope
public class ArViewModel extends CoachMarkViewModel {

    public final ObservableBoolean mShowTopBar = new ObservableBoolean(true);

    @Inject
    public NavigationHandler mNavigator;

    @Inject
    public IntentProvider mIntentProvider;

    @Inject
    public ArViewModel() {
    }

    public void hideTopBar() {
        mShowTopBar.set(false);
    }

    public void showTopBar() {
        mShowTopBar.set(true);
    }

    public void launchProfileScreen() {
        mNavigator.startActivity(UserProfileActivity.class);
    }

    public void launchUserActionsScreen() {
        mNavigator.startActivity(UserActionsActivity.class);
    }

    public void launchUserZooScreen() {
        mNavigator.startActivity(ZooActivity.class);
    }

    public void showCoachMark() {
        mShowCoachMark.set(!mAppSettings.shouldSkipArCoachMark());
    }

    @Override
    public @NonNull String getCoachMarkTitle() {
        return mContext.getString(R.string.ar_coach_mark_title);
    }

    @Override
    public @NonNull String getCoachMarkContent() {
        return mContext.getString(R.string.ar_coach_mark_content);
    }

    @Override
    public @NonNull void hideCoachMark() {
        mAppSettings.hideArCoachMark();
        mShowCoachMark.set(false);
    }

    public void openFrienjiProfile(@NonNull Frienji frienji) {
        mNavigator.startActivity(mIntentProvider.createUserProfile(buildUserProfile(checkNotNull(frienji))));
    }

    protected @NonNull UserProfile buildUserProfile(@NonNull Frienji frienji) {
        return UserProfile.builder().id(frienji.id())
                .userName(frienji.name())
                .avatar(frienji.avatar())
                .whoYouAre(frienji.whoYouAre())
                .coverImage(frienji.coverPhoto()).build();
    }
}