package com.ripple.frienji.ui.view.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.ActivitySecondInfoBinding;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.ViewModelModule;
import com.ripple.frienji.di.module.ViewModule;
import com.ripple.frienji.ui.model.activity.SecondInfoViewModel;
import javax.inject.Inject;
import static com.google.common.base.Preconditions.checkNotNull;

public class SecondInfoActivity extends BaseActivity {

    @Inject
    protected SecondInfoViewModel mModel;

    @Inject
    public SecondInfoActivity() {
    }

    public static @NonNull Intent createIntent(@NonNull Context context) {
        Intent intent = new Intent(checkNotNull(context), SecondInfoActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrienjiApplication app = (FrienjiApplication) getApplication();

        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new ViewModule(this)).plus(new ViewModelModule()).inject(this);

        ActivitySecondInfoBinding mBinding = DataBindingUtil.setContentView(this, R.layout.activity_second_info);
        mBinding.setModel(mModel);

        mModel.skipStartInfoScreen();
    }
}