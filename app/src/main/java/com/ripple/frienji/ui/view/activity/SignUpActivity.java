package com.ripple.frienji.ui.view.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.ActivitySignUpBinding;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.ViewModelModule;
import com.ripple.frienji.di.module.ViewModule;
import com.ripple.frienji.di.scope.ActivityScope;
import com.ripple.frienji.model.auth.SignUpBody;
import com.ripple.frienji.ui.model.activity.SignUpViewModel;
import com.ripple.frienji.ui.model.activity.WhoAreYouViewModel;
import javax.inject.Inject;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 30.07.2016
 */
public class SignUpActivity extends BaseActivity {
    private static final String EXTRA_SIGNUPBODY = "signUpBody";

    @Inject
    protected SignUpViewModel mModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrienjiApplication app = (FrienjiApplication) getApplication();

        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new ViewModule(this)).plus(new ViewModelModule()).inject(this);

        ActivitySignUpBinding mBinding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);
        mBinding.setModel(mModel);

        fetchExtras();
    }

    private void fetchExtras() {
        Bundle extras = getIntent().getExtras();

        if (extras != null && extras.containsKey(EXTRA_SIGNUPBODY)) {
            SignUpBody signUpBody = extras.getParcelable(EXTRA_SIGNUPBODY);
            if (signUpBody != null) {
                mModel.populateSignUp(signUpBody);
            }
        }
    }

     public static @NonNull Intent createIntent(@NonNull SignUpBody signUpBody, @NonNull Context context) {
        Intent intent = new Intent(checkNotNull(context), SignUpActivity.class);
        intent.putExtra(EXTRA_SIGNUPBODY, checkNotNull(signUpBody));
        return intent;
    }
}