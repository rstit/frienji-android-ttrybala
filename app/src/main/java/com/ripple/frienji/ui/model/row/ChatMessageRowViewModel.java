package com.ripple.frienji.ui.model.row;

import android.content.Context;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import com.google.android.gms.maps.model.LatLng;
import com.google.common.base.Strings;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.message.ChatMessage;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.net.FrienjiApi;
import com.ripple.frienji.ui.binding.ObservableString;
import com.ripple.frienji.ui.model.BaseViewModel;
import com.ripple.frienji.ui.model.RowViewModel;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.util.rx.ObservableTransformers;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 12.08.2016
 */
public abstract class ChatMessageRowViewModel extends BaseViewModel implements RowViewModel {
    public final ObservableString mMessage = new ObservableString();
    public final ObservableString mCreationTime = new ObservableString();
    public final ObservableString mImageName = new ObservableString();
    public final ObservableField<LatLng> mLatLongPosition = new ObservableField();
    public final ObservableString mAvatarName = new ObservableString();
    public final ObservableInt mAvatarType = new ObservableInt();

    protected UserProfile mUserProfile;
    protected NavigationHandler mNavigationHandler;

    protected ChatMessageRowViewModel(@NonNull Context context, @NonNull NavigationHandler navigationHandler,
                                      @NonNull AvatarReader avatarReader, @NonNull FrienjiApi frienjiApi,
                                      @NonNull ObservableTransformers observableTransformers, @NonNull IntentProvider intentProvider,
                                      @NonNull ChatMessage message, @NonNull UserProfile userProfile) {
        mContext = checkNotNull(context);
        mNavigationHandler = checkNotNull(navigationHandler);
        mAvatarReader = checkNotNull(avatarReader);
        mApi = checkNotNull(frienjiApi);
        mObservableTransformers = checkNotNull(observableTransformers);
        mIntentProvider = checkNotNull(intentProvider);

        mMessage.set(message.content());

        if (!Strings.isNullOrEmpty(message.attachmentImageUrl())) {
            mImageName.set(message.attachmentImageUrl());
        }

        if (!Strings.isNullOrEmpty(message.attachmentImageUrl())) {
            mImageName.set(message.attachmentImageUrl());
        }

        if (message.location() != null) {
            LatLng latLng = new LatLng(message.location().latitude(), message.location().longitude());
            mLatLongPosition.set(latLng);
        }

        Avatar avatar;
        if (userProfile.avatar() != null) {
            avatar = userProfile.avatar();
        } else {
            avatar = Avatar.createDefaultAvatar();
        }

        mAvatarName.set(avatar.name());
        mAvatarType.set(avatar.type());
        mUserProfile = userProfile;
    }

    public void createImage() {
        if (!Strings.isNullOrEmpty(mImageName.get())) {
            mNavigationHandler.startActivity(createImageIntent(mImageName.get()));
        }
    }

    public abstract void openUserProfile();
}