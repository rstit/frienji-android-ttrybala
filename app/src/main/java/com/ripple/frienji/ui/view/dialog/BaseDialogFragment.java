package com.ripple.frienji.ui.view.dialog;

import android.support.v4.app.DialogFragment;
import com.google.common.eventbus.EventBus;
import javax.inject.Inject;

/**
 * @author Marcin Przepiórkowski
 * @since 02.06.2016
 */
public class BaseDialogFragment extends DialogFragment {
    @Inject
    protected EventBus mEventBus;

    @Override
    public void onStart() {
        super.onStart();
        mEventBus.register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mEventBus.unregister(this);
    }
}