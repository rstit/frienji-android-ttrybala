package com.ripple.frienji.ui.model.activity;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.ripple.frienji.R;
import com.ripple.frienji.model.auth.SignUpBody;
import com.ripple.frienji.model.auth.SignUpResponse;
import com.ripple.frienji.model.error.ErrorCodes;
import com.ripple.frienji.util.ErrorManager;
import java.io.IOException;
import javax.inject.Inject;
import retrofit2.adapter.rxjava.HttpException;
import rx.Observable;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 03.08.2016
 */
public class SignUpViewModel extends BaseSignViewModel {

    private SignUpBody mSignUpBody;

    @Inject
    public SignUpViewModel() {}

    @Override
    public void send() {
        if (validateInput(mPhoneCountryCode.get(), mPhoneNumber.get())) {
            updateSignUpBody();
            sendSignUpRequestBody(buildRequestConfirmCodeBody(mSignUpBody));
        }
    }

    public void handleApiError(@Nullable Throwable throwable) {
        handleApiError(throwable,mErrorManager.getSignUpErrorMessageId(throwable));
    }

    public void populateSignUp(@NonNull SignUpBody signUpBody) {
        mSignUpBody = checkNotNull(signUpBody);;
    }

    private void updateSignUpBody() {
        mSignUpBody = SignUpBody.builder().avatar(mSignUpBody.avatar())
                .whoYouAre(mSignUpBody.whoYouAre())
                .userName(mSignUpBody.userName())
                .phoneNumber(mPhoneNumber.get())
                .countryCode(mPhoneCountryCode.get()).build();
    }

    protected void sendSignUpRequestBody(@NonNull Observable<SignUpBody> requestConfirmCodeBodyObservable) {
        subscribe(requestConfirmCodeBodyObservable
                .flatMap(mApi::signUp)
                .compose(mObservableTransformers.networkOperation())
                .doOnSubscribe(() -> mViewsEnabled.set(false))
                .doOnTerminate(() -> mViewsEnabled.set(true))
                .subscribe(this::handleSignInSuccess, this::handleApiError));
    }

    protected @NonNull Observable<SignUpBody> buildRequestConfirmCodeBody(@NonNull SignUpBody signUpBody) {
        return Observable.just(checkNotNull(signUpBody));
    }

    private void handleSignInSuccess(@NonNull SignUpResponse requestConfirmCodeResponse) {
        sendRequestConfirmCodeBody(buildRequestConfirmCodeBody(mSignUpBody.countryCode(), mSignUpBody.phoneNumber()));
    }
}