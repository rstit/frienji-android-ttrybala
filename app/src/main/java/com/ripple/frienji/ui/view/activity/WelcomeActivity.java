package com.ripple.frienji.ui.view.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.provider.Settings;
import com.google.firebase.iid.FirebaseInstanceId;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.ActivityWelcomeBinding;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.ViewModelModule;
import com.ripple.frienji.di.module.ViewModule;
import com.ripple.frienji.settings.AppSettings;
import com.ripple.frienji.ui.model.activity.WelcomeViewModel;
import javax.inject.Inject;

/**
 * @author kamil ratajczak
 * @since 30.07.2016
 */
public class WelcomeActivity extends BaseActivity {

    @Inject
    protected WelcomeViewModel mModel;

    @Inject
    AppSettings mAppSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrienjiApplication app = (FrienjiApplication) getApplication();

        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new ViewModule(this)).plus(new ViewModelModule()).inject(this);

        if (mModel.shouldSkipSignIn()) {
            mModel.launchArScreen();
        } else {
            ActivityWelcomeBinding mBinding = DataBindingUtil.setContentView(this, R.layout.activity_welcome);
            mBinding.setModel(mModel);
        }

        mAppSettings.setDeviceId(Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID));
    }
}