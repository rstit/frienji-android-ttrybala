package com.ripple.frienji.ui.model.activity;

import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.common.base.Strings;
import com.ripple.frienji.R;
import com.ripple.frienji.model.auth.RequestConfirmCodeBody;
import com.ripple.frienji.model.auth.RequestConfirmCodeResponse;
import com.ripple.frienji.model.auth.SignInBody;
import com.ripple.frienji.model.auth.SignInResponse;
import com.ripple.frienji.settings.AppSettings;
import com.ripple.frienji.ui.binding.ObservableString;
import com.ripple.frienji.ui.model.BaseViewModel;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.util.validation.SecretCodeValidator;
import com.ripple.frienji.ui.view.activity.ArActivity;
import com.ripple.frienji.ui.view.activity.BaseActivity;
import javax.inject.Inject;
import rx.Observable;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 02.08.2016
 */
public class ConfirmCodeViewModel extends BaseViewModel {
    public final ObservableString mSecretCode = new ObservableString();
    public final ObservableBoolean mViewsEnabled = new ObservableBoolean(true);
    public final ObservableString mError = new ObservableString();

    @Inject
    public AppSettings mAppSettings;

    @Inject
    public NavigationHandler mNavigator;

    @Inject
    public SecretCodeValidator mSecretCodeValidator;

    protected SignInBody mSignInBody;

    @Inject
    public ConfirmCodeViewModel() {
    }

    public void populateWithSignInBody(@NonNull SignInBody signInBody) {
        mSignInBody = checkNotNull(signInBody);
    }

    public void next() {
        if (validateInput(mSecretCode.get())) {
            sendConfirmCode(updateSignInBodyWithConfirmCode());
        }
    }

    private void sendConfirmCode(@NonNull SignInBody signInBody) {
        sendRequestConfirmCodeBody(buildRequestConfirmCodeBody(checkNotNull(signInBody)));
    }

    private boolean validateInput(@Nullable String secretCode) {

        if (!mSecretCodeValidator.isValid(secretCode)) {
            mError.set(mContext.getString(R.string.input_error_secret_code_not_valid));
            return false;
        } else {
            mError.set(null);
            return true;
        }
    }

    public void resendConfirmCode() {
        resendRequestConfirmCodeBody(buildRequestConfirmCodeBody(mSignInBody.countryCode(), mSignInBody.phoneNumber()));
    }

    protected void resendRequestConfirmCodeBody(@NonNull Observable<RequestConfirmCodeBody> requestConfirmCodeBodyObservable) {
        subscribe(requestConfirmCodeBodyObservable
                .flatMap(mApi::getConfirmCode)
                .compose(mObservableTransformers.networkOperation())
                .doOnSubscribe(() -> mViewsEnabled.set(false))
                .doOnTerminate(() -> mViewsEnabled.set(true))
                .subscribe(this::handleResendConfirmCodeSuccess, this::handleApiError));
    }

    protected @NonNull Observable<RequestConfirmCodeBody> buildRequestConfirmCodeBody(@NonNull String countryPhoneCode, @NonNull String phoneNumber) {
        return Observable.just(RequestConfirmCodeBody
                .builder()
                .countryCode(checkNotNull(countryPhoneCode))
                .phoneNumber(checkNotNull(phoneNumber))
                .build());
    }

    protected @NonNull SignInBody updateSignInBodyWithConfirmCode() {
        SignInBody.Builder builder = SignInBody.builder();

        builder.phoneNumber(mSignInBody.phoneNumber())
                .countryCode(mSignInBody.countryCode())
                .deviceType(SignInBody.ANDROID_DEVICE_TYPE)
                .hardwareToken(mAppSettings.loadDeviceId().get())
                .password(mSecretCode.get());

        if (mAppSettings.loadDeviceId().isPresent()) {
            builder.hardwareToken(mAppSettings.loadDeviceId().get());
        }

        if (mAppSettings.loadFcmToken().isPresent()) {
            builder.registrationToken(mAppSettings.loadFcmToken().get());
        }

        return builder.build();
    }

    private void sendRequestConfirmCodeBody(@NonNull Observable<SignInBody> signInBodyObservable) {
        subscribe(signInBodyObservable
                .flatMap(mApi::signIn)
                .doOnNext(this::saveUserToken)
                .compose(mObservableTransformers.networkOperation(0))
                .doOnSubscribe(() -> mViewsEnabled.set(false))
                .doOnTerminate(() -> mViewsEnabled.set(true))
                .subscribe(this::handleSignInSuccess, this::handleApiError));
    }

    protected @NonNull Observable<SignInBody> buildRequestConfirmCodeBody(@NonNull SignInBody signInBody) {
        checkNotNull(signInBody);
        checkNotNull(signInBody.password());
        checkNotNull(signInBody.phoneNumber());
        checkNotNull(signInBody.countryCode());

        return Observable.just(signInBody);
    }

    private void handleResendConfirmCodeSuccess(@NonNull RequestConfirmCodeResponse requestConfirmCodeResponse) {
    }

    private void handleSignInSuccess(@NonNull SignInResponse signInResponse) {
        if (saveSignInResponseData(signInResponse)) {
            launchArScreen();
        }
        else {
            mEventBus.post(new BaseActivity.ShowSnackbarEvent(R.string.error_cannot_sign_in));
        }
    }

    private boolean saveSignInResponseData(@NonNull SignInResponse signInResponse) {
        boolean dataAreCorrect = true;

        if (!Strings.isNullOrEmpty(signInResponse.token())) {
            mAppSettings.setUserToken(signInResponse.token());
        } else {
            dataAreCorrect = false;
        }

        if (!Strings.isNullOrEmpty(signInResponse.client())) {
            mAppSettings.setClientId(signInResponse.client());
        } else {
            dataAreCorrect = false;
        }

        if (!Strings.isNullOrEmpty(signInResponse.uid())) {
            mAppSettings.setUId(signInResponse.uid());
        } else {
            dataAreCorrect = false;
        }

        if (signInResponse.userProfile() != null && signInResponse.userProfile().avatar() != null) {
            mAppSettings.setUserName(signInResponse.userProfile().userName());
            mAppSettings.setUserWhoYouAre(signInResponse.userProfile().whoYouAre());
            mAppSettings.setUserAvatarName(signInResponse.userProfile().avatar().name());
            mAppSettings.setUserAvatarType(signInResponse.userProfile().avatar().type());
            mAppSettings.setUserProfileId(signInResponse.userProfile().id());
        } else {
            dataAreCorrect = false;
        }

        if (signInResponse.userProfile().coverImage() != null) {
            mAppSettings.setUserCoverImage(signInResponse.userProfile().coverImage());
        }

        return dataAreCorrect;
    }

    private void saveUserToken(@NonNull SignInResponse signInResponse) {
        mAppSettings.setUserToken(signInResponse.token());
    }

    private void handleApiError(@Nullable Throwable throwable) {
        if (throwable != null && isUnauthorizedError(throwable)) {
            handleApiError(throwable, R.string.error_wrong_secret_code);
        } else {
            handleApiError(throwable, R.string.error_cannot_sign_up);
        }
    }

    private void launchArScreen() {
        mNavigator.startActivity(ArActivity.class);
    }
}