package com.ripple.frienji.ui.util.instrumentation;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.common.eventbus.EventBus;

/**
 * Created by Tomasz Trybala
 * @since 13.07.16.
 */
public interface InstrumentationProvider {
    void startService(@NonNull Intent intent);

    void startActivity(@NonNull Intent intent);

    void startActivityForResult(@NonNull Intent intent, int requestCode);

    void finishActivityWithResult(int resultCode, @NonNull Intent intent);

    void finishAffinity();

    void startActivity(@NonNull Class<? extends AppCompatActivity> activity);

    void startActivityAndFinish(@NonNull Class<? extends AppCompatActivity> activity);

    void finishActivity();

    void onBackPressed();

    void addFragment(@NonNull Fragment fragment);

    void replaceFragment(@NonNull Fragment fragment);

    void replaceFragment(@IdRes int layout, @NonNull Fragment fragment);

    void replaceFragmentAndAddToBackStack(@NonNull Fragment fragment);

    void replaceFragmentAndAddToBackStack(@IdRes int layout, @NonNull Fragment fragment);

    void replaceChildFragment(@NonNull Fragment fragment);

    void replaceChildFragment(@IdRes int layout, @NonNull Fragment fragment);

    void replaceChildFragmentAndAddToBackStack(@NonNull Fragment fragment);

    void replaceChildFragmentAndAddToBackStack(@IdRes int layout, @NonNull Fragment fragment);

    void popBackStack();

    void safePopBackStack();

    void hideKeyboard();

    int checkPermission(@NonNull String permission);

    void requestPermissions(@NonNull String[] permissions, int requestCode);

    @Nullable Context getActivityContext();

    @NonNull Resources getResources();

    @NonNull String getResString(@StringRes int id);

    @NonNull FragmentManager getFragmentManager();

    @Nullable GoogleApiClient getGoogleApiClient();
}
