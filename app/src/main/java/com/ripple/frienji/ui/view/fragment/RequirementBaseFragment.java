package com.ripple.frienji.ui.view.fragment;

import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.ripple.frienji.ui.util.requierements.CompositeRequirements;

/**
 * @author Tomasz Trybala
 * @since 2016-08-08
 */
public class RequirementBaseFragment extends BaseFragment {
    protected final static int REQUEST_CHECK_SETTINGS = 1001;

    protected static final int UPDATE_INTERVAL = 20000; // 20 sec
    protected static final int FASTEST_INTERVAL = 5000; // 5 sec
    protected static final int DISPLACEMENT = 10; // 10 meters

    public static final int CAMERA_PERMISSION_REQUEST_CODE = 9000;
    protected static final int LOCATION_PERMISSION_REQUEST_CODE = 9001;
    protected static final int GOOGLE_API_CLIENT_REQUEST_CODE = 9002;
    protected static final int LOCATION_PROVIDER_REQUEST_CODE = 9003;
    protected static final int CURRENT_LOCATION_REQUEST_CODE = 9004;

    protected CompositeRequirements mRequirements;
    protected GoogleApiClient mGoogleApiClient;
    protected LocationRequest mLocationRequest;

    protected boolean mIsResolving;
    private boolean mWasLocationDenied;

    protected GoogleApiClient.ConnectionCallbacks mConnectionsCallback = new GoogleApiClient.ConnectionCallbacks() {
        @Override
        public void onConnected(Bundle bundle) {
            handleDoneRequirement(GOOGLE_API_CLIENT_REQUEST_CODE);
        }

        @Override
        public void onConnectionSuspended(int i) {
            resolveNextRequirement();
        }
    };

    protected GoogleApiClient.OnConnectionFailedListener mFailedListener = connectionResult -> handleUndoneRequirement(GOOGLE_API_CLIENT_REQUEST_CODE);

    protected ResultCallback<LocationSettingsResult> mResultCallbackFromSettings = this::handleSettingsCallback;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        createLocationRequest();
        createGoogleApiClient();
        createRequirements();
    }

    @Override
    public void onPause() {
        super.onPause();
        onRequirementsPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        resolveNextRequirement();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            handleUndoneRequirement(LOCATION_PROVIDER_REQUEST_CODE);
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    protected void createRequirements() {

    }

    protected void resolveNextRequirement() {
        if (mIsResolving) {
            return;
        } else {
            mIsResolving = true;
        }
    }

    private void createLocationRequest() {
        if (mLocationRequest == null) {
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(UPDATE_INTERVAL);
            mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
        }
    }

    private void createGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                    .addConnectionCallbacks(mConnectionsCallback)
                    .addOnConnectionFailedListener(mFailedListener)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    /*package*/ void handleSettingsCallback(@NonNull LocationSettingsResult result) {
        final Status status = result.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                handleDoneRequirement(LOCATION_PROVIDER_REQUEST_CODE);
                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                if (mWasLocationDenied) {
                    handleUndoneRequirement(LOCATION_PROVIDER_REQUEST_CODE);
                } else {
                    mWasLocationDenied = true;
                    try {
                        status.startResolutionForResult(
                                getActivity(),
                                REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException e) {
                        // Ignore the error.
                    }
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                handleUndoneRequirement(LOCATION_PROVIDER_REQUEST_CODE);
                break;
        }
    }

    protected void handleDoneRequirement(int requestCode) {
        mRequirements.setIsDone(requestCode, true);
        resolveNextRequirement();
    }

    protected void handleUndoneRequirement(int requestCode) {
        mRequirements.setAsImpossible(requestCode);
        resolveNextRequirement();
    }

    private void onRequirementsPause() {
        mRequirements.setIsDone(CURRENT_LOCATION_REQUEST_CODE, false);
        mRequirements.setIsDone(GOOGLE_API_CLIENT_REQUEST_CODE, false);
    }
}
