package com.ripple.frienji.ui.util.requierements;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.ripple.frienji.ui.view.fragment.BaseFragment;
import java.lang.ref.WeakReference;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Tomasz Trybala
 * @since 2016-08-04
 */
public class PermissionRequirement extends InstrumentationStateRequirement {
    private int mRequestCode;
    @StringRes
    private int mErrorMessage;
    private String[] mPermissions;
    private WeakReference<BaseFragment> mFragment;

    public PermissionRequirement(int requestCode, @NonNull String[] permissions, @NonNull BaseFragment fragment,
                                 @StringRes int errorMessage) {
        mRequestCode = requestCode;
        mPermissions = checkNotNull(permissions);
        mErrorMessage = errorMessage;
        mFragment = new WeakReference<>(fragment);
    }

    @Override
    public void resolve() {
        if (mFragment.get() != null) {
            mFragment.get().requestPermissions(mPermissions, mRequestCode);
        }
    }

    @Override
    public int getErrorMessage() {
        return mErrorMessage;
    }
}
