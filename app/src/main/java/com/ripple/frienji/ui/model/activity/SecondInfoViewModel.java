package com.ripple.frienji.ui.model.activity;

import com.ripple.frienji.settings.AppSettings;
import com.ripple.frienji.ui.model.BaseViewModel;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.view.activity.LoginActivity;
import javax.inject.Inject;

/**
 * Created by kamil ratajczak on 24.08.16.
 */
public class SecondInfoViewModel extends BaseViewModel {

    @Inject
    public AppSettings mAppSettings;

    @Inject
    public NavigationHandler mNavigator;

    @Inject
    public SecondInfoViewModel() {
    }

    public void skipStartInfoScreen() {
        mAppSettings.setSkipStartInfoActivity();
    }


    public void launchLoginScreen() {
        mNavigator.startActivity(LoginActivity.class);
    }
}