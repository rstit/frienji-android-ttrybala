package com.ripple.frienji.ui.util.validation;

import android.support.annotation.Nullable;

/**
 * @author Marcin Przepiórkowski
 * @since 13.11.2015
 */
public interface TextValidator {
    boolean isValid(@Nullable String text);
}
