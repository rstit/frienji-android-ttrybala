package com.ripple.frienji.ui.util.requierements;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.ripple.frienji.R;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Tomasz Trybala
 * @since 2016-08-08
 */
public class LocationProviderRequirement extends InstrumentationStateRequirement {
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private ResultCallback<LocationSettingsResult> mResultCallbackFromSettings;


    public LocationProviderRequirement(@Nullable GoogleApiClient googleApiClient, @NonNull LocationRequest locationRequest,
                                       @NonNull ResultCallback<LocationSettingsResult> callback) {
        mGoogleApiClient = googleApiClient;
        mLocationRequest = checkNotNull(locationRequest);
        mResultCallbackFromSettings = checkNotNull(callback);
    }

    @Override
    public void resolve() {
        if (mGoogleApiClient != null) {
            LocationSettingsRequest.Builder locationSettingsRequestBuilder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest);
            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, locationSettingsRequestBuilder.build());
            result.setResultCallback(mResultCallbackFromSettings);
        } else {
            setAsImpossibleToDo();
        }
    }

    @Override
    public @StringRes int getErrorMessage() {
        return R.string.location_provider_error;
    }
}
