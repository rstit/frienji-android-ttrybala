package com.ripple.frienji.ui.view.dialog;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.SwipeLayout.DragEdge;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.DialogCatchFrienjiBinding;
import com.ripple.frienji.model.frienji.Frienji;
import com.ripple.frienji.ui.model.dialog.CatchFrienjiDialogViewModel;
import javax.inject.Inject;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by kamil ratajczak on 15.09.16.
 */
public class CatchFrienjiDialogFragment extends BaseDialogFragment {

    @Inject
    protected CatchFrienjiDialogViewModel mCatchFrienjiDialogViewModel;

    private View mView;

    @Inject
    public CatchFrienjiDialogFragment() {
    }

    @Override
    public @Nullable View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = View.inflate(getContext(), R.layout.dialog_catch_frienji, null);
        DialogCatchFrienjiBinding mBinding = DataBindingUtil.bind(mView);
        mBinding.setModel(mCatchFrienjiDialogViewModel);

        SwipeLayout swipeLayout = (SwipeLayout) mView.findViewById(R.id.swipeLayout);
        swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
        swipeLayout.setLeftSwipeEnabled(false);
        swipeLayout.setRightSwipeEnabled(false);
        swipeLayout.addDrag(DragEdge.Top, mView.findViewById(R.id.bottom_wrapper));
        swipeLayout.addDrag(DragEdge.Bottom, mView.findViewById(R.id.bottom_wrapper));

        swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override public void onStartOpen(SwipeLayout layout) {
                Log.d("SwipeTest", "onStartOpen");
                if (layout.getDragEdge() == DragEdge.Bottom) {
                    mCatchFrienjiDialogViewModel.setRecjectAction();
                } else if (layout.getDragEdge() == DragEdge.Top) {
                    mCatchFrienjiDialogViewModel.setKeepAction();
                }
            }

            @Override public void onOpen(SwipeLayout layout) {
                mCatchFrienjiDialogViewModel.cleanAction();

                if (layout.getDragEdge() == DragEdge.Bottom) {
                    mCatchFrienjiDialogViewModel.rejectFrienji();
                } else if (layout.getDragEdge() == DragEdge.Top) {
                    mCatchFrienjiDialogViewModel.catchFrienji();
                }
                dismiss();
            }

            @Override public void onStartClose(SwipeLayout layout) {
            }

            @Override public void onClose(SwipeLayout layout) {
                mCatchFrienjiDialogViewModel.cleanAction();
            }

            @Override public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
            }

            @Override public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
            }
        });

        return mBinding.getRoot();
    }

    public void uploadFrienji(@NonNull Frienji frienji) {
        mCatchFrienjiDialogViewModel.loadFrienji(checkNotNull(frienji));
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mEventBus.post(new OnDismissDialogEvent());
    }

    public static class CatchFrienjiEvent {

        private final CatchFrienjiEventType mType;

        public CatchFrienjiEvent(@NonNull CatchFrienjiEventType type) {
            mType = checkNotNull(type);
        }

        public @NonNull CatchFrienjiEventType getType() {
            return mType;
        }

        public enum CatchFrienjiEventType {
            CATCHED, REJECTED, ERROR
        }
    }

    public class OnDismissDialogEvent {
    }
}