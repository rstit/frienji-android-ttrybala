package com.ripple.frienji.ui.view.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.ActivityZooBinding;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.ViewModelModule;
import com.ripple.frienji.di.module.ViewModule;
import com.ripple.frienji.ui.model.activity.ZooViewModel;

import javax.inject.Inject;

/**
 * @author kamil ratajczak
 * @since 11.08.2016
 */
public class ZooActivity extends BaseActivity {

    @Inject
    protected ZooViewModel mModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrienjiApplication app = (FrienjiApplication) getApplication();

        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new ViewModule(this)).plus(new ViewModelModule()).inject(this);

        ActivityZooBinding mBinding = DataBindingUtil.setContentView(this, R.layout.activity_zoo);
        mBinding.setModel(mModel);

    }

    @Override
    public void onStart() {
        super.onStart();
        mModel.loadUserZoo();
    }

    @Override
    public void onBackPressed() {
        if (mModel.mEditMode.get()) {
            mModel.cancelEditMode();
        } else {
            super.onBackPressed();
        }
    }
}