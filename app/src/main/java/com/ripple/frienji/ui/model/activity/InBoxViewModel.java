package com.ripple.frienji.ui.model.activity;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.RowPreviewMessageBinding;
import com.ripple.frienji.model.page.PagePaginator;
import com.ripple.frienji.model.response.FrienjiConversationsResponse;
import com.ripple.frienji.ui.model.BaseViewModel;
import com.ripple.frienji.ui.model.row.InBoxRowViewModel;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.view.activity.SelectFrienjiActivity;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import rx.Observable;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 11.08.2016
 */
public class InBoxViewModel extends BaseViewModel {
    public final InBoxListAdapter mInBoxAdapter = new InBoxListAdapter();
    public final PagePaginatorScrollListener mPagePaginatorScrollListener = new InBoxPagePaginatorScrollListener();
    protected PagePaginator mPagePaginator;

    @Inject
    public NavigationHandler mNavigator;

    @Inject
    protected IntentProvider mIntentProvider;

    @Inject
    public InBoxViewModel() {
    }

    public void openConversation() {
        mNavigator.startActivity(SelectFrienjiActivity.class);
    }

    public void loadUserBoxMessages() {
        fillUserBoxMessages();
    }

    private void fillUserBoxMessages() {
        subscribe(mApi.loadUserInBox()
                .doOnNext(this::savePagePaginator)
                .flatMap(frienjiConversationsResponse -> Observable.from(frienjiConversationsResponse.conversations()))
                .map(shortMessage -> InBoxRowViewModel.from(mContext, mNavigator, mAvatarReader, mDateTimeUtil, mIntentProvider, shortMessage))
                .toList()
                .doOnSubscribe(() -> mHideLoadingBar.set(false))
                .doOnTerminate(() -> mHideLoadingBar.set(true))
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleLoadInBoxSuccess, this::handleLoadInBoxError));
    }

    private void fillUserBoxMessages(int page) {
        subscribe(mApi.loadUserInBox(page)
                .doOnNext(this::savePagePaginator)
                .flatMap(frienjiConversationsResponse -> Observable.from(frienjiConversationsResponse.conversations()))
                .map(shortMessage -> InBoxRowViewModel.from(mContext, mNavigator, mAvatarReader, mDateTimeUtil, mIntentProvider, shortMessage))
                .toList()
                .doOnSubscribe(() -> mHideLoadingBar.set(false))
                .doOnTerminate(() -> mHideLoadingBar.set(true))
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleLoadInBoxPageSuccess, this::handleLoadInBoxError));
    }

    private void handleLoadInBoxSuccess(@NonNull List<InBoxRowViewModel> inBoxMessages) {
        mInBoxAdapter.fillData(inBoxMessages);
    }

    private void handleLoadInBoxPageSuccess(@NonNull List<InBoxRowViewModel> inBoxMessages) {
        mPagePaginatorScrollListener.disableLoading();
        mInBoxAdapter.appendData(checkNotNull(inBoxMessages));
    }

    private void handleLoadInBoxError(@Nullable Throwable throwable) {
        mPagePaginatorScrollListener.disableLoading();
        handleApiError(throwable, R.string.error_cannot_load_user_in_box);
    }

    private void savePagePaginator(@NonNull FrienjiConversationsResponse frienjiPostsResponse) {
        mPagePaginator = frienjiPostsResponse.pagePaginator();
    }

    public class InBoxListAdapter extends RecyclerView.Adapter<InBoxListAdapter.InBoxViewHolder> {
        public final List<InBoxRowViewModel> mModels = new ArrayList<>();

        @Override
        public InBoxViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            RowPreviewMessageBinding rowInBoxBinding = DataBindingUtil.inflate(inflater, R.layout.row_preview_message, parent, false);
            return new InBoxViewHolder(rowInBoxBinding);
        }

        @Override
        public void onBindViewHolder(InBoxViewHolder holder, int position) {
            holder.bind(mModels.get(position));
        }

        @Override
        public int getItemCount() {
            return mModels.size();
        }

        public void fillData(@NonNull List<InBoxRowViewModel> models) {
            mModels.clear();
            mModels.addAll(checkNotNull(models));
            notifyChange();
        }

        public void appendData(@NonNull List<InBoxRowViewModel> models) {
            mModels.addAll(checkNotNull(models));
            notifyDataSetChanged();
        }

        public class InBoxViewHolder extends RecyclerView.ViewHolder {
            private RowPreviewMessageBinding mViewDataBinding;

            public InBoxViewHolder(@NonNull RowPreviewMessageBinding viewDataBinding) {
                super(viewDataBinding.getRoot());
                mViewDataBinding = viewDataBinding;
            }

            public void bind(@NonNull InBoxRowViewModel model) {
                mViewDataBinding.setModel(checkNotNull(model));
                mViewDataBinding.executePendingBindings();
            }
        }
    }

    public class InBoxPagePaginatorScrollListener extends PagePaginatorScrollListener {
        @Override public void loadElements() {
            if (mPagePaginator.hasNext()) {
                mIsLoading = true;
                fillUserBoxMessages(mPagePaginator.nextPage());
            }
        }
    }
}