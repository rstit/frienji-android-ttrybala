package com.ripple.frienji.ui.model.row;

import android.app.Activity;
import android.content.Context;
import android.databinding.ObservableInt;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.ui.binding.ObservableString;
import com.ripple.frienji.ui.model.BaseViewModel;
import com.ripple.frienji.ui.model.activity.ChangeFrienjiDataViewModel;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import java.util.HashMap;
import java.util.Map;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 29.07.2016
 */
public class AvatarRowViewModel extends BaseViewModel {

    public final ObservableString mAvatarName = new ObservableString();
    public final ObservableInt mAvatarType = new ObservableInt();

    private NavigationHandler mNavigator;

    private AvatarRowViewModel(@NonNull Context context, @NonNull NavigationHandler navigationHandler, @NonNull IntentProvider intentProvider,
                               @NonNull AvatarReader avatarReader, @NonNull String avatarName, @Avatar.AvatarType int avatarType) {
        mContext = checkNotNull(context);
        mNavigator = checkNotNull(navigationHandler);
        mIntentProvider = checkNotNull(intentProvider);
        mAvatarReader = checkNotNull(avatarReader);
        mAvatarName.set(avatarName);
        mAvatarType.set(avatarType);
    }

    public static @NonNull AvatarRowViewModel from(@NonNull Context context, @NonNull NavigationHandler navigationHandler, @NonNull IntentProvider intentProvider,
                                                   @NonNull AvatarReader avatarReader, @NonNull String avatarName, int avatarType) {
        return new AvatarRowViewModel(checkNotNull(context), checkNotNull(navigationHandler), checkNotNull(intentProvider),
                checkNotNull(avatarReader), checkNotNull(avatarName), avatarType);
    }

    public void chooseAvatar() {
        if (mNavigator.isStartedForResult()) {
            finishWithResult();
        } else {
            launchCreateWhoAreYouScreen();
        }
    }

    private void launchCreateWhoAreYouScreen() {
        Avatar avatar = Avatar.builder()
                .name(mAvatarName.get())
                .type(mAvatarType.get())
                .build();
        mNavigator.startActivity(mIntentProvider.createWhoAreYou(avatar));
    }

    private void finishWithResult() {
        Avatar avatar = Avatar.builder().type(mAvatarType.get()).name(mAvatarName.get()).build();

        Map<String, Parcelable> results = new HashMap<>();
        results.put(ChangeFrienjiDataViewModel.EXTRA_AVATAR, avatar);

        mNavigator.finishWithResult(Activity.RESULT_OK, results);
    }
}