package com.ripple.frienji.ui.model.row;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.ui.binding.ObservableString;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.view.activity.ConversationActivity;
import com.ripple.frienji.ui.view.activity.UserProfileActivity;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by kamil ratajczak on 22.08.16.
 */
public class ZooRowViewModel extends SimpleRowViewModel {
    public final ObservableString mCoverImage = new ObservableString();
    public final ObservableBoolean mEditMode = new ObservableBoolean();

    private RemoveFrienjiListener mRemoveFrienjiListener;
    protected NavigationHandler mNavigationHandler;
    private UserProfile mUserProfile;
    private long mUserId;

    protected IntentProvider mIntentProvider;

    private ZooRowViewModel(@NonNull Context context, @NonNull AvatarReader avatarReader,
                            @NonNull NavigationHandler navigationHandler, @NonNull IntentProvider intentProvider, @NonNull UserProfile userProfile) {
        super(checkNotNull(context), checkNotNull(avatarReader), userProfile.userName(), userProfile.avatar().name(), userProfile.avatar().type(), null);
        mUserId = userProfile.id();
        mCoverImage.set(userProfile.coverImage());
        mNavigationHandler = checkNotNull(navigationHandler);
        mUserProfile = userProfile;
        mIntentProvider = checkNotNull(intentProvider);
    }

    public static @NonNull ZooRowViewModel from(@NonNull Context context, @NonNull AvatarReader avatarReader,
                                                @NonNull NavigationHandler navigationHandler, @NonNull IntentProvider intentProvider,
                                                @NonNull UserProfile userProfile) {
        return new ZooRowViewModel(checkNotNull(context), checkNotNull(avatarReader),
                checkNotNull(navigationHandler), checkNotNull(intentProvider), checkNotNull(userProfile));
    }

    public void remove() {
        if (mRemoveFrienjiListener != null) {
            mRemoveFrienjiListener.removeUser(this);
        }
    }

    public void setRemoveFrienjiListener(@Nullable RemoveFrienjiListener removeFrienjiListener) {
        mRemoveFrienjiListener = removeFrienjiListener;
    }

    public long getUserId() {
        return mUserId;
    }

    public void openUserProfile() {
        if (!mEditMode.get()) {
            mNavigationHandler.startActivity(mIntentProvider.createUserProfile(mUserProfile));
        }
    }

    public void openUserChat() {
        if (!mEditMode.get()) {
            mNavigationHandler.startActivity(mIntentProvider.createConversation(mUserProfile));
        }
    }

    public interface RemoveFrienjiListener {
        void removeUser(ZooRowViewModel zooRowViewModel);
    }
}