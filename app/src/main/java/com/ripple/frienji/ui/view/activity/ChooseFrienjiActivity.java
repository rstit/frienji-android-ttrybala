package com.ripple.frienji.ui.view.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.ActivityChooseFrienjiBinding;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.ViewModelModule;
import com.ripple.frienji.di.module.ViewModule;
import com.ripple.frienji.ui.model.activity.ChooseFrienjiViewModel;
import javax.inject.Inject;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 30.07.2016
 */
public class ChooseFrienjiActivity extends BaseActivity {

    public static final String AVATAR_TYPE = "avatar_type";

    @Inject
    protected ChooseFrienjiViewModel mModel;

    public static @NonNull Intent createIntent(int avatarType, @NonNull Context context) {
        Intent intent = new Intent(checkNotNull(context), ChooseFrienjiActivity.class);
        intent.putExtra(AVATAR_TYPE, avatarType);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrienjiApplication app = (FrienjiApplication) getApplication();

        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new ViewModule(this)).plus(new ViewModelModule()).inject(this);

        ActivityChooseFrienjiBinding mBinding = DataBindingUtil.setContentView(this, R.layout.activity_choose_frienji);
        mBinding.setModel(mModel);

        fetchExtras();
    }

    private void fetchExtras() {
        Bundle extras = getIntent().getExtras();

        if (extras != null && extras.containsKey(AVATAR_TYPE)) {
            mModel.fillAvatarList(extras.getInt(AVATAR_TYPE));
        }
    }
}