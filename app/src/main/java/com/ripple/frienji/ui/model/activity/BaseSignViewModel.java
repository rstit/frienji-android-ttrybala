package com.ripple.frienji.ui.model.activity;

import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.i18n.phonenumbers.NumberParseException;
import com.ripple.frienji.R;
import com.ripple.frienji.model.auth.RequestConfirmCodeBody;
import com.ripple.frienji.model.auth.RequestConfirmCodeResponse;
import com.ripple.frienji.model.auth.SignInBody;
import com.ripple.frienji.ui.binding.ObservableString;
import com.ripple.frienji.ui.model.BaseViewModel;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.util.validation.PhoneNumberValidator;
import javax.inject.Inject;
import rx.Observable;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 02.08.2016
 */
public abstract class BaseSignViewModel extends BaseViewModel {

    public final ObservableString mPhoneCountryCode = new ObservableString();
    public final ObservableString mPhoneNumber = new ObservableString();
    public final ObservableBoolean mViewsEnabled = new ObservableBoolean(true);
    public final ObservableBoolean mErrorVisible = new ObservableBoolean();
    public final ObservableString mError = new ObservableString();

    @Inject
    public NavigationHandler mNavigator;

    @Inject
    public PhoneNumberValidator mPhoneNumberValidator;

    protected boolean validateInput(@Nullable String countryCode, @Nullable String phoneNumber) {
        try {
            if (!mPhoneNumberValidator.validate(countryCode, phoneNumber)) {
                mErrorVisible.set(true);
                mError.set(mContext.getString(R.string.input_error_number_not_valid));
                return false;
            } else {
                mErrorVisible.set(false);
                return true;
            }
        } catch (NumberParseException | NumberFormatException e) {
            mErrorVisible.set(true);
            mError.set(mContext.getString(R.string.input_error_number_not_valid));
            return false;
        }
    }

    protected void sendRequestConfirmCodeBody(@NonNull Observable<RequestConfirmCodeBody> requestConfirmCodeBodyObservable) {
        subscribe(requestConfirmCodeBodyObservable
                .flatMap(mApi::getConfirmCode)
                .compose(mObservableTransformers.networkOperation())
                .doOnSubscribe(() -> mViewsEnabled.set(false))
                .doOnTerminate(() -> mViewsEnabled.set(true))
                .subscribe(this::handleSignInSuccess, this::handleSignInError));
    }

    protected @NonNull Observable<RequestConfirmCodeBody> buildRequestConfirmCodeBody(@NonNull String countryPhoneCode, @NonNull String phoneNumber) {
        return Observable.just(RequestConfirmCodeBody
                .builder()
                .countryCode(checkNotNull(countryPhoneCode))
                .phoneNumber(checkNotNull(phoneNumber))
                .build());
    }

    protected void handleSignInSuccess(@NonNull RequestConfirmCodeResponse requestConfirmCodeResponse) {
        launchConfirmCodeScreen();
    }

    private void launchConfirmCodeScreen() {
        SignInBody signInBody = SignInBody.builder()
                .countryCode(mPhoneCountryCode.get())
                .phoneNumber(mPhoneNumber.get())
                .deviceType(SignInBody.ANDROID_DEVICE_TYPE)
                .build();

        mNavigator.startActivity(mIntentProvider.createConfirmCode(signInBody, true));
    }

    protected void handleSignInError(@Nullable Throwable throwable) {
        handleApiError(throwable, mErrorManager.getSignInErrorMessageId(throwable));
    }

    public abstract void send();
}