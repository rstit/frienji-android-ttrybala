package com.ripple.frienji.ui.navigation;

import android.content.Intent;
import android.support.annotation.NonNull;
import com.ripple.frienji.model.auth.SignInBody;
import com.ripple.frienji.model.auth.SignUpBody;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.frienji.Frienji;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.ui.view.activity.ConversationActivity;

/**
 * @author kamil ratajczak
 * @since 14.08.2016
 */
public interface IntentProvider {

    @NonNull Intent createWhoAreYou(@NonNull Avatar avatar);

    @NonNull Intent createConfirmCode(@NonNull SignInBody signInBody, boolean isRegisterConfirm);

    @NonNull Intent createSignUp(@NonNull SignUpBody signUpBody);

    @NonNull Intent createFirstIntroduction();

    @NonNull Intent createSecondIntroduction();

    @NonNull Intent createChoseFrienji(@Avatar.AvatarType int avatarType);

    @NonNull Intent createLogIn();

    @NonNull Intent createUserProfile(@NonNull UserProfile userProfile);

    @NonNull Intent createConversation(@NonNull UserProfile userProfile);

    @NonNull Intent createMyOwnUserProfile();

    @NonNull Intent createComents(long frienjiId, long postId);

    @NonNull Intent createChangeAvatar();

    @NonNull Intent createChangeCoverImage();

    @NonNull Intent createChangeBio();
}