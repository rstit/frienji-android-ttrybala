package com.ripple.frienji.ui.view.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.TransformationUtils;
import com.google.common.eventbus.Subscribe;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.ActivityChangeFrienjiDataBinding;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.ViewModelModule;
import com.ripple.frienji.di.module.ViewModule;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.ui.model.activity.ChangeFrienjiDataViewModel;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.view.dialog.ChoosePhotoDialogFragment;
import com.ripple.frienji.util.rx.ObservableTransformers;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;
import javax.inject.Inject;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;
import static com.google.common.base.Preconditions.checkNotNull;

public class ChangeFrienjiDataActivity extends BaseActivity {
    public static final int CHANGE_BIO_TEXT = 1;
    public static final int CHANGE_AVATAR = 2;
    public static final int CHANGE_COVER_IMAGE = 3;

    protected static final int EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 1;
    protected static final String COVER_IMAGE_NAME = "avatar";
    protected static final String COVER_IMAGE_EXT = ".jpg";
    protected static final int COVER_IMAGE_WIDTH_SIZE = 800;
    protected static final int COVER_IMAGE_HEIGHT_SIZE = 600;
    protected static final int REQUEST_CAMERA = 101;
    protected static final int REQUEST_GALLERY = 102;
    private static final boolean OPERATION_SUCCESS = true;
    private static final String EXTRA_CHANGE_TYPE = "changeType";

    protected final CompositeSubscription mSubscriptions = new CompositeSubscription();

    @Inject
    protected ChangeFrienjiDataViewModel mModel;

    @Inject
    protected ChoosePhotoDialogFragment mChoosePhotoDialogFragment;

    @Inject
    protected NavigationHandler mNavigationHandler;

    @Inject
    protected ObservableTransformers mObservableTransformers;

    protected File mImageTempFile;

    private ActivityChangeFrienjiDataBinding mBinding;

    public static @NonNull Intent createIntent(int changeType, @NonNull Context context) {
        Intent intent = new Intent(checkNotNull(context), ChangeFrienjiDataActivity.class);
        intent.putExtra(EXTRA_CHANGE_TYPE, changeType);
        return intent;
    }

    @Subscribe
    public void onChoosePhotoEvent(@NonNull ChoosePhotoDialogFragment.ChoosePhotoEvent event) {
        if (event.chooseFromCamera()) {
            requestCamera();
        } else if (event.chooseFromGallery()) {
            startGallery();
        } else if (event.removeImage()) {
            removeCoverImage();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrienjiApplication app = (FrienjiApplication) getApplication();

        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new ViewModule(this)).plus(new ViewModelModule()).inject(this);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_change_frienji_data);
        mBinding.setModel(mModel);

        fetchExtras();
        bindUiActions();
    }

    protected void bindUiActions() {
        mBinding.imgCoverImage.setOnClickListener(v -> selectPhoto());
    }

    public void selectPhoto() {
        mChoosePhotoDialogFragment.show(getSupportFragmentManager(), "choose photo");
    }

    private void fetchExtras() {
        Bundle extras = getIntent().getExtras();

        if (extras != null && extras.containsKey(EXTRA_CHANGE_TYPE)) {
            int changeType = extras.getInt(EXTRA_CHANGE_TYPE, CHANGE_BIO_TEXT);

            switch (changeType) {
                case CHANGE_BIO_TEXT:
                    mModel.editFrienjiBio();
                    break;
                case CHANGE_AVATAR:
                    mModel.editFrienjiAvatar();
                    break;
                case CHANGE_COVER_IMAGE:
                    mModel.editFrienjiCoverImage();
                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ChangeFrienjiDataViewModel.CHANGE_AVATAR_ACTION && resultCode == RESULT_OK) {
            Avatar avatar = data.getParcelableExtra(ChangeFrienjiDataViewModel.EXTRA_AVATAR);

            if (avatar != null) {
                mModel.setAvatar(avatar);
            }
        } else if (requestCode == REQUEST_GALLERY && resultCode == RESULT_OK) {
            Uri fileUri = data.getData();

            if (fileUri != null) {
                loadImageFromFile(fileUri);
            }
        } else if (requestCode == REQUEST_CAMERA && mImageTempFile != null && mImageTempFile.exists()) {
            executeCompressImageFileOperation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    try {
                        initializeTempImageFile();
                        startCamera();
                    } catch (IOException e) {
                        View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
                        Snackbar.make(rootView, R.string.error_cannot_access_external_storage,
                                Snackbar.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    protected void startGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);

        mNavigationHandler.startActivityForResult(Intent.createChooser(intent, "choose image"), REQUEST_GALLERY);
    }

    protected void requestCamera() {
        try {
            if (!checkPermissionForExternalStorage()) {
                requestPermissionForExternalStorage();
            } else {
                initializeTempImageFile();
                startCamera();
            }
        } catch (IOException ioe) {
            View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
            Snackbar.make(rootView, R.string.error_cannot_access_external_storage,
                    Snackbar.LENGTH_SHORT).show();
        }
    }

    protected boolean checkPermissionForExternalStorage() {
        return PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    protected void requestPermissionForExternalStorage() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
            Snackbar.make(rootView, R.string.rationale_external_storage,
                    Snackbar.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
        }
    }

    protected void initializeTempImageFile() throws IOException {
        mImageTempFile = File.createTempFile(COVER_IMAGE_NAME, COVER_IMAGE_EXT,
                Environment.getExternalStorageDirectory());
        if (!mImageTempFile.exists()) {
            throw new IOException();
        }
    }

    protected void startCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mImageTempFile));

        mNavigationHandler.startActivityForResult(intent, REQUEST_CAMERA);
    }

    protected void loadImageFromFile(final Uri fileUri) {
        mSubscriptions.add(Observable.defer(() -> loadFromUriFile(fileUri))
                .compose(mObservableTransformers.backgroundOperation())
                .subscribe(this::loadImageFromFileSuccess, this::loadImageFromFileError));
    }

    protected Observable<File> loadFromUriFile(final Uri fileUri) {
        File f;

        try {
            final byte[] bmpData = Glide
                    .with(ChangeFrienjiDataActivity.this)
                    .load(fileUri.toString())
                    .asBitmap()
                    .toBytes()
                    .centerCrop()
                    .into(COVER_IMAGE_WIDTH_SIZE, COVER_IMAGE_HEIGHT_SIZE)
                    .get();

            f = new File(getFilesDir(), UUID.randomUUID().toString());

            Bitmap bmp = BitmapFactory.decodeByteArray(bmpData, 0, bmpData.length);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(f));

            return Observable.just(f);

        } catch (Exception e) {
            return Observable.error(e);
        }
    }

    public void loadImageFromFileSuccess(File imageFile) {
        handleLoadedFile(imageFile);
    }

    public void loadImageFromFileError(Throwable error) {
        mEventBus.post(new BaseActivity.ShowSnackbarEvent(R.string.error_cannot_load_cover_image));
    }

    protected void executeCompressImageFileOperation() {
        mSubscriptions.add(Observable.defer(this::compressImageFileOperation)
                .compose(mObservableTransformers.backgroundOperation())
                .subscribe(this::compressImageFileOperationSuccess, this::compresImageFileOperationError));
    }

    private Observable<Boolean> compressImageFileOperation() {
        try {

            final byte[] bmpData =
                    Glide
                            .with(ChangeFrienjiDataActivity.this)
                            .load(mImageTempFile)
                            .asBitmap()
                            .toBytes()
                            .centerCrop()
                            .into(COVER_IMAGE_WIDTH_SIZE, COVER_IMAGE_HEIGHT_SIZE).get();

            Bitmap bmp = BitmapFactory.decodeByteArray(bmpData, 0, bmpData.length);

            if (bmp != null) {
                //noinspection deprecation
                int orientation = TransformationUtils.getOrientation(mImageTempFile.getAbsolutePath());
                Bitmap b = TransformationUtils.rotateImage(bmp, orientation);
                b.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(mImageTempFile));

                return Observable.just(OPERATION_SUCCESS);
            }
            return Observable.just(false);

        } catch (Exception e) {
            return Observable.error(e);
        }
    }

    public void compressImageFileOperationSuccess(Boolean status) {
        if (status) {
            handleLoadedFile(mImageTempFile);
        }
    }

    public void compresImageFileOperationError(@Nullable Throwable error) {
        mEventBus.post(new BaseActivity.ShowSnackbarEvent(R.string.error_cannot_load_cover_image));
    }

    protected void handleLoadedFile(@NonNull File imageTempFile) {
        mModel.uploadCoverImageFile(checkNotNull(imageTempFile));
    }

    protected void removeCoverImage() {
        mModel.uploadCoverImageFile(null);
    }
}