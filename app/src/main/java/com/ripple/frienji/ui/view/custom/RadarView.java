package com.ripple.frienji.ui.view.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import com.ripple.frienji.R;
import com.ripple.frienji.ar.model.Bubble;
import java.util.List;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Kamil Ratajczak
 * @since 05.10.16
 */
public class RadarView extends View {
    private static final int RADIUS_OF_RADAR_POINTS = 10;
    private static final int DEFAULT_NUMBER_RADAR_CIRCLES = 6;
    private static final int DEFAULT_RADAR_CIRCLE_COLOR = Color.WHITE;
    private static final int DEFAULT_RADAR_CENTRAL_CIRCLE_FILL_COLOR = Color.GREEN;
    private static final int DEFAULT_POINT_CIRCLE_COLOR = Color.BLUE;
    private static final int DEFAULT_START_GRADIENT_COLOR = Color.BLACK;
    private static final int DEFAULT_END_GRADIENT_COLOR = Color.WHITE;
    private static final int NO_BACKGROUND_RADAR_CIRCLE_DOWN_LIMIT = 2;
    private static final int RADAR_TRIANGLE_AREA_START_ANGLE = 220;
    private static final int RADAR_TRIANGLE_AREA_SWEEP_ANGLE = 100;
    private static final int PERCENT_OF_WIDTH_SPACE_FOR_BORDER_AREA = 10;
    private static final int FIRST_RADAR_CIRCLE = 1;
    private static final int SECOND_RADAR_CIRCLE = 2;
    private static final int THIRD_RADAR_CIRCLE = 3;
    private static final int FOURTH_RADAR_CIRCLE = 4;
    private static final int FIVE_RADAR_CIRCLE = 5;
    private static final int SIXTH_RADAR_CIRCLE = 6;
    private static final float ANGLE_CORRECTION = 360f;
    private static final float NORTH_ANGLE_CORRECTION = 270f;

    private List<Bubble> mBubbles;

    private int mNumberOfRadarCircles;
    private int mRadarCircleColor;
    private int mCentralRadarCircleFillColor;
    private int mPointCircleFillColor;

    private Paint mRadarCirclePaint;
    private Paint mPointCirclePaint;
    private Paint mCentralRadarCirclePaint;
    private Paint mRadarArcPaint;

    private int mStartGradientColor;
    private int mEndGradientColor;

    private float mRotation;

    public RadarView(Context context) {
        super(context);
    }

    public RadarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.RadarView, 0, 0);

        try {
            mNumberOfRadarCircles = a.getInt(R.styleable.RadarView_numberOfRadarCircles, DEFAULT_NUMBER_RADAR_CIRCLES);
            mRadarCircleColor = a.getColor(R.styleable.RadarView_radarCircleColor, DEFAULT_RADAR_CIRCLE_COLOR);
            mPointCircleFillColor = a.getColor(R.styleable.RadarView_pointCircleFillColor, DEFAULT_POINT_CIRCLE_COLOR);
            mCentralRadarCircleFillColor = a.getColor(R.styleable.RadarView_centralRadarCircleFillColor, DEFAULT_RADAR_CENTRAL_CIRCLE_FILL_COLOR);
            mStartGradientColor = a.getColor(R.styleable.RadarView_startGradientColor, DEFAULT_START_GRADIENT_COLOR);
            mEndGradientColor = a.getColor(R.styleable.RadarView_endGradientColor, DEFAULT_END_GRADIENT_COLOR);
        } finally {
            a.recycle();
        }
        initPaints();
    }

    public RadarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setBubbles(@NonNull List<Bubble> bubbles) {
        mBubbles = checkNotNull(bubbles);
    }

    public void updateRotation(float rotation) {
        mRotation = rotation;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int mBaseRadius = calculateBaseRadius();
        drawArc(canvas);

        // draw radar circles
        for (int i = mNumberOfRadarCircles; i >= NO_BACKGROUND_RADAR_CIRCLE_DOWN_LIMIT; i--) {
            drawCircle(mBaseRadius, i, canvas);
        }

        //draw central filled circle
        drawCentralCircle(mBaseRadius, canvas);

        if (mBubbles != null) {
            for (int i = 0; i < mBubbles.size(); i++) {
                Bubble bubble = mBubbles.get(i);

                float angle = bubble.getStartingAzimuth();

                if (angle < 0.0f) {
                    angle += ANGLE_CORRECTION;
                }

                angle = -(angle + mRotation + NORTH_ANGLE_CORRECTION); // minus to make move in opposite direction

                drawPoint((int) bubble.getDistance(), angle, canvas);
            }
        }
    }

    private void initPaints() {
        mRadarCirclePaint = new Paint();
        mRadarCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mRadarCirclePaint.setStyle(Paint.Style.STROKE);
        mRadarCirclePaint.setColor(mRadarCircleColor);

        mPointCirclePaint = new Paint();
        mPointCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPointCirclePaint.setStyle(Paint.Style.FILL);
        mPointCirclePaint.setColor(mPointCircleFillColor);

        mCentralRadarCirclePaint = new Paint();
        mCentralRadarCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCentralRadarCirclePaint.setStyle(Paint.Style.FILL);
        mCentralRadarCirclePaint.setColor(mCentralRadarCircleFillColor);

        mRadarArcPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    }

    private int calculateBaseRadius() {
        double borderArea = ((getWidth() / 100) * PERCENT_OF_WIDTH_SPACE_FOR_BORDER_AREA);
        double radarCirclesArea = getWidth() - borderArea;
        return ((int) radarCirclesArea / 2) / mNumberOfRadarCircles;
    }

    private double calculateX(int radius, double angle) {
        return radius * Math.cos(Math.toRadians(angle));
    }

    private double calculateY(int radius, double angle) {
        return radius * Math.sin(Math.toRadians(angle));
    }

    private int getRadiusForCircle(int circleId) {
        int baseRadius = calculateBaseRadius();
        return circleId * baseRadius;
    }

    private int getRadiusForDistance(int distance) {
        int deltaDistance;
        int baseRadius = calculateBaseRadius();
        int radius = 0;

        if (distance <= Bubble.METERS_DISTANCE_RANGE_1) {
            radius = calculateDeltaRadius(baseRadius, distance, Bubble.METERS_DISTANCE_RANGE_1);
        } else if (distance <= Bubble.METERS_DISTANCE_RANGE_2) {
            deltaDistance = distance - Bubble.METERS_DISTANCE_RANGE_1;
            radius = calculateDeltaRadius(baseRadius, deltaDistance, Bubble.METERS_DISTANCE_RANGE_2 - Bubble.METERS_DISTANCE_RANGE_1) + getRadiusForCircle(FIRST_RADAR_CIRCLE);
        } else if (distance <= Bubble.METERS_DISTANCE_RANGE_3) {
            deltaDistance = distance - Bubble.METERS_DISTANCE_RANGE_2;
            radius = calculateDeltaRadius(baseRadius, deltaDistance, Bubble.METERS_DISTANCE_RANGE_3 - Bubble.METERS_DISTANCE_RANGE_2) + getRadiusForCircle(SECOND_RADAR_CIRCLE);
        } else if (distance <= Bubble.METERS_DISTANCE_RANGE_4) {
            deltaDistance = distance - Bubble.METERS_DISTANCE_RANGE_3;
            radius = calculateDeltaRadius(baseRadius, deltaDistance, Bubble.METERS_DISTANCE_RANGE_4 - Bubble.METERS_DISTANCE_RANGE_3) + getRadiusForCircle(THIRD_RADAR_CIRCLE);
        } else if (distance <= Bubble.METERS_DISTANCE_RANGE_5) {
            deltaDistance = distance - Bubble.METERS_DISTANCE_RANGE_4;
            radius = calculateDeltaRadius(baseRadius, deltaDistance, Bubble.METERS_DISTANCE_RANGE_5 - Bubble.METERS_DISTANCE_RANGE_4) + getRadiusForCircle(FOURTH_RADAR_CIRCLE);
        } else if (distance <= Bubble.METERS_DISTANCE_RANGE_6) {
            deltaDistance = distance - Bubble.METERS_DISTANCE_RANGE_5;
            radius = calculateDeltaRadius(baseRadius, deltaDistance, Bubble.METERS_DISTANCE_RANGE_6 - Bubble.METERS_DISTANCE_RANGE_5) + getRadiusForCircle(FIVE_RADAR_CIRCLE);
        } else if (distance > Bubble.METERS_DISTANCE_RANGE_6) {
            radius = getRadiusForCircle(SIXTH_RADAR_CIRCLE);
        }

        return radius;
    }

    private int calculateDeltaRadius(int baseRadius, int deltaDistance, int sectionDistance) {
        double percentOfBaseRadius = ((deltaDistance) * 100) / sectionDistance;
        return (int) ((baseRadius * percentOfBaseRadius) / 100);
    }

    private void drawArc(@NonNull Canvas canvas) {
        mRadarArcPaint.setShader(new LinearGradient(0, getHeight(), 0, 0, mStartGradientColor, mEndGradientColor, Shader.TileMode.MIRROR));
        RectF rect = new RectF(0, 0, getWidth(), getHeight());
        canvas.drawArc(rect, RADAR_TRIANGLE_AREA_START_ANGLE, RADAR_TRIANGLE_AREA_SWEEP_ANGLE, true, mRadarArcPaint);
    }

    private void drawCircle(int radius, int multiplier, @NonNull Canvas canvas) {
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, radius * multiplier, mRadarCirclePaint);
    }

    private void drawCentralCircle(int radius, @NonNull Canvas canvas) {
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, radius, mCentralRadarCirclePaint);
        canvas.drawCircle(getWidth() / 2, getHeight() / 2, radius, mRadarCirclePaint);
    }

    private void drawPointCircle(int radius, double x, double y, @NonNull Canvas canvas) {
        int newX = (int) calculateXPositionOnView(x);
        int newY = (int) calculateYPositionOnView(y);
        canvas.drawCircle(newX, newY, radius, mPointCirclePaint);
        canvas.drawCircle(newX, newY, radius, mRadarCirclePaint);
    }

    private double calculateXPositionOnView(double x) {
        return (getWidth() / 2) + x;
    }

    private double calculateYPositionOnView(double y) {
        return (getHeight() / 2) - y;
    }

    private void drawPoint(int distance, double angle, @NonNull Canvas canvas) {
        int radius = getRadiusForDistance(distance);
        drawPointCircle(RADIUS_OF_RADAR_POINTS,
                calculateX(radius, angle),
                calculateY(radius, angle),
                checkNotNull(canvas));
    }
}