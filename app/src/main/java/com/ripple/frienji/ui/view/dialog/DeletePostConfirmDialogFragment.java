package com.ripple.frienji.ui.view.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.DialogModule;

/**
 * @author Kamil Ratajczak
 * @since 29.09.16
 */
public class DeletePostConfirmDialogFragment extends ConfirmDialogFragment {
    private long mPostId;
    private int mPosition;

    public static @NonNull DeletePostConfirmDialogFragment createInstance(long postId, int position) {
        Bundle args = new Bundle();
        args.putLong("postId", postId);
        args.putInt("position", position);

        DeletePostConfirmDialogFragment deletePostConfirmDialogFragment = new DeletePostConfirmDialogFragment();
        deletePostConfirmDialogFragment.setArguments(args);
        return deletePostConfirmDialogFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrienjiApplication app = (FrienjiApplication) getActivity().getApplication();
        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new DialogModule(getActivity())).inject(this);

        if (getArguments() != null && getArguments().containsKey("postId")
                && getArguments().containsKey("position")) {
            mPostId = getArguments().getLong("postId");
            mPosition = getArguments().getInt("position");
        }
    }

    @Override
    protected @NonNull String getTitle() {
        return getContext().getString(R.string.delete_post_title);
    }

    @Override
    protected @NonNull String getConfirmMessage() {
        return getContext().getString(R.string.delete_post_message);
    }

    @Override
    protected @NonNull ConfirmEvent getConfirmEvent() {
        return new DeletePostConfirmEvent(mPostId, mPosition);
    }

    public class DeletePostConfirmEvent extends ConfirmEvent {
        private long mPostId;
        private int mPosition;

        public DeletePostConfirmEvent(long postId, int position) {
            mPostId = postId;
            mPosition = position;
        }

        public long getPostId() {
            return mPostId;
        }

        public int getPosition() {
            return mPosition;
        }
    }
}