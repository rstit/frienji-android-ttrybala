package com.ripple.frienji.ui.model.row;

import android.content.Context;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.ui.binding.ObservableString;
import com.ripple.frienji.ui.model.BaseViewModel;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 12.08.2016
 */
public class SimpleRowViewModel extends BaseViewModel {

    public final ObservableString mUserName = new ObservableString();
    public final ObservableString mContent = new ObservableString();
    public final ObservableString mAvatarName = new ObservableString();
    public final ObservableInt mAvatarType = new ObservableInt();

    public SimpleRowViewModel(@NonNull Context context, @NonNull AvatarReader avatarReader) {
        mContext = checkNotNull(context);
        mAvatarReader = checkNotNull(avatarReader);
    }

    public SimpleRowViewModel(@NonNull Context context, @NonNull AvatarReader avatarReader, @NonNull String userName, @NonNull String avatarName, int avatarType, @Nullable String content) {
        mContext = checkNotNull(context);
        mAvatarReader = checkNotNull(avatarReader);
        mAvatarName.set(checkNotNull(avatarName));
        mAvatarType.set(avatarType);
        mUserName.set(checkNotNull(userName));
        mContent.set(content);
    }
}