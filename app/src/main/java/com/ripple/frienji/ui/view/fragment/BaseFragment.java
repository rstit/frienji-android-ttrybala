package com.ripple.frienji.ui.view.fragment;

import android.support.v4.app.Fragment;
import com.google.common.eventbus.EventBus;
import javax.inject.Inject;

/**
 * Created by Tomasz Trybala
 *
 * @since 17.05.16.
 */
public class BaseFragment extends Fragment {

    @Inject
    protected EventBus mEventBus;

    @Override
    public void onStart() {
        super.onStart();
        mEventBus.register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mEventBus.unregister(this);
    }
}