package com.ripple.frienji.ui.util.requierements;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * @author Tomasz Trybala
 * @since 2016-08-04
 */
public interface Requirements {
    void add(int requestCode, @NonNull InstrumentationStateRequirement requirement);

    void setIsDone(int requestCode, boolean isDone);

    void setAsImpossible(int requestCode);

    void clear();

    @Nullable
    InstrumentationStateRequirement next();

    @Nullable
    InstrumentationStateRequirement get(int requestCode);
}
