package com.ripple.frienji.ui.view.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import com.ripple.frienji.R;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by kamil ratajczak on 26.08.16.
 */
public class PermissionsDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {

    public interface OnInteractionListener {

        void onPermissionDialogDontAllowClicked(int permission);

        void onPermissionDialogOkClicked(int permission);
    }

    private static final String EXTRA_MESSAGE = "extra_message";
    private static final String EXTRA_PERMISSION = "extra_permission";

    private OnInteractionListener mListener;
    private int mPermission;

    public static @NonNull PermissionsDialogFragment newInstance(@NonNull String message, int permission) {
        PermissionsDialogFragment dialogFragment = new PermissionsDialogFragment();
        Bundle args = new Bundle();
        args.putString(EXTRA_MESSAGE, checkNotNull(message));
        args.putInt(EXTRA_PERMISSION, permission);
        dialogFragment.setArguments(args);
        return dialogFragment;
    }


    @Override
    public @NonNull Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle arguments = getArguments();
        String message = null;
        if (arguments != null) {
            message = arguments.getString(EXTRA_MESSAGE);
            mPermission = arguments.getInt(EXTRA_PERMISSION);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setPositiveButton(getString(android.R.string.ok), this)
                .setMessage(message).setNegativeButton(getString(R.string.dont_allow), this);
        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                if (mListener != null) {
                    mListener.onPermissionDialogOkClicked(mPermission);
                }
                break;

            case DialogInterface.BUTTON_NEGATIVE:
                if (mListener != null) {
                    mListener.onPermissionDialogDontAllowClicked(mPermission);
                }
                break;
        }

        dialog.dismiss();
    }

    public void setListener(@Nullable OnInteractionListener listener) {
        this.mListener = listener;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
}

