package com.ripple.frienji.ui.view.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.ActivityConfirmCodeBinding;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.ViewModelModule;
import com.ripple.frienji.di.module.ViewModule;
import com.ripple.frienji.model.auth.SignInBody;
import com.ripple.frienji.ui.model.activity.ConfirmCodeViewModel;
import javax.inject.Inject;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 30.07.2016
 */
public class ConfirmCodeActivity extends BaseActivity {
    private static final String EXTRA_SIGN_IN_BODY = "signInBody";

    @Inject
    protected ConfirmCodeViewModel mModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrienjiApplication app = (FrienjiApplication) getApplication();

        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new ViewModule(this)).plus(new ViewModelModule()).inject(this);

        ActivityConfirmCodeBinding mBinding = DataBindingUtil.setContentView(this, R.layout.activity_confirm_code);
        mBinding.setModel(mModel);

        fetchExtras();
    }

    private void fetchExtras() {
        Bundle extras = getIntent().getExtras();

        if (extras != null && extras.containsKey(EXTRA_SIGN_IN_BODY)) {
            SignInBody signInBody = extras.getParcelable(EXTRA_SIGN_IN_BODY);
            if (signInBody != null) {
                mModel.populateWithSignInBody(signInBody);
            }
        }
    }

    public static @NonNull Intent createIntent(@NonNull SignInBody signInBody, boolean isRegisterConfirm, @NonNull Context context) {
        Intent intent = new Intent(checkNotNull(context), ConfirmCodeActivity.class);
        intent.putExtra(EXTRA_SIGN_IN_BODY, checkNotNull(signInBody));
        return intent;
    }
}
