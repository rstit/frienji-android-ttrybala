package com.ripple.frienji.ui.model.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import com.ripple.frienji.R;
import com.ripple.frienji.ui.model.BaseViewModel;
import com.ripple.frienji.ui.util.instrumentation.InstrumentationProvider;
import com.ripple.frienji.ui.util.requierements.InstrumentationStateRequirement;
import com.ripple.frienji.ui.util.requierements.LocationProviderRequirement;
import com.ripple.frienji.ui.util.requierements.PermissionRequirement;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by Tomasz Trybala
 *
 * @since 17.05.16.
 */
public class BaseFragmentViewModel extends BaseViewModel {
    protected Runnable mNetworkRunnable = this::goToNetworkSettings;
    protected Runnable mSettingsRunnable = this::goToAppSettings;
    protected Runnable mLocationRunnable = this::goToLocationSettings;
    protected Runnable mCurrentRunnable = mNetworkRunnable;

    private InstrumentationProvider mInstrumentationProvider;

    protected BaseFragmentViewModel() {
    }

    protected BaseFragmentViewModel(@NonNull InstrumentationProvider provider) {
        mInstrumentationProvider = checkNotNull(provider);
    }

    public void setInstrumentationProvider(@NonNull InstrumentationProvider instrumentationProvider) {
        mInstrumentationProvider = checkNotNull(instrumentationProvider);
    }

    @Nullable
    protected Context getActivityContext() {
        return mInstrumentationProvider.getActivityContext();
    }

    @NonNull
    protected Resources getResources() {
        return mInstrumentationProvider.getResources();
    }

    protected int checkPermission(@NonNull String permission) {
        return mInstrumentationProvider.checkPermission(checkNotNull(permission));
    }

    @NonNull
    protected String getString(@StringRes int id) {
        return mInstrumentationProvider.getResString(id);
    }

    protected void requestPermissions(@NonNull String[] permissions, int requestCode) {
        mInstrumentationProvider.requestPermissions(checkNotNull(permissions), requestCode);
    }

    protected void startActivity(@NonNull Intent intent) {
        mInstrumentationProvider.startActivity(checkNotNull(intent));
    }

    protected void goToAppSettings() {
        final Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.addCategory(Intent.CATEGORY_DEFAULT);
        intent.setData(Uri.parse(getResources().getString(R.string.package_error_settings,
                getActivityContext().getPackageName())));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        startActivity(intent);
    }

    protected void goToLocationSettings() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        startActivity(intent);
    }

    protected void goToNetworkSettings() {
        Intent intent = new Intent(Settings.ACTION_NETWORK_OPERATOR_SETTINGS);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        startActivity(intent);
    }

    protected void setCurrentRunnable(@Nullable InstrumentationStateRequirement requirement) {
        if (requirement instanceof PermissionRequirement) {
            mCurrentRunnable = mSettingsRunnable;
        } else if (requirement instanceof LocationProviderRequirement) {
            mCurrentRunnable = mLocationRunnable;
        } else {
            mCurrentRunnable = mNetworkRunnable;
        }
    }
}