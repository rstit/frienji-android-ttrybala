package com.ripple.frienji.ui.util.validation;

import android.support.annotation.Nullable;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

/**
 * @author kamil ratajczak
 * @since 02.08.2016
 */
public class PhoneNumberValidator {

    public boolean validate(@Nullable String countryCode, @Nullable String phoneNumberValue) throws NumberParseException {

        if (countryCode == null || phoneNumberValue == null) {
            return false;
        }

        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

        Phonenumber.PhoneNumber phoneNumber;

        String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(countryCode.replace("+", "")));
        phoneNumber = phoneNumberUtil.parse(phoneNumberValue, isoCode);

        if (!phoneNumberUtil.isValidNumber(phoneNumber)) {
            return phoneNumberUtil.isPossibleNumberWithReason(phoneNumber) == PhoneNumberUtil.ValidationResult.IS_POSSIBLE;
        }

        return true;
    }
}