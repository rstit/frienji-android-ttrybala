package com.ripple.frienji.ui.model.activity;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.RowLeftChatMessageBinding;
import com.ripple.frienji.databinding.RowRightChatMessageBinding;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.conversation.Conversation;
import com.ripple.frienji.model.message.ChatMessage;
import com.ripple.frienji.model.message.Message;
import com.ripple.frienji.model.page.PagePaginator;
import com.ripple.frienji.model.request.CreateConversationRequest;
import com.ripple.frienji.model.response.ConversationMessagesResponse;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.ui.binding.ObservableString;
import com.ripple.frienji.ui.model.RowViewModel;
import com.ripple.frienji.ui.model.row.ChatLeftMessageRowViewModel;
import com.ripple.frienji.ui.model.row.ChatMessageRowViewModel;
import com.ripple.frienji.ui.model.row.ChatRightMessageRowViewModel;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.util.MultiViewAdapter;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import rx.Observable;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 12.08.2016
 */
public class ConversationViewModel extends BaseMessageViewModel {
    public static final int MINIMAL_MESSAGES_NUMBER = 15;

    public final ObservableInt mProgressBarType = new ObservableInt(PROGRESS_BAR_LOADING);
    public final ObservableBoolean mEnableButtons = new ObservableBoolean(true);

    public final MultiViewAdapter mMessagesAdapter;
    public final PagePaginatorScrollListener mPagePaginatorScrollListener = new ConversationPagePaginatorScrollListener();
    public final ObservableString mUserName = new ObservableString();
    public final ObservableBoolean mShowNoResultsInfo = new ObservableBoolean();

    @Inject
    public NavigationHandler mNavigator;

    public final List<RowViewModel> mModels = new ArrayList<>();

    protected UserProfile mFrienjiUserProfile;
    protected UserProfile mMyOwnUserProfile;

    private PagePaginator mPagePaginator;
    private long mConversationId;
    private long mUserId;
    private boolean mGoToBottomOfMessageList;

    @Inject
    public ConversationViewModel() {
        mMessagesAdapter = new MultiViewAdapter(mModels);
        registerLeftChatRowView();
        registerRightChatRowView();
    }

    protected void showCoachmarkIfNecessary() {
        mShowCoachMark.set(!mAppSettings.shouldSkipConversationCoachMark());
    }

    public long getConversationUserId() {
        return mUserId;
    }

    public void addChatMessage(@NonNull ChatMessage chatMessage) {
        mShowNoResultsInfo.set(false);
        appendMessage(createChatMessageRowViewModel(chatMessage));
    }

    public void loadUserProfile(@NonNull UserProfile userProfile) {
        mMyOwnUserProfile = createMyOwnUserProfile();
        mFrienjiUserProfile = checkNotNull(userProfile);

        showCoachmarkIfNecessary();

        mUserId = userProfile.id();
        mAppSettings.setConversationUserProfileId(mUserId);

        fillUserData(checkNotNull(userProfile));
    }

    public void loadConversation() {
        createConversation(CreateConversationRequest.builder().recipientId(mUserId).build());
    }

    private void loadMessages(long conversationId) {
        subscribe(mApi.loadConversationMessages(conversationId)
                .doOnNext(this::savePagePaginator)
                .flatMap(conversationMessagesResponse -> Observable.from(conversationMessagesResponse.messages()))
                .map(this::createChatMessageRowViewModel)
                .toList()
                .doOnSubscribe(() -> {
                    mHideLoadingBar.set(false);
                    mProgressBarType.set(PROGRESS_BAR_LOADING);
                    mEnableButtons.set(false);
                })
                .doOnTerminate(() -> {
                    mHideLoadingBar.set(true);
                    mEnableButtons.set(true);
                })
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleLoadMessagesSuccess, this::handleLoadMessagesApiError));
    }

    private void loadMessages(long conversationId, int page) {
        subscribe(mApi.loadConversationMessages(conversationId, page)
                .doOnNext(this::savePagePaginator)
                .flatMap(conversationMessagesResponse -> Observable.from(conversationMessagesResponse.messages()))
                .map(this::createChatMessageRowViewModel)
                .toList()
                .doOnSubscribe(() -> {
                    mHideLoadingBar.set(false);
                    mProgressBarType.set(PROGRESS_BAR_LOADING);
                    mEnableButtons.set(false);
                })
                .doOnTerminate(() -> {
                    mHideLoadingBar.set(true);
                    mEnableButtons.set(true);
                })
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleLoadCommentsPageSuccess, this::handleLoadMessagesApiError));
    }

    private @NonNull Message rebuildMessage(@NonNull Message message) {
        return Message.builder().message(message.message()).
                messageImageData(message.messageImageData()).isChatMesssage(true).location(message.location()).build();
    }

    private void createConversation(@NonNull CreateConversationRequest createConversationRequest) {
        subscribe(mApi.createConversation(checkNotNull(createConversationRequest))
                .doOnSubscribe(() -> {
                    mHideLoadingBar.set(false);
                    mProgressBarType.set(PROGRESS_BAR_LOADING);
                    mEnableButtons.set(false);
                })
                .doOnTerminate(() -> {
                    mHideLoadingBar.set(true);
                    mEnableButtons.set(true);
                })
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleCreateConversationSuccess, this::handleLoadMessagesApiError));
    }

    protected void fillUserData(@NonNull UserProfile userProfile) {
        mUserName.set(userProfile.userName());
    }

    private void handleSendMessageSuccess(@NonNull ChatMessageRowViewModel messageRowViewModel) {
        mShowNoResultsInfo.set(false);
        clearNewMessageData();
        appendMessage(checkNotNull(messageRowViewModel));
    }

    private void handleSendMessageError(@Nullable Throwable throwable) {
        handleApiError(throwable, R.string.error_send_user_post);
    }

    private void handleLoadMessagesSuccess(@NonNull List<ChatMessageRowViewModel> messages) {
        if (messages.isEmpty()) {
            mShowNoResultsInfo.set(true);
        }
        fillMessages(messages);
        goToBottom();

        if (mPagePaginator.currentPage() > 1 && messages.size() < MINIMAL_MESSAGES_NUMBER) {
            mGoToBottomOfMessageList = true;
            mPagePaginatorScrollListener.enableleLoading();
            loadMessages(mConversationId, mPagePaginator.prevPage());
        }
    }

    private void handleLoadCommentsPageSuccess(@NonNull List<ChatMessageRowViewModel> messages) {
        mPagePaginatorScrollListener.disableLoading();
        appendMessages(checkNotNull(messages));

        if (mGoToBottomOfMessageList) {
            goToBottom();
        }
    }

    private void handleCreateConversationSuccess(@NonNull Conversation conversation) {
        mConversationId = conversation.id();
        loadMessages(conversation.id());
    }

    public void handleLoadMessagesApiError(@Nullable Throwable throwable) {
        handleApiError(throwable, R.string.error_cannot_load_user_messages);
    }

    private void savePagePaginator(@NonNull ConversationMessagesResponse conversationMessagesResponse) {
        mPagePaginator = conversationMessagesResponse.pagePaginator();
    }

    @Override
    public @NonNull NavigationHandler getNavigationHandler() {
        return mNavigator;
    }

    @Override
    public void onSendMessage(@NonNull Message message) {
        subscribe(mApi.sendConversationMessage(mConversationId, rebuildMessage(checkNotNull(message)).buildMultipartBodyParts())
                .map(this::createChatMessageRowViewModel)
                .doOnSubscribe(() -> {
                    mHideLoadingBar.set(false);
                    mProgressBarType.set(PROGRESS_BAR_SENDING);
                    mEnableButtons.set(false);
                })
                .doOnTerminate(() -> {
                    mHideLoadingBar.set(true);
                    mEnableButtons.set(true);
                })
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleSendMessageSuccess, this::handleSendMessageError));
    }

    @Override
    public @NonNull String getCoachMarkTitle() {
        return mContext.getString(R.string.conversation_coach_mark_title);
    }

    @Override
    public @NonNull String getCoachMarkContent() {
        return mContext.getString(R.string.conversation_coach_mark_content);
    }

    @Override
    public void hideCoachMark() {
        mAppSettings.hideConversationCoachMark();
        mShowCoachMark.set(false);
    }

    private @NonNull UserProfile createMyOwnUserProfile() {
        Avatar avatar = Avatar.builder().name(mAppSettings.loadUserAvatarName().get())
                .type(mAppSettings.loadUserAvatarType().get()).build();

        UserProfile userProfile = UserProfile.builder().avatar(avatar)
                .coverImage(mAppSettings.loadUserCoverImage().get())
                .userName(mAppSettings.loadUserName().get())
                .id(mAppSettings.loadUserProfileId().get())
                .whoYouAre(mAppSettings.loadUUserWhoYouAre().get())
                .build();
        userProfile.setMyOwnProfile(true);

        return userProfile;
    }

    private void registerLeftChatRowView() {
        mMessagesAdapter.addViewHolderCreator(ChatLeftMessageRowViewModel.class, (inflater, parent) -> {
            final RowLeftChatMessageBinding binding = DataBindingUtil.inflate(inflater,
                    R.layout.row_left_chat_message, parent, false);

            return new MultiViewAdapter.BaseViewHolder<ChatLeftMessageRowViewModel>(binding.getRoot()) {
                @Override
                public void bind(@NonNull ChatLeftMessageRowViewModel model) {
                    binding.setModel(model);
                }
            };
        });
    }

    private @NonNull ChatMessageRowViewModel createChatMessageRowViewModel(@NonNull ChatMessage chatMessage) {
        if (chatMessage.myOwnMessage()) {
            return ChatRightMessageRowViewModel.from(mContext, mAvatarReader, mApi, mNavigator,
                    mObservableTransformers, mIntentProvider, chatMessage, mMyOwnUserProfile);
        } else {
            return ChatLeftMessageRowViewModel.from(mContext, mAvatarReader, mApi, mNavigator,
                    mObservableTransformers, mIntentProvider, chatMessage, mFrienjiUserProfile);
        }
    }

    private void registerRightChatRowView() {
        mMessagesAdapter.addViewHolderCreator(ChatRightMessageRowViewModel.class, (inflater, parent) -> {
            final RowRightChatMessageBinding binding = DataBindingUtil.inflate(inflater,
                    R.layout.row_right_chat_message, parent, false);

            return new MultiViewAdapter.BaseViewHolder<ChatRightMessageRowViewModel>(binding.getRoot()) {
                @Override
                public void bind(@NonNull ChatRightMessageRowViewModel model) {
                    binding.setModel(model);
                }
            };
        });
    }

    public void fillMessages(@NonNull List<ChatMessageRowViewModel> models) {
        checkNotNull(models);
        mModels.clear();
        mModels.addAll(models);
        notifyChange();
    }

    private void appendMessages(@NonNull List<ChatMessageRowViewModel> models) {
        mModels.addAll(0, models);
        mMessagesAdapter.notifyItemRangeInserted(0, models.size());
    }

    private void appendMessage(@NonNull RowViewModel message) {
        mModels.add(checkNotNull(message));
        mMessagesAdapter.getRecyclerView().scrollToPosition(mMessagesAdapter.getItemCount() - 1);
    }

    private void goToBottom() {
        mGoToBottomOfMessageList = false;
        mMessagesAdapter.getRecyclerView().scrollToPosition(mMessagesAdapter.getItemCount() - 1);
    }

    public class ConversationPagePaginatorScrollListener extends PagePaginatorScrollListener {

        public ConversationPagePaginatorScrollListener() {
            super(PagePaginatorScrollListener.SCROLL_UP_MODE);
        }

        @Override public void loadElements() {
            if (mPagePaginator.currentPage() > 1) {
                mIsLoading = true;
                loadMessages(mConversationId, mPagePaginator.prevPage());
            }
        }
    }
}