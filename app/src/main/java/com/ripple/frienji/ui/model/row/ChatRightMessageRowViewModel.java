package com.ripple.frienji.ui.model.row;

import android.content.Context;
import android.support.annotation.NonNull;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.model.message.ChatMessage;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.net.FrienjiApi;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.util.rx.ObservableTransformers;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Kamil Ratajczak
 * @since 08.11.16
 */

public class ChatRightMessageRowViewModel extends ChatMessageRowViewModel {

    private ChatRightMessageRowViewModel(@NonNull Context context, @NonNull NavigationHandler navigationHandler,
                                         @NonNull AvatarReader avatarReader, @NonNull FrienjiApi frienjiApi,
                                         @NonNull ObservableTransformers observableTransformers, @NonNull IntentProvider intentProvider,
                                         @NonNull ChatMessage message, @NonNull UserProfile userProfile) {
        super(checkNotNull(context), checkNotNull(navigationHandler), checkNotNull(avatarReader), checkNotNull(frienjiApi),
                checkNotNull(observableTransformers), checkNotNull(intentProvider), checkNotNull(message), checkNotNull(userProfile));
    }

    public static @NonNull ChatMessageRowViewModel from(@NonNull Context context, @NonNull AvatarReader avatarReader, @NonNull FrienjiApi frienjiApi,
                                                        @NonNull NavigationHandler navigationHandler, @NonNull ObservableTransformers observableTransformers, @NonNull IntentProvider intentProvider,
                                                        @NonNull ChatMessage message, @NonNull UserProfile userProfile) {
        return new ChatRightMessageRowViewModel(checkNotNull(context), checkNotNull(navigationHandler), checkNotNull(avatarReader), checkNotNull(frienjiApi),
                checkNotNull(observableTransformers), checkNotNull(intentProvider), checkNotNull(message), checkNotNull(userProfile));
    }

    public void openUserProfile() {
        mNavigationHandler.startActivity(mIntentProvider.createMyOwnUserProfile());
    }
}