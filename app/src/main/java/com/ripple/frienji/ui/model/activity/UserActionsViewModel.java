package com.ripple.frienji.ui.model.activity;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.RowUserActionBinding;
import com.ripple.frienji.model.page.PagePaginator;
import com.ripple.frienji.model.response.FriejiActiviesResponse;
import com.ripple.frienji.ui.model.row.UserActionRowViewModel;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import rx.Observable;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 11.08.2016
 */
public class UserActionsViewModel extends CoachMarkViewModel {
    public final UserActionListAdapter mUserActionsAdapter = new UserActionListAdapter();
    public final PagePaginatorScrollListener mPagePaginatorScrollListener = new UserActionsPagePaginatorScrollListener();
    private PagePaginator mPagePaginator;

    @Inject
    public NavigationHandler mNavigationHandler;

    @Inject
    public IntentProvider mIntentProvider;

    @Inject
    public UserActionsViewModel() {
    }

    public void fillUserActions() {
        showCoachmarkIfNecessary();
        subscribe(mApi.loadUserActions()
                .doOnNext(this::savePagePaginator)
                .flatMap(response -> Observable.from(response.frienjiActivities()))
                .map(userAction -> UserActionRowViewModel.from(mContext, mAvatarReader, mDateTimeUtil,
                        mNavigationHandler, mIntentProvider, userAction))
                .toList()
                .doOnSubscribe(() -> mHideLoadingBar.set(false))
                .doOnTerminate(() -> mHideLoadingBar.set(true))
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleLoadUserActionsSuccess, this::handleLoadUserActionsError));
    }

    public void fillUserActions(int page) {
        subscribe(mApi.loadUserActions(page)
                .doOnNext(this::savePagePaginator)
                .flatMap(response -> Observable.from(response.frienjiActivities()))
                .map(userAction -> UserActionRowViewModel.from(mContext, mAvatarReader, mDateTimeUtil,
                        mNavigationHandler, mIntentProvider, userAction))
                .toList()
                .doOnSubscribe(() -> mHideLoadingBar.set(false))
                .doOnTerminate(() -> mHideLoadingBar.set(true))
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleLoadUserActionsPageSuccess, this::handleLoadUserActionsError));
    }

    protected void showCoachmarkIfNecessary() {
        mShowCoachMark.set(!mAppSettings.shouldSkipUserActionsCoachMark());
    }

    protected void handleLoadUserActionsSuccess(@NonNull List<UserActionRowViewModel> actions) {
        mUserActionsAdapter.fillData(checkNotNull(actions));
    }

    private void handleLoadUserActionsPageSuccess(@NonNull List<UserActionRowViewModel> messages) {
        mPagePaginatorScrollListener.disableLoading();
        mUserActionsAdapter.appendData(checkNotNull(messages));
    }

    private void handleLoadUserActionsError(@Nullable Throwable throwable) {
        mPagePaginatorScrollListener.disableLoading();
        handleApiError(throwable, R.string.error_cannot_load_user_actions);
    }

    @Override
    public @NonNull String getCoachMarkTitle() {
        return mContext.getString(R.string.user_actions_coach_mark_title);
    }

    @Override
    public @NonNull String getCoachMarkContent() {
        return mContext.getString(R.string.user_actions_coach_mark_content);
    }

    @Override
    public @NonNull void hideCoachMark() {
        mAppSettings.hideUserActionsCoachMark();
        mShowCoachMark.set(false);
    }

    private void savePagePaginator(@NonNull FriejiActiviesResponse friejiActiviesResponse) {
        mPagePaginator = friejiActiviesResponse.pagePaginator();
    }

    public class UserActionListAdapter extends RecyclerView.Adapter<UserActionListAdapter.UserActionListViewHolder> {

        public final List<UserActionRowViewModel> mModels = new ArrayList<>();

        @Override
        public UserActionListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());

            RowUserActionBinding rowUserActionBinding = DataBindingUtil.inflate(inflater, R.layout.row_user_action, parent, false);

            return new UserActionListViewHolder(rowUserActionBinding);
        }

        @Override
        public void onBindViewHolder(UserActionListViewHolder holder, int position) {
            holder.bind(mModels.get(position));
        }

        @Override
        public int getItemCount() {
            return mModels.size();
        }

        public void fillData(@NonNull List<UserActionRowViewModel> models) {
            checkNotNull(models);
            mModels.clear();
            mModels.addAll(models);
            notifyChange();
        }

        public void appendData(@NonNull List<UserActionRowViewModel> models) {
            mModels.addAll(checkNotNull(models));
            notifyDataSetChanged();
        }

        public class UserActionListViewHolder extends RecyclerView.ViewHolder {

            private RowUserActionBinding mViewDataBinding;

            public UserActionListViewHolder(@NonNull RowUserActionBinding viewDataBinding) {
                super(viewDataBinding.getRoot());
                mViewDataBinding = viewDataBinding;
            }

            public void bind(@NonNull UserActionRowViewModel model) {
                mViewDataBinding.setModel(checkNotNull(model));
                mViewDataBinding.executePendingBindings();
            }
        }
    }

    public class UserActionsPagePaginatorScrollListener extends PagePaginatorScrollListener {
        @Override public void loadElements() {
            if (mPagePaginator.hasNext()) {
                mIsLoading = true;
                fillUserActions(mPagePaginator.nextPage());
            }
        }
    }
}