package com.ripple.frienji.ui.navigation;

import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import com.github.droibit.rxactivitylauncher.RxLauncher;
import com.ripple.frienji.ui.view.activity.BaseActivity;
import java.util.Map;
import rx.functions.Action1;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Marcin Przepiórkowski
 * @since 12.05.2016
 */
public class NavigationHandlerImpl implements NavigationHandler {
    private static final String MAILTO_URI_STRING = "mailto:";
    private static final String ACTIVITY_REQUEST_CODE = "ACTIVITY_REQUEST_CODE";
    private static final int ACTIVITY_NO_REQUEST_CODE = -1;

    protected BaseActivity mActivity;

    protected RxLauncher mLauncher = RxLauncher.getInstance();

    public NavigationHandlerImpl(@NonNull BaseActivity activity) {
        mActivity = checkNotNull(activity);
    }

    @Override
    public void finish() {
        mActivity.finish();
    }

    @Override
    public void finishAffinity() {
        mActivity.finishAffinity();
    }

    @Override
    public void finishWithResult(int resultCode, @NonNull Intent intent) {
        mActivity.setResult(resultCode, checkNotNull(intent));
        mActivity.finish();
    }

    @Override
    public void startActivity(@NonNull Intent intent) {
        mActivity.startActivity(checkNotNull(intent));
    }

    @Override
    public void startActivities(@NonNull Intent[] intents) {
        mActivity.startActivities(checkNotNull(intents));
    }

    @Override
    public void startActivity(@NonNull Class<? extends BaseActivity> activity) {
        Intent intent = new Intent(mActivity, checkNotNull(activity));
        startActivity(intent);
    }

    @Override
    public void startActivityForResult(@NonNull Intent intent, int requestCode) {
        mActivity.startActivityForResult(checkNotNull(intent), requestCode);
    }

    @Override
    public void startActivityForResult(@NonNull Class<? extends BaseActivity> cls, int requestCode) {
        Intent intent = new Intent(mActivity, checkNotNull(cls));
        intent.putExtra(ACTIVITY_REQUEST_CODE, requestCode);
        mActivity.startActivityForResult(intent, requestCode);
    }

    @Override
    public void startActivityForResult(@NonNull Intent intent, int requestCode, final Action1<Intent> onNext, final Action1<Throwable> onError) {

        mLauncher.from(mActivity)
                .startActivityForResult(intent, requestCode, null).subscribe(activityResult -> {
            if (onNext != null) {
                onNext.call(activityResult.data);
            }
        });
    }

    public void finishWithResult(int resultCode) {
        mActivity.setResult(resultCode);
        finish();
    }

    public void finishWithResult(int resultCode, @NonNull Map<String, Parcelable> extra) {
        Intent results = new Intent();
        for (String key : extra.keySet()) {
            results.putExtra(key, extra.get(key));
        }
        mActivity.setResult(resultCode, results);

        finish();
    }

    @Override
    public void showDialog(@NonNull DialogFragment dialogFragment) {
        dialogFragment.show(mActivity.getSupportFragmentManager(), dialogFragment.getClass().getName());
    }

    @Override
    public void handleOnActivityResult(int requestCode, int resultCode, Intent intent) {
        mLauncher.activityResult(requestCode, resultCode, intent);
    }

    @Override
    public boolean isStartedForResult() {
        return mActivity.getCallingActivity() != null;
    }

    @Override
    public int getRequestCode() {
        return mActivity.getIntent().getIntExtra(ACTIVITY_REQUEST_CODE, ACTIVITY_NO_REQUEST_CODE);
    }

    @Override
    public void openUrlLink(@NonNull String urlLink) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(checkNotNull(urlLink)));
        startActivity(browserIntent);
    }

    @Override
    public void openEmailActivity(@NonNull String emailAddress, @NonNull String emailSubject) {
        String[] addresses = new String[]{checkNotNull(emailAddress)};

        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.setData(Uri.parse(MAILTO_URI_STRING));
        intent.putExtra(Intent.EXTRA_SUBJECT, checkNotNull(emailSubject));
        startActivity(intent);
    }
}