package com.ripple.frienji.ui.model;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableInt;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.TextInputLayout;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.common.base.Strings;
import com.google.common.eventbus.EventBus;
import com.google.gson.Gson;
import com.ripple.frienji.R;
import com.ripple.frienji.ar.BubbleSurfaceView;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.frienji.Frienji;
import com.ripple.frienji.net.FrienjiApi;
import com.ripple.frienji.settings.AppSettings;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.util.DateTimeUtil;
import com.ripple.frienji.ui.view.activity.BaseActivity;
import com.ripple.frienji.ui.view.custom.RadarView;
import com.ripple.frienji.util.ErrorManager;
import com.ripple.frienji.util.rx.ObservableTransformers;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import retrofit2.adapter.rxjava.HttpException;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Marcin Przepiórkowski
 * @since 08.03.2016
 */
public class BaseViewModel extends BaseObservable {
    public final static int PROGRESS_BAR_LOADING = 1;
    public final static int PROGRESS_BAR_SENDING = 2;
    public final static int PROGRESS_BAR_REMOVING = 3;

    private static final int ATTACHE_IMAGE_WIDTH = 800;
    private static final int ATTACHE_IMAGE_HEIGHT = 600;
    private static final int ATTACHE_IMAGE_QUALITY = 80;
    private static final int ZOOM = 15;
    protected final CompositeSubscription mSubscriptions = new CompositeSubscription();
    public ObservableBoolean mHideLoadingBar = new ObservableBoolean();

    public DateTimeUtil mDateTimeUtil = new DateTimeUtil();

    @Inject
    @Named("activityContext")
    public Context mContext;

    @Inject
    public FrienjiApi mApi;

    @Inject
    public EventBus mEventBus;

    @Inject
    public IntentProvider mIntentProvider;

    @Inject
    public AvatarReader mAvatarReader;

    @Inject
    public AppSettings mAppSettings;

    @Inject
    public ObservableTransformers mObservableTransformers;

    @Inject
    public Gson mGson;

    @Inject
    public ErrorManager mErrorManager;

    @BindingAdapter("editError")
    public static void setEditError(@NonNull TextInputLayout view, @Nullable String error) {
        view.setErrorEnabled(true);
        view.setError(error);
    }

    @BindingAdapter("editErrorEnabled")
    public static void setEditError(@NonNull TextInputLayout view, boolean enabled) {
        view.setErrorEnabled(enabled);
    }

    @BindingAdapter("imageUrl")
    public static void bitmapSource(@NonNull ImageView view, @Nullable String uri) {
        if (uri != null) {
            Glide.with(view.getContext()).load(uri).centerCrop().into(view);
        } else {
            view.setImageDrawable(null);
        }
    }

    @BindingAdapter("circleImage")
    public static void circleImageSource(@NonNull ImageView view, @NonNull String imageUrl) {
        Glide.with(view.getContext()).load(imageUrl).asBitmap().centerCrop().into(new BitmapImageViewTarget(view) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(view.getContext().getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                view.setImageDrawable(circularBitmapDrawable);
            }
        });
    }

    @BindingAdapter("imageResource")
    public static void setImageResource(@NonNull ImageView imageView, @DrawableRes int resource) {
        imageView.setImageResource(resource);
    }

    @BindingAdapter({"avatarName", "avatarType", "avatarReader"})
    public static void setAvatarImage(@NonNull ImageView imageView, @NonNull String avatarName, int avatarType, @NonNull AvatarReader avatarReader) {
        String path = null;
        if (avatarType == Avatar.IMAGE_AVATAR) {
            path = avatarReader.getImageAvatarPath(avatarName);
        } else if (avatarType == Avatar.GIF_AVATAR) {
            path = avatarReader.getGifAvatarPath(avatarName);
        }

        Glide.with(imageView.getContext()).load(path)
                .bitmapTransform(new CropCircleTransformation(imageView.getContext()))
                .centerCrop().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(imageView);
    }

    @BindingAdapter({"avatarName", "avatarType", "avatarReader"})
    public static void setAvatarEmoticon(@NonNull TextView textView, @Nullable String avatarName, int avatarType, @NonNull AvatarReader avatarReader) {
        if (avatarType == Avatar.EMOTICON_AVATAR && avatarName != null) {
            textView.setText(avatarReader.getEmoticonAvatar(avatarName));
        }
    }

    @BindingAdapter({"azimuth", "pitch"})
    public static void updateCoordinates(@NonNull BubbleSurfaceView surfaceView, int azimuth, int pitch) {
        surfaceView.updateCoordinates(azimuth, pitch);
    }

    @BindingAdapter("displayOrientation")
    public static void setDisplayOrientation(@NonNull BubbleSurfaceView surfaceView, @NonNull ObservableInt orientation) {
        surfaceView.setDisplayOrientation(orientation.get());
    }

    @BindingAdapter({"frienjisList", "currentLocation"})
    public static void addFrienjiesToAR(@NonNull BubbleSurfaceView surfaceView,
                                     @NonNull List<Frienji> frienjis,
                                     @Nullable Location currentLocation) {
        if (currentLocation != null && !frienjis.isEmpty()) {
            surfaceView.addFrienjis(new ArrayList<>(frienjis), currentLocation);
        }
    }

    @BindingAdapter({"compassAzimuth"})
    public static void updateCompasAzimuth(@NonNull RadarView radarView,
                                           float mAzimuth) {
        radarView.updateRotation(mAzimuth);
    }

    @BindingAdapter("mapLocation")
    public static void setMapLocation(@NonNull FrameLayout view, @Nullable LatLng latLng) {
        if (latLng != null) {
            GoogleMapOptions googleMapOptions = new GoogleMapOptions();
            googleMapOptions.mapType(GoogleMap.MAP_TYPE_NORMAL)
                    .liteMode(true)
                    .compassEnabled(false)
                    .rotateGesturesEnabled(false)
                    .mapToolbarEnabled(false)
                    .camera(new CameraPosition(latLng, ZOOM, 0, 0))
                    .tiltGesturesEnabled(false);

            MapView mapView = new MapView(view.getContext(), googleMapOptions);
            mapView.onCreate(new Bundle());
            mapView.getMapAsync(map -> map.addMarker(new MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_pin))));

            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
            view.addView(mapView, params);
            view.setVisibility(View.VISIBLE);
        }
    }

    @BindingAdapter("visibleIf")
    public static void changeViewVisibility(@NonNull View view, @NonNull Boolean bool) {
        view.setVisibility(bool ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter("setTextWithLink")
    public static void setTextWithLink(@NonNull TextView view, @NonNull String text) {
        view.setText(Html.fromHtml(text));
        view.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @BindingAdapter({"text", "parameter"})
    public static void setTextWithParameter(@NonNull TextView view, @Nullable String text, @Nullable String parameter) {
        if (!Strings.isNullOrEmpty(text) && !Strings.isNullOrEmpty(parameter)) {
            view.setText(String.format(text, parameter));
        }
    }

    @BindingAdapter({"layout_marginRight"})
    public static void setViewMarginRight(@NonNull View view, float maringRight) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(p.leftMargin, p.topMargin, (int) maringRight, p.bottomMargin);
            view.requestLayout();
        }
    }

    @BindingAdapter({"layout_marginLeft"})
    public static void setViewMarginLeft(@NonNull View view, float maringLeft) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins((int) maringLeft, p.topMargin, p.rightMargin, p.bottomMargin);
            view.requestLayout();
        }
    }

    @BindingAdapter({"layout_gravity"})
    public static void setLayoutGravity(@NonNull LinearLayout linearLayout, int gravity) {
        if (linearLayout.getLayoutParams() instanceof FrameLayout.LayoutParams) {
            ((FrameLayout.LayoutParams) linearLayout.getLayoutParams()).gravity = gravity;
        }
    }

    protected void subscribe(@NonNull Subscription s) {
        mSubscriptions.add(checkNotNull(s));
    }

    public void handleApiError(@Nullable Throwable throwable,
                               @StringRes int errorMessageId) {
        if (throwable instanceof IOException) {
            mEventBus.post(new BaseActivity.ShowSnackbarEvent(R.string.error_no_internet));
        } else {
            mEventBus.post(new BaseActivity.ShowSnackbarEvent(errorMessageId));

            if (throwable != null) {
                throwable.printStackTrace();
            }
        }
    }

    public boolean isUnauthorizedError(@NonNull Throwable throwable) {
        return throwable instanceof HttpException &&
                ((HttpException) throwable).code() == HttpURLConnection.HTTP_UNAUTHORIZED;
    }

    public @NonNull String getEmoticon(@NonNull String emoticonName) {
        return mAvatarReader.getEmoticonAvatar(checkNotNull(emoticonName));
    }

    protected void makeImageSmaller(@NonNull String imageUri, @NonNull SimpleTarget<byte[]> simpleTarget) {
        Glide.with(mContext)
                .load(checkNotNull(imageUri))
                .asBitmap()
                .toBytes(Bitmap.CompressFormat.JPEG, ATTACHE_IMAGE_QUALITY)
                .override(ATTACHE_IMAGE_WIDTH, ATTACHE_IMAGE_HEIGHT)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(checkNotNull(simpleTarget));
    }

    protected @NonNull Intent createImageIntent(@NonNull String imagePath) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse(imagePath), "image/*");
        return intent;
    }

    protected @Nullable Integer getIntegerNumber(@Nullable String str) {
        if (Strings.isNullOrEmpty(str)) {
            return null;
        }

        try {
            return Integer.valueOf(str);
        } catch (NumberFormatException nfe) {
            return null;
        }
    }

    protected void closeKeyboard() {
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (mContext instanceof Activity) {
            View view = ((Activity) mContext).getCurrentFocus();
            if (view != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }


    public abstract class PagePaginatorScrollListener extends RecyclerView.OnScrollListener {
        public static final int SCROLL_DOWN_MODE = 1;
        public static final int SCROLL_UP_MODE = 2;

        private static final int LOAD_THRESHOLD = 5;
        protected boolean mIsLoading;

        private int mScrollMode;

        public PagePaginatorScrollListener() {
            mScrollMode = SCROLL_DOWN_MODE;
        }

        public PagePaginatorScrollListener(int scrollMode) {
            mScrollMode = scrollMode;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            if (dy >= 0 && mScrollMode == SCROLL_DOWN_MODE) { //check if scroll down

                int lastPosition = 0;
                int totalNumberOfElements;

                totalNumberOfElements = recyclerView.getLayoutManager().getItemCount();

                if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                    lastPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition();
                }

                if (LOAD_THRESHOLD >= (totalNumberOfElements - lastPosition) && !mIsLoading) {
                    loadElements();
                }
            } else if (dy < 0 && mScrollMode == SCROLL_UP_MODE) { //check if scroll up

                int firstVisiblePosition = 0;

                if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
                    firstVisiblePosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                }

                if (LOAD_THRESHOLD >= firstVisiblePosition && !mIsLoading) {
                    loadElements();
                }
            }
        }

        public void disableLoading() {
            mIsLoading = false;
        }

        public void enableleLoading() {
            mIsLoading = true;
        }

        public abstract void loadElements();
    }
}