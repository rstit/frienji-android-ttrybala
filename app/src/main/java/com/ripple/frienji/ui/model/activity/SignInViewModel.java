package com.ripple.frienji.ui.model.activity;

import javax.inject.Inject;

/**
 * @author kamil ratajczak
 * @since 27.09.2016
 */
public class SignInViewModel extends BaseSignViewModel {

    @Inject
    public SignInViewModel() {}

    @Override
    public void send() {
        if (validateInput(mPhoneCountryCode.get(), mPhoneNumber.get())) {
            sendRequestConfirmCodeBody(buildRequestConfirmCodeBody(mPhoneCountryCode.get(), mPhoneNumber.get()));
        }
    }
}