package com.ripple.frienji.ui.view.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.google.common.eventbus.Subscribe;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.ActivityUserProfileBinding;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.ViewModelModule;
import com.ripple.frienji.di.module.ViewModule;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.ui.model.activity.UserProfileViewModel;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.view.dialog.DeletePostConfirmDialogFragment;
import javax.inject.Inject;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 30.07.2016
 */
public class UserProfileActivity extends BaseActivity {
    private static final String EXTRA_USER_PROFILE = "userProfile";

    @Inject
    protected UserProfileViewModel mModel;

    @Inject
    protected NavigationHandler navigationHandler;

    public static @NonNull Intent createIntent(@NonNull Context context) {
        return new Intent(checkNotNull(context), UserProfileActivity.class);
    }

    public static @NonNull Intent createIntent(@NonNull UserProfile userProfile, @NonNull Context context) {
        Intent intent = createIntent(context);
        intent.putExtra(EXTRA_USER_PROFILE, checkNotNull(userProfile));
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrienjiApplication app = (FrienjiApplication) getApplication();

        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new ViewModule(this)).plus(new ViewModelModule()).inject(this);

        ActivityUserProfileBinding mBinding = DataBindingUtil.setContentView(this, R.layout.activity_user_profile);
        mBinding.setModel(mModel);

        fetchExtras();
    }

    @Subscribe
    public void onDeleteMessageEvent(@NonNull UserProfileViewModel.DeleteMessageEvent event) {
        DeletePostConfirmDialogFragment.createInstance(event.getId(), event.getPosition()).show(getSupportFragmentManager(), null);
    }

    @Subscribe
    public void onDeletePostConfirmEvent(@NonNull DeletePostConfirmDialogFragment.DeletePostConfirmEvent event) {
        mModel.deleteMessage(event.getPostId(), event.getPosition());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        navigationHandler.handleOnActivityResult(requestCode, resultCode, intent);
    }

    @Override public void onBackPressed() {
        if (mModel.hasNewMessageContent()) {
            mModel.clearNewMessageData();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mModel.refreshUserProfileData();
    }

    private void fetchExtras() {
        Bundle extras = getIntent().getExtras();

        if (extras != null && extras.containsKey(EXTRA_USER_PROFILE)) {
            UserProfile userProfile = extras.getParcelable(EXTRA_USER_PROFILE);
            if (userProfile != null) {
                mModel.loadFrienjiProfileData(userProfile);
            }
        } else {
            mModel.loadUserOwnProfileData();
        }
    }
}