package com.ripple.frienji.ui.model.activity;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.RowMessageBinding;
import com.ripple.frienji.model.message.FrienjiPost;
import com.ripple.frienji.model.message.Message;
import com.ripple.frienji.model.page.PagePaginator;
import com.ripple.frienji.model.response.FrienjiPostsResponse;
import com.ripple.frienji.model.response.StatusResponse;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.ui.binding.ObservableString;
import com.ripple.frienji.ui.model.row.MessageRowViewModel;
import com.ripple.frienji.ui.model.row.UserProfileMessageRowViewModel;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.view.activity.MySettingsActivity;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import rx.Observable;
import rx.functions.Action1;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 04.08.2016
 */
public class UserProfileViewModel extends BaseMessageViewModel {
    public final ObservableInt mProgressBarType = new ObservableInt(PROGRESS_BAR_LOADING);
    public final ObservableBoolean mEnableButtons = new ObservableBoolean(true);

    public final ObservableString mUserName = new ObservableString();
    public final ObservableString mWhoYouAre = new ObservableString();
    public final ObservableString mAvatarName = new ObservableString();
    public final ObservableString mCoverImage = new ObservableString();
    public final ObservableInt mAvatarType = new ObservableInt();
    public final ObservableBoolean mIsMyOwnProfile = new ObservableBoolean();
    public final ObservableBoolean mShowNoResultsInfo = new ObservableBoolean();

    public final PagePaginatorScrollListener mPagePaginatorScrollListener = new UserProfilePagePaginatorScrollListener();
    public final MessagesAdapter mMessagesAdapter = new MessagesAdapter();

    @Inject
    public NavigationHandler mNavigator;

    protected long mUserId;
    protected PagePaginator mPagePaginator;
    protected UserProfile mFrienjiProfile;

    @Inject
    protected IntentProvider mIntentProvider;

    @Inject
    public UserProfileViewModel() {
    }

    public void editUserProfile() {
        mNavigator.startActivity(MySettingsActivity.class);
    }

    public void openChat() {
        mNavigator.startActivity(mIntentProvider.createConversation(mFrienjiProfile));
    }

    public void loadUserOwnProfileData() {
        initUserCoachMark();

        mIsMyOwnProfile.set(true);
        mUserId = mAppSettings.loadUserProfileId().get();

        setUserProfileData();
        fillMessagesList();
    }

    protected void initUserCoachMark() {
        mShowCoachMark.set(!mAppSettings.shouldSkipUserWallCoachMark());
    }

    public void loadFrienjiProfileData(@NonNull UserProfile frienjiProfile) {
        initFrienjiCoachMark();

        mFrienjiProfile = frienjiProfile;
        mUserId = frienjiProfile.id();

        mUserName.set(frienjiProfile.userName());
        mWhoYouAre.set(frienjiProfile.whoYouAre());

        if (frienjiProfile.avatar() != null) {
            mAvatarName.set(frienjiProfile.avatar().name());
            mAvatarType.set(frienjiProfile.avatar().type());
        }

        mCoverImage.set(frienjiProfile.coverImage());

        fillMessagesList();
    }

    public void refreshUserProfileData() {
        if (mIsMyOwnProfile.get()) {
            setUserProfileData();
        }
        fillMessagesList();
    }

    protected void setUserProfileData() {
        mUserName.set(mAppSettings.loadUserName().get());
        mWhoYouAre.set(mAppSettings.loadUUserWhoYouAre().get());
        mAvatarName.set(mAppSettings.loadUserAvatarName().get());
        mAvatarType.set(mAppSettings.loadUserAvatarType().get());

        if (mAppSettings.loadUserCoverImage().isPresent()) {
            mCoverImage.set(mAppSettings.loadUserCoverImage().get());
        }
    }

    protected void initFrienjiCoachMark() {
        mShowCoachMark.set(!mAppSettings.shouldSkipFrienjiWallCoachMark());
    }

    protected void fillMessagesList(int page) {
        subscribe(mApi.loadUserPosts(mUserId, page)
                .doOnNext(this::savePagePaginator)
                .flatMap(frienjiPostsResponse -> Observable.from(frienjiPostsResponse.frienjiPosts()))
                .filter(this::isFrienjiPostDesirable)
                .map(frienjiPost -> UserProfileMessageRowViewModel.from(mContext, mAvatarReader, mApi,
                        mNavigator, mEventBus, mObservableTransformers, mDateTimeUtil, frienjiPost, mUserId))
                .toList()
                .doOnSubscribe(() -> {
                    mHideLoadingBar.set(false);
                    mProgressBarType.set(PROGRESS_BAR_LOADING);
                    mEnableButtons.set(false);
                })
                .doOnTerminate(() -> {
                    mHideLoadingBar.set(true);
                    mEnableButtons.set(true);
                })
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleLoadUserPostsPageSuccess, this::handleLoadUserPostsError));
    }

    protected void fillMessagesList() {
        subscribe(mApi.loadUserPosts(mUserId)
                .doOnNext(this::savePagePaginator)
                .flatMap(frienjiPostsResponse -> Observable.from(frienjiPostsResponse.frienjiPosts()))
                .filter(this::isFrienjiPostDesirable)
                .map(frienjiPost -> UserProfileMessageRowViewModel.from(mContext, mAvatarReader, mApi,
                        mNavigator, mEventBus, mObservableTransformers, mDateTimeUtil, frienjiPost, mUserId))
                .toList()
                .doOnSubscribe(() -> {
                    mHideLoadingBar.set(false);
                    mProgressBarType.set(PROGRESS_BAR_LOADING);
                    mEnableButtons.set(false);
                })
                .doOnTerminate(() -> {
                    mHideLoadingBar.set(true);
                    mEnableButtons.set(true);
                })
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleLoadUserPostsSuccess, this::handleLoadUserPostsError));
    }

    @Override
    public @NonNull String getCoachMarkTitle() {
        if (mIsMyOwnProfile.get()) {
            return mContext.getString(R.string.user_wall_coach_mark_title);
        } else {
            return mContext.getString(R.string.frienji_wall_coach_mark_title);
        }
    }

    @Override
    public @NonNull String getCoachMarkContent() {
        if (mIsMyOwnProfile.get()) {
            return mContext.getString(R.string.user_wall_coach_mark_content);
        } else {
            return mContext.getString(R.string.frienji_wall_coach_mark_content);
        }
    }

    @Override
    public void hideCoachMark() {
        if (mIsMyOwnProfile.get()) {
            mAppSettings.hideUserWallCoachMark();
        } else {
            mAppSettings.hideFrienjiWallCoachMark();
        }
        mShowCoachMark.set(false);
    }

    private void handleSendPostSuccess(@NonNull MessageRowViewModel messageRowViewModel) {
        mShowNoResultsInfo.set(false);
        clearNewMessageData();
        mMessagesAdapter.mModels.add(0, checkNotNull(messageRowViewModel));
        mMessagesAdapter.notifyDataSetChanged();
    }

    private void handleSendPostError(@Nullable Throwable throwable) {
        handleApiError(throwable, R.string.error_send_user_post);
    }

    private void handleLoadUserPostsSuccess(@NonNull List<MessageRowViewModel> messages) {
        if (messages.isEmpty()) {
            mShowNoResultsInfo.set(true);
        }
        mMessagesAdapter.fillMessages(messages);
    }

    private void handleLoadUserPostsPageSuccess(@NonNull List<MessageRowViewModel> messages) {
        mPagePaginatorScrollListener.disableLoading();
        mMessagesAdapter.appendMessages(checkNotNull(messages));
    }

    public void handleLoadUserPostsError(@Nullable Throwable throwable) {
        mPagePaginatorScrollListener.disableLoading();
        handleApiError(throwable, R.string.error_cannot_load_user_messages);
    }

    private void savePagePaginator(@NonNull FrienjiPostsResponse frienjiPostsResponse) {
        mPagePaginator = frienjiPostsResponse.pagePaginator();
    }

    public void deleteMessage(long messageId, int position) {
        subscribe(mApi.deletePost(messageId)
                .compose(mObservableTransformers.networkOperation())
                .subscribe(new DeleteMessageSuccessAction(position), this::handleDeleteError));
    }

    public void handleDeleteError(@Nullable Throwable throwable) {
        handleApiError(throwable, R.string.error_cannot_delete_message);
    }

    @Override
    public @NonNull NavigationHandler getNavigationHandler() {
        return mNavigator;
    }

    private boolean isFrienjiPostDesirable(@NonNull FrienjiPost frienjiPost) {
        return frienjiPost.avatarTrail() == null;
    }

    @Override
    public void onSendMessage(@NonNull Message message) {
        subscribe(mApi.sendUserPost(mUserId, message.buildMultipartBodyParts())
                .map(frienjiPost -> UserProfileMessageRowViewModel.from(mContext, mAvatarReader, mApi,
                        mNavigator, mEventBus, mObservableTransformers, mDateTimeUtil, frienjiPost, mUserId))
                .doOnSubscribe(() -> {
                    mHideLoadingBar.set(false);
                    mProgressBarType.set(PROGRESS_BAR_SENDING);
                    mEnableButtons.set(false);
                })
                .doOnTerminate(() -> {
                    mHideLoadingBar.set(true);
                    mEnableButtons.set(true);
                })
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleSendPostSuccess, this::handleSendPostError));
    }

    public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MessageViewHolder> {

        public final List<MessageRowViewModel> mModels = new ArrayList<>();

        @Override
        public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            RowMessageBinding messageBinding = DataBindingUtil.inflate(inflater, R.layout.row_message, parent, false);

            return new MessageViewHolder(messageBinding);
        }

        @Override
        public void onBindViewHolder(@NonNull MessageViewHolder holder, int position) {
            holder.bind(mModels.get(position));
        }

        @Override
        public int getItemCount() {
            return mModels.size();
        }

        public void fillMessages(@NonNull List<MessageRowViewModel> models) {
            checkNotNull(models);
            mModels.clear();
            mModels.addAll(models);
            notifyChange();
        }

        public void removeMessage(int position) {
            mModels.remove(position);
            notifyItemRemoved(position);
        }

        public void appendMessages(@NonNull List<MessageRowViewModel> models) {
            mModels.addAll(checkNotNull(models));
            notifyDataSetChanged();
        }

        public class MessageViewHolder extends RecyclerView.ViewHolder {

            private RowMessageBinding mRowBinding;

            public MessageViewHolder(@NonNull RowMessageBinding binding) {
                super(binding.getRoot());
                mRowBinding = binding;
            }

            public void bind(@NonNull MessageRowViewModel model) {
                mRowBinding.setModel(checkNotNull(model));
                mRowBinding.executePendingBindings();

                mRowBinding.imgDelete.setOnClickListener(view -> {
                    if (model.isMyOwnMessage()) {
                        mEventBus.post(new DeleteMessageEvent(model.getId(), getAdapterPosition()));
                    }
                });
            }
        }
    }

    public class UserProfilePagePaginatorScrollListener extends PagePaginatorScrollListener {
        @Override public void loadElements() {
            if (mPagePaginator.hasNext()) {
                mIsLoading = true;
                fillMessagesList(mPagePaginator.nextPage());
            }
        }
    }

    private class DeleteMessageSuccessAction implements Action1<StatusResponse> {
        int mMessagePosition;

        public DeleteMessageSuccessAction(int position) {
            mMessagePosition = position;
        }

        @Override public void call(StatusResponse statusResponse) {
            mMessagesAdapter.removeMessage(mMessagePosition);
        }
    }

    public class DeleteMessageEvent {
        private long mId;
        private int mPosition;

        public DeleteMessageEvent(long id, int position) {
            mId = id;
            mPosition = position;
        }

        public long getId() {
            return mId;
        }

        public int getPosition() {
            return mPosition;
        }
    }
}