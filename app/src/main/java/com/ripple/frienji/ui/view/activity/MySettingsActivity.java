package com.ripple.frienji.ui.view.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.google.common.eventbus.Subscribe;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.ActivityMySettingsBinding;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.ViewModelModule;
import com.ripple.frienji.di.module.ViewModule;
import com.ripple.frienji.ui.model.activity.CommentsViewModel;
import com.ripple.frienji.ui.model.activity.MySettingsViewModel;
import com.ripple.frienji.ui.view.dialog.DeletePostConfirmDialogFragment;
import com.ripple.frienji.ui.view.dialog.LogOutConfirmDialogFragment;
import javax.inject.Inject;

/**
 * @author kamil ratajczak
 * @since 11.08.2016
 */
public class MySettingsActivity extends BaseActivity {

    @Inject
    protected MySettingsViewModel mModel;

    private ActivityMySettingsBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrienjiApplication app = (FrienjiApplication) getApplication();

        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new ViewModule(this)).plus(new ViewModelModule()).inject(this);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_my_settings);
        mBinding.setModel(mModel);
    }

    @Override
    public void onResume() {
        super.onResume();
        mModel.loadUserProfileData();
    }

}
