package com.ripple.frienji.ui.view.fragment;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationSettingsResult;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.FragmentArBinding;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.FragmentModule;
import com.ripple.frienji.model.frienji.Frienji;
import com.ripple.frienji.ui.model.fragment.ArViewModel;
import com.ripple.frienji.ui.util.instrumentation.FragmentInstrumentationProvider;
import com.ripple.frienji.ui.util.requierements.CompositeRequirements;
import com.ripple.frienji.ui.util.requierements.CurrentLocationRequirement;
import com.ripple.frienji.ui.util.requierements.GoogleApiClientRequirement;
import com.ripple.frienji.ui.util.requierements.InstrumentationStateRequirement;
import com.ripple.frienji.ui.util.requierements.LocationProviderRequirement;
import com.ripple.frienji.ui.util.requierements.PermissionRequirement;
import javax.inject.Inject;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Tomasz Trybala
 * @since 2016-07-21
 */
public class ArFragment extends RequirementBaseFragment implements SensorEventListener {
    private static final int FULL_ROTATION = 360;
    private static final int INDEX_AZIMUTH = 0;
    private static final int INDEX_PITCH = 1;

    private final float[] mRotation = new float[9];
    private final float[] mOrientation = new float[3];

    @Inject
    protected ArViewModel mModel;

    /*package*/ LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            mModel.setCurrentLocation(location);
            handleDoneRequirement(CURRENT_LOCATION_REQUEST_CODE);
        }
    };

    /*package*/ ResultCallback<LocationSettingsResult> mResultCallbackFromSettings = this::handleSettingsCallback;
    /*package*/ SensorManager mSensorManager;
    /*package*/ Sensor mSensorRotation;

    private FragmentArBinding mBinding;

    @Override
    public void onPause() {
        super.onPause();
        cancelSensor();
        mBinding.surfaceView.onPause();
        mModel.clearBubbles();
    }

    @Override
    public void onResume() {
        super.onResume();
        initializeSensor();
        mBinding.surfaceView.onResume();
        mModel.fetchNearbyFrienjis();
    }

    @Override
    protected void createRequirements() {
        mRequirements = new CompositeRequirements();
        mRequirements.add(CAMERA_PERMISSION_REQUEST_CODE, new PermissionRequirement(CAMERA_PERMISSION_REQUEST_CODE,
                new String[]{Manifest.permission.CAMERA},
                this,
                R.string.permission_camera_error));
        mRequirements.add(LOCATION_PERMISSION_REQUEST_CODE, new PermissionRequirement(LOCATION_PERMISSION_REQUEST_CODE,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                this,
                R.string.permission_location_error));
        mRequirements.add(GOOGLE_API_CLIENT_REQUEST_CODE, new GoogleApiClientRequirement(mGoogleApiClient));
        mRequirements.add(LOCATION_PROVIDER_REQUEST_CODE, new LocationProviderRequirement(mGoogleApiClient,
                mLocationRequest, mResultCallbackFromSettings));
        mRequirements.add(CURRENT_LOCATION_REQUEST_CODE, new CurrentLocationRequirement(mGoogleApiClient,
                mLocationRequest, mLocationListener, getContext()));
    }

    @Override
    protected void resolveNextRequirement() {
        super.resolveNextRequirement();

        InstrumentationStateRequirement requirement = mRequirements.next();
        if (requirement == null) {
            mIsResolving = false;
        } else if (!requirement.isPossibleToDo()) {
            mIsResolving = false;
            mModel.setErrorMessage(requirement.getErrorMessage(), requirement);
        } else {
            requirement.resolve();
        }
    }

    private void initializeSensor() {
        if (mSensorManager == null) {
            mSensorManager = (SensorManager) FrienjiApplication.getAppContext().getSystemService(Context.SENSOR_SERVICE);
            mSensorRotation = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        }

        mSensorManager.registerListener(this, mSensorRotation, SensorManager.SENSOR_DELAY_GAME);
    }

    private void updateViews() {
        int azimuth = (int) (Math.toDegrees(SensorManager.getOrientation(mRotation, mOrientation)[INDEX_AZIMUTH])
                + FULL_ROTATION) % FULL_ROTATION;
        int pitch = (int) (Math.toDegrees(SensorManager.getOrientation(mRotation, mOrientation)[INDEX_PITCH]));
        mModel.updateCoordinates(FULL_ROTATION - azimuth, azimuth, pitch);
    }

    @Override
    public void onSensorChanged(@NonNull SensorEvent event) {
        checkNotNull(event);
        if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            SensorManager.getRotationMatrixFromVector(mRotation, event.values);
            SensorManager.remapCoordinateSystem(mRotation, SensorManager.AXIS_X, SensorManager.AXIS_Z, mRotation);
            updateViews();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        //no needed
    }

    private void cancelSensor() {
        if (mSensorManager != null) {
            mSensorManager.unregisterListener(this, mSensorRotation);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        checkNotNull(permissions);
        checkNotNull(grantResults);

        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE || requestCode == CAMERA_PERMISSION_REQUEST_CODE) {
            for (int i = 0; i < permissions.length; ++i) {
                String permission = permissions[i];
                int grantResult = grantResults[i];

                if (Manifest.permission.ACCESS_FINE_LOCATION.equals(permission)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        handleDoneRequirement(LOCATION_PERMISSION_REQUEST_CODE);
                    } else {
                        handleUndoneRequirement(LOCATION_PERMISSION_REQUEST_CODE);
                    }
                } else if (Manifest.permission.CAMERA.equals(permission)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        handleDoneRequirement(CAMERA_PERMISSION_REQUEST_CODE);
                    } else {
                        handleUndoneRequirement(CAMERA_PERMISSION_REQUEST_CODE);
                    }
                }
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrienjiApplication app = (FrienjiApplication) getActivity().getApplication();

        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new FragmentModule(getActivity())).inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mModel.setInstrumentationProvider(FragmentInstrumentationProvider.from(this));
        mModel.mDisplayOrientation.set(getDisplayOrientation());

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_ar, container, false);
        mBinding.setModel(mModel);
        mBinding.surfaceView.setRadarView(mBinding.radarView);

        return mBinding.getRoot();
    }

    private int getDisplayOrientation() {
        int degrees = 0;

        int rotation = getActivity().getWindowManager().getDefaultDisplay().getRotation();
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break; // Natural orientation
            case Surface.ROTATION_90:
                degrees = 90;
                break; // Landscape left
            case Surface.ROTATION_180:
                degrees = 180;
                break;// Upside down
            case Surface.ROTATION_270:
                degrees = 270;
                break;// Landscape right
        }

        return degrees;
    }

    public static class BubbleCatchEvent {
        private Frienji mFrienji;

        public BubbleCatchEvent(@NonNull Frienji frienji) {
            mFrienji = checkNotNull(frienji);
        }

        public @NonNull Frienji getFrienji() {
            return mFrienji;
        }
    }

    public static class NearbyFrienjisEvent {
        private int mCount;

        public NearbyFrienjisEvent(int count) {
            mCount = count;
        }

        public int getCount() {
            return mCount;
        }
    }
}