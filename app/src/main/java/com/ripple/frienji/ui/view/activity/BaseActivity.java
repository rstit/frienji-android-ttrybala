package com.ripple.frienji.ui.view.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import javax.inject.Inject;

/**
 * @author kamil ratajczak
 * @since 27.07.2016
 */
public class BaseActivity extends AppCompatActivity {

    @Inject
    protected EventBus mEventBus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStop() {
        super.onStop();
        mEventBus.unregister(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onStart() {
        super.onStart();
        mEventBus.register(this);
    }

    @Subscribe
    public void onShowSnackbarMessage(@NonNull ShowSnackbarEvent event) {
        showSnackbarMessage(event.getMessageResId());
    }

    protected void showSnackbarMessage(@StringRes int message) {
        showSnackbarMessage(message, Snackbar.LENGTH_LONG);
    }

    protected void showSnackbarMessage(@StringRes int message, int duration) {
        View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar.make(rootView, message, duration).show();
    }

    public static class ShowSnackbarEvent {
        private final int mMessageResId;

        public ShowSnackbarEvent(@StringRes int messageResId) {
            mMessageResId = messageResId;
        }

        public @StringRes int getMessageResId() {
            return mMessageResId;
        }
    }
}