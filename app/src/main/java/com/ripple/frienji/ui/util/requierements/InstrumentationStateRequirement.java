package com.ripple.frienji.ui.util.requierements;

import android.support.annotation.StringRes;

/**
 * @author Tomasz Trybala
 * @since 2016-08-04
 */
public abstract class InstrumentationStateRequirement {
    private boolean mIsDone;
    private boolean mIsPossible = true;

    public void setAsImpossibleToDo() {
        mIsPossible = false;
    }

    public void setAsDone() {
        mIsDone = true;
    }

    public boolean isDone() {
        return mIsDone;
    }

    public boolean isPossibleToDo() {
        return mIsPossible;
    }

    public void undo() {
        mIsDone = false;
    }

    public abstract void resolve();

    public @StringRes abstract int getErrorMessage();
}
