package com.ripple.frienji.ui.model.row;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.maps.model.LatLng;
import com.google.common.base.Strings;
import com.google.common.eventbus.EventBus;
import com.ripple.frienji.R;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.model.response.StatusResponse;
import com.ripple.frienji.net.FrienjiApi;
import com.ripple.frienji.ui.binding.ObservableString;
import com.ripple.frienji.ui.model.BaseViewModel;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.util.rx.ObservableTransformers;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 04.08.2016
 */
public abstract class MessageRowViewModel extends BaseViewModel {
    private final static int MINIMAL_NUMBER_LIKES_TO_DISPLAY = 1;

    public final ObservableString mMessage = new ObservableString();
    public final ObservableString mAvatarName = new ObservableString();
    public final ObservableInt mAvatarType = new ObservableInt();
    public final ObservableString mUserName = new ObservableString();
    public final ObservableString mCreationTime = new ObservableString();
    public final ObservableString mImageName = new ObservableString();
    public final ObservableString mNumberOfComments = new ObservableString();
    public final ObservableBoolean mHasLike = new ObservableBoolean();
    public final ObservableString mNumberOfLikes = new ObservableString();
    public final ObservableField<LatLng> mLatLongPosition = new ObservableField();
    public final ObservableBoolean mShowLikeAndComment = new ObservableBoolean();

    protected long mId;
    protected long mFrienjiId;
    protected boolean mMyOwnMessage;
    protected NavigationHandler mNavigationHandler;

    public MessageRowViewModel(@NonNull Context context, @NonNull NavigationHandler navigationHandler, @NonNull EventBus eventBus,
                               @NonNull AvatarReader avatarReader, @NonNull FrienjiApi frienjiApi, @NonNull ObservableTransformers observableTransformers,
                               boolean showLikeAndComment) {
        mContext = checkNotNull(context);
        mShowLikeAndComment.set(showLikeAndComment);
        mNavigationHandler = checkNotNull(navigationHandler);
        mAvatarReader = checkNotNull(avatarReader);
        mApi = checkNotNull(frienjiApi);
        mObservableTransformers = checkNotNull(observableTransformers);
        mEventBus = checkNotNull(eventBus);
    }

    protected static boolean checkImagePath(@Nullable String imagePath) {
        return !Strings.isNullOrEmpty(imagePath);
    }

    public boolean isMyOwnMessage() {
        return mMyOwnMessage;
    }

    public void setMyOwnMessage(boolean myOwnMessage) {
        mMyOwnMessage = myOwnMessage;
    }

    public void likeMessage() {
        subscribe(mApi.likeUnlikePost(mId)
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleLikeUnlikeSuccess, this::handleLikeUnlikeError));
    }

    private void handleLikeUnlikeSuccess(@NonNull StatusResponse status) {
        if (StatusResponse.SUCCESS_STATUS.equals(status.status())) {
            mHasLike.set(!mHasLike.get());

            if (mHasLike.get()) {
                increaseNumberOfLikes();
            } else {
                decreaseNumberOfLikes();
            }

        }
    }

    public void handleLikeUnlikeError(@Nullable Throwable throwable) {
        handleApiError(throwable, R.string.error_cannot_like_user_messages);
    }

    private void increaseNumberOfLikes() {
        Integer currentNumberOfLikes = getIntegerNumber(mNumberOfLikes.get());

        if (currentNumberOfLikes != null) {
            mNumberOfLikes.set(String.valueOf(currentNumberOfLikes + 1));
        } else {
            mNumberOfLikes.set(String.valueOf(MINIMAL_NUMBER_LIKES_TO_DISPLAY));
        }
    }

    private void decreaseNumberOfLikes() {
        Integer currentNumberOfLikes = getIntegerNumber(mNumberOfLikes.get());

        if (currentNumberOfLikes != null && currentNumberOfLikes > MINIMAL_NUMBER_LIKES_TO_DISPLAY) {
            mNumberOfLikes.set(String.valueOf(currentNumberOfLikes - 1));
        } else {
            mNumberOfLikes.set(null);
        }
    }

    public long getId() {
        return mId;
    }

    public abstract void showComments();

    public abstract void createImage();

    public abstract void showUser();
}