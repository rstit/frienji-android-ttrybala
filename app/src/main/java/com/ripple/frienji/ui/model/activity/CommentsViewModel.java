package com.ripple.frienji.ui.model.activity;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.google.android.gms.maps.model.LatLng;
import com.google.common.base.Strings;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.RowCommentBinding;
import com.ripple.frienji.model.message.FrienjiPost;
import com.ripple.frienji.model.message.Message;
import com.ripple.frienji.model.page.PagePaginator;
import com.ripple.frienji.model.response.FrienjiPostCommentsResponse;
import com.ripple.frienji.model.response.StatusResponse;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.ui.binding.ObservableString;
import com.ripple.frienji.ui.model.row.CommentMessageRowViewModel;
import com.ripple.frienji.ui.model.row.MessageRowViewModel;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import rx.Observable;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 10.08.2016
 */
public class CommentsViewModel extends BaseMessageViewModel {
    public final ObservableInt mProgressBarType = new ObservableInt(PROGRESS_BAR_LOADING);
    public final ObservableBoolean mEnableButtons = new ObservableBoolean(true);

    public final PagePaginatorScrollListener mPagePaginatorScrollListener = new CommentsPagePaginatorScrollListener();
    public final CommentsAdapter mCommentsAdapter = new CommentsAdapter();

    public final ObservableString mMessageBody = new ObservableString();
    public final ObservableString mUserName = new ObservableString();
    public final ObservableString mCreationTime = new ObservableString();
    public final ObservableString mImageName = new ObservableString();
    public final ObservableString mAvatarName = new ObservableString();
    public final ObservableInt mAvatarType = new ObservableInt();
    public final ObservableField<LatLng> mMessageLatLongPosition = new ObservableField();
    public final ObservableBoolean mShowNoResultsInfo = new ObservableBoolean();

    protected UserProfile mCommentAuthor;

    @Inject
    public NavigationHandler mNavigator;

    private long mPostId;
    private long mFrienjiId;
    private PagePaginator mPagePaginator;

    @Inject
    public CommentsViewModel() {
    }

    protected void showCoachmarkIfNecessary() {
        mShowCoachMark.set(!mAppSettings.shouldSkipCommentsCoachMark());
    }

    public void showUser() {
        if (mCommentAuthor != null) {
            long loggedUserId = mAppSettings.loadUserProfileId().get();

            if (mCommentAuthor.id() == loggedUserId) {
                mNavigator.startActivity(mIntentProvider.createMyOwnUserProfile());
            } else {
                mNavigator.startActivity(mIntentProvider.createUserProfile(mCommentAuthor));
            }
        }
    }

    public void fillMessageCommentsList(long frienjiId, long postId) {
        showCoachmarkIfNecessary();

        mFrienjiId = frienjiId;
        mPostId = postId;

        subscribe(mApi.loadPostComments(frienjiId, postId)
                .doOnNext(this::savePagePaginator)
                .doOnNext(response -> this.fillMessage(response.post()))
                .flatMap(response -> Observable.from(response.post().comments()))
                .map(message -> CommentMessageRowViewModel.from(mContext, mAvatarReader, mApi,
                        mNavigator, mEventBus, mObservableTransformers, mIntentProvider, mDateTimeUtil, message))
                .toList()
                .doOnSubscribe(() -> {
                    mHideLoadingBar.set(false);
                    mProgressBarType.set(PROGRESS_BAR_LOADING);
                    mEnableButtons.set(false);
                })
                .doOnTerminate(() -> {
                    mHideLoadingBar.set(true);
                    mEnableButtons.set(true);
                })
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleLoadCommentsSuccess, this::handleLoadCommentsError));
    }

    private void fillMessageCommentsList(long frienjiId, long postId, int page) {
        subscribe(mApi.loadPostComments(frienjiId, postId, page)
                .doOnNext(this::savePagePaginator)
                .doOnNext(response -> this.fillMessage(response.post()))
                .flatMap(response -> Observable.from(response.post().comments()))
                .map(message -> CommentMessageRowViewModel.from(mContext, mAvatarReader, mApi,
                        mNavigator, mEventBus, mObservableTransformers, mIntentProvider, mDateTimeUtil, message))
                .toList()
                .doOnSubscribe(() -> {
                    mHideLoadingBar.set(false);
                    mProgressBarType.set(PROGRESS_BAR_LOADING);
                    mEnableButtons.set(false);
                })
                .doOnTerminate(() -> {
                    mHideLoadingBar.set(true);
                    mEnableButtons.set(true);
                })
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleLoadCommentsPageSuccess, this::handleLoadCommentsError));
    }

    public void deleteComment(long messageId, int position) {
        subscribe(mApi.deletePost(messageId)
                .compose(mObservableTransformers.networkOperation())
                .subscribe(sucess -> {
                    if (StatusResponse.SUCCESS_STATUS.equals(sucess.status())) {
                        mCommentsAdapter.removeMessage(position);
                    }
                }, this::handleDeleteError));
    }

    public void handleDeleteError(@Nullable Throwable throwable) {
        handleApiError(throwable, R.string.error_cannot_delete_message);
    }

    private void handleLoadCommentsSuccess(@NonNull List<MessageRowViewModel> messages) {
        if (messages.isEmpty()) {
            mShowNoResultsInfo.set(true);
        }
        mCommentsAdapter.fillMessages(checkNotNull(messages));
    }

    private void handleLoadCommentsPageSuccess(@NonNull List<MessageRowViewModel> messages) {
        mPagePaginatorScrollListener.disableLoading();
        mCommentsAdapter.appendMessages(checkNotNull(messages));
    }

    private void handleLoadCommentsError(@Nullable Throwable throwable) {
        mPagePaginatorScrollListener.disableLoading();
        handleApiError(throwable, R.string.error_cannot_load_user_messages);
    }

    private void handleSendCommentSuccess(@NonNull MessageRowViewModel messageRowViewModel) {
        mShowNoResultsInfo.set(false);
        clearNewMessageData();
        mCommentsAdapter.mModels.add(0, checkNotNull(messageRowViewModel));
        mCommentsAdapter.notifyDataSetChanged();
    }

    private void handleSendCommentError(@Nullable Throwable throwable) {
        handleApiError(throwable, R.string.error_send_user_post);
    }

    private void savePagePaginator(@NonNull FrienjiPostCommentsResponse frienjiPostCommentsResponse) {
        mPagePaginator = frienjiPostCommentsResponse.pagePaginator();
    }

    private void fillMessage(@NonNull FrienjiPost frienjiPost) {
        mMessageBody.set(frienjiPost.message());

        if (frienjiPost.author() != null) {
            mCommentAuthor = frienjiPost.author();
            mUserName.set(frienjiPost.author().userName());
        }

        mCreationTime.set(mDateTimeUtil.getTimestampString(frienjiPost.createdAt()));

        if (!Strings.isNullOrEmpty(frienjiPost.attachmentImageUrl())) {
            mImageName.set(frienjiPost.attachmentImageUrl());
        }

        if (frienjiPost.author() != null && frienjiPost.author().avatar() != null) {
            mAvatarName.set(frienjiPost.author().avatar().name());
            mAvatarType.set(frienjiPost.author().avatar().type());

        }

        if (frienjiPost.location() != null) {
            LatLng latLng = new LatLng(frienjiPost.location().latitude(), frienjiPost.location().longitude());
            mMessageLatLongPosition.set(latLng);
        }
    }

    @Override
    public @NonNull NavigationHandler getNavigationHandler() {
        return mNavigator;
    }

    @Override
    public void onSendMessage(@NonNull Message message) {
        subscribe(mApi.sendUserComment(mFrienjiId, mPostId, message.buildMultipartBodyParts())
                .map(frienjiPost -> CommentMessageRowViewModel.from(mContext, mAvatarReader, mApi,
                        mNavigator, mEventBus, mObservableTransformers, mIntentProvider, mDateTimeUtil, frienjiPost))
                .doOnSubscribe(() -> {
                    mHideLoadingBar.set(false);
                    mProgressBarType.set(PROGRESS_BAR_SENDING);
                    mEnableButtons.set(false);
                })
                .doOnTerminate(() -> {
                    mHideLoadingBar.set(true);
                    mEnableButtons.set(true);
                })
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleSendCommentSuccess, this::handleSendCommentError));
    }

    @Override
    public @NonNull String getCoachMarkTitle() {
        return mContext.getString(R.string.comments_coach_mark_title);
    }

    @Override
    public @NonNull String getCoachMarkContent() {
        return mContext.getString(R.string.comments_coach_mark_content);
    }

    @Override
    public void hideCoachMark() {
        mAppSettings.hideCommentsCoachMark();
        mShowCoachMark.set(false);
    }

    public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.MessageViewHolder> {

        public final List<MessageRowViewModel> mModels = new ArrayList<>();

        @Override
        public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            RowCommentBinding messageBinding = DataBindingUtil.inflate(inflater, R.layout.row_comment, parent, false);
            return new MessageViewHolder(messageBinding);
        }

        @Override
        public void onBindViewHolder(MessageViewHolder holder, int position) {
            holder.bind(mModels.get(position));
        }

        @Override
        public int getItemCount() {
            return mModels.size();
        }

        public void fillMessages(@NonNull List<MessageRowViewModel> models) {
            checkNotNull(models);
            mModels.clear();
            mModels.addAll(models);
            notifyChange();
        }

        public void appendMessages(@NonNull List<MessageRowViewModel> models) {
            mModels.addAll(checkNotNull(models));
            notifyDataSetChanged();
        }

        public void removeMessage(int position) {
            mModels.remove(position);
            notifyItemRemoved(position);
        }

        public class MessageViewHolder extends RecyclerView.ViewHolder {

            private RowCommentBinding mRowBinding;

            public MessageViewHolder(@NonNull RowCommentBinding binding) {
                super(binding.getRoot());
                mRowBinding = binding;
            }

            public void bind(@NonNull MessageRowViewModel model) {
                checkNotNull(model);

                mRowBinding.setModel(model);
                mRowBinding.executePendingBindings();

                mRowBinding.imgDelete.setOnClickListener(view -> {
                    if (model.isMyOwnMessage()) {
                        mEventBus.post(new DeleteCommentEvent(model.getId(), getAdapterPosition()));
                    }
                });
            }
        }
    }

    public class CommentsPagePaginatorScrollListener extends PagePaginatorScrollListener {
        @Override public void loadElements() {
            if (mPagePaginator.hasNext()) {
                mIsLoading = true;
                fillMessageCommentsList(mFrienjiId, mPostId, mPagePaginator.nextPage());
            }
        }
    }

    public class DeleteCommentEvent {
        private long mId;
        private int mPosition;

        public DeleteCommentEvent(long id, int position) {
            mId = id;
            mPosition = position;
        }

        public long getId() {
            return mId;
        }

        public int getPosition() {
            return mPosition;
        }
    }
}