package com.ripple.frienji.ui.model.row;

import android.content.Context;
import android.support.annotation.NonNull;
import com.ripple.frienji.R;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.model.action.Activity;
import com.ripple.frienji.ui.binding.ObservableString;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.util.DateTimeUtil;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak on 22.08.16.
 */
public class UserActionRowViewModel extends SimpleRowViewModel {
    public final ObservableString mCreationTime = new ObservableString();
    private Activity mActivity;

    private NavigationHandler mNavigationHandler;
    private IntentProvider mIntentProvider;

    private UserActionRowViewModel(@NonNull Context context, @NonNull AvatarReader avatarReader, @NonNull DateTimeUtil dateTimeUtil,
                                   @NonNull NavigationHandler navigationHandler, @NonNull IntentProvider intentProvider, @NonNull Activity activity) {
        super(checkNotNull(context), checkNotNull(avatarReader));
        mActivity = checkNotNull(activity);
        mNavigationHandler = checkNotNull(navigationHandler);
        mIntentProvider = checkNotNull(intentProvider);

        fillContent();
        setAvatar();

        if (activity.createdAt() != null) {
            mCreationTime.set(dateTimeUtil.getTimestampString(activity.createdAt()));
        }
    }

    public static @NonNull UserActionRowViewModel from(@NonNull Context context, @NonNull AvatarReader avatarReader,
                                                       @NonNull DateTimeUtil dateTimeUtil, @NonNull NavigationHandler navigationHandler,
                                                       @NonNull IntentProvider intentProvider, @NonNull Activity activity) {
        return new UserActionRowViewModel(checkNotNull(context), checkNotNull(avatarReader),
                checkNotNull(dateTimeUtil), checkNotNull(navigationHandler), checkNotNull(intentProvider),
                checkNotNull(activity));
    }

    private void fillContent() {
        String content = null;
        String userName = mActivity.owner().userName();

        switch (mActivity.key()) {
            case Activity.POST_CONTENT_COMMENT_ON_YOUR_WALL_CREATED:
                content = mContext.getString(R.string.user_action_comment_post_on_wall_template, userName, mActivity.activityEntity().message());
                break;

            case Activity.POST_CONTENT_COMMENT_CREATED:
                content = mContext.getString(R.string.user_action_comment_on_post_template, userName, mActivity.activityEntity().message());
                break;

            case Activity.LIKE_FOR_COMMENT_CREATED:
                content = mContext.getString(R.string.user_action_like_on_comment, userName);
                break;

            case Activity.LIKE_FOR_POST_CREATED:
                content = mContext.getString(R.string.user_action_like_on_post, userName);
                break;

            case Activity.RELATIONSHIP_CREATED_SAVED:
                content = mContext.getString(R.string.user_action_saved, userName);
                break;

            case Activity.RELATIONSHIP_REMOVED_SAVED:
                content = mContext.getString(R.string.user_action_rejected, userName);
                break;

            case Activity.POST_CONTENT_CREATED:
                content = mContext.getString(R.string.user_action_post_on_wall_template, userName, mActivity.activityEntity().message());
                break;

            case Activity.RELATIONSHIP_CREATED_REJECTED:
                content = mContext.getString(R.string.user_action_rejected, userName);
                break;

            case Activity.RECEIVE_MESSAGE:
                content = mContext.getString(R.string.user_message_received, userName);
                break;
        }

        mContent.set(content);
    }

    public void openAction() {
        if (mActivity.key() != null) {
            switch (mActivity.key()) {
                case Activity.POST_CONTENT_CREATED:
                    goToMyOwnWall();
                    break;

                case Activity.POST_CONTENT_COMMENT_ON_YOUR_WALL_CREATED:
                    goToCommentsView();
                    break;

                case Activity.POST_CONTENT_COMMENT_CREATED:
                    goToCommentsView();
                    break;

                case Activity.LIKE_FOR_COMMENT_CREATED:
                    goToLikedCommentPostView();
                    break;

                case Activity.LIKE_FOR_POST_CREATED:
                    goToLikedPostView();
                    break;

                case Activity.RELATIONSHIP_CREATED_SAVED:
                    goToUserWall();
                    break;

                case Activity.RELATIONSHIP_REMOVED_SAVED:
                    goToUserWall();
                    break;

                case Activity.RELATIONSHIP_CREATED_REJECTED:
                    goToUserWall();
                    break;

                case Activity.RECEIVE_MESSAGE:
                    goToChat();
                    break;
            }
        }
    }

    protected void goToMyOwnWall() {
        mNavigationHandler.startActivity(mIntentProvider.createMyOwnUserProfile());
    }

    protected void goToUserWall() {
        if (mActivity.owner() != null) {
            mNavigationHandler.startActivity(mIntentProvider.createUserProfile(mActivity.owner()));
        }
    }

    protected void goToCommentsView() {
        if (mActivity.activityEntity() != null) {
            mNavigationHandler.startActivity(mIntentProvider.createComents(mActivity.activityEntity().wallOwnerId(), mActivity.activityEntity().parentId()));
        }
    }

    protected void goToLikedPostView() {
        if (mActivity.receiver() != null) {
            mNavigationHandler.startActivity(mIntentProvider.createUserProfile(mActivity.receiver()));
        }
    }

    protected void goToLikedCommentPostView() {
        if (mActivity.activityEntity() != null && mActivity.receiver() != null) {
            mNavigationHandler.startActivity(mIntentProvider.createComents(mActivity.receiver().id(), mActivity.activityEntity().postId()));
        }
    }

    protected void goToChat() {
        if (mActivity.owner() != null) {
            mNavigationHandler.startActivity(mIntentProvider.createConversation(mActivity.owner()));
        }
    }

    private void setAvatar() {
        mAvatarName.set(mActivity.owner().avatar().name());
        mAvatarType.set(mActivity.owner().avatar().type());
    }
}