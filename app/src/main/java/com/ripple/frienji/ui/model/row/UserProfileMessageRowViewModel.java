package com.ripple.frienji.ui.model.row;

import android.content.Context;
import android.support.annotation.NonNull;
import com.google.android.gms.maps.model.LatLng;
import com.google.common.base.Strings;
import com.google.common.eventbus.EventBus;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.model.message.FrienjiPost;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.net.FrienjiApi;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.util.DateTimeUtil;
import com.ripple.frienji.ui.view.activity.CommentsActivity;
import com.ripple.frienji.ui.view.activity.UserProfileActivity;
import com.ripple.frienji.util.rx.ObservableTransformers;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 10.08.2016
 */
public class UserProfileMessageRowViewModel extends MessageRowViewModel {
    protected UserProfile mMessageAuthor;
    protected long mWallOwnerId;

    private UserProfileMessageRowViewModel(@NonNull Context context, @NonNull NavigationHandler navigationHandler, @NonNull EventBus eventBus, @NonNull AvatarReader avatarReader,
                                           @NonNull FrienjiApi frienjiApi, @NonNull ObservableTransformers observableTransformers, boolean showLikeAndComment) {

        super(checkNotNull(context), checkNotNull(navigationHandler), checkNotNull(eventBus), checkNotNull(avatarReader),
                checkNotNull(frienjiApi), checkNotNull(observableTransformers), showLikeAndComment);
        mNavigationHandler = checkNotNull(navigationHandler);
    }

    public static @NonNull MessageRowViewModel from(@NonNull Context context, @NonNull AvatarReader avatarReader,
                                                    @NonNull FrienjiApi frienjiApi, @NonNull NavigationHandler navigationHandler, @NonNull EventBus eventBus,
                                                    @NonNull ObservableTransformers observableTransformers, @NonNull DateTimeUtil dateTimeUtil,
                                                    @NonNull FrienjiPost frienjiPost, long wlallOwnerId) {

        UserProfileMessageRowViewModel userProfileMessageRowViewModel = new UserProfileMessageRowViewModel(checkNotNull(context),
                checkNotNull(navigationHandler), checkNotNull(eventBus), checkNotNull(avatarReader),
                checkNotNull(frienjiApi), checkNotNull(observableTransformers), true);

        userProfileMessageRowViewModel.mWallOwnerId = wlallOwnerId;
        userProfileMessageRowViewModel.mMessageAuthor = frienjiPost.author();
        userProfileMessageRowViewModel.mId = frienjiPost.id();
        userProfileMessageRowViewModel.mFrienjiId = frienjiPost.author().id();
        userProfileMessageRowViewModel.mMessage.set(frienjiPost.message());
        userProfileMessageRowViewModel.mUserName.set(frienjiPost.author().userName());

        if (frienjiPost.author().avatar() != null) {
            userProfileMessageRowViewModel.mAvatarName.set(frienjiPost.author().avatar().name());
            userProfileMessageRowViewModel.mAvatarType.set(frienjiPost.author().avatar().type());
        }

        userProfileMessageRowViewModel.mCreationTime.set(dateTimeUtil.getTimestampString(frienjiPost.createdAt()));

        if (checkImagePath(frienjiPost.attachmentImageUrl())) {
            userProfileMessageRowViewModel.mImageName.set(frienjiPost.attachmentImageUrl());
        }

        userProfileMessageRowViewModel.mHasLike.set(frienjiPost.liked());

        if (frienjiPost.numberOfComments() > 0) {
            userProfileMessageRowViewModel.mNumberOfComments.set(String.valueOf(frienjiPost.numberOfComments()));
        }

        if (frienjiPost.numberOfLikes() > 0) {
            userProfileMessageRowViewModel.mNumberOfLikes.set(String.valueOf(frienjiPost.numberOfLikes()));
        }

        userProfileMessageRowViewModel.setMyOwnMessage(frienjiPost.myOwnMessage());

        if (frienjiPost.location() != null) {
            LatLng latLng = new LatLng(frienjiPost.location().latitude(), frienjiPost.location().longitude());
            userProfileMessageRowViewModel.mLatLongPosition.set(latLng);
        }

        userProfileMessageRowViewModel.mMyOwnMessage = frienjiPost.myOwnMessage();

        return userProfileMessageRowViewModel;
    }

    @Override
    public void showComments() {
        mNavigationHandler.startActivity(CommentsActivity.createIntent(mWallOwnerId, mId, mContext));
    }

    @Override
    public void createImage() {
        if (!Strings.isNullOrEmpty(mImageName.get())) {
            mNavigationHandler.startActivity(createImageIntent(mImageName.get()));
        }
    }

    @Override
    public void showUser() {
        if (!mMyOwnMessage && mMessageAuthor.id() != mWallOwnerId) {
            mNavigationHandler.startActivity(UserProfileActivity.createIntent(mMessageAuthor, mContext));
        }
    }
}