package com.ripple.frienji.ui.model.fragment;

import android.databinding.ObservableArrayList;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.databinding.ObservableFloat;
import android.databinding.ObservableInt;
import android.databinding.ObservableList;
import android.location.Location;
import android.support.annotation.FloatRange;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import com.ripple.frienji.BuildConfig;
import com.ripple.frienji.R;
import com.ripple.frienji.model.frienji.Frienji;
import com.ripple.frienji.model.location.FrienjiLocation;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.net.FrienjiApi;
import com.ripple.frienji.settings.AppSettings;
import com.ripple.frienji.ui.util.requierements.InstrumentationStateRequirement;
import com.ripple.frienji.ui.view.fragment.ArFragment;
import java.util.List;
import javax.inject.Inject;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 16.08.2016
 */
public class ArViewModel extends BaseFragmentViewModel {
    public final ObservableBoolean mProgressVisible = new ObservableBoolean(true);
    public final ObservableBoolean mIsError = new ObservableBoolean();
    public final ObservableFloat mCompassAzimuth = new ObservableFloat();
    public final ObservableField<Location> mCurrentLocation = new ObservableField<>();
    public final ObservableList<Frienji> mBubblesList = new ObservableArrayList<>();
    public final ObservableInt mDisplayOrientation = new ObservableInt();
    public final ObservableInt mAzimuth = new ObservableInt();
    public final ObservableInt mPitch = new ObservableInt();
    public final ObservableInt mErrorMessage = new ObservableInt(R.string.errorDisabledPermissions);

    @Inject
    public FrienjiApi mFrienjiApi;

    @Inject
    public AppSettings mAppSettings;

    @Inject
    public ArViewModel() {
    }

    public void clearBubbles() {
        mBubblesList.clear();
    }

    public void setCurrentLocation(@NonNull Location location) {
        mCurrentLocation.set(checkNotNull(location));
        fetchNearbyFrienjis();
        sendCurrentLocationToServer();
    }

    public void setErrorMessage(@StringRes int message, @Nullable InstrumentationStateRequirement requirement) {
        mIsError.set(true);
        mErrorMessage.set(message);
        setCurrentRunnable(requirement);
    }

    public void updateCoordinates(@FloatRange(from = 0f, to = 360f) float compassAzimuth,
                                  @IntRange(from = 0, to = 360) int azimuth,
                                  @IntRange(from = -90, to = 90) int pitch) {
        checkArgument(compassAzimuth >= 0 && compassAzimuth <= 360f);
        checkArgument(azimuth >= 0 && azimuth <= 360);
        checkArgument(pitch >= -90 && pitch <= 90);

        mCompassAzimuth.set(compassAzimuth);
        mAzimuth.set(azimuth);
        mPitch.set(pitch);
    }

    /*package*/ void handleNearbyFrienjisResponse(@NonNull List<Frienji> frienjies) {
        checkNotNull(frienjies);
        mBubblesList.clear();
        mBubblesList.addAll(frienjies);
        mEventBus.post(new ArFragment.NearbyFrienjisEvent(frienjies.size()));
    }

    /*package*/ void handleNearbyFrienjisError(@Nullable Throwable throwable) {
        setErrorMessage(R.string.errorBackendSide, null);
    }

    public void fetchNearbyFrienjis() {
        subscribe(mFrienjiApi.getAroundFrienjis(BuildConfig.FRIENJI_AROUND_RADIUS)
                .compose(mObservableTransformers.networkOperation())
                .doOnSubscribe(() -> mProgressVisible.set(true))
                .doOnTerminate(() -> mProgressVisible.set(false))
                .subscribe(this::handleNearbyFrienjisResponse,
                        this::handleNearbyFrienjisError));
    }

    private void sendCurrentLocationToServer() {
        subscribe(mFrienjiApi.updateUserProfile(buildFrienjiUserProfile().buildMultipartBodyParts())
                .compose(mObservableTransformers.networkOperation())
                .doOnSubscribe(() -> mProgressVisible.set(true))
                .doOnTerminate(() -> mProgressVisible.set(false))
                .subscribe(res -> {},
                        this::handleNearbyFrienjisError));
    }

    private @NonNull UserProfile buildFrienjiUserProfile() {
        FrienjiLocation frienjiLocation = FrienjiLocation.builder()
                .latitude(mCurrentLocation.get().getLatitude())
                .longitude(mCurrentLocation.get().getLongitude())
                .build();

        return UserProfile.builder()
                .id(mAppSettings.loadUserProfileId().get())
                .location(frienjiLocation)
                .build();
    }
}