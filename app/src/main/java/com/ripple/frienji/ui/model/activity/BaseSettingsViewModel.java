package com.ripple.frienji.ui.model.activity;

import android.databinding.ObservableInt;
import android.databinding.ObservableLong;
import android.support.annotation.NonNull;
import com.ripple.frienji.R;
import com.ripple.frienji.settings.AppSettings;
import com.ripple.frienji.ui.binding.ObservableString;
import javax.inject.Inject;

/**
 * Created by kamil ratajczak on 13.09.16.
 */
public class BaseSettingsViewModel extends CoachMarkViewModel {

    public final ObservableLong mUserProfileId = new ObservableLong();
    public final ObservableString mUserName = new ObservableString();
    public final ObservableString mWhoYouAre = new ObservableString();
    public final ObservableString mAvatarName = new ObservableString();
    public final ObservableInt mAvatarType = new ObservableInt();
    public final ObservableString mCoverImage = new ObservableString();

    @Inject
    public AppSettings mAppSettings;

    public void loadUserProfileData() {
        mShowCoachMark.set(!mAppSettings.shouldSkipMySettingsCoachMark());

        mUserName.set(mAppSettings.loadUserName().get());
        mWhoYouAre.set(mAppSettings.loadUUserWhoYouAre().get());
        mAvatarName.set(mAppSettings.loadUserAvatarName().get());
        mAvatarType.set(mAppSettings.loadUserAvatarType().get());
        mUserProfileId.set(mAppSettings.loadUserProfileId().get());

        if (mAppSettings.loadUserCoverImage().isPresent()) {
            mCoverImage.set(mAppSettings.loadUserCoverImage().get());
        }
    }

    @Override
    public @NonNull String getCoachMarkTitle() {
        return mContext.getString(R.string.my_settings_wall_coach_mark_title);
    }

    @Override
    public @NonNull String getCoachMarkContent() {
        return mContext.getString(R.string.my_settings_wall_coach_mark_content);
    }

    @Override
    public @NonNull void hideCoachMark() {
        mAppSettings.hideMySettingsCoachMark();
        mShowCoachMark.set(false);
    }
}