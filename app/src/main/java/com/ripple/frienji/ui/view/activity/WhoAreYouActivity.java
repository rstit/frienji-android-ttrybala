package com.ripple.frienji.ui.view.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.ActivityWhoAreYouBinding;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.ViewModelModule;
import com.ripple.frienji.di.module.ViewModule;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.ui.model.activity.WhoAreYouViewModel;
import javax.inject.Inject;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 30.07.2016
 */
public class WhoAreYouActivity extends BaseActivity {
    private static final String EXTRA_WHO_YOU_ARE = "avatar";

    @Inject
    protected WhoAreYouViewModel mModel;

    public static @NonNull Intent createIntent(@NonNull Avatar avatar, @NonNull Context context) {
        Intent intent = new Intent(checkNotNull(context), WhoAreYouActivity.class);
        intent.putExtra(EXTRA_WHO_YOU_ARE, checkNotNull(avatar));
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrienjiApplication app = (FrienjiApplication) getApplication();

        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new ViewModule(this)).plus(new ViewModelModule()).inject(this);

        ActivityWhoAreYouBinding mBinding = DataBindingUtil.setContentView(this, R.layout.activity_who_are_you);
        mBinding.setModel(mModel);

        fetchExtras();
    }

    private void fetchExtras() {
        Bundle extras = getIntent().getExtras();

        if (extras != null && extras.containsKey(EXTRA_WHO_YOU_ARE)) {
            Avatar avatar = extras.getParcelable(EXTRA_WHO_YOU_ARE);
            if (avatar != null) {
                mModel.populateAvatar(avatar);
            }
        }
    }
}