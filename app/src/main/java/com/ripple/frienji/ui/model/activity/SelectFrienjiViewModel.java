package com.ripple.frienji.ui.model.activity;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.RowChoseFrienjiBinding;
import com.ripple.frienji.model.page.PagePaginator;
import com.ripple.frienji.model.response.FrienjiZooResponse;
import com.ripple.frienji.ui.model.BaseViewModel;
import com.ripple.frienji.ui.model.row.ZooRowViewModel;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import rx.Observable;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Kamil Ratajczak
 * @since 09.10.16
 */
public class SelectFrienjiViewModel extends BaseViewModel {
    public final PagePaginatorScrollListener mPagePaginatorScrollListener = new ZooPagePaginatorScrollListener();
    public ZooListAdapter mZooAdapter = new ZooListAdapter();

    @Inject
    public NavigationHandler mNavigator;

    @Inject
    protected IntentProvider mIntentProvider;

    private PagePaginator mPagePaginator;

    @Inject
    public SelectFrienjiViewModel() {
    }

    public void loadUserZoo() {
        fillUserZoo();
    }

    private void handleLoadZooError(@Nullable Throwable throwable) {
        mPagePaginatorScrollListener.disableLoading();
        handleApiError(throwable, R.string.error_cannot_load_user_zoo);
    }

    private void handleLoadUserZooSuccess(@NonNull List<ZooRowViewModel> zooElements) {
        mZooAdapter.fillData(checkNotNull(zooElements));
    }

    private void handleUserZooPageLoadSuccess(@NonNull List<ZooRowViewModel> zooElements) {
        mPagePaginatorScrollListener.disableLoading();
        mZooAdapter.appendData(checkNotNull(zooElements));
    }

    private void fillUserZoo() {
        subscribe(mApi.loadUserZoo()
                .doOnNext(this::savePagePaginator)
                .flatMap(zooResponse -> Observable.from(zooResponse.frienjiZoos()))
                .map(zoo -> ZooRowViewModel.from(mContext, mAvatarReader, mNavigator, mIntentProvider, zoo))
                .toList()
                .doOnSubscribe(() -> mHideLoadingBar.set(false))
                .doOnTerminate(() -> mHideLoadingBar.set(true))
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleLoadUserZooSuccess, this::handleLoadZooError));
    }

    private void savePagePaginator(@NonNull FrienjiZooResponse frienjiZooResponse) {
        mPagePaginator = frienjiZooResponse.pagePaginator();
    }

    protected void fillUserZoo(int page) {
        subscribe(mApi.loadUserZoo(page)
                .doOnNext(this::savePagePaginator)
                .flatMap(zooResponse -> Observable.from(zooResponse.frienjiZoos()))
                .map(zoo -> ZooRowViewModel.from(mContext, mAvatarReader, mNavigator, mIntentProvider, zoo))
                .toList()
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleUserZooPageLoadSuccess, this::handleLoadZooError));
    }

    public class ZooListAdapter extends RecyclerView.Adapter<ZooListAdapter.ZooListViewHolder> {
        public final List<ZooRowViewModel> mModels = new ArrayList<>();

        @Override
        public ZooListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            RowChoseFrienjiBinding rowChoseFrienjiBinding = DataBindingUtil.inflate(inflater, R.layout.row_chose_frienji, parent, false);
            return new ZooListViewHolder(rowChoseFrienjiBinding);
        }

        @Override
        public void onBindViewHolder(@NonNull ZooListViewHolder holder, int position) {
            holder.bind(mModels.get(position));
        }

        @Override
        public int getItemCount() {
            return mModels.size();
        }

        public void fillData(@NonNull List<ZooRowViewModel> models) {
            checkNotNull(models);
            mModels.clear();
            mModels.addAll(models);
            notifyChange();
        }

        public void appendData(@NonNull List<ZooRowViewModel> models) {
            mModels.addAll(checkNotNull(models));
            notifyDataSetChanged();
        }

        public class ZooListViewHolder extends RecyclerView.ViewHolder {
            private RowChoseFrienjiBinding mViewDataBinding;

            public ZooListViewHolder(@NonNull RowChoseFrienjiBinding viewDataBinding) {
                super(viewDataBinding.getRoot());
                mViewDataBinding = viewDataBinding;
            }

            public void bind(@NonNull ZooRowViewModel model) {
                mViewDataBinding.setModel(checkNotNull(model));
                mViewDataBinding.executePendingBindings();
            }
        }
    }

    public class ZooPagePaginatorScrollListener extends PagePaginatorScrollListener {
        @Override public void loadElements() {
            if (mPagePaginator.hasNext()) {
                mIsLoading = true;
                fillUserZoo(mPagePaginator.nextPage());
            }
        }
    }
}