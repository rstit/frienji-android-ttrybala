package com.ripple.frienji.ui.model.activity;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.ripple.frienji.R;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.databinding.RowAvatarBinding;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.ui.binding.ObservableString;
import com.ripple.frienji.ui.model.BaseViewModel;
import com.ripple.frienji.ui.model.row.AvatarRowViewModel;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import rx.Observable;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 29.07.2016
 */
public class ChooseFrienjiViewModel extends BaseViewModel {

    public final ObservableString mTitle = new ObservableString();

    @Inject
    public NavigationHandler mNavigator;

    @Inject
    public AvatarReader mAvatarReader;

    public final AvatarAdapter mAvatarAdapter = new AvatarAdapter();

    @Inject
    public ChooseFrienjiViewModel() {}

    public void fillAvatarList(@Avatar.AvatarType int avatarType) {
        if (avatarType == Avatar.EMOTICON_AVATAR) {
            mTitle.set(mContext.getString(R.string.emoji_title));
            fillEmoticonAvatarList();
        } else if (avatarType == Avatar.IMAGE_AVATAR) {
            mTitle.set(mContext.getString(R.string.choose_frienji_image_title));
            fillImageAvatarList();
        } else if (avatarType == Avatar.GIF_AVATAR) {
            mTitle.set(mContext.getString(R.string.choose_frienji_gif_title));
            fillGifAvatarList();
        }
    }

    private void fillEmoticonAvatarList() {
        subscribe(Observable.defer(() -> mAvatarReader.getEmoticonAvatars())
                .map(avatarName -> AvatarRowViewModel.from(mContext, mNavigator, mIntentProvider, mAvatarReader, avatarName, Avatar.EMOTICON_AVATAR))
                .toList()
                .compose(mObservableTransformers.backgroundOperation())
                .subscribe(mAvatarAdapter::fillEmoticons, this::handleLoadAvatarsError));
    }

    private void fillGifAvatarList() {
        subscribe(Observable.defer(() -> mAvatarReader.getGifAvatarNames())
                .map(avatarName -> AvatarRowViewModel.from(mContext, mNavigator, mIntentProvider, mAvatarReader, avatarName, Avatar.GIF_AVATAR))
                .toList()
                .compose(mObservableTransformers.backgroundOperation())
                .subscribe(mAvatarAdapter::fillEmoticons, this::handleLoadAvatarsError));
    }

    private void fillImageAvatarList() {
        subscribe(Observable.defer(() -> mAvatarReader.getImageAvatarNames())
                .map(avatarName -> AvatarRowViewModel.from(mContext, mNavigator, mIntentProvider, mAvatarReader, avatarName, Avatar.IMAGE_AVATAR))
                .toList()
                .compose(mObservableTransformers.backgroundOperation())
                .subscribe(mAvatarAdapter::fillEmoticons, this::handleLoadAvatarsError));
    }

    protected void handleLoadAvatarsError(@Nullable Throwable throwable) {
        handleApiError(throwable, R.string.error_cannot_load_avatars);
    }

    public class AvatarAdapter extends RecyclerView.Adapter<AvatarAdapter.AvatarViewHolder> {

        public final List<AvatarRowViewModel> mModels = new ArrayList<>();

        @Override
        public AvatarViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            RowAvatarBinding emoticonBinding = DataBindingUtil.inflate(inflater, R.layout.row_avatar, parent, false);

            return new AvatarViewHolder(emoticonBinding);
        }

        public void fillEmoticons(@NonNull List<AvatarRowViewModel> models) {
            mModels.clear();
            mModels.addAll(checkNotNull(models));
            notifyChange();
        }

        @Override
        public void onBindViewHolder(AvatarViewHolder holder, int position) {
            holder.bind(mModels.get(position));
        }

        @Override
        public int getItemCount() {
            return mModels.size();
        }

        public class AvatarViewHolder extends RecyclerView.ViewHolder {

            private RowAvatarBinding mBinding;

            public AvatarViewHolder(@NonNull RowAvatarBinding binding) {
                super(binding.getRoot());
                mBinding = binding;
            }

            public void bind(@NonNull AvatarRowViewModel model) {
                mBinding.setModel(checkNotNull(model));
                mBinding.executePendingBindings();
            }
        }
    }
}