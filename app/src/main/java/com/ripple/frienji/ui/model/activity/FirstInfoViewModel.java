package com.ripple.frienji.ui.model.activity;

import com.ripple.frienji.ui.model.BaseViewModel;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import javax.inject.Inject;

/**
 * @author kamil ratajczak
 * @since 02.08.2016
 */
public class FirstInfoViewModel extends BaseViewModel {

    @Inject
    public NavigationHandler mNavigator;

    @Inject
    public FirstInfoViewModel() {
    }

    public void launchSecondInfoScreen() {
        mNavigator.startActivity(mIntentProvider.createSecondIntroduction());
    }
}