package com.ripple.frienji.ui.model.activity;

import com.ripple.frienji.ui.model.BaseViewModel;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.view.activity.AvatarTypeActivity;
import com.ripple.frienji.ui.view.activity.SignInActivity;
import javax.inject.Inject;

/**
 * @author kamil ratajczak
 * @since 02.08.2016
 */
public class LoginViewModel extends BaseViewModel {

    @Inject
    public NavigationHandler mNavigator;

    @Inject
    public LoginViewModel() {
    }

    public void login() {
        mNavigator.startActivity(SignInActivity.class);
    }

    public void register() {
        mNavigator.startActivity(AvatarTypeActivity.class);
    }
}