package com.ripple.frienji.ui.model.activity;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.common.base.Strings;
import com.ripple.frienji.R;
import com.ripple.frienji.model.auth.SignUpBody;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.response.CheckUserNameResponse;
import com.ripple.frienji.settings.AppSettings;
import com.ripple.frienji.ui.binding.ObservableString;
import com.ripple.frienji.ui.model.BaseViewModel;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.util.validation.UserNameValidator;
import com.ripple.frienji.ui.view.activity.BaseActivity;
import javax.inject.Inject;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 01.08.2016
 */
public class WhoAreYouViewModel extends BaseViewModel {

    public final ObservableString mUserName = new ObservableString();
    public final ObservableString mWhoYouAre = new ObservableString();
    public final ObservableString mUserNameError = new ObservableString();
    public final ObservableString mWhoYouAreError = new ObservableString();

    @Inject
    public AppSettings mAppSettings;

    @Inject
    public NavigationHandler mNavigator;

    @Inject
    public UserNameValidator mUserNameValidator;

    protected Avatar mAvatar;

    @Inject
    public WhoAreYouViewModel() {
    }

    public void populateAvatar(@NonNull Avatar avatar) {
        mAvatar = checkNotNull(avatar);
    }

    public void send() {
        if (validateInput(mUserName.get(), mWhoYouAre.get())) {
            checkUserNameAvailable(mUserName.get());
        }
    }

    protected @NonNull SignUpBody createSignInBody() {
        return SignUpBody.builder().userName(mUserName.get())
                .whoYouAre(mWhoYouAre.get())
                .avatar(mAvatar)
                .build();
    }

    protected void checkUserNameAvailable(@NonNull String userName) {
        subscribe(mApi.checkUserName(checkNotNull(userName))
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleUserNameAvailableSuccess, this::handleApiError));
    }

    protected void startConfirmationCodeScreen() {
        SignUpBody signUpBody = createSignInBody();
        mNavigator.startActivity(mIntentProvider.createSignUp(signUpBody));
    }

    protected void handleUserNameAvailableSuccess(@NonNull CheckUserNameResponse checkUserNameResponse) {
        if (checkUserNameResponse.usernameAvailable()) {
            startConfirmationCodeScreen();
        } else {
            mEventBus.post(new BaseActivity.ShowSnackbarEvent(R.string.error_user_name_already_in_use));
        }
    }

    protected void handleApiError(@Nullable Throwable throwable) {
        handleApiError(throwable, R.string.error_no_internet);
    }

    protected boolean validateInput(@NonNull String userName, @NonNull String whoYouAre) {
        boolean valid = true;

        if (!mUserNameValidator.isValid(checkNotNull(userName))) {
            mUserNameError.set(mContext.getString(R.string.input_error_username_not_valid));
            valid = false;
        } else {
            mUserNameError.set(null);
        }

        if (Strings.isNullOrEmpty(checkNotNull(whoYouAre))) {
            mWhoYouAreError.set(mContext.getString(R.string.input_error_whoyouare_not_valid));
            valid = false;
        } else {
            mWhoYouAreError.set(null);
        }

        return valid;
    }
}