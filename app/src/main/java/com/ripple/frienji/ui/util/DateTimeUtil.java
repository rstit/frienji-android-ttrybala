package com.ripple.frienji.ui.util;

import android.support.annotation.NonNull;
import android.text.format.DateUtils;
import java.util.Date;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 10.08.2016
 */
public class DateTimeUtil {
    public @NonNull String getTimestampString(@NonNull Date dateTime) {
        return String.valueOf(DateUtils.getRelativeTimeSpanString(dateTime.getTime(), System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS,
                DateUtils.FORMAT_ABBREV_RELATIVE));
    }
}
