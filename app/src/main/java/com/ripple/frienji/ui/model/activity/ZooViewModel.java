package com.ripple.frienji.ui.model.activity;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.RowZooBinding;
import com.ripple.frienji.model.page.PagePaginator;
import com.ripple.frienji.model.request.RejectFrienjiesRequest;
import com.ripple.frienji.model.response.FrienjiZooResponse;
import com.ripple.frienji.model.response.StatusResponse;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.ui.binding.ObservableString;
import com.ripple.frienji.ui.model.row.ZooRowViewModel;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.view.activity.BaseActivity;
import com.ripple.frienji.ui.view.activity.InBoxActivity;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import rx.Observable;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 11.08.2016
 */
public class ZooViewModel extends CoachMarkViewModel implements ZooRowViewModel.RemoveFrienjiListener {
    public final ObservableInt mProgressBarType = new ObservableInt(PROGRESS_BAR_LOADING);
    public final ObservableBoolean mEnableButtons = new ObservableBoolean(true);

    public final ObservableBoolean mEditMode = new ObservableBoolean();
    public final ObservableBoolean mHasUsersToDelete = new ObservableBoolean();
    public final List<Long> mZooUsersToDelete = new ArrayList<>();
    public final PagePaginatorScrollListener mPagePaginatorScrollListener = new ZooPagePaginatorScrollListener();
    public ZooListAdapter mZooAdapter = new ZooListAdapter();

    public final ObservableString mUnreadMessagesNumber = new ObservableString();

    @Inject
    public NavigationHandler mNavigator;

    @Inject
    protected IntentProvider mIntentProvider;

    private PagePaginator mPagePaginator;

    @Inject
    public ZooViewModel() {
    }

    public void loadUserZoo() {
        showCoachmarkIfNecessary();
        loadUserInfo();
        fillUserZoo();
    }

    protected void showCoachmarkIfNecessary() {
        mShowCoachMark.set(!mAppSettings.shouldSkipZooCoachMark());
    }

    public void handleLoadZooError(@Nullable Throwable throwable) {
        mPagePaginatorScrollListener.disableLoading();
        handleApiError(throwable, R.string.error_cannot_load_user_zoo);
    }

    public void handleDeleteZooError(@Nullable Throwable throwable) {
        handleApiError(throwable, R.string.error_cannot_delete_user_zoo);
    }

    public void openMessageBox() {
        mNavigator.startActivity(InBoxActivity.class);
    }

    public void switchToEditMode() {
        mEditMode.set(true);
        mZooAdapter.setEditMode(true);
    }

    public void deleteUsers() {
        if (!mZooUsersToDelete.isEmpty()) {
            deleteZooUsers();
        }
    }

    public void cancelEditMode() {
        cleanDeleteMode();
    }

    @Override
    public void removeUser(@NonNull ZooRowViewModel zooRowViewModel) {
        if (mZooAdapter.removeElement(checkNotNull(zooRowViewModel))) {
            mHasUsersToDelete.set(true);
            mZooUsersToDelete.add(zooRowViewModel.getUserId());
        }
    }

    private void cleanDeleteMode() {
        mZooUsersToDelete.clear();
        mEditMode.set(false);
        mZooAdapter.setEditMode(false);
        mHasUsersToDelete.set(false);
        loadUserZoo();
    }

    protected void handleLoadUserZooSuccess(@NonNull List<ZooRowViewModel> zooElements) {
        mZooAdapter.fillData(checkNotNull(zooElements));
    }

    private void handleUserZooPageLoadSuccess(@NonNull List<ZooRowViewModel> zooElements) {
        mPagePaginatorScrollListener.disableLoading();
        mZooAdapter.appendData(checkNotNull(zooElements));
    }

    public void loadUserInfo() {
        subscribe(mApi.loadUserProfile()
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleLoadUserProfileSuccess, this::handleLoadZooError));
    }

    protected void handleLoadUserProfileSuccess(@NonNull UserProfile userProfile) {
        if (userProfile.unreadMessagesCount() > 0) {
            mUnreadMessagesNumber.set(String.valueOf(userProfile.unreadMessagesCount()));
        } else {
            mUnreadMessagesNumber.set(null);
        }
    }

    private void fillUserZoo() {
        subscribe(mApi.loadUserZoo()
                .doOnNext(this::savePagePaginator)
                .flatMap(zooResponse -> Observable.from(zooResponse.frienjiZoos()))
                .map(zoo -> ZooRowViewModel.from(mContext, mAvatarReader, mNavigator, mIntentProvider, zoo))
                .doOnNext(zooRowViewModel -> zooRowViewModel.setRemoveFrienjiListener(this))
                .toList()
                .doOnSubscribe(() -> {
                    mHideLoadingBar.set(false);
                    mProgressBarType.set(PROGRESS_BAR_LOADING);
                    mEnableButtons.set(false);
                })
                .doOnTerminate(() -> {
                    mHideLoadingBar.set(true);
                    mEnableButtons.set(true);
                })
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleLoadUserZooSuccess, this::handleLoadZooError));
    }

    protected void fillUserZoo(int page) {
        subscribe(mApi.loadUserZoo(page)
                .doOnNext(this::savePagePaginator)
                .flatMap(zooResponse -> Observable.from(zooResponse.frienjiZoos()))
                .map(zoo -> ZooRowViewModel.from(mContext, mAvatarReader, mNavigator, mIntentProvider, zoo))
                .doOnNext(zooRowViewModel -> zooRowViewModel.setRemoveFrienjiListener(this))
                .toList()
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleUserZooPageLoadSuccess, this::handleLoadZooError));
    }

    private void deleteZooUsers() {
        subscribe(mApi.rejectFrienjiBulks(RejectFrienjiesRequest.builder().ids(mZooUsersToDelete).build())
                .doOnSubscribe(() -> {
                    mHideLoadingBar.set(false);
                    mProgressBarType.set(PROGRESS_BAR_REMOVING);
                    mEnableButtons.set(false);
                })
                .doOnTerminate(() -> {
                    mHideLoadingBar.set(true);
                    mEnableButtons.set(true);
                })
                .compose(mObservableTransformers.networkOperation())
                .subscribe(this::handleDeleteSuccess, this::handleDeleteZooError));
    }

    private void handleDeleteSuccess(@NonNull StatusResponse statusResponse) {
        if (StatusResponse.SUCCESS_STATUS.equals(statusResponse.status())) {
            cleanDeleteMode();
            mEventBus.post(new BaseActivity.ShowSnackbarEvent(R.string.delete_success_message));
        }
    }

    @Override
    public @NonNull String getCoachMarkTitle() {
        return mContext.getString(R.string.zoo_coach_mark_title);
    }

    @Override
    public @NonNull String getCoachMarkContent() {
        return mContext.getString(R.string.zoo_coach_mark_content);
    }

    @Override
    public void hideCoachMark() {
        mAppSettings.hideZooCoachMark();
        mShowCoachMark.set(false);
    }

    private void savePagePaginator(@NonNull FrienjiZooResponse frienjiZooResponse) {
        mPagePaginator = frienjiZooResponse.pagePaginator();
    }

    public class ZooListAdapter extends RecyclerView.Adapter<ZooListAdapter.ZooListViewHolder> {

        public final List<ZooRowViewModel> mModels = new ArrayList<>();

        @Override
        public ZooListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());

            RowZooBinding rowZooBinding = DataBindingUtil.inflate(inflater, R.layout.row_zoo, parent, false);

            return new ZooListViewHolder(rowZooBinding);
        }

        @Override
        public void onBindViewHolder(@NonNull ZooListViewHolder holder, int position) {
            holder.bind(mModels.get(position));
        }

        @Override
        public int getItemCount() {
            return mModels.size();
        }

        public boolean removeElement(@NonNull ZooRowViewModel zooRowViewModel) {
            int position = mModels.indexOf(checkNotNull(zooRowViewModel));

            if (mModels.remove(zooRowViewModel)) {
                notifyItemRemoved(position);
                return true;
            } else {
                return false;
            }
        }

        public void fillData(@NonNull List<ZooRowViewModel> models) {
            checkNotNull(models);
            mModels.clear();
            mModels.addAll(models);
            notifyChange();
        }

        public void appendData(@NonNull List<ZooRowViewModel> models) {
            if (mEditMode.get()) {
                for (ZooRowViewModel zooRowViewModel : models) {
                    zooRowViewModel.mEditMode.set(true);
                }
            }
            mModels.addAll(checkNotNull(models));
            notifyDataSetChanged();
        }

        public void setEditMode(boolean editMode) {
            for (ZooRowViewModel zooRowViewModel : mModels) {
                zooRowViewModel.mEditMode.set(editMode);
            }
        }

        public class ZooListViewHolder extends RecyclerView.ViewHolder {

            private RowZooBinding mViewDataBinding;

            public ZooListViewHolder(@NonNull RowZooBinding viewDataBinding) {
                super(viewDataBinding.getRoot());
                mViewDataBinding = viewDataBinding;
            }

            public void bind(@NonNull ZooRowViewModel model) {
                mViewDataBinding.setModel(checkNotNull(model));
                mViewDataBinding.executePendingBindings();
            }
        }
    }

    public class ZooPagePaginatorScrollListener extends PagePaginatorScrollListener {
        @Override public void loadElements() {
            if (mPagePaginator.hasNext()) {
                mIsLoading = true;
                fillUserZoo(mPagePaginator.nextPage());
            }
        }
    }
}