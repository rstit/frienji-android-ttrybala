package com.ripple.frienji.ui.view.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.google.common.eventbus.Subscribe;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.ActivityCommentsBinding;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.ViewModelModule;
import com.ripple.frienji.di.module.ViewModule;
import com.ripple.frienji.ui.model.activity.CommentsViewModel;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.view.dialog.DeletePostConfirmDialogFragment;
import javax.inject.Inject;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 11.08.2016
 */
public class CommentsActivity extends BaseActivity {
    private static final String EXTRA_FRIENJI_ID = "frienjiId";
    private static final String EXTRA_POST_ID = "postId";

    @Inject
    protected CommentsViewModel mModel;

    @Inject
    protected NavigationHandler mNavigationHandler;

    public static @NonNull Intent createIntent(long frienjiId, long postId, @NonNull Context context) {
        Intent intent = new Intent(checkNotNull(context), CommentsActivity.class);
        intent.putExtra(EXTRA_FRIENJI_ID, frienjiId);
        intent.putExtra(EXTRA_POST_ID, postId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrienjiApplication app = (FrienjiApplication) getApplication();

        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new ViewModule(this)).plus(new ViewModelModule()).inject(this);

        ActivityCommentsBinding mBinding = DataBindingUtil.setContentView(this, R.layout.activity_comments);
        mBinding.setModel(mModel);

        fetchExtras();
    }

    @Subscribe
    public void onDeleteCommentEvent(@NonNull CommentsViewModel.DeleteCommentEvent event) {
        DeletePostConfirmDialogFragment.createInstance(event.getId(), event.getPosition()).show(getSupportFragmentManager(), null);
    }

    @Subscribe
    public void onDeleteCommentConfirmEvent(@NonNull DeletePostConfirmDialogFragment.DeletePostConfirmEvent event) {
        mModel.deleteComment(event.getPostId(), event.getPosition());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        mNavigationHandler.handleOnActivityResult(requestCode, resultCode, intent);
    }

    @Override public void onBackPressed() {
        if (mModel.hasNewMessageContent()) {
            mModel.clearNewMessageData();
        } else {
            super.onBackPressed();
        }
    }

    private void fetchExtras() {
        Bundle extras = getIntent().getExtras();

        if (extras != null && extras.containsKey(EXTRA_FRIENJI_ID) && extras.containsKey(EXTRA_POST_ID)) {
            long frienjiId = extras.getLong(EXTRA_FRIENJI_ID);
            long postId = extras.getLong(EXTRA_POST_ID);
            mModel.fillMessageCommentsList(frienjiId, postId);
        }
    }
}