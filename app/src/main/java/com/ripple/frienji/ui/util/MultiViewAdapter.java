package com.ripple.frienji.ui.util;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ripple.frienji.ui.model.RowViewModel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Marcin Przepiórkowski
 * @since 20.02.2016
 */
public class MultiViewAdapter extends RecyclerView.Adapter<MultiViewAdapter.BaseViewHolder> {

    public interface ViewHolderCreator<T extends RowViewModel> {
        @NonNull
        BaseViewHolder<T> create(@NonNull LayoutInflater inflater, @NonNull ViewGroup parent);
    }

    private final Map<Integer, ViewHolderCreator> mViewHolderCreators = new HashMap<>();
    private final Map<Class<?>, Integer> mClassToTypeMap = new HashMap<>();
    private final List<RowViewModel> mModels;

    private RecyclerView mRecyclerView;
    private int mViewTypeCounter = 1;

    public static abstract class BaseViewHolder<T extends RowViewModel> extends RecyclerView.ViewHolder {
        public BaseViewHolder(View itemView) {
            super(itemView);
        }

        public abstract void bind(final @NonNull T model);
    }

    public MultiViewAdapter(@NonNull List<RowViewModel> models) {
        mModels = checkNotNull(models);
    }

    public <T extends RowViewModel> void addViewHolderCreator(@NonNull Class<T> cls,
                                                              @NonNull ViewHolderCreator creator) {
        if (mClassToTypeMap.containsKey(checkNotNull(cls))) {
            throw new IllegalArgumentException("cls already added");
        }

        mViewHolderCreators.put(mViewTypeCounter, creator);
        mClassToTypeMap.put(cls, mViewTypeCounter++);
    }

    @Override
    public @NonNull BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolderCreator creator = mViewHolderCreators.get(viewType);
        return creator.create(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        //noinspection unchecked
        holder.bind(mModels.get(position));
    }

    @Override
    public int getItemCount() {
        return mModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        RowViewModel model = mModels.get(position);
        Integer type = mClassToTypeMap.get(model.getClass());
        if (type == null) {
            throw new IllegalArgumentException("unknown view model type, did you forget to register it?");
        }

        return type;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        mRecyclerView = recyclerView;
    }

    /**
     * For tests only because {@link #notifyDataSetChanged()} is declared final.
     */
    public void refresh() {
        notifyDataSetChanged();
    }

    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }
}