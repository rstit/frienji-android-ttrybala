package com.ripple.frienji.ui.view.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.google.common.eventbus.Subscribe;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.ActivityArBinding;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.ViewModelModule;
import com.ripple.frienji.di.module.ViewModule;
import com.ripple.frienji.model.frienji.Frienji;
import com.ripple.frienji.ui.model.activity.ArViewModel;
import com.ripple.frienji.ui.view.dialog.CatchFrienjiDialogFragment;
import com.ripple.frienji.ui.view.fragment.ArFragment;
import javax.inject.Inject;

/**
 * @author kamil ratajczak
 * @since 03.08.2016
 */
public class ArActivity extends BaseActivity {

    @Inject
    protected ArViewModel mModel;

    @Inject
    protected CatchFrienjiDialogFragment mCatchFrienjiDialogFragment;

    private ArFragment mArFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrienjiApplication app = (FrienjiApplication) getApplication();

        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new ViewModule(this)).plus(new ViewModelModule()).inject(this);

        ActivityArBinding mBinding = DataBindingUtil.setContentView(this, R.layout.activity_ar);
        mBinding.setModel(mModel);

        mModel.showCoachMark();

        mArFragment = new ArFragment();

       getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.flContainer, mArFragment)
                .commit();
    }

    @Subscribe
    public void onBubbleCatchEvent(@NonNull ArFragment.BubbleCatchEvent event) {
        if (Frienji.RELATION_TYPE_SAVED.equals(event.getFrienji().relationType())) {
            mModel.openFrienjiProfile(event.getFrienji());
        } else {
            mModel.hideTopBar();
            mCatchFrienjiDialogFragment.uploadFrienji(event.getFrienji());
            mCatchFrienjiDialogFragment.show(getSupportFragmentManager(), null);
        }
    }

    @Subscribe
    public void onDismissDialogEvent(@NonNull CatchFrienjiDialogFragment.OnDismissDialogEvent event) {
        mModel.showTopBar();
    }
}