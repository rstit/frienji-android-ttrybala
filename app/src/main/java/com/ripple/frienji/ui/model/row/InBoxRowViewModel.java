package com.ripple.frienji.ui.model.row;

import android.content.Context;
import android.databinding.ObservableBoolean;
import android.support.annotation.NonNull;
import com.google.common.base.Strings;
import com.ripple.frienji.R;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.conversation.Conversation;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.ui.binding.ObservableString;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.util.DateTimeUtil;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 12.08.2016
 */
public class InBoxRowViewModel extends SimpleRowViewModel {
    private final static String ERROR_TEXT = "INCORRECT DATA";

    public final ObservableString mCreationTime = new ObservableString();
    public final ObservableBoolean mUnreadMessage = new ObservableBoolean();

    protected NavigationHandler mNavigationHandler;
    protected UserProfile mAuthorUserProfile;
    protected IntentProvider mIntentProvider;

    private InBoxRowViewModel(@NonNull Context context, @NonNull NavigationHandler navigationHandler, @NonNull AvatarReader avatarReader,
                              @NonNull IntentProvider intentProvider, @NonNull String userName,
                              @NonNull String avatarName, int avatarType, @NonNull String content, @NonNull String createdAt) {
        super(checkNotNull(context), checkNotNull(avatarReader), checkNotNull(userName), checkNotNull(avatarName), avatarType, checkNotNull(content));
        mCreationTime.set(checkNotNull(createdAt));
        mNavigationHandler = checkNotNull(navigationHandler);
        mIntentProvider = checkNotNull(intentProvider);
    }

    public static @NonNull InBoxRowViewModel from(@NonNull Context context, @NonNull NavigationHandler navigationHandler, @NonNull AvatarReader avatarReader,
                                                  @NonNull DateTimeUtil dateTimeUtil, @NonNull IntentProvider intentProvider, @NonNull Conversation conversation) {

        if (conversation.receiver() != null && conversation.receiver().avatar() != null && conversation.lastMessage() != null) {

            String content;
            if (Strings.isNullOrEmpty(conversation.lastMessage().content())) {
                content = context.getString(R.string.message_with_image);
            } else {
                content = conversation.lastMessage().content();
            }

            InBoxRowViewModel inBoxRowViewModel = new InBoxRowViewModel(checkNotNull(context), checkNotNull(navigationHandler), checkNotNull(avatarReader),
                    checkNotNull(intentProvider),
                    conversation.receiver().userName(),
                    conversation.receiver().avatar().name(),
                    conversation.receiver().avatar().type(),
                    content,
                    dateTimeUtil.getTimestampString(conversation.lastMessage().createdAt()));
            inBoxRowViewModel.mAuthorUserProfile = conversation.receiver();
            inBoxRowViewModel.mUnreadMessage.set(conversation.unreadCount() > 0);

            return inBoxRowViewModel;
        } else {
            return new InBoxRowViewModel(checkNotNull(context), checkNotNull(navigationHandler), checkNotNull(avatarReader),
                    checkNotNull(intentProvider),
                    ERROR_TEXT,
                    Avatar.ERROR_AVATAR_NAME,
                    Avatar.EMOTICON_AVATAR,
                    ERROR_TEXT,
                    ERROR_TEXT);
        }
    }

    public void openConversation() {
        mNavigationHandler.startActivity(mIntentProvider.createConversation(mAuthorUserProfile));
    }
}