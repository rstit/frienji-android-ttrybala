package com.ripple.frienji.ui.view.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.DialogModule;

/**
 * @author Kamil Ratajczak
 * @since 11.10.16
 */
public class LogOutConfirmDialogFragment extends ConfirmDialogFragment {

    public static @NonNull LogOutConfirmDialogFragment createInstance() {
        LogOutConfirmDialogFragment logOutConfirmDialogFragment = new LogOutConfirmDialogFragment();
        return logOutConfirmDialogFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrienjiApplication app = (FrienjiApplication) getActivity().getApplication();
        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new DialogModule(getActivity())).inject(this);
    }

    @NonNull @Override protected String getTitle() {
        return getContext().getString(R.string.logout_dialog_title);
    }

    @NonNull @Override protected String getConfirmMessage() {
        return getContext().getString(R.string.logout_dialog_message);
    }

    @NonNull @Override protected ConfirmEvent getConfirmEvent() {
        return new ConfirmEvent();
    }
}