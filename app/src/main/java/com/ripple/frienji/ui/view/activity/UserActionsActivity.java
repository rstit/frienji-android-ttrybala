package com.ripple.frienji.ui.view.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.ActivityUserActionsBinding;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.ViewModelModule;
import com.ripple.frienji.di.module.ViewModule;
import com.ripple.frienji.di.scope.ActivityScope;
import com.ripple.frienji.ui.model.activity.UserActionsViewModel;
import javax.inject.Inject;

/**
 * @author kamil ratajczak
 * @since 30.07.2016
 */
public class UserActionsActivity extends BaseActivity {

    @Inject
    protected UserActionsViewModel mModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrienjiApplication app = (FrienjiApplication) getApplication();

        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new ViewModule(this)).plus(new ViewModelModule()).inject(this);

        ActivityUserActionsBinding mBinding = DataBindingUtil.setContentView(this, R.layout.activity_user_actions);
        mBinding.setModel(mModel);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mModel.fillUserActions();
    }
}