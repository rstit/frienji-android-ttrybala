package com.ripple.frienji.ui.util.requierements;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import com.google.android.gms.common.api.GoogleApiClient;
import com.ripple.frienji.R;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Tomasz Trybala
 * @since 2016-08-08
 */
public class GoogleApiClientRequirement extends InstrumentationStateRequirement {
    private GoogleApiClient mGoogleApiClient;

    public GoogleApiClientRequirement(@NonNull GoogleApiClient googleApiClient) {
        mGoogleApiClient = checkNotNull(googleApiClient);
    }

    @Override
    public void resolve() {
        mGoogleApiClient.connect();
    }

    @Override
    public void undo() {
        super.undo();
        mGoogleApiClient.disconnect();
    }

    @Override
    public @StringRes int getErrorMessage() {
        return R.string.google_api_client_error;
    }
}
