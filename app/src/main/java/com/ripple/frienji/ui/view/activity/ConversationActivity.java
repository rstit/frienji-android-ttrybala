package com.ripple.frienji.ui.view.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import com.google.common.eventbus.Subscribe;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.databinding.ActivityConversationBinding;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.ViewModelModule;
import com.ripple.frienji.di.module.ViewModule;
import com.ripple.frienji.firebase.FrienjiFirebaseMessagingService;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.settings.AppSettings;
import com.ripple.frienji.ui.model.activity.ConversationViewModel;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.util.rx.ObservableTransformers;
import javax.inject.Inject;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author kamil ratajczak
 * @since 11.08.2016
 */
public class ConversationActivity extends BaseActivity {
    private static final String EXTRA_USER_PROFILE = "userProfile";

    @Inject
    public ObservableTransformers mObservableTransformers;

    @Inject
    protected ConversationViewModel mModel;

    @Inject
    protected NavigationHandler navigationHandler;

    @Inject
    protected AppSettings mAppSettings;

    public static @NonNull Intent createIntent(@NonNull UserProfile userProfile, @NonNull Context context) {
        Intent intent = new Intent(checkNotNull(context), ConversationActivity.class);
        intent.putExtra(EXTRA_USER_PROFILE, checkNotNull(userProfile));
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrienjiApplication app = (FrienjiApplication) getApplication();

        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new ViewModule(this)).plus(new ViewModelModule()).inject(this);

        ActivityConversationBinding mBinding = DataBindingUtil.setContentView(this, R.layout.activity_conversation);
        mBinding.setModel(mModel);

        fetchExtras();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        navigationHandler.handleOnActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public void onBackPressed() {
        if (mModel.hasNewMessageContent()) {
            mModel.clearNewMessageData();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mAppSettings.setConversationUserProfileId(-1);
    }

    @Override
    public void onResume() {
        super.onResume();
        mAppSettings.setConversationUserProfileId(mModel.getConversationUserId());
        mModel.loadConversation();
    }

    private void fetchExtras() {
        Bundle extras = getIntent().getExtras();

        if (extras != null && extras.containsKey(EXTRA_USER_PROFILE)) {
            UserProfile userProfile = extras.getParcelable(EXTRA_USER_PROFILE);
            if (userProfile != null) {
                mModel.loadUserProfile(userProfile);
            }
        }
    }

    @Subscribe
    public void onAddChatMessageEvent(@NonNull FrienjiFirebaseMessagingService.AddChatMessageEvent event) {
        Runnable myRunnable = () -> mModel.addChatMessage(event.getChatMessage());
        runOnUiThread(myRunnable);
    }
}