package com.ripple.frienji.ui.util.instrumentation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.common.eventbus.EventBus;
import com.ripple.frienji.ui.view.fragment.BaseFragment;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Tomasz Trybala
 * @since 2016-07-21
 */
public class FragmentInstrumentationProvider implements InstrumentationProvider {

    private final BaseFragment mFragment;

    private FragmentInstrumentationProvider(@NonNull BaseFragment baseFragment) {
        mFragment = checkNotNull(baseFragment);
    }

    @Override
    public void startService(@NonNull Intent intent) {
        Context c = getActivityContext();
        if (c != null) {
            c.startService(checkNotNull(intent));
        }
    }

    @Override
    public void startActivity(@NonNull Intent intent) {
        checkNotNull(intent);

        Context c = getActivityContext();
        if (c != null) {
            c.startActivity(intent);
        }
    }

    @Override
    public void startActivity(@NonNull Class<? extends AppCompatActivity> activity) {
        checkNotNull(activity);

        Context context = getActivityContext();
        if (context != null) {
            Intent intent = new Intent(context, activity);
            context.startActivity(intent);
        }
    }

    @Override
    public void finishActivityWithResult(int resultCode, @NonNull Intent intent) {
        checkNotNull(intent);

        Activity activity = (Activity) getActivityContext();
        if (activity != null) {
            activity.setResult(resultCode, intent);
            activity.finish();
        }
    }

    @Override
    public void startActivityForResult(@NonNull Intent intent, int requestCode) {
        mFragment.startActivityForResult(checkNotNull(intent), requestCode);
    }

    @Override
    public void startActivityAndFinish(@NonNull Class<? extends AppCompatActivity> activity) {
        Activity context = (Activity) getActivityContext();
        if (context != null) {
            Intent intent = new Intent(context, checkNotNull(activity));
            context.startActivity(intent);
            context.finish();
        }
    }

    @Override
    public void finishActivity() {
        Activity activity = (Activity) getActivityContext();
        if (activity != null) {
            activity.finish();
        }
    }

    @Override
    public void finishAffinity() {
        Activity activity = (Activity) getActivityContext();
        if (activity != null) {
            activity.finishAffinity();
        }
    }

    @Override
    public void onBackPressed() {
        Activity activity = (Activity) getActivityContext();
        if (activity != null) {
            activity.onBackPressed();
        }
    }

    @Override
    public void addFragment(@NonNull Fragment fragment) {
        if (mFragment.getActivity() != null) {
            mFragment.getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .add(android.R.id.content, checkNotNull(fragment))
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    public void replaceFragment(@NonNull Fragment fragment) {
    }

    @Override
    public void replaceFragment(@IdRes int layout, @NonNull Fragment fragment) {
        if (mFragment.getActivity() != null) {
            mFragment.getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(layout, checkNotNull(fragment))
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .commit();
        }
    }

    @Override
    public void replaceFragmentAndAddToBackStack(@NonNull Fragment fragment) {
    }

    @Override
    public void replaceFragmentAndAddToBackStack(@IdRes int layout, @NonNull Fragment fragment) {
        if (mFragment.getActivity() != null) {
            mFragment.getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(layout, checkNotNull(fragment))
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    public void replaceChildFragment(@NonNull Fragment fragment) {
    }

    @Override
    public void replaceChildFragment(@IdRes int layout, @NonNull Fragment fragment) {
        if (mFragment.getActivity() != null) {
            mFragment.getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(layout, checkNotNull(fragment))
                    .commit();
        }
    }

    @Override
    public void replaceChildFragmentAndAddToBackStack(@NonNull Fragment fragment) {
    }

    @Override
    public void replaceChildFragmentAndAddToBackStack(@IdRes int layout, @NonNull Fragment fragment) {
        if (mFragment.getActivity() != null) {
            mFragment.getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(layout, checkNotNull(fragment))
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    public void popBackStack() {
        if (mFragment.getActivity() != null) {
            mFragment.getActivity().getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void safePopBackStack() {
        if (mFragment.getActivity() != null) {
            if (mFragment.getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                popBackStack();
            } else {
                finishActivity();
            }
        }
    }

    @Override
    public void hideKeyboard() {
        if (mFragment.getActivity() != null && mFragment.getActivity().getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) mFragment.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mFragment.getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    public int checkPermission(@NonNull String permission) {
        Context context = getActivityContext();
        if (context != null) {
            return ContextCompat.checkSelfPermission(getActivityContext(), permission);
        } else {
            return PackageManager.PERMISSION_DENIED;
        }
    }

    @Override
    public void requestPermissions(@NonNull String[] permissions, int requestCode) {
        mFragment.requestPermissions(permissions, requestCode);
    }

    @Override
    @Nullable
    public Context getActivityContext() {
        return mFragment.getActivity();
    }

    @Override
    @NonNull
    public Resources getResources() {
        return mFragment.getResources();
    }

    @Override
    @NonNull
    public String getResString(@StringRes int id) {
        return mFragment.getString(id);
    }

    @Override
    @NonNull
    public FragmentManager getFragmentManager() {
        return mFragment.getChildFragmentManager();
    }

    @Override
    @Nullable
    public GoogleApiClient getGoogleApiClient() {
        return null;
    }

    @NonNull
    public static FragmentInstrumentationProvider from(@NonNull BaseFragment baseFragment) {
        return new FragmentInstrumentationProvider(baseFragment);
    }
}