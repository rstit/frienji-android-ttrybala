package com.ripple.frienji.ar.model;

import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import com.ripple.frienji.R;
import com.ripple.frienji.util.ShadersUtils;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * @author Tomasz Trybala
 * @since 2016-07-20
 */

public class DirectVideo {
    private static final int FLOAT_SIZE_BYTES = 4;
    private static final int DRAW_FIRST_POSITION = 0;
    private static final short DRAW_ORDER[] = {0, 1, 2, 0, 2, 3};
    private static final int COORDS_PER_VERTEX = 2;
    private static final float SQUARE_COORDS[] = {
            -1.0f, 1.0f,
            -1.0f, -1.0f,
            1.0f, -1.0f,
            1.0f, 1.0f,
    };

    private static final String SHADER_HANDLER_POSITION = "vPosition";
    private static final String SHADER_HANDLER_COORDINATION = "inputTextureCoordinate";

    private FloatBuffer mVertexBuffer;
    private FloatBuffer mTextureVerticesBuffer;
    private ShortBuffer mDrawListBuffer;

    private int mTexture;
    private int mProgram;

    private int mPositionHandler;
    private int mTextureCoordHandler;

    public DirectVideo(int texture, float[] textureVertices) {
        mTexture = texture;

        initBuffers(textureVertices);
        createProgram();
        handleAttributesFromShaders();
    }

    public void draw() {
        GLES20.glUseProgram(mProgram);
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, mTexture);

        int vertexStride = COORDS_PER_VERTEX * FLOAT_SIZE_BYTES;
        GLES20.glVertexAttribPointer(mPositionHandler, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, vertexStride, mVertexBuffer);
        GLES20.glVertexAttribPointer(mTextureCoordHandler, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, vertexStride, mTextureVerticesBuffer);
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, DRAW_ORDER.length, GLES20.GL_UNSIGNED_SHORT, mDrawListBuffer);
    }

    private void initBuffers(float[] textureVertices) {
        ByteBuffer vertexBuffer = ByteBuffer.allocateDirect(SQUARE_COORDS.length * FLOAT_SIZE_BYTES);
        vertexBuffer.order(ByteOrder.nativeOrder());
        mVertexBuffer = vertexBuffer.asFloatBuffer();
        mVertexBuffer.put(SQUARE_COORDS);
        mVertexBuffer.position(DRAW_FIRST_POSITION);

        ByteBuffer drawListBuffer = ByteBuffer.allocateDirect(DRAW_ORDER.length * FLOAT_SIZE_BYTES / 2);
        drawListBuffer.order(ByteOrder.nativeOrder());
        mDrawListBuffer = drawListBuffer.asShortBuffer();
        mDrawListBuffer.put(DRAW_ORDER);
        mDrawListBuffer.position(DRAW_FIRST_POSITION);

        ByteBuffer textureBuffer = ByteBuffer.allocateDirect(textureVertices.length * FLOAT_SIZE_BYTES);
        textureBuffer.order(ByteOrder.nativeOrder());
        mTextureVerticesBuffer = textureBuffer.asFloatBuffer();
        mTextureVerticesBuffer.put(textureVertices);
        mTextureVerticesBuffer.position(DRAW_FIRST_POSITION);
    }

    private void createProgram() {
        ShadersUtils utils = ShadersUtils.getInstance();
        int vertexShader = utils.loadShader(R.raw.direct_video_vertex_shader, GLES20.GL_VERTEX_SHADER);
        int fragmentShader = utils.loadShader(R.raw.direct_video_fragment_shader, GLES20.GL_FRAGMENT_SHADER);

        mProgram = GLES20.glCreateProgram();
        GLES20.glAttachShader(mProgram, vertexShader);
        GLES20.glAttachShader(mProgram, fragmentShader);
        GLES20.glLinkProgram(mProgram);
    }

    private void handleAttributesFromShaders() {
        mPositionHandler = GLES20.glGetAttribLocation(mProgram, SHADER_HANDLER_POSITION);
        mTextureCoordHandler = GLES20.glGetAttribLocation(mProgram, SHADER_HANDLER_COORDINATION);

        GLES20.glEnableVertexAttribArray(mPositionHandler);
        GLES20.glEnableVertexAttribArray(mTextureCoordHandler);
    }
}