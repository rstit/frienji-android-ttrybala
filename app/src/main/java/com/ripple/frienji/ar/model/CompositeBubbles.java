package com.ripple.frienji.ar.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.common.collect.Collections2;
import com.ripple.frienji.model.frienji.Frienji;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Tomasz Trybala
 * @since 2016-08-04
 */
public class CompositeBubbles implements BubblesCollection {
    private List<Bubble> mBubbles = new ArrayList<>();

    @Override
    public void draw() {
        for (Bubble bubble : mBubbles) {
            if (bubble.shouldBeDrawn() && !bubble.isHidden()) {
                bubble.draw();
            }
        }
    }

    @Override
    public void destroyList() {
        for (Bubble bubble : mBubbles) {
            bubble.destroyBubble();
        }
        mBubbles.clear();
    }

    @Override
    public void updateCoordinates(int azimuth, int pitch) {
        checkArgument(azimuth >= 0 && azimuth < 360);
        checkArgument(pitch >= -90 && pitch <= 90);

        for (int i = 0; i < mBubbles.size(); i++) {
            mBubbles.get(i).setNewXYPositions(pitch, azimuth);
        }
    }

    @Override
    public void clearList() {
        mBubbles.clear();
    }

    @Override
    public void addBubble(@NonNull Bubble bubble) {
        mBubbles.add(checkNotNull(bubble));
    }

    @Override
    public @NonNull List<Bubble> getList() {
        return mBubbles;
    }

    @Override
    public @Nullable Bubble getClickedBubble(int x, int y) {
        for (int i = mBubbles.size() - 1; i >= 0; i--) {
            if (mBubbles.get(i).isClicked(x, y) && !mBubbles.get(i).isHidden()) {
                return mBubbles.get(i);
            }
        }

        return null;
    }

    public @Nullable Bubble getBubble(int frienjiId) {
        for (Bubble bubble : mBubbles) {
            if (bubble.getFrienji().id() == frienjiId) {
                return bubble;
            }
        }

        return null;
    }

    public void removeInvisibleBubbles(@NonNull List<Frienji> frienjies) {
        Set<Integer> ids = new HashSet<>();
        ids.addAll(Collections2.transform(frienjies, Frienji::id));
        mBubbles = new ArrayList<>(
                Collections2.filter(mBubbles, input -> ids.contains(input.getFrienji().id())));
    }
}