package com.ripple.frienji.ar.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.util.List;

/**
 * @author Tomasz Trybala
 * @since 2016-08-04
 */
public interface BubblesCollection {
    void draw();

    void destroyList();

    void updateCoordinates(int azimuth, int pitch);

    void clearList();

    void addBubble(@NonNull Bubble bubble);

    @NonNull List<Bubble> getList();

    @Nullable Bubble getClickedBubble(int x, int y);
}
