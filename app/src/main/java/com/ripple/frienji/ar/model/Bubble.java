/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ripple.frienji.ar.model;

import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.LruCache;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.google.common.base.Strings;
import com.ripple.frienji.BuildConfig;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.R;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.model.avatar.Avatar;
import com.ripple.frienji.model.frienji.Frienji;
import com.ripple.frienji.util.BitmapUtils;
import com.ripple.frienji.util.BubbleProgramUtils;
import com.ripple.frienji.util.ShadersUtils;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import javax.microedition.khronos.opengles.GL10;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Tomasz Trybala
 * @since 2016-07-21
 */
public class Bubble {
    public static final int METERS_DISTANCE_RANGE_1 = 10;
    public static final int METERS_DISTANCE_RANGE_2 = 100;
    public static final int METERS_DISTANCE_RANGE_3 = 300;
    public static final int METERS_DISTANCE_RANGE_4 = 500;
    public static final int METERS_DISTANCE_RANGE_5 = 700;
    public static final int METERS_DISTANCE_RANGE_6 = 1000;
    public static final float MAX_BUBBLE_DISTANCE = 1000f;

    private static final float MIN_BUBBLE_DISTANCE = 10f;

    private static final int KB_NUMBER_OF_BYTES = 1024;

    private static final int GIF_ANIMATION_MILLISECONDS = 200;
    private static final int EMOTICON_ROTATION_ANGLE = -90;

    private static final int FLOAT_SIZE_BYTES = 4;
    private static final int COORDS_PER_VERTEX = 2;
    private static final int DRAW_FIRST_POSITION = 0;
    private static final int DRAW_BITMAP_POSITION = 1;
    private static final short DRAW_ORDER[] = {0, 1, 2, 0, 2, 3}; // order to draw vertices

    private static final float MIN_BUBBLE_RADIUS = 0.2f;
    private static final float MAX_BUBBLE_RADIUS = 0.5f; //max is 1.0f -  half of screen's width

    private static final float MAX_BUBBLE_Y_POSITION = 90f;
    private static final float MAX_BUBBLE_X_POSITION = 360f;

    private static final float MAX_BUBBLE_POSITION_Y_OFFSET = 3f;
    private static final float MAX_BUBBLE_POSITION_X_OFFSET = 6f;

    private static final float FLOATING_MOVE_X = 0.001f;
    private static final float FLOATING_MOVE_Y = 0.001f;

    private static final float FLOATING_MAX_X = 0.5f;
    private static final float FLOATING_START_Y = -0.7f;
    private static final float FLOATING_Y_AREA_INTERVAL = 0.33f;

    private static final float STATE_NORMAL = 0f;
    private static final float STATE_GLOW = 0.6f;

    private static final int BUBBLE_NORMAL = 0;
    private static final int BUBBLE_WITH_GLOW = 1;
    private static final int NO_BUBBLE = 2;

    private static final float ANIMATION_SIZE_FACTOR = 4f;
    private static final float FLOATING_MIN_X = -FLOATING_MAX_X;
    private static final float ANIMATION_GLOW_INTERVAL = 0.02f;
    private static final float MAX_ANIMATION_GLOW_TIME = 5.0f;

    private static final float LOW_PASS_FILTER_VALUE_MAX = 0.75f;
    private static final String SHADER_HANDLER_LOGO_TEXTURE = "LogoTexture";
    private static final String SHADER_HANDLER_POSITION = "Position";
    private static final String SHADER_HANDLER_COORDINATION = "TexCoordIn";

    private static final String SHADER_HANDLER_STATE = "state";
    private static final String SHADER_HANDLER_ANIMATION_TIME = "holdAnimationTime";

    private static final int EMOTICON_SIZE = 50;
    private static final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / KB_NUMBER_OF_BYTES);
    private static final int cacheSize = maxMemory / 8;

    private static final LruCache<String, IntBuffer> mFlatAvatarImages = new LruCache<String, IntBuffer>(cacheSize) {
        @Override
        protected int sizeOf(String key, IntBuffer intBuffer) {
            return intBuffer.array().length / KB_NUMBER_OF_BYTES;
        }
    };

    private static final LruCache<String, List<IntBuffer>> mGifAvatarImages = new LruCache<String, List<IntBuffer>>(cacheSize) {
        @Override
        protected int sizeOf(String key, List<IntBuffer> gifFrames) {
            int totalSize = 0;

            for (IntBuffer intBuffer : gifFrames) {
                totalSize += intBuffer.array().length / KB_NUMBER_OF_BYTES;
            }
            return totalSize;
        }
    };

    private final float mBitmapRotationAngle;
    private final int[] mBitmapTextures = new int[DRAW_BITMAP_POSITION];
    private int mCurrentFrame;
    private FloatBuffer mVertexBuffer;
    private FloatBuffer mTextureVerticesBuffer;
    private ShortBuffer mDrawListBuffer;
    private IntBuffer mBitmapBuffer;

    private float mFloatingMaxY;
    private float mFloatingMinY;

    //region Floating
    private float mFloatingCurrentX;
    private float mFloatingCurrentY;

    //endregion
    private boolean mIfFloatingXToRight;
    private boolean mIfFloatingYToTop;
    private int mBubbleProgram;
    private int mExplosionProgram;
    private int mNoBubbleProgram;

    private Frienji mFrienji;
    private int mPositionHandler;
    private int mTextureCoordinationHandler;
    private int mTextureLogoHandler;

    private int mStateHandler;
    private int mAnimationTimeHandler;

    private float mBubbleCoords[];
    private float mRatio;
    private float mDistanceRatio;
    private float mYPosition;
    private float mNewYPosition;
    private float mCurrentYPosition;
    private float mXPosition;
    private float mNewXPosition;
    private float mCurrentXPosition;
    private float mLeft;
    private float mBottom;
    private float mRight;
    private float mTop;
    private int mScreenWidth;
    private int mScreenHeight;
    private int mBitmapWidth;
    private int mBitmapHeight;
    private float mStartingAzimuth;
    private boolean mIsFirstPosition = true;
    private float mCenterXPosition;
    private float mCenterYPosition;
    private float mCurrentDistance;
    private Timer mGifAnimationTimer;
    private List<IntBuffer> mFrames = new ArrayList<>();
    private boolean mCoordinateUpdated;
    private Bitmap mBitmap;
    private final List<Integer> mBuffers = new ArrayList<>();
    private boolean mWasSizeRefactored;
    private boolean mIsAnimating;
    private boolean mIsHidden;
    private int mState;
    private float mStartedGlowAnimationTime;

    public Bubble(int width, int height, @NonNull float[] textureVertices, float distance, float startingAzimuth,
                  @NonNull Frienji frienji, float bitmapRotationAngle, @NonNull AvatarReader avatarReader) {
        mCurrentFrame = 0;
        checkNotNull(textureVertices);

        if (Frienji.RELATION_TYPE_SAVED.equals(frienji.relationType())) {
            mState = BUBBLE_WITH_GLOW;
        } else if (Frienji.RELATION_TYPE_REJECTED.equals(frienji.relationType())) {
            mState = NO_BUBBLE;
        } else {
            mState = BUBBLE_NORMAL;
        }

        if (frienji.avatar() != null && frienji.avatar().type() == Avatar.IMAGE_AVATAR) {
            mBitmapBuffer = mFlatAvatarImages.get(frienji.avatar().name());
            if (mBitmapBuffer == null) {
                loadBitmap(avatarReader.getImageAvatarPath(frienji.avatar().name()));
            }
        } else if (frienji.avatar() != null && frienji.avatar().type() == Avatar.EMOTICON_AVATAR) {
            mBitmapBuffer = convertTextEmoticonToBitmap(avatarReader.getEmoticonAvatar(frienji.avatar().name()));
        } else if (frienji.avatar() != null && frienji.avatar().type() == Avatar.GIF_AVATAR) {
            mFrames = mGifAvatarImages.get(frienji.avatar().name());
            if (mFrames == null) {
                mFrames = new ArrayList<>();
                loadGifImage(avatarReader.getGifAvatarPath(frienji.avatar().name()));
            }
        } else {
            loadBitmap(frienji.coverPhoto());
        }

        mFrienji = frienji;
        mScreenWidth = width;
        mScreenHeight = height;
        mRatio = (float) mScreenWidth / mScreenHeight;
        mStartingAzimuth = startingAzimuth;
        mBitmapRotationAngle = bitmapRotationAngle;

        calculateDistance(distance);
        randFloatingValues();
        calculateCurrentCoordinates();
        initBuffers(textureVertices);
        createProgram();
        handleAttributesFromShaders();
    }

    public void animate() {
        mBuffers.addAll(getFramesObservable());
        mWasSizeRefactored = false;
        mIsAnimating = true;
        loadAnimationFrame(BitmapUtils.getBitmapSize());
    }

    public boolean isHidden() {
        return mIsHidden;
    }

    public void setIsHidden(boolean isHidden) {
        if (!isHidden) {
            finishAnimation();
        }

        mIsHidden = isHidden;
    }

    public void draw() {
        if (mBitmapBuffer != null && mCoordinateUpdated) {
            if (shouldDisplayAnimationFrame()) {
                GLES20.glUseProgram(mExplosionProgram);
            } else {
                if (mState == NO_BUBBLE) {
                    GLES20.glUseProgram(mNoBubbleProgram);
                } else {
                    GLES20.glUseProgram(mBubbleProgram);
                }
            }
            calculateCurrentCoordinates();
            clearVertexBuffer();

            drawBitmap();

            int vertexStride = COORDS_PER_VERTEX * FLOAT_SIZE_BYTES;
            GLES20.glVertexAttribPointer(mPositionHandler, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, vertexStride, mVertexBuffer);
            GLES20.glVertexAttribPointer(mTextureCoordinationHandler, COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, vertexStride, mTextureVerticesBuffer);
            GLES20.glDrawElements(GLES20.GL_TRIANGLES, DRAW_ORDER.length, GLES20.GL_UNSIGNED_SHORT, mDrawListBuffer);
            GLES20.glDeleteTextures(1, mBitmapTextures, 0);
        }
    }

    public boolean isClicked(int x, int y) {
        float screenSize = 2f; //since -1f to 1f
        float halfScreen = screenSize / 2f;

        float bubbleHeight = Math.abs(mTop - mBottom) / screenSize * mScreenHeight;
        float bubbleWidth = Math.abs(mRight - mLeft) / screenSize * mScreenWidth;

        int left = (int) (((mLeft + halfScreen) / screenSize) * mScreenWidth);
        int top = (int) (((halfScreen - mTop) / screenSize) * mScreenHeight);
        int centerX = left + (int) bubbleWidth / 2;
        int centerY = top + (int) bubbleHeight / 2;
        int bubbleRadius = (int) bubbleHeight / 2;

        int power = 2;
        double dX = Math.pow(x - centerX, power);
        double dY = Math.pow(y - centerY, power);
        double dR = Math.pow(bubbleRadius, power);
        return dX + dY < dR;
    }

    public void setNewXYPositions(float positionY, float positionX) {
        mYPosition = positionY;
        mXPosition = positionX;
        mNewYPosition = getCurrentYOffset();
        mNewXPosition = getXOffsetBetweenStartingAzimuth();
        calculateCurrentCoordinates();
        mCoordinateUpdated = true;
    }

    public void changeCurrentLocation(float distance, float startingAzimuth) {
        mStartingAzimuth = startingAzimuth;
        calculateDistance(distance);
    }

    public boolean shouldBeDrawn() {
        float halfMaxPosition = MAX_BUBBLE_X_POSITION / 2;
        float visibleAngle = MAX_BUBBLE_X_POSITION / 4;

        int newPositionDiff = (int) mXPosition - (int) mStartingAzimuth;
        return ((halfMaxPosition - Math.abs(halfMaxPosition - Math.abs((newPositionDiff)))) < visibleAngle
                && mBitmapBuffer != null && mCoordinateUpdated);
    }

    public void destroyBubble() {
        mVertexBuffer.clear();
        mTextureVerticesBuffer.clear();
        mDrawListBuffer.clear();
        if (mBitmapBuffer != null) {
            mBitmapBuffer.clear();
        }

        if (mGifAnimationTimer != null) {
            mGifAnimationTimer.cancel();
            mGifAnimationTimer.purge();
        }

        if (mFrames != null) {
            for (IntBuffer frameIntBuffer : mFrames) {
                frameIntBuffer.clear();
            }
        }

        if (mFlatAvatarImages != null) {
            Map<String, IntBuffer> snapshot = mFlatAvatarImages.snapshot();
            for (String key : snapshot.keySet()) {
                mFlatAvatarImages.get(key).clear();
            }
            mFlatAvatarImages.evictAll();
        }

        if (mGifAvatarImages != null) {
            Map<String, List<IntBuffer>> snapshot = mGifAvatarImages.snapshot();

            for (String key : snapshot.keySet()) {
                List<IntBuffer> frames = mGifAvatarImages.get(key);
                for (IntBuffer frameIntBuffer : frames) {
                    frameIntBuffer.clear();
                }
            }
            mGifAvatarImages.evictAll();
        }
    }

    public int getStartingAzimuth() {
        return (int) mStartingAzimuth;
    }

    public float getDistance() {
        return mCurrentDistance;
    }

    @NonNull
    public Frienji getFrienji() {
        return mFrienji;
    }

    private @NonNull IntBuffer convertTextEmoticonToBitmap(@NonNull String textEmoticon) {
        Paint paint = new Paint();
        paint.setTextSize(EMOTICON_SIZE);

        float baseline = -paint.ascent(); // ascent() is negative
        int width = (int) (paint.measureText(checkNotNull(textEmoticon)));
        int height = (int) (baseline + paint.descent());

        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(image);
        canvas.drawText(textEmoticon, 0, baseline, paint);

        Matrix matrix = new Matrix();
        matrix.postRotate(EMOTICON_ROTATION_ANGLE);

        Bitmap rotatedBitmap = Bitmap.createBitmap(image, 0, 0, image.getWidth(), image.getHeight(), matrix, true);
        return getBitmapBuffer(rotatedBitmap);
    }

    private void loadGifImage(@NonNull String gifImagePath) {
        new Handler(Looper.getMainLooper()).post(() -> {
            Glide.with(FrienjiApplication.getAppContext()).
                    load(checkNotNull(gifImagePath))
                    .asGif()
                    .into(new SimpleTarget<GifDrawable>() {
                        @Override public void onResourceReady(GifDrawable resource, GlideAnimation<? super GifDrawable> glideAnimation) {
                            resource.start(); // start animation, it will load all gif frames

                            //decode each bitmap frame to IntBuffer
                            for (int i = 0; i < resource.getDecoder().getFrameCount(); i++) {
                                mFrames.add(getBitmapBuffer(resource.getDecoder().getNextFrame()));
                                resource.getDecoder().advance(); // move animation, without this "getNextFrame()" always will return first frame
                            }

                            mGifAvatarImages.put(gifImagePath, mFrames);

                            mGifAnimationTimer = new Timer();
                            mGifAnimationTimer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    animateGifImage();
                                }
                            }, 0, GIF_ANIMATION_MILLISECONDS);
                        }
                    });
        });
    }

    private void animateGifImage() {
        if (mCurrentFrame == mFrames.size()) {
            mCurrentFrame = 0;
        }

        if (mFrames.size() > 0) {
            mBitmapBuffer = mFrames.get(mCurrentFrame);
            mCurrentFrame++;
        }
    }

    private @NonNull List<Integer> getFramesObservable() {
        TypedArray typedArray = FrienjiApplication.getAppContext().getResources().obtainTypedArray(R.array.animation_frames);
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < typedArray.length(); i++) {
            list.add(typedArray.getResourceId(i, 0));
        }
        typedArray.recycle();
        Collections.reverse(list);
        return list;
    }

    private void calculateCurrentCoordinates() {
        calculateFloatingX();
        calculateFloatingY();

        mCenterXPosition = mIsFirstPosition ? mNewXPosition : getLowPassFilterXPosition();
        mCenterYPosition = mIsFirstPosition ? mNewYPosition : getLowPassFilterYPosition();

        mLeft = -mDistanceRatio + mCenterXPosition;
        mRight = mDistanceRatio + mCenterXPosition;
        mBottom = -mDistanceRatio * mRatio + mCenterYPosition;
        mTop = mDistanceRatio * mRatio + mCenterYPosition;

        mBubbleCoords = new float[]{
                mLeft, mTop,
                mLeft, mBottom,
                mRight, mBottom,
                mRight, mTop
        };

        mIsFirstPosition = false;
    }

    private void calculateDistance(float distance) {
        mCurrentDistance = distance;

        if (distance <= MIN_BUBBLE_DISTANCE) {
            mDistanceRatio = MAX_BUBBLE_RADIUS;
        } else if (distance >= MAX_BUBBLE_DISTANCE) {
            mDistanceRatio = MIN_BUBBLE_RADIUS;
        } else {
            float ratio = (distance - MIN_BUBBLE_DISTANCE) / (MAX_BUBBLE_DISTANCE - MIN_BUBBLE_DISTANCE);
            mDistanceRatio = ((MAX_BUBBLE_RADIUS - MIN_BUBBLE_RADIUS) * (1.0f - ratio)) + MIN_BUBBLE_RADIUS;
        }

        if (shouldDisplayAnimationFrame()) {
            mDistanceRatio = mDistanceRatio * ANIMATION_SIZE_FACTOR;
        }
    }

    private void createProgram() {
        ShadersUtils shadersUtils = ShadersUtils.getInstance();
        int vertexShader = shadersUtils.loadShader(R.raw.bubble_vertex_shader, GLES20.GL_VERTEX_SHADER);
        int fragmentShader = shadersUtils.loadShader(R.raw.bubble_fragment_shader, GLES20.GL_FRAGMENT_SHADER);
        int explosionShader = shadersUtils.loadShader(R.raw.explosion_fragment_shader, GLES20.GL_FRAGMENT_SHADER);
        int noBubbleShader = shadersUtils.loadShader(R.raw.no_bubble_fragment_shader, GLES20.GL_FRAGMENT_SHADER);

        mBubbleProgram = BubbleProgramUtils.getBubbleProgram();
        mExplosionProgram = BubbleProgramUtils.getExplosionProgram();
        mNoBubbleProgram = BubbleProgramUtils.getNoBubbleProgram();

        GLES20.glAttachShader(mBubbleProgram, vertexShader);
        GLES20.glAttachShader(mBubbleProgram, fragmentShader);
        GLES20.glLinkProgram(mBubbleProgram);

        GLES20.glAttachShader(mExplosionProgram, vertexShader);
        GLES20.glAttachShader(mExplosionProgram, explosionShader);
        GLES20.glLinkProgram(mExplosionProgram);

        GLES20.glAttachShader(mNoBubbleProgram, vertexShader);
        GLES20.glAttachShader(mNoBubbleProgram, noBubbleShader);
        GLES20.glLinkProgram(mNoBubbleProgram);

        checkShader(vertexShader);
        checkShader(fragmentShader);
        checkShader(explosionShader);
        checkShader(noBubbleShader);
        checkProgram(mBubbleProgram);
        checkProgram(mExplosionProgram);
        checkProgram(mNoBubbleProgram);
    }

    private void handleAttributesFromShaders() {
        mPositionHandler = GLES20.glGetAttribLocation(mBubbleProgram, SHADER_HANDLER_POSITION);
        mTextureCoordinationHandler = GLES20.glGetAttribLocation(mBubbleProgram, SHADER_HANDLER_COORDINATION);
        mTextureLogoHandler = GLES20.glGetUniformLocation(mBubbleProgram, SHADER_HANDLER_LOGO_TEXTURE);
        mStateHandler = GLES20.glGetUniformLocation(mBubbleProgram, SHADER_HANDLER_STATE);
        mAnimationTimeHandler = GLES20.glGetUniformLocation(mBubbleProgram, SHADER_HANDLER_ANIMATION_TIME);
        mTextureCoordinationHandler = GLES20.glGetAttribLocation(mNoBubbleProgram, SHADER_HANDLER_COORDINATION);
        mTextureLogoHandler = GLES20.glGetUniformLocation(mNoBubbleProgram, SHADER_HANDLER_LOGO_TEXTURE);

        GLES20.glEnableVertexAttribArray(mPositionHandler);
        GLES20.glEnableVertexAttribArray(mTextureCoordinationHandler);
        GLES20.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
        GLES20.glEnable(GL10.GL_BLEND);
    }

    private void initBuffers(@NonNull float[] textureVertices) {
        checkNotNull(textureVertices);

        ByteBuffer vertexBuffer = ByteBuffer.allocateDirect(mBubbleCoords.length * FLOAT_SIZE_BYTES);
        vertexBuffer.order(ByteOrder.nativeOrder());
        mVertexBuffer = vertexBuffer.asFloatBuffer();

        ByteBuffer drawListBuffer = ByteBuffer.allocateDirect(DRAW_ORDER.length * FLOAT_SIZE_BYTES / 2);
        drawListBuffer.order(ByteOrder.nativeOrder());
        mDrawListBuffer = drawListBuffer.asShortBuffer();
        mDrawListBuffer.put(DRAW_ORDER);
        mDrawListBuffer.position(DRAW_FIRST_POSITION);

        ByteBuffer textureBuffer = ByteBuffer.allocateDirect(textureVertices.length * FLOAT_SIZE_BYTES);
        textureBuffer.order(ByteOrder.nativeOrder());
        mTextureVerticesBuffer = textureBuffer.asFloatBuffer();
        mTextureVerticesBuffer.put(textureVertices);
        mTextureVerticesBuffer.position(DRAW_FIRST_POSITION);
    }

    private void clearVertexBuffer() {
        mVertexBuffer.clear();
        mVertexBuffer.put(mBubbleCoords);
        mVertexBuffer.position(DRAW_FIRST_POSITION);
    }

    private void drawBitmap() {
        if (mBitmapBuffer != null) {
            GLES20.glGenTextures(DRAW_BITMAP_POSITION, mBitmapTextures, 0);
            GLES20.glActiveTexture(GLES20.GL_TEXTURE1);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mBitmapTextures[DRAW_FIRST_POSITION]);

            if (shouldDisplayAnimationFrame()) {
                GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, mBitmap, 0);
                new Handler(Looper.getMainLooper()).post(() -> loadAnimationFrame(BitmapUtils.getBitmapSize()));
            } else {
                if (mState == BUBBLE_NORMAL) {
                    GLES20.glUniform1f(mStateHandler, STATE_NORMAL);
                } else if (mState == BUBBLE_WITH_GLOW) {
                    GLES20.glUniform1f(mStateHandler, STATE_GLOW);
                    mStartedGlowAnimationTime += ANIMATION_GLOW_INTERVAL;
                    if (mStartedGlowAnimationTime == MAX_ANIMATION_GLOW_TIME) {
                        mStartedGlowAnimationTime = 0.0f;
                    }
                    GLES20.glUniform1f(mAnimationTimeHandler, mStartedGlowAnimationTime);
                }

                GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, mBitmapWidth, mBitmapHeight,
                        0, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, mBitmapBuffer);
            }

            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
            GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
            GLES20.glUniform1i(mTextureLogoHandler, DRAW_BITMAP_POSITION);
        }
    }

    private boolean shouldDisplayAnimationFrame() {
        return mIsAnimating && mBitmap != null;
    }

    private void randFloatingValues() {
        mIfFloatingXToRight = Math.random() > 0.5;
        mIfFloatingYToTop = Math.random() > 0.5;
        mFloatingCurrentX = FLOATING_MIN_X + (float) ((FLOATING_MAX_X - FLOATING_MIN_X) * Math.random());

        if (mCurrentDistance <= METERS_DISTANCE_RANGE_1) {
            mFloatingMinY = FLOATING_START_Y + (6 * FLOATING_Y_AREA_INTERVAL);
            mFloatingMaxY = FLOATING_START_Y + (8 * FLOATING_Y_AREA_INTERVAL);
        } else if (mCurrentDistance <= METERS_DISTANCE_RANGE_2) {
            mFloatingMinY = FLOATING_START_Y + (5 * FLOATING_Y_AREA_INTERVAL);
            mFloatingMaxY = FLOATING_START_Y + (7 * FLOATING_Y_AREA_INTERVAL);
        } else if (mCurrentDistance <= METERS_DISTANCE_RANGE_3) {
            mFloatingMinY = FLOATING_START_Y + (4 * FLOATING_Y_AREA_INTERVAL);
            mFloatingMaxY = FLOATING_START_Y + (6 * FLOATING_Y_AREA_INTERVAL);
        } else if (mCurrentDistance <= METERS_DISTANCE_RANGE_4) {
            mFloatingMinY = FLOATING_START_Y + (3 * FLOATING_Y_AREA_INTERVAL);
            mFloatingMaxY = FLOATING_START_Y + (5 * FLOATING_Y_AREA_INTERVAL);
        } else if (mCurrentDistance <= METERS_DISTANCE_RANGE_5) {
            mFloatingMinY = FLOATING_START_Y + (2 * FLOATING_Y_AREA_INTERVAL);
            mFloatingMaxY = FLOATING_START_Y + (4 * FLOATING_Y_AREA_INTERVAL);
        } else if (mCurrentDistance <= METERS_DISTANCE_RANGE_6) {
            mFloatingMinY = FLOATING_START_Y + FLOATING_Y_AREA_INTERVAL;
            mFloatingMaxY = FLOATING_START_Y + (3 * FLOATING_Y_AREA_INTERVAL);
        } else if (mCurrentDistance > METERS_DISTANCE_RANGE_6) {
            mFloatingMinY = FLOATING_START_Y;
            mFloatingMaxY = FLOATING_START_Y + (2 * FLOATING_Y_AREA_INTERVAL);
        }

        mFloatingCurrentY = mFloatingMinY + (float) ((2*FLOATING_Y_AREA_INTERVAL) * Math.random());
    }

    private void loadAnimationFrame(int size) {
        if (mIsAnimating) {
            if (!mBuffers.isEmpty()) {
                Glide.with(FrienjiApplication.getAppContext())
                        .load(mBuffers.remove(0))
                        .asBitmap()
                        .centerCrop()
                        .into(new SimpleTarget<Bitmap>(size, size) {
                            @Override
                            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                if (mIsAnimating) {
                                    mBitmap = resource;
                                    if (!mWasSizeRefactored) {
                                        mWasSizeRefactored = true;
                                        calculateDistance(getDistance());
                                    }
                                }
                            }
                        });
            } else {
                finishAnimation();
            }
        }
    }

    private void finishAnimation() {
        mBitmap = null;
        mIsAnimating = false;
        setIsHidden(true);

        calculateDistance(getDistance());
    }

    private void calculateFloatingX() {
        if (mIfFloatingXToRight) {
            if (mFloatingCurrentX + FLOATING_MOVE_X > FLOATING_MAX_X) {
                mIfFloatingXToRight = false;
                mFloatingCurrentX -= FLOATING_MOVE_X;
            } else {
                mFloatingCurrentX += FLOATING_MOVE_X;
            }
        } else {
            if (mFloatingCurrentX - FLOATING_MOVE_X < FLOATING_MIN_X) {
                mIfFloatingXToRight = true;
                mFloatingCurrentX += FLOATING_MOVE_X;
            } else {
                mFloatingCurrentX -= FLOATING_MOVE_X;
            }
        }
    }

    private void calculateFloatingY() {
        if (mIfFloatingYToTop) {
            if (mFloatingCurrentY + FLOATING_MOVE_Y > mFloatingMaxY) {
                mIfFloatingYToTop = false;
                mFloatingCurrentY -= FLOATING_MOVE_Y;
            } else {
                mFloatingCurrentY += FLOATING_MOVE_Y;
            }
        } else {
            if (mFloatingCurrentY - FLOATING_MOVE_Y < mFloatingMinY) {
                mIfFloatingYToTop = true;
                mFloatingCurrentY += FLOATING_MOVE_Y;
            } else {
                mFloatingCurrentY -= FLOATING_MOVE_Y;
            }
        }
    }

    private @NonNull int[] convertBitmapToPixels(@NonNull Bitmap bitmap) {
        checkNotNull(bitmap);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        mBitmapWidth = bitmap.getWidth();
        mBitmapHeight = bitmap.getHeight();
        int[] pixels = new int[mBitmapWidth * mBitmapHeight];
        bitmap.getPixels(pixels, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        return pixels;
    }

    private void argbToAbgr(@NonNull int[] pixels) {
        int length = pixels.length;
        for (int i = 0; i < length; i++) {
            int red = (pixels[i] >> 16) & 0xff;
            int green = (pixels[i] >> 8) & 0xff;
            int blue = pixels[i] & 0xff;
            int alpha = pixels[i] & 0xff000000;
            pixels[i] = alpha | (green << 8) | (red) | (blue << 16);
        }
    }

    private float getLowPassFilterYPosition() {
        float position = LOW_PASS_FILTER_VALUE_MAX * mCurrentYPosition + (1f - LOW_PASS_FILTER_VALUE_MAX) * (mNewYPosition + mFloatingCurrentY);
        mCurrentYPosition = position;

        return position;
    }

    private float getLowPassFilterXPosition() {
        float position = LOW_PASS_FILTER_VALUE_MAX * mCurrentXPosition + (1f - LOW_PASS_FILTER_VALUE_MAX) * (mNewXPosition + mFloatingCurrentX);
        mCurrentXPosition = position;

        return position;
    }

    private float getCurrentYOffset() {
        float ratio = mYPosition / MAX_BUBBLE_Y_POSITION;
        return ratio * MAX_BUBBLE_POSITION_Y_OFFSET;
    }

    private float getXOffsetBetweenStartingAzimuth() {
        float halfMaxPosition = MAX_BUBBLE_X_POSITION / 2;
        int newPositionDiff = (int) mXPosition - (int) mStartingAzimuth;
        int newAzimuth = newPositionDiff < 0 ? newPositionDiff + (int) MAX_BUBBLE_X_POSITION : newPositionDiff;
        boolean shouldBeMinus = newAzimuth > (int) (MAX_BUBBLE_X_POSITION / 2);
        float ratio = (halfMaxPosition - Math.abs(halfMaxPosition - Math.abs((newPositionDiff)))) / halfMaxPosition;
        return shouldBeMinus ? ratio * MAX_BUBBLE_POSITION_X_OFFSET : ratio * -MAX_BUBBLE_POSITION_X_OFFSET;
    }

    private @NonNull IntBuffer getBitmapBuffer(@NonNull Bitmap source) {
        checkNotNull(source);

        Matrix matrix = new Matrix();
        matrix.postRotate(mBitmapRotationAngle);

        int[] pixels = convertBitmapToPixels(Bitmap.createBitmap(source, 0, 0,
                source.getWidth(), source.getHeight(), matrix, true));
        argbToAbgr(pixels);

        return IntBuffer.wrap(pixels);
    }

    private void loadUrl(@NonNull String url, int size) {
        Glide.with(FrienjiApplication.getAppContext())
                .load(checkNotNull(url))
                .asBitmap()
                .override(size, size)
                .listener(new RequestListener<String, Bitmap>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<Bitmap> target, boolean isFirstResource) {
                        loadResource(R.drawable.placeholder_square, size);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, String model, Target<Bitmap> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        mBitmapBuffer = getBitmapBuffer(resource);
                        mFlatAvatarImages.put(url, mBitmapBuffer);
                        return false;
                    }
                }).into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
            }
        });
    }

    private void loadResource(@DrawableRes int drawable, int size) {
        Glide.with(FrienjiApplication.getAppContext())
                .load(drawable)
                .asBitmap()
                .centerCrop()
                .into(new SimpleTarget<Bitmap>(size, size) {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        mBitmapBuffer = getBitmapBuffer(resource);
                    }
                });
    }

    private void loadBitmap(@Nullable String url) {
        new Handler(Looper.getMainLooper()).post(() -> {
            int size = BitmapUtils.getBitmapSize();
            if (!Strings.isNullOrEmpty(url)) {
                loadUrl(url, size);
            } else {
                loadResource(R.drawable.placeholder_square, size);
            }
        });
    }

    private void checkProgram(int program) {
        if (BuildConfig.DEBUG) {
            Log.d("GL_TRACES", "Program info:" + program + " " + GLES20.glGetProgramInfoLog(program));
        }
    }

    private void checkShader(int shader) {
        if (BuildConfig.DEBUG) {
            Log.d("GL_TRACES", "Shader info:" + shader + " " + GLES20.glGetShaderInfoLog(shader));
        }
    }
}