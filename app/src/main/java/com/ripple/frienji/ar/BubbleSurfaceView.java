package com.ripple.frienji.ar;

import android.content.Context;
import android.location.Location;
import android.opengl.GLSurfaceView;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.MotionEvent;
import com.google.common.eventbus.EventBus;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.ar.model.Bubble;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.OpenGlModule;
import com.ripple.frienji.model.frienji.Frienji;
import com.ripple.frienji.settings.AppSettings;
import com.ripple.frienji.ui.view.custom.RadarView;
import com.ripple.frienji.ui.view.fragment.ArFragment;
import java.util.List;
import javax.inject.Inject;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Tomasz Trybala
 * @since 2016-07-20
 */
public class BubbleSurfaceView extends GLSurfaceView {
    private static final int OPEN_GL_VERSION = 2;
    private static final int DELAY_DURATION = 400;
    private static final int FRIENJI_CLICK_DELAY_DURATION = 700;

    private BubblesRenderer mRenderer;
    private boolean mStopAddingFrienjies;
    private Bubble mClickedBubble;

    @Inject
    protected EventBus mEventBus;

    @Inject
    protected AvatarReader mAvatarReader;

    @Inject
    protected AppSettings mAppSettings;

    public BubbleSurfaceView(@NonNull Context context) {
        super(checkNotNull(context));
        injectDependecies();
        initRenderer();
    }

    public BubbleSurfaceView(@NonNull Context context, @NonNull AttributeSet attrs) {
        super(checkNotNull(context), checkNotNull(attrs));
        injectDependecies();
        initRenderer();
    }

    private void injectDependecies() {
        FrienjiApplication app = (FrienjiApplication) getContext().getApplicationContext();
        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new OpenGlModule()).inject(this);
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent e) {
        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mClickedBubble = mRenderer.getClickedBubble((int) e.getX(), (int) e.getY());
                if (mClickedBubble != null) {
                    mClickedBubble.animate();
                    new Handler().postDelayed(() -> {
                        if (mClickedBubble != null) {
                            mClickedBubble.setIsHidden(true);
                            mEventBus.post(new
                                    ArFragment.BubbleCatchEvent(mClickedBubble.getFrienji()));
                            mClickedBubble = null;
                        }
                    }, FRIENJI_CLICK_DELAY_DURATION);
                }
        }

        return false;
    }

    public void setBubbleVisible(int bubleId) {
        mRenderer.setBubbleVisible(bubleId);
    }

    @Override
    public void onPause() {
        super.onPause();
        mStopAddingFrienjies = true;
        if (mRenderer != null) {
            mRenderer.onPause();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mStopAddingFrienjies = false;
        if (mRenderer != null) {
            mRenderer.onResume();
        }
    }

    @Override
    protected void onAttachedToWindow() {
        setZOrderOnTop(false);
        super.onAttachedToWindow();
    }

    public void setRadarView(@NonNull RadarView view) {
        checkNotNull(view);
        if (mRenderer != null) {
            mRenderer.setRadarView(view);
        }
    }

    public void setDisplayOrientation(int displayOrientation) {
        mRenderer.setDisplayOrientation(displayOrientation);
    }

    public void updateCoordinates(int azimuth, int pitch) {
        if (mRenderer != null) {
            mRenderer.updateCoordinates(azimuth, pitch);
        }
    }

    public void addFrienjis(@NonNull List<Frienji> frienjis, @NonNull Location location) {
        checkNotNull(frienjis);
        checkNotNull(location);

        new Handler().postDelayed(() -> queueEvent(() -> {
            /* Variable mStopAddingFrienjies is for protection from random opengl error. When
            sleep button on phone is pressed and "ar" activity is not visible any more, this "Handler().postDelayed"
            is executed (with delay) in random. In this situation execution of code (from next steps)
            from Buble class "GLES20.glAttachShader(mProgram, mVertexShader) and GLES20.glAttachShader(mProgram, mFragmentShader)"
            generate opengl error because I think context of opengl is not extis when activity is not visible */
            if (!mStopAddingFrienjies && mRenderer != null) {
                mRenderer.addFrienjis(frienjis, location);
            }
        }), DELAY_DURATION);
    }

    private void initRenderer() {
        mStopAddingFrienjies = false;
        setEGLContextClientVersion(OPEN_GL_VERSION);
        mRenderer = new BubblesRenderer(mAvatarReader, mAppSettings);
        setRenderer(mRenderer);
    }
}