package com.ripple.frienji.ar;

import android.graphics.SurfaceTexture;
import android.location.Location;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.ripple.frienji.ar.model.Bubble;
import com.ripple.frienji.ar.model.CompositeBubbles;
import com.ripple.frienji.ar.model.DirectVideo;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.camera.CameraWrapper;
import com.ripple.frienji.model.frienji.Frienji;
import com.ripple.frienji.model.location.FrienjiLocation;
import com.ripple.frienji.settings.AppSettings;
import com.ripple.frienji.ui.view.custom.RadarView;
import com.ripple.frienji.util.BubbleProgramUtils;
import com.ripple.frienji.util.LocationUtils;
import java.io.IOException;
import java.util.List;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Tomasz Trybala
 * @since 2016-07-21
 */
public class BubblesRenderer implements GLSurfaceView.Renderer {
    private static final float[] PROJECTION_MATRIX = new float[16];
    private static final int TEXTURES_COUNT = 2;
    private static final int NEAR = -1;
    private static final int FAR = 1;
    private static final int INCORRECT_ANGLE = 400;

    private boolean mIsCameraRunning;
    private boolean mIsSurfaceCreated;

    private int mDisplayOrientation;
    private int mWidth;
    private int mHeight;
    private float[] mTextureCoordinates;
    private float mBitmapRotationAngle = INCORRECT_ANGLE;

    private CameraWrapper mCamera;
    private SurfaceTexture mSurfaceTextures;
    private DirectVideo mDirectVideo;
    private CompositeBubbles mCompositeBubbles;
    private RadarView mRadarView;

    private AvatarReader mAvatarReader;
    private AppSettings mAppSettings;

    public BubblesRenderer(@NonNull AvatarReader avatarReader, @NonNull AppSettings appSettings) {
        super();
        mCompositeBubbles = new CompositeBubbles();
        mAvatarReader = checkNotNull(avatarReader);
        mAppSettings = checkNotNull(appSettings);
    }

    public void setDisplayOrientation(int displayOrientation) {
        mDisplayOrientation = displayOrientation;
    }

    public @Nullable Bubble getClickedBubble(int x, int y) {
        return mCompositeBubbles.getClickedBubble(x, y);
    }

    public void onPause() {
        stopCamera();
    }

    public void onResume() {
        prepareCamera();
    }

    public void updateCoordinates(int azimuth, int pitch) {
        mCompositeBubbles.updateCoordinates(azimuth, pitch);
    }

    public void setBubbleVisible(int traceId) {
        Bubble bubble = mCompositeBubbles.getBubble(traceId);
        if (bubble != null) {
            bubble.setIsHidden(false);
        }
    }

    public void setRadarView(@NonNull RadarView view) {
        mRadarView = checkNotNull(view);
    }

    public void addFrienjis(@NonNull List<Frienji> frienjis, @NonNull Location currentLocation) {
        checkNotNull(frienjis);
        checkNotNull(currentLocation);

        long myOwnUserId = mAppSettings.loadUserProfileId().get();

        if (mBitmapRotationAngle == INCORRECT_ANGLE) {
            mBitmapRotationAngle = CameraWrapper.getBitmapRotationAngle();
        }

        if (mTextureCoordinates != null) {
            for (int i = 0; i < frienjis.size(); i++) {
                Frienji frienji = frienjis.get(i);

                //backend return my own frienji, in current version we will not display my own frienji.
                if (frienji.id() == myOwnUserId) {
                    continue;
                }

                float[] coordinates = new float[2];
                coordinates[0] = 0;
                coordinates[1] = 0;

                if (frienji.location() != null) {
                    coordinates = LocationUtils.getDistanceAndBearing(getLocation(currentLocation), frienji.location());
                }

                Bubble bubble = mCompositeBubbles.getBubble(frienji.id());
                if (bubble == null) {
                    mCompositeBubbles.addBubble(new Bubble(mWidth, mHeight, mTextureCoordinates,
                            coordinates[0], coordinates[1], frienji,
                            mBitmapRotationAngle, mAvatarReader));

                } else {
                    bubble.changeCurrentLocation(coordinates[0], coordinates[1]);
                }
            }

            mCompositeBubbles.removeInvisibleBubbles(frienjis);
            if (mRadarView != null) {
                mRadarView.setBubbles(mCompositeBubbles.getList());
                drawRadar();
            }
        }
    }

    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        mIsSurfaceCreated = true;
        prepareCamera();
    }

    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
        GLES20.glViewport(0, 0, width, height);
        mWidth = width;
        mHeight = height;

        prepareCamera();

        int halfWidth = mWidth / 2;
        int halfHeight = mWidth / 2;
        Matrix.orthoM(PROJECTION_MATRIX, 0, -halfWidth, halfWidth, -halfHeight, halfHeight, NEAR, FAR);
    }

    @Override
    public void onDrawFrame(GL10 gl) {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        mSurfaceTextures.updateTexImage();
        mDirectVideo.draw();
        mCompositeBubbles.draw();
        drawRadar();
    }

    /* package */ void startCameraPreview(@NonNull SurfaceTexture holder, int width, int height) {
        stopCameraIfPossible();
        mCamera = new CameraWrapper(checkNotNull(holder), width, height);
        startCameraIfPossible();
    }

    /* package */ void stopCameraIfPossible() {
        if (mCamera != null) {
            mCamera.releaseCamera();
            mCamera = null;
        }
    }

    /* package */ void startCameraIfPossible() {
        if (mCamera != null) {
            try {
                mCamera.startCamera(false, mDisplayOrientation);
            } catch (IOException | RuntimeException e) {
                stopCameraIfPossible();
            }
        }
    }

    /* package */ void prepareCamera() {
        if (isCameraAvailable()) {
            int texture = createTexture();
            mTextureCoordinates = CameraWrapper.getTextureCoordinates(mDisplayOrientation);
            mDirectVideo = new DirectVideo(texture, mTextureCoordinates);
            mSurfaceTextures = new SurfaceTexture(texture);
            startCameraPreview(mSurfaceTextures, mWidth, mHeight);
            mIsCameraRunning = true;
        }
    }

    /* package */ void stopCamera() {
        stopCameraIfPossible();
        mIsSurfaceCreated = false;
        mIsCameraRunning = false;
        BubbleProgramUtils.clearBubbleProgram();
        BubbleProgramUtils.clearExplosionProgram();
        BubbleProgramUtils.clearNoBubbleProgram();
        mCompositeBubbles.destroyList();
    }

    /* package */ boolean isCameraAvailable() {
        return !mIsCameraRunning && mIsSurfaceCreated && mWidth != 0 && mHeight != 0;
    }

    private int createTexture() {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        GLES20.glEnable(GLES20.GL_DITHER);
        GLES20.glEnable(GLES20.GL_BLEND);
        GLES20.glBlendFunc(GLES20.GL_ONE, GLES20.GL_ONE_MINUS_SRC_ALPHA);

        int[] textures = new int[TEXTURES_COUNT];

        GLES20.glGenTextures(TEXTURES_COUNT, textures, 0);
        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, textures[1]);

        GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
        GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);

        return textures[TEXTURES_COUNT - 1];
    }

    private void drawRadar() {
        new Handler(Looper.getMainLooper()).post(() -> mRadarView.invalidate());
    }

    @NonNull
    private FrienjiLocation getLocation(@NonNull Location location) {
        return FrienjiLocation.builder().latitude(location.getLatitude()).longitude(location.getLongitude()).build();
    }
}