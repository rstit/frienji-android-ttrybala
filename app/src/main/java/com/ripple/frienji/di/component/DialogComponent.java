package com.ripple.frienji.di.component;

import com.ripple.frienji.di.module.ApiModule;
import com.ripple.frienji.di.module.AppSettingsModule;
import com.ripple.frienji.di.module.AvatarReaderModule;
import com.ripple.frienji.di.module.DialogModule;
import com.ripple.frienji.di.module.IntentProviderModule;
import com.ripple.frienji.di.module.NavigationHandlerModule;
import com.ripple.frienji.di.module.NetModule;
import com.ripple.frienji.di.module.ValidationModule;
import com.ripple.frienji.di.module.ViewModelModule;
import com.ripple.frienji.di.scope.ActivityScope;
import com.ripple.frienji.ui.view.dialog.ChoosePhotoDialogFragment;
import com.ripple.frienji.ui.view.dialog.DeletePostConfirmDialogFragment;
import com.ripple.frienji.ui.view.dialog.LogOutConfirmDialogFragment;
import dagger.Subcomponent;

/**
 * @author Marcin Przepiórkowski
 * @since 01.06.2016
 */
@ActivityScope
@Subcomponent(modules = {DialogModule.class, ViewModelModule.class, NavigationHandlerModule.class, AppSettingsModule.class, ApiModule.class,
        NetModule.class, ValidationModule.class, IntentProviderModule.class, AvatarReaderModule.class})
public interface DialogComponent {

    void inject(ChoosePhotoDialogFragment dialog);

    void inject(DeletePostConfirmDialogFragment dialog);

    void inject(LogOutConfirmDialogFragment dialog);
}