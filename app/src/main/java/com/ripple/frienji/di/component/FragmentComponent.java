package com.ripple.frienji.di.component;

import com.ripple.frienji.di.module.ApiModule;
import com.ripple.frienji.di.module.AvatarReaderModule;
import com.ripple.frienji.di.module.FragmentModule;
import com.ripple.frienji.di.module.IntentProviderModule;
import com.ripple.frienji.di.module.NetModule;
import com.ripple.frienji.di.module.ViewModelModule;
import com.ripple.frienji.di.scope.ActivityScope;
import com.ripple.frienji.ui.view.fragment.ArFragment;
import dagger.Subcomponent;

/**
 * Created by kamil ratajczak on 19.09.16.
 */
@ActivityScope
@Subcomponent(modules = {FragmentModule.class, ApiModule.class, NetModule.class, ViewModelModule.class, IntentProviderModule.class, AvatarReaderModule.class})
public interface FragmentComponent {

    void inject(ArFragment fragment);
}
