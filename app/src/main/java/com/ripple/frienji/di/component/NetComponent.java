package com.ripple.frienji.di.component;

import com.ripple.frienji.di.module.ApiModule;
import com.ripple.frienji.di.module.AppSettingsModule;
import com.ripple.frienji.di.module.NetModule;
import dagger.Subcomponent;

/**
 * @author Marcin Przepiórkowski
 * @since 08.05.2016
 */
@Subcomponent(modules = {NetModule.class, AppSettingsModule.class})
public interface NetComponent {

    ApiComponent plus(ApiModule apiModule);
}