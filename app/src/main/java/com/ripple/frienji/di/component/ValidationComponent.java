package com.ripple.frienji.di.component;

import com.ripple.frienji.di.module.ValidationModule;
import dagger.Subcomponent;

/**
 * @author Marcin Przepiórkowski
 * @since 14.05.2016
 */
@Subcomponent(modules = ValidationModule.class)
public interface ValidationComponent {
}