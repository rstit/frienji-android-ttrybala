package com.ripple.frienji.di.component;

import android.support.annotation.NonNull;
import com.ripple.frienji.ar.BubbleSurfaceView;
import com.ripple.frienji.di.module.AppModule;
import com.ripple.frienji.di.module.AvatarReaderModule;
import com.ripple.frienji.di.module.OpenGlModule;
import javax.inject.Singleton;
import dagger.Subcomponent;

@Singleton
@Subcomponent(modules = {OpenGlModule.class, AppModule.class, AvatarReaderModule.class})
public interface OpenGlComponent {

    void inject(@NonNull BubbleSurfaceView bubbleSurfaceView);
}