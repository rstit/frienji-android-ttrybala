package com.ripple.frienji.di.component;

import com.ripple.frienji.di.module.ApiModule;
import com.ripple.frienji.di.module.AppSettingsModule;
import com.ripple.frienji.di.module.AvatarReaderModule;
import com.ripple.frienji.di.module.IntentProviderModule;
import com.ripple.frienji.di.module.NavigationHandlerModule;
import com.ripple.frienji.di.module.NetModule;
import com.ripple.frienji.di.module.ValidationModule;
import com.ripple.frienji.di.module.ViewModelModule;
import com.ripple.frienji.di.scope.ActivityScope;
import com.ripple.frienji.ui.view.activity.ArActivity;
import com.ripple.frienji.ui.view.activity.AvatarTypeActivity;
import com.ripple.frienji.ui.view.activity.ChangeFrienjiDataActivity;
import com.ripple.frienji.ui.view.activity.ChooseFrienjiActivity;
import com.ripple.frienji.ui.view.activity.CommentsActivity;
import com.ripple.frienji.ui.view.activity.ConfirmCodeActivity;
import com.ripple.frienji.ui.view.activity.ConversationActivity;
import com.ripple.frienji.ui.view.activity.FirstInfoActivity;
import com.ripple.frienji.ui.view.activity.InBoxActivity;
import com.ripple.frienji.ui.view.activity.InfoActivity;
import com.ripple.frienji.ui.view.activity.LoginActivity;
import com.ripple.frienji.ui.view.activity.MySettingsActivity;
import com.ripple.frienji.ui.view.activity.SecondInfoActivity;
import com.ripple.frienji.ui.view.activity.SelectFrienjiActivity;
import com.ripple.frienji.ui.view.activity.SignInActivity;
import com.ripple.frienji.ui.view.activity.SignUpActivity;
import com.ripple.frienji.ui.view.activity.UserActionsActivity;
import com.ripple.frienji.ui.view.activity.UserProfileActivity;
import com.ripple.frienji.ui.view.activity.WelcomeActivity;
import com.ripple.frienji.ui.view.activity.WhoAreYouActivity;
import com.ripple.frienji.ui.view.activity.ZooActivity;
import com.ripple.frienji.ui.view.fragment.ArFragment;
import dagger.Subcomponent;

/**
 * @author kamil ratajczak
 * @since 27.07.2016
 */
@ActivityScope
@Subcomponent(modules = {ViewModelModule.class, NavigationHandlerModule.class, AppSettingsModule.class, ApiModule.class,
        NetModule.class, ValidationModule.class, IntentProviderModule.class, AvatarReaderModule.class})
public interface ViewModelComponent {

    void inject(WelcomeActivity activity);

    void inject(LoginActivity activity);

    void inject(SignInActivity activity);

    void inject(ChooseFrienjiActivity activity);

    void inject(WhoAreYouActivity activity);

    void inject(ConfirmCodeActivity activity);

    void inject(SignUpActivity activity);

    void inject(ArActivity activity);

    void inject(UserProfileActivity activity);

    void inject(CommentsActivity activity);

    void inject(MySettingsActivity activity);

    void inject(UserActionsActivity activity);

    void inject(ZooActivity activity);

    void inject(InBoxActivity activity);

    void inject(ConversationActivity activity);

    void inject(ArFragment fragment);

    void inject(FirstInfoActivity firstInfoActivity);

    void inject(SecondInfoActivity secondInfoActivity);

    void inject(AvatarTypeActivity avatarTypeActivity);

    void inject(SelectFrienjiActivity selectFrienjiActivity);

    void inject(ChangeFrienjiDataActivity changeFrienjiDataActivity);

    void inject(InfoActivity infoActivity);
}