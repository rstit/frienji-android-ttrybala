package com.ripple.frienji.di.module;

import android.support.annotation.NonNull;
import com.ripple.frienji.net.FrienjiApi;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * @author kamil ratajczak
 * @since 02.08.2016
 */
@Module
public class ApiModule {

    @Provides
    /*package*/ FrienjiApi provideFrienjiApi(@NonNull Retrofit retrofit) {
        return retrofit.create(FrienjiApi.class);
    }
}