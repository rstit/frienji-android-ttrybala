package com.ripple.frienji.di.component;

import com.ripple.frienji.di.module.AppModule;
import com.ripple.frienji.di.module.DialogModule;
import com.ripple.frienji.di.module.FragmentModule;
import com.ripple.frienji.di.module.NetModule;
import com.ripple.frienji.di.module.OpenGlModule;
import com.ripple.frienji.di.module.ServiceModule;
import com.ripple.frienji.di.module.ValidationModule;
import com.ripple.frienji.di.module.ViewModule;
import javax.inject.Singleton;
import dagger.Component;

/**
 * @author Marcin Przepiórkowski
 * @since 08.03.2016
 */
@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {

    ViewComponent plus(ViewModule viewModule);

    NetComponent plus(NetModule netModule);

    ValidationComponent plus(ValidationModule validationModule);

    DialogComponent plus(DialogModule dialogModule);

    FragmentComponent plus(FragmentModule fragmentModule);

    OpenGlComponent plus(OpenGlModule openGlModule);

    ServiceComponent plus(ServiceModule serviceModule);
}