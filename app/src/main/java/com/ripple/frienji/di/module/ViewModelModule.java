package com.ripple.frienji.di.module;

import android.support.annotation.NonNull;
import com.ripple.frienji.di.scope.ActivityScope;
import com.ripple.frienji.util.rx.ObservableTransformers;
import com.ripple.frienji.util.rx.ObservableTransformersImpl;
import dagger.Module;
import dagger.Provides;

/**
 * @author kamil ratajczak
 * @since 27.07.2016
 */
@Module
public class ViewModelModule {

    @Provides
    @ActivityScope
    /*package*/ @NonNull ObservableTransformers provideObservableTransformers() {
        return new ObservableTransformersImpl();
    }
}