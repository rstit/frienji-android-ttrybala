package com.ripple.frienji.di.module;

import android.content.Context;
import android.support.annotation.NonNull;
import com.ripple.frienji.di.scope.ActivityScope;
import com.ripple.frienji.ui.view.activity.BaseActivity;
import javax.inject.Named;
import dagger.Module;
import dagger.Provides;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Marcin Przepiórkowski
 * @since 11.05.2016
 */
@Module
public class ViewModule {
    protected BaseActivity mActivity;

    public ViewModule(@NonNull BaseActivity activity) {
        mActivity = checkNotNull(activity);
    }

    @Provides
    @ActivityScope
    /*package*/ @NonNull BaseActivity provideBaseActivity() {
        return mActivity;
    }

    @Provides
    @Named("activityContext")
    @ActivityScope
    /*package*/ @NonNull Context provideActivityContext() {
        return mActivity;
    }
}