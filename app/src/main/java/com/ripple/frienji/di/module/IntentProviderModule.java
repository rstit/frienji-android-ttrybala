package com.ripple.frienji.di.module;

import android.content.Context;
import android.support.annotation.NonNull;
import com.ripple.frienji.di.scope.ActivityScope;
import com.ripple.frienji.ui.navigation.IntentProvider;
import com.ripple.frienji.ui.navigation.IntentProviderImpl;
import dagger.Module;
import dagger.Provides;

/**
 * @author kamil ratajczak
 * @since 14.08.2016
 */
@Module
public class IntentProviderModule {

    @Provides
    @ActivityScope
    /*package*/ @NonNull IntentProvider provideIntentProvider(@NonNull Context activityContext) {
        return new IntentProviderImpl(activityContext);
    }
}
