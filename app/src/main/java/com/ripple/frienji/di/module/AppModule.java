package com.ripple.frienji.di.module;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import com.google.common.eventbus.EventBus;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Marcin Przepiórkowski
 * @since 08.05.2016
 */
@Module
public class AppModule {

    Application mApplication;

    EventBus mEventBus;

    public AppModule(@NonNull Application application) {
        mApplication = checkNotNull(application);
        mEventBus = new EventBus("frienji event bus");
    }

    @Provides
    @Singleton Application provideApplication() {
        return mApplication;
    }

    @Provides
    @Singleton Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton Resources provideResources() {
        return mApplication.getResources();
    }

    @Provides
    @Singleton EventBus provideEventBus() {
        return mEventBus;
    }
}