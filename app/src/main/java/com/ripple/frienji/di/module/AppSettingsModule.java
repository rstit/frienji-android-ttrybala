package com.ripple.frienji.di.module;

import android.content.Context;
import android.support.annotation.NonNull;
import com.ripple.frienji.settings.AppSettings;
import dagger.Module;
import dagger.Provides;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Marcin Przepiórkowski
 * @since 14.05.2016
 */
@Module
public class AppSettingsModule {

    @Provides
    /*package*/ @NonNull AppSettings provideAppSettings(@NonNull Context context) {
        checkNotNull(context);
        return new AppSettings(context);
    }
}