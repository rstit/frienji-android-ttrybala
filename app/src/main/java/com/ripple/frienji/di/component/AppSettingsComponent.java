package com.ripple.frienji.di.component;

import com.ripple.frienji.di.module.AppSettingsModule;
import dagger.Subcomponent;

/**
 * @author Marcin Przepiórkowski
 * @since 14.05.2016
 */
@Subcomponent(modules = AppSettingsModule.class)
public interface AppSettingsComponent {
}