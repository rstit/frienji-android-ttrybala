package com.ripple.frienji.di.module;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import com.ripple.frienji.di.scope.ActivityScope;
import com.ripple.frienji.ui.view.activity.BaseActivity;
import javax.inject.Named;
import dagger.Module;
import dagger.Provides;

/**
 * @author Marcin Przepiórkowski
 * @since 01.06.2016
 */
@Module
public class DialogModule {
    protected Activity mActivity;

    public DialogModule(@NonNull Activity activity) {
        mActivity = activity;
    }

    @Provides
    @Named("activityContext")
    @ActivityScope
    /*package*/ Context provideActivityContext() {
        return mActivity;
    }

    @Provides
    @ActivityScope
    /*package*/ BaseActivity provideBaseActivity() {
        return (BaseActivity) mActivity;
    }
}