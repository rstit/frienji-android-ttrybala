package com.ripple.frienji.di.component;

import com.ripple.frienji.di.module.ViewModelModule;
import com.ripple.frienji.di.module.ViewModule;
import com.ripple.frienji.di.scope.ActivityScope;
import dagger.Subcomponent;

/**
 * @author Marcin Przepiórkowski
 * @since 08.03.2016
 */

@ActivityScope
@Subcomponent(modules = {ViewModule.class})
public interface ViewComponent {

    ViewModelComponent plus(ViewModelModule viewModelModule);
}
