package com.ripple.frienji.di.module;

import android.support.annotation.NonNull;
import com.ripple.frienji.di.scope.ActivityScope;
import com.ripple.frienji.ui.navigation.NavigationHandler;
import com.ripple.frienji.ui.navigation.NavigationHandlerImpl;
import com.ripple.frienji.ui.view.activity.BaseActivity;
import dagger.Module;
import dagger.Provides;

/**
 * @author Marcin Przepiórkowski
 * @since 012.05.2016
 */
@Module
public class NavigationHandlerModule {
    @Provides
    @ActivityScope
    /*package*/ @NonNull NavigationHandler provideNavigationHandler(@NonNull BaseActivity activity) {
        return new NavigationHandlerImpl(activity);
    }
}