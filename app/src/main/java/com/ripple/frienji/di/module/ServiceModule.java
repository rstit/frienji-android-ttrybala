package com.ripple.frienji.di.module;

import android.content.Context;
import android.support.annotation.NonNull;
import com.ripple.frienji.firebase.FrienjiFirebaseInstanceIdService;
import com.ripple.frienji.firebase.FrienjiFirebaseMessagingService;
import dagger.Module;

/**
 * @author Kamil Ratajczak
 * @since 03.10.16
 */
@Module
public class ServiceModule {
    protected Context mContext;

    public ServiceModule(@NonNull FrienjiFirebaseInstanceIdService frienjiFirebaseInstanceIdService) {
        mContext = frienjiFirebaseInstanceIdService.getApplicationContext();
    }

    public ServiceModule(@NonNull FrienjiFirebaseMessagingService frienjiFirebaseMessagingService) {
        mContext = frienjiFirebaseMessagingService.getApplicationContext();
    }
}