package com.ripple.frienji.di.component;

import com.ripple.frienji.di.module.ApiModule;
import dagger.Subcomponent;

/**
 * @author Marcin Przepiórkowski
 * @since 08.05.2016
 */
@Subcomponent(modules = ApiModule.class)
public interface ApiComponent {
}