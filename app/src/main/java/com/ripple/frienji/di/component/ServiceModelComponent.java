package com.ripple.frienji.di.component;

import android.support.annotation.NonNull;
import com.ripple.frienji.di.module.AppSettingsModule;
import com.ripple.frienji.di.module.FrienjiNotificationBuilderModule;
import com.ripple.frienji.di.module.NetModule;
import com.ripple.frienji.di.module.ServiceModelModule;
import com.ripple.frienji.firebase.FrienjiFirebaseInstanceIdService;
import com.ripple.frienji.firebase.FrienjiFirebaseMessagingService;
import dagger.Subcomponent;

/**
 * @author Kamil Ratajczak
 * @since 03.10.16
 */
@Subcomponent(modules = {ServiceModelModule.class, AppSettingsModule.class, NetModule.class, FrienjiNotificationBuilderModule.class})
public interface ServiceModelComponent {

    void inject(@NonNull FrienjiFirebaseInstanceIdService frienjiFirebaseInstanceIdService);

    void inject(@NonNull FrienjiFirebaseMessagingService frienjiFirebaseMessagingService);
}