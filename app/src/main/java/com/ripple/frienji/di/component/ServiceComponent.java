package com.ripple.frienji.di.component;

import android.support.annotation.NonNull;
import com.ripple.frienji.di.module.ServiceModelModule;
import com.ripple.frienji.di.module.ServiceModule;
import dagger.Subcomponent;

/**
 * @author Kamil Ratajczak
 * @since 03.10.16
 */
@Subcomponent(modules = {ServiceModule.class})
public interface ServiceComponent {

    ServiceModelComponent plus(@NonNull ServiceModelModule derviceModelModule);
}