package com.ripple.frienji.di.module;

import com.ripple.frienji.ui.util.validation.PhoneNumberValidator;
import com.ripple.frienji.ui.util.validation.SecretCodeValidator;
import com.ripple.frienji.ui.util.validation.UserNameValidator;
import dagger.Module;
import dagger.Provides;

/**
 * @author kamil ratajczak
 * @since 02.08.2016
 */
@Module
public class ValidationModule {

    @Provides
    /*package*/ PhoneNumberValidator providePhoneNumberValidator() {
        return new PhoneNumberValidator();
    }

    @Provides
    /*package*/ SecretCodeValidator provideSecretCodeValidator() {
        return new SecretCodeValidator();
    }

    @Provides
    /*package*/ UserNameValidator provideUserNameValidator() {
        return new UserNameValidator();
    }
}