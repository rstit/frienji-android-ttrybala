package com.ripple.frienji.di.module;

import android.support.annotation.NonNull;
import com.google.common.base.Optional;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ripple.frienji.BuildConfig;
import com.ripple.frienji.settings.AppSettings;
import com.ryanharter.auto.value.gson.AutoValueGsonTypeAdapterFactory;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Marcin Przepiórkowski
 * @since 08.05.2016
 */
@Module
public class NetModule {
    public static final String API_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
    public static final String HEADER_TOKEN = "token";
    public static final String HEADER_TOKEN_TYPE = "token-type";
    public static final String HEADER_TOKEN_TYPE_VALUE = "Bearer";
    public static final String HEADER_UID = "uid";
    public static final String HEADER_CLIENT = "client";

    @Provides
    /*package*/ Gson provideGson() {
        return new GsonBuilder()
                .setDateFormat(API_DATE_FORMAT)
                .registerTypeAdapterFactory(new AutoValueGsonTypeAdapterFactory())
                .create();
    }

    @Provides
    /*package*/ Retrofit provideRetrofit(@NonNull Gson gson, @NonNull OkHttpClient client) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(checkNotNull(gson)))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(BuildConfig.API_URL)
                .client(client)
                .build();
    }

    @Provides
    /*package*/ OkHttpClient provideOkHttpClient(@NonNull AppSettings appSettings) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request original = chain.request();
                    Optional<String> token = appSettings.loadUserToken();
                    Optional<String> uid = appSettings.loadUId();
                    Optional<String> client = appSettings.loadClientId();

                    if (shouldAppendAuthToken(appSettings)) {
                        Request request = original.newBuilder()
                                .header(HEADER_TOKEN, token.get())
                                .header(HEADER_TOKEN_TYPE, HEADER_TOKEN_TYPE_VALUE)
                                .header(HEADER_UID, uid.get())
                                .header(HEADER_CLIENT, client.get())
                                .method(original.method(), original.body())
                                .build();

                        return chain.proceed(request);
                    } else {
                        return chain.proceed(original);
                    }
                })
                .addInterceptor(logging)
                .build();
    }

    protected boolean shouldAppendAuthToken(@NonNull AppSettings appSettings) {
        Optional<String> token = appSettings.loadUserToken();
        Optional<String> uid = appSettings.loadUId();
        Optional<String> client = appSettings.loadClientId();

        return token.isPresent() && uid.isPresent() && client.isPresent();
    }
}