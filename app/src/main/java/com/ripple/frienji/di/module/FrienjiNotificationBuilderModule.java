package com.ripple.frienji.di.module;

import android.content.Context;
import android.support.annotation.NonNull;
import com.ripple.frienji.notification.FrienjiNotificationBuilder;
import dagger.Module;
import dagger.Provides;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Kamil Ratajczak
 * @since 07.10.16
 */
@Module
public class FrienjiNotificationBuilderModule {

    @Provides
    /*package*/ FrienjiNotificationBuilder provideFrienjiNotificationBuilder(@NonNull Context context) {
        return new FrienjiNotificationBuilder(checkNotNull(context));
    }
}
