package com.ripple.frienji.di.module;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import com.ripple.frienji.di.scope.ActivityScope;
import javax.inject.Named;
import dagger.Module;
import dagger.Provides;
import static com.google.common.base.Preconditions.checkNotNull;

@Module
public class FragmentModule {
    protected Activity mActivity;

    public FragmentModule(@NonNull Activity activity) {
        mActivity = checkNotNull(activity);
    }

    @Provides
    @Named("activityContext")
    @ActivityScope
    /*package*/ Context provideActivityContext() {
        return mActivity;
    }
}