package com.ripple.frienji.di.module;

import android.content.Context;
import android.support.annotation.NonNull;
import com.ripple.frienji.avatar.AvatarReader;
import com.ripple.frienji.avatar.AvatarReaderImpl;
import dagger.Module;
import dagger.Provides;

/**
 * @author kamil ratajczak
 * @since 07.09.2016
 */
@Module
public class AvatarReaderModule {

    @Provides
    /*package*/ @NonNull AvatarReader provideAvatarReader(@NonNull Context activityContext) {
        return new AvatarReaderImpl(activityContext);
    }
}