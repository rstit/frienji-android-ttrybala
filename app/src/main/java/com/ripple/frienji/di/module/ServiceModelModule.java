package com.ripple.frienji.di.module;

import android.support.annotation.NonNull;
import com.ripple.frienji.util.rx.ObservableTransformers;
import com.ripple.frienji.util.rx.ObservableTransformersImpl;
import dagger.Module;
import dagger.Provides;

/**
 * @author Kamil Ratajczak
 * @since 03.10.16
 */
@Module
public class ServiceModelModule {

    @Provides
    /*package*/ @NonNull ObservableTransformers provideObservableTransformers() {
        return new ObservableTransformersImpl();
    }
}