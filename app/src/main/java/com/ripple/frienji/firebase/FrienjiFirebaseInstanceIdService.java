package com.ripple.frienji.firebase;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.ServiceModelModule;
import com.ripple.frienji.di.module.ServiceModule;
import com.ripple.frienji.settings.AppSettings;
import javax.inject.Inject;

/**
 * @author Kamil Ratajczak
 * @since 03.10.16
 */
public class FrienjiFirebaseInstanceIdService extends FirebaseInstanceIdService {
    private static final String TAG = "FFirebaseInstanceId";

    @Inject
    public AppSettings mAppSettings;

    @Override
    public void onCreate() {
        super.onCreate();

        FrienjiApplication app = (FrienjiApplication) getApplication();

        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new ServiceModule(this)).plus(new ServiceModelModule()).inject(this);
    }

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        mAppSettings.setFcmToken(refreshedToken);
    }
}