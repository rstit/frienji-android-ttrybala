package com.ripple.frienji.firebase;

import android.support.annotation.NonNull;
import android.util.Log;
import com.crashlytics.android.Crashlytics;
import com.google.common.eventbus.EventBus;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.ripple.frienji.FrienjiApplication;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.module.ServiceModelModule;
import com.ripple.frienji.di.module.ServiceModule;
import com.ripple.frienji.model.action.ActivityEntity;
import com.ripple.frienji.model.message.ChatMessage;
import com.ripple.frienji.model.user.UserProfile;
import com.ripple.frienji.notification.FrienjiNotificationBuilder;
import com.ripple.frienji.settings.AppSettings;
import java.io.IOException;
import java.util.Map;
import javax.inject.Inject;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Kamil Ratajczak
 * @since 03.10.16
 */
public class FrienjiFirebaseMessagingService extends FirebaseMessagingService {
    private static final String LOG_TAG = "FrienjiFirebaseMessagingService";
    private static final String ERROR_MESSAGE = "Cannot map json notificationData to Activity";
    private static final String CHAT_ERROR_MESSAGE = "Cannot map json notificationData to Chat";
    private static final String KEY_NAME = "key";
    private static final String KEY_MESSAGE_DATA = "data";
    private static final String KEY_MESSAGE_VALUE = "receive_message";
    private static final String KEY_MESSAGE = "entity";
    private static final String KEY_OWNER = "owner";

    private static final String TAG = "FrienjiFCMService";

    @Inject
    public Gson mGson;

    @Inject
    public FrienjiNotificationBuilder mFrienjiNotificationBuilder;

    @Inject
    protected EventBus mEventBus;

    @Inject
    protected AppSettings mAppSettings;

    @Override
    public void onCreate() {
        super.onCreate();

        FrienjiApplication app = (FrienjiApplication) getApplication();

        AppComponent appComponent = app.getAppComponent();
        appComponent.plus(new ServiceModule(this)).plus(new ServiceModelModule()).inject(this);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "Notification received");

        Map<String, String> messageData = remoteMessage.getData();

        if (mAppSettings.loadUserToken().isPresent()) { //check user is signed in
            if (messageData.containsKey(KEY_NAME) && messageData.get(KEY_NAME).equals(KEY_MESSAGE_VALUE)
                    && messageData.containsKey(KEY_MESSAGE_DATA)) {
                notifyAboutNewChatMessage(messageData.get(KEY_MESSAGE_DATA));
            } else if (messageData.containsKey(KEY_MESSAGE)) {
                String notificationData = messageData.get(KEY_MESSAGE);
                String notificationType = messageData.get(KEY_NAME);
                String ownerJson = messageData.get(KEY_OWNER);

                try {
                    ActivityEntity activityEntity = ActivityEntity.typeAdapter(mGson).fromJson(notificationData);
                    UserProfile ownerUserProfile = UserProfile.typeAdapter(mGson).fromJson(ownerJson);
                    mFrienjiNotificationBuilder.showActivityNotyfication(activityEntity, notificationType, ownerUserProfile.userName());
                } catch (IOException e) {
                    Crashlytics.log(Log.ERROR, LOG_TAG, ERROR_MESSAGE + ": notificationData: "
                            + notificationData + ", ownerUserProfile: " + ownerJson);
                }
            }
        }
    }

    private void notifyAboutNewChatMessage(@NonNull String messageData) {
        try {
            ChatMessage chatMessage = ChatMessage.typeAdapter(mGson).fromJson(checkNotNull(messageData));

            if (mAppSettings.loadConversationUserProfileId().isPresent() &&
                    mAppSettings.loadConversationUserProfileId().get() == chatMessage.author().id()) {
                mEventBus.post(new AddChatMessageEvent(chatMessage));
            } else {
                mFrienjiNotificationBuilder.showNewChatMessageNotyfication(chatMessage);
            }
        } catch (Exception e) {
            Crashlytics.log(Log.ERROR, LOG_TAG, CHAT_ERROR_MESSAGE + ": " + messageData);
        }
    }

    public class AddChatMessageEvent {
        private ChatMessage mChatMessage;

        public AddChatMessageEvent(@NonNull ChatMessage chatMessage) {
            this.mChatMessage = checkNotNull(chatMessage);
        }

        public @NonNull ChatMessage getChatMessage() {
            return mChatMessage;
        }
    }
}