package com.ripple.frienji.camera;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.io.IOException;
import java.util.List;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Tomasz Trybala
 * @since 2016-07-20
 */
@SuppressWarnings("deprecation")
public class CameraWrapper {
    private static final double ASPECT_TOLERANCE = 0.1;
    private static final int FULL_ROTATION = 360;
    private static final int INVERSE_ROTATION = 270;
    private static final float DEFAULT_BITMAP_ROTATION = -90f;

    private static final float mTextureVertices[] = {
            0.0f, 1.0f,
            1.0f, 1.0f,
            1.0f, 0.0f,
            0.0f, 0.0f,
    };

    private static final float mReverseTextureVertices[] = {
            1.0f, 0.0f,
            0.0f, 0.0f,
            0.0f, 1.0f,
            1.0f, 1.0f,
    };

    private final SurfaceTexture mSurfaceHolder;

    private final int mSurfaceWidth;
    private final int mSurfaceHeight;

    private Camera mCamera;

    private int mBackCameraId = -1;
    private int mBackCameraRotation;
    private int mFrontCameraId = -1;
    private int mFrontCameraRotation;
    private int mDeviceRotation;

    private boolean mBackCameraEnabled;
    private boolean mFrontCameraEnabled;

    public CameraWrapper(@NonNull SurfaceTexture holder, int width, int height) {
        mSurfaceHolder = checkNotNull(holder);
        mSurfaceWidth = width;
        mSurfaceHeight = height;

        checkBackAndFrontCameras();
    }

    private void checkBackAndFrontCameras() {
        for (int i = 0; i < Camera.getNumberOfCameras(); ++i) {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            Camera.getCameraInfo(i, cameraInfo);

            if (!mBackCameraEnabled && cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                mBackCameraRotation = cameraInfo.orientation;
                mBackCameraEnabled = true;
                mBackCameraId = i;
            } else if (!mFrontCameraEnabled && cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
                mFrontCameraRotation = cameraInfo.orientation;
                mFrontCameraEnabled = true;
                mFrontCameraId = i;
            }

            if (mFrontCameraEnabled && mBackCameraEnabled) {
                break;
            }
        }
    }

    public int getDisplayRotation(boolean frontCamera) {
        int rotation;
        if (frontCamera) {
            rotation = (FULL_ROTATION - (mFrontCameraRotation + mDeviceRotation)) % FULL_ROTATION;
        } else {
            rotation = (mBackCameraRotation - mDeviceRotation) % (FULL_ROTATION);
        }
        return Math.abs(rotation);
    }

    public int getPreviewRotation(boolean frontCamera) {
        return frontCamera ? (mFrontCameraRotation + mDeviceRotation) % FULL_ROTATION :
                Math.abs(mBackCameraRotation - mDeviceRotation) % (FULL_ROTATION);
    }

    public static @NonNull float[] getTextureCoordinates(int deviceOrientation) {
        float[] result = mTextureVertices;
        for (int i = 0; i < Camera.getNumberOfCameras(); ++i) {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            Camera.getCameraInfo(i, cameraInfo);

            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                int orientation = cameraInfo.orientation;
                if ((Math.abs(orientation - deviceOrientation) % (FULL_ROTATION)) == INVERSE_ROTATION) {
                    return mReverseTextureVertices;
                }
            }
        }

        return result;
    }

    public static float getBitmapRotationAngle() {
        for (int i = 0; i < Camera.getNumberOfCameras(); ++i) {
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            Camera.getCameraInfo(i, cameraInfo);

            if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                return -cameraInfo.orientation;
            }
        }

        return DEFAULT_BITMAP_ROTATION;
    }

    public void startCamera(boolean frontCamera, int degrees) throws IOException {
        mDeviceRotation = degrees;
        releaseCamera();

        openCamera(frontCamera);

        Camera.Size optimalPreviewSize = getOptimalPreviewSize(mSurfaceHeight, mSurfaceWidth);
        if (optimalPreviewSize != null) {
            setupCamera(getDisplayRotation(frontCamera));
            setupCameraParameters(optimalPreviewSize, getPreviewRotation(frontCamera));
            setupCamcoderProfile(optimalPreviewSize);
        }

        startPreview();
    }

    public void releaseCamera() {
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    private void openCamera(boolean frontCamera) throws RuntimeException {
        mCamera = Camera.open(frontCamera ? mFrontCameraId : mBackCameraId);
    }

    private @Nullable Camera.Size getOptimalPreviewSize(int surfaceWidth, int surfaceHeight) {
        List<Camera.Size> supportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();

        double surfaceRatio = (double) surfaceWidth / surfaceHeight;

        Camera.Size optimalSize = null;

        double minDiff = Double.MAX_VALUE;

        for (Camera.Size previewSize : supportedPreviewSizes) {
            double previewRatio = (double) previewSize.width / previewSize.height;

            if (Math.abs(previewRatio - surfaceRatio) > ASPECT_TOLERANCE) {
                continue;
            }

            if (Math.abs(previewSize.height - surfaceHeight) < minDiff) {
                optimalSize = previewSize;
                minDiff = Math.abs(previewSize.height - surfaceHeight);
            }
        }

        if (optimalSize == null) {
            minDiff = Double.MAX_VALUE;

            for (Camera.Size previewSize : supportedPreviewSizes) {
                if (Math.abs(previewSize.height - surfaceHeight) < minDiff) {
                    optimalSize = previewSize;
                    minDiff = Math.abs(previewSize.height - surfaceHeight);
                }
            }
        }

        return optimalSize;
    }

    private void setupCamera(int rotation) {
        mCamera.setDisplayOrientation(rotation);
    }

    private void setupCameraParameters(@NonNull Camera.Size optimalPreviewSize, int rotation) {
        Camera.Parameters parameters = mCamera.getParameters();

        parameters.setPreviewSize(optimalPreviewSize.width, optimalPreviewSize.height);
        parameters.setRotation(rotation);
        parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        mCamera.setParameters(parameters);
    }

    private void setupCamcoderProfile(@NonNull Camera.Size optimalPreviewSize) {
        CamcorderProfile profile = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
        profile.videoFrameWidth = optimalPreviewSize.width;
        profile.videoFrameHeight = optimalPreviewSize.height;
    }

    private void startPreview() throws IOException {
        mCamera.setPreviewTexture(mSurfaceHolder);
        mCamera.startPreview();
    }
}