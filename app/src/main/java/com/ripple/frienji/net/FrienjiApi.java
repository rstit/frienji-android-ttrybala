package com.ripple.frienji.net;

import android.support.annotation.NonNull;
import com.ripple.frienji.model.auth.RequestConfirmCodeBody;
import com.ripple.frienji.model.auth.RequestConfirmCodeResponse;
import com.ripple.frienji.model.auth.SignInBody;
import com.ripple.frienji.model.auth.SignInResponse;
import com.ripple.frienji.model.auth.SignUpBody;
import com.ripple.frienji.model.auth.SignUpResponse;
import com.ripple.frienji.model.conversation.Conversation;
import com.ripple.frienji.model.frienji.Frienji;
import com.ripple.frienji.model.message.ChatMessage;
import com.ripple.frienji.model.message.FrienjiPost;
import com.ripple.frienji.model.request.CreateConversationRequest;
import com.ripple.frienji.model.request.RejectFrienjiesRequest;
import com.ripple.frienji.model.response.CheckUserNameResponse;
import com.ripple.frienji.model.response.ConversationMessagesResponse;
import com.ripple.frienji.model.response.FriejiActiviesResponse;
import com.ripple.frienji.model.response.FrienjiConversationsResponse;
import com.ripple.frienji.model.response.FrienjiPostCommentsResponse;
import com.ripple.frienji.model.response.FrienjiPostsResponse;
import com.ripple.frienji.model.response.FrienjiZooResponse;
import com.ripple.frienji.model.response.StatusResponse;
import com.ripple.frienji.model.user.UserProfile;
import java.util.List;
import okhttp3.MultipartBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * @author kamil ratajczak
 * @since 02.08.2016
 */
public interface FrienjiApi {

    @POST("frienjis/send_login_code")
    @NonNull Observable<RequestConfirmCodeResponse> getConfirmCode(@Body @NonNull RequestConfirmCodeBody body);

    @POST("frienjis/sign_in")
    @NonNull Observable<SignInResponse> signIn(@Body @NonNull SignInBody body);

    @POST("frienjis")
    @NonNull Observable<SignUpResponse> signUp(@Body @NonNull SignUpBody body);

    @GET("frienjis")
    @NonNull Observable<List<Frienji>> getAroundFrienjis();

    @GET("frienjis/")
    @NonNull Observable<List<Frienji>> getAroundFrienjis(@Query("distance") long distanceInMeters);

    @GET("frienjis/{frienji_id}/posts")
    @NonNull Observable<FrienjiPostsResponse> loadUserPosts(@Path("frienji_id") long frienjiId);

    @GET("frienjis/{frienji_id}/posts")
    @NonNull Observable<FrienjiPostsResponse> loadUserPosts(@Path("frienji_id") long frienjiId, @Query("page") int page);

    @DELETE("posts/{id}")
    @NonNull Observable<StatusResponse> deletePost(@Path("id") long postId);

    @Multipart
    @POST("frienjis/{frienji_id}/posts")
    @NonNull Observable<FrienjiPost> sendUserPost(@Path("frienji_id") long frienjiId, @NonNull @Part List<MultipartBody.Part> fields);

    @POST("posts/{id}/likes")
    @NonNull Observable<StatusResponse> likeUnlikePost(@Path("id") long postId);

    @GET("frienjis/{frienji_id}/posts/{id}")
    @NonNull Observable<FrienjiPostCommentsResponse> loadPostComments(@Path("frienji_id") long frienjiId, @Path("id") long postId);

    @GET("frienjis/{frienji_id}/posts/{id}")
    @NonNull Observable<FrienjiPostCommentsResponse> loadPostComments(@Path("frienji_id") long frienjiId, @Path("id") long postId, @Query("page") int page);

    @Multipart
    @POST("frienjis/{frienji_id}/posts/{id}/comments")
    @NonNull Observable<FrienjiPost> sendUserComment(@Path("frienji_id") long frienjiId, @Path("id") long postId, @NonNull @Part List<MultipartBody.Part> fields);

    @GET("profile")
    @NonNull Observable<UserProfile> loadUserProfile();

    @GET("profile/frienjis")
    @NonNull Observable<FrienjiZooResponse> loadUserZoo();

    @GET("profile/frienjis")
    @NonNull Observable<FrienjiZooResponse> loadUserZoo(@Query("page") int page);

    @POST("conversations")
    @NonNull Observable<Conversation> createConversation(@Body @NonNull CreateConversationRequest createConversationRequest);

    @GET("conversations")
    @NonNull Observable<FrienjiConversationsResponse> loadUserInBox();

    @GET("conversations")
    @NonNull Observable<FrienjiConversationsResponse> loadUserInBox(@Query("page") int page);

    @GET("conversations/{id}")
    @NonNull Observable<ConversationMessagesResponse> loadConversationMessages(@Path("id") long conversationId);

    @GET("conversations/{id}")
    @NonNull Observable<ConversationMessagesResponse> loadConversationMessages(@Path("id") long conversationId, @Query("page") int page);

    @Multipart
    @POST("conversations/{id}/messages")
    @NonNull Observable<ChatMessage> sendConversationMessage(@Path("id") long conversationId, @NonNull @Part List<MultipartBody.Part> fields);

    @GET("profile/notifications")
    @NonNull Observable<FriejiActiviesResponse> loadUserActions();

    @GET("profile/notifications")
    @NonNull Observable<FriejiActiviesResponse> loadUserActions(@Query("page") int page);

    @POST("frienjis/reject_bulks")
    @NonNull Observable<StatusResponse> rejectFrienjiBulks(@Body @NonNull RejectFrienjiesRequest rejectFrienjiesRequest);

    @Multipart
    @PATCH("profile")
    @NonNull Observable<UserProfile> updateUserProfile(@NonNull @Part List<MultipartBody.Part> fields);

    @POST("frienjis/{id}/saves")
    @NonNull Observable<StatusResponse> catchFrienji(@Path("id") int frienjiId);

    @POST("frienjis/{id}/rejects")
    @NonNull Observable<StatusResponse> rejectFrienji(@Path("id") int frienjiId);

    @GET("frienjis/check_username_availability")
    @NonNull Observable<CheckUserNameResponse> checkUserName(@Query("username") @NonNull String userName);
}