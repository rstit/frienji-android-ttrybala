package com.ripple.frienji;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDexApplication;
import com.crashlytics.android.Crashlytics;
import com.ripple.frienji.di.component.AppComponent;
import com.ripple.frienji.di.component.DaggerAppComponent;
import com.ripple.frienji.di.module.AppModule;
import com.ripple.frienji.di.module.NetModule;
import com.ripple.frienji.di.module.ValidationModule;
import com.squareup.leakcanary.LeakCanary;
import io.fabric.sdk.android.Fabric;

/**
 * @author kamil ratajczak
 * @since 27.07.2016
 */
public class FrienjiApplication extends MultiDexApplication {
    private static Context sContext;

    private AppComponent mAppComponent;

    public static @NonNull Context getAppContext() {
        return sContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        if (LeakCanary.isInAnalyzerProcess(this)) {
            return;
        }
        LeakCanary.install(this);

        sContext = getApplicationContext();

        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this)).build();
        mAppComponent.plus(new NetModule());
        mAppComponent.plus(new ValidationModule());
    }

    public @NonNull AppComponent getAppComponent() {
        return mAppComponent;
    }
}