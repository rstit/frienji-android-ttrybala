<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools">

    <data>
        <import type="android.view.View"/>
        <import type="com.ripple.frienji.model.avatar.Avatar"/>

        <variable
            name="model"
            type="com.ripple.frienji.ui.model.row.MessageRowViewModel"/>
    </data>

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:fitsSystemWindows="true"
        android:orientation="horizontal"
        android:padding="@dimen/padding_default">

        <TextView
            style="@style/EmojiTextViewSmall"
            android:layout_width="@dimen/profile_pic_medium_size"
            android:layout_height="@dimen/profile_pic_medium_size"
            android:background="@drawable/emoji_background"
            android:gravity="center"
            android:padding="@dimen/padding_default"
            app:visibleIf="@{model.mAvatarType == Avatar.EMOTICON_AVATAR}"
            android:onClick="@{() -> model.showUser()}"
            app:avatarName="@{model.mAvatarName}"
            app:avatarReader="@{model.mAvatarReader}"
            app:avatarType="@{model.mAvatarType}"
            tools:text=":)"/>

        <ImageView
            style="@style/EmojiTextViewSmall"
            android:layout_width="@dimen/profile_pic_medium_size"
            android:layout_height="@dimen/profile_pic_medium_size"
            android:contentDescription="@string/avatar_image_description"
            android:background="@drawable/emoji_background"
            android:padding="@dimen/padding_default"
            android:scaleType="centerCrop"
            app:visibleIf="@{model.mAvatarType == Avatar.IMAGE_AVATAR || model.mAvatarType == Avatar.GIF_AVATAR}"
            android:onClick="@{() -> model.showUser()}"
            app:avatarName="@{model.mAvatarName}"
            app:avatarReader="@{model.mAvatarReader}"
            app:avatarType="@{model.mAvatarType}"/>

        <LinearLayout
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginEnd="@dimen/padding_default"
            android:layout_marginLeft="@dimen/padding_default"
            android:layout_marginRight="@dimen/padding_default"
            android:layout_marginStart="@dimen/padding_default"
            android:layout_weight="1"
            android:orientation="vertical">

            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center_vertical"
                android:orientation="horizontal">

                <TextView
                    style="@style/TextAppearance.AppCompat.Caption"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_weight="1"
                    android:ellipsize="end"
                    android:gravity="start"
                    android:text="@{model.mUserName}"
                    android:textColor="@android:color/white"
                    android:textStyle="bold"
                    tools:text="\@username"/>

                <TextView
                    style="@style/TextAppearance.AppCompat.Caption"
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:layout_marginEnd="@dimen/margin_default"
                    android:layout_marginRight="@dimen/margin_default"
                    android:gravity="center"
                    android:text="@{model.mCreationTime}"
                    android:textColor="@android:color/white"
                    tools:text="2 mins ago"/>

                <ImageView
                    android:id="@+id/imgDelete"
                    style="@style/EmojiTextViewSmall"
                    android:layout_width="@dimen/delete_row_cross_size"
                    android:layout_height="@dimen/delete_row_cross_size"
                    android:background="?attr/selectableItemBackground"
                    android:contentDescription="@string/small_cross_delete_image_description"
                    app:visibleIf="@{model.isMyOwnMessage()}"
                    android:src="@drawable/ic_cross_small"/>
            </LinearLayout>

            <TextView
                style="@style/TextAppearance.AppCompat.Caption"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="@dimen/margin_default"
                android:gravity="start"
                android:text="@{model.mMessage}"
                android:textColor="@color/primary_text"
                tools:text="Pay day! I am so ready to go shopping with my friends! Pay day! I am so ready to go shopping with my friends! Pay day! I am so ready to go shopping with my friends!"/>

            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="@dimen/message_height"
                android:orientation="horizontal"
                app:visibleIf="@{model.mImageName != null || model.mLatLongPosition != null}">

                <ImageView
                    android:layout_width="0dp"
                    android:layout_height="@dimen/message_image_height"
                    android:layout_margin="@dimen/margin_small"
                    android:layout_weight="1"
                    android:background="?attr/selectableItemBackground"
                    android:contentDescription="@string/user_profile_activity_message_image_desc"
                    android:onClick="@{() -> model.createImage()}"
                    android:scaleType="centerCrop"
                    app:visibleIf="@{model.mImageName != null}"
                    app:imageUrl="@{model.mImageName}"/>

                <FrameLayout
                    android:layout_width="0dp"
                    android:layout_height="@dimen/message_image_height"
                    android:layout_margin="@dimen/margin_small"
                    android:layout_weight="1"
                    app:visibleIf="@{model.mLatLongPosition != null}"
                    app:mapLocation="@{model.mLatLongPosition}"/>
            </LinearLayout>

            <LinearLayout
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_gravity="start"
                android:layout_marginTop="@dimen/margin_default"
                app:visibleIf="@{model.mShowLikeAndComment}">

                <ImageView
                    style="@style/SelectableImageView"
                    android:layout_width="@dimen/comment_button_size"
                    android:layout_height="@dimen/comment_button_size"
                    android:background="?attr/selectableItemBackground"
                    android:contentDescription="@string/user_profile_activity_like_image_desc"
                    android:gravity="center"
                    android:onClick="@{() -> model.likeMessage()}"
                    android:src="@{model.mHasLike ? @drawable/ic_pink_heart : @drawable/ic_broken_heart}"/>

                <TextView
                    android:layout_width="wrap_content"
                    android:layout_height="wrap_content"
                    android:gravity="center"
                    android:padding="@dimen/padding_default"
                    android:text="@{model.mNumberOfLikes}"
                    android:textColor="@android:color/black"
                    tools:text="1"/>

                <ImageView
                    style="@style/SelectableImageView"
                    android:layout_width="@dimen/comment_button_size"
                    android:layout_height="@dimen/comment_button_size"
                    android:layout_marginLeft="@dimen/margin_default"
                    android:layout_marginStart="@dimen/margin_default"
                    android:background="?attr/selectableItemBackground"
                    android:contentDescription="@string/user_profile_activity_comment_image_desc"
                    android:gravity="center"
                    android:onClick="@{() -> model.showComments()}"
                    android:src="@drawable/ic_speech_balloon"/>

                <TextView
                    android:layout_width="wrap_content"
                    android:layout_height="match_parent"
                    android:background="?attr/selectableItemBackground"
                    android:gravity="center"
                    android:padding="@dimen/padding_default"
                    android:text="@{model.mNumberOfComments}"
                    android:textColor="@android:color/black"
                    tools:text="1"/>
            </LinearLayout>
        </LinearLayout>
    </LinearLayout>
</layout>