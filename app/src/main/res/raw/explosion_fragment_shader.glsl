#extension GL_OES_EGL_image_external : require
precision mediump float;

uniform sampler2D LogoTexture;
varying vec2 CameraTextureCoord;

void drawExplosion();

void main() {
    drawExplosion();
}

void drawExplosion() {
   gl_FragColor = texture2D(LogoTexture, CameraTextureCoord.xy);
}