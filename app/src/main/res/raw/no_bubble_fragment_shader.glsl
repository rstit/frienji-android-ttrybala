// Author Tomasz Trybala
// 02.11.2016
#extension GL_OES_EGL_image_external : require
precision mediump float;

uniform samplerExternalOES Texture;
uniform sampler2D LogoTexture;
varying vec2 CameraTextureCoord;
uniform float state;
uniform float holdAnimationTime;

#define OPACITY 0.7
#define LOGO_RADIUS 0.35
#define CENTER vec2(0.5, 0.5)
#define REFRACTIVE_INDEX 0.71
#define AMBIENT_COLOUR vec3(0.5, 0.5, 0.5)
#define SPECULAR_COLOUR vec3(0.5, 0.5, 0.5)
#define SPECULAR_INTENSITY 6.0
#define DELTA 0.00001

void drawBitmapWithOutBubble();

void main() {
    drawBitmapWithOutBubble();
}

void drawBitmapWithOutBubble() {
    // check for presence in circle
    highp float distanceFromCenter = distance(CENTER, CameraTextureCoord.xy);
    highp float checkForPresenceWithinCircle = 1.0 - smoothstep(LOGO_RADIUS - DELTA, LOGO_RADIUS + DELTA, distanceFromCenter);

    // helper values
    highp float distanceFromCenterNormalized = distanceFromCenter / LOGO_RADIUS;
    highp float checkForPresenceWithinLogo = 1.0 - smoothstep(LOGO_RADIUS - DELTA, LOGO_RADIUS + DELTA, distanceFromCenter);
    highp vec2 coordinatesMinusCenter = CameraTextureCoord.xy - CENTER;

    // glass without aberation
    highp float normalizedDepth = LOGO_RADIUS * sqrt(1.0 - distanceFromCenterNormalized * distanceFromCenterNormalized);
    highp vec3 sphereNormal = normalize(vec3(coordinatesMinusCenter, normalizedDepth));
    highp vec3 refractedVector = refract(vec3(0.0, 0.0, -1.0), sphereNormal, REFRACTIVE_INDEX);

    highp vec2 finalSphereCoordinate = (refractedVector.xy + 1.0) * 0.5;
    highp vec3 finalSphereColor = texture2D(Texture, finalSphereCoordinate).rgb;

    // logo
    highp float imageDistanceFromCenter = distanceFromCenter / LOGO_RADIUS;
    highp float imageNormalizedDepth = LOGO_RADIUS * sqrt(1.0 - imageDistanceFromCenter * imageDistanceFromCenter);
    highp vec3 imageNormal = normalize(vec3(coordinatesMinusCenter, imageNormalizedDepth));
    highp vec3 imageRefractedVector = refract(vec3(0.0, 0.0, -1.0), imageNormal, 0.5);
    imageRefractedVector.xy = -imageRefractedVector.xy;
    highp vec2 finalImageCoordinate = (imageRefractedVector.xy + 1.0) * 0.5;

    highp vec4 logoColor = texture2D(LogoTexture, 2.0 * finalImageCoordinate - 0.5);

    if(logoColor.a > 0.0) {
        highp float value = 1.0 - smoothstep(0.8, 1.0, imageDistanceFromCenter);
        finalSphereColor = mix(finalSphereColor, logoColor.rgb, value);
    }

    // ambient light
    finalSphereColor += 0.3 * (1.0 - pow(clamp(dot(vec3(0.0, 0.0, 1.0), sphereNormal), 0.0, 1.0), 0.25)) * AMBIENT_COLOUR;

    // specular light
    highp float lightingIntensity = clamp(dot(normalize(vec3(-0.5, -0.5, 1.0)), sphereNormal), 0.0, 1.0);
    lightingIntensity = pow(lightingIntensity, SPECULAR_INTENSITY);
    finalSphereColor += (SPECULAR_COLOUR * lightingIntensity) * 0.3;

    gl_FragColor = vec4(finalSphereColor, checkForPresenceWithinLogo);
}